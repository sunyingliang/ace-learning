<?php
	abstract class Event{
		public $starttime;
		public $endtime;
		public $type;
		public $course;
		public $title;
		public $color;
		public $message;
		public $isallday;
		public $classarr;
		public $grouparr;
		public $studentarr;
		
		protected function __construct($_st, $_et, $_tp, $_course, $_title, $_color, $_message, $_isAllDay, $_classarr, $_grouparr, $_studentarr){
			$this->starttime = $_st;
			$this->endtime = $_et;
			$this->type = $_tp;
			$this->course = $_course;
			$this->title = $_title;
			$this->color = $_color;
			$this->message = $_message;
			$this->isallday = $_isAllDay;
			$this->classarr = $_classarr;
			$this->grouparr = $_grouparr;
			$this->studentarr = $_studentarr;
		}	
	};
	
	class Announcement extends Event{
		public function __construct($_st, $_et, $_tp, $_course, $_title, $_color, $_message, $_isAllDay, $_classarr, $_grouparr, $_studentarr){
			parent::__construct($_st, $_et, $_tp, $_course, $_title, $_color, $_message, $_isAllDay, $_classarr, $_grouparr, $_studentarr);	
		}	
	};
	
	class Links extends Event{
		public $url;	
		
		public function __construct($_st, $_et, $_tp, $_course, $_title, $_color, $_message, $_isAllDay, $_classarr, $_grouparr, $_studentarr, $_url){
			$this->url = $_url;
			parent::__construct($_st, $_et, $_tp, $_course, $_title, $_color, $_message, $_isAllDay, $_classarr, $_grouparr, $_studentarr);	
		}
	};
	
	class Resources extends Event{
		public $serverpath;
		public $tempfolder;
		public $realfolder;
		public $files;
		public $filename;
		public $originalfilename;
		public $filetypes;
		public $filesizes;
		public $editmode;
		public $newflag;
		
		public function __construct($_st, $_et, $_tp, $_course, $_title, $_color, $_message, $_isAllDay, $_classarr, $_grouparr, $_studentarr, $servPath, $tempFolder, $realFolder, &$files, $_newflag, $_edmode){
			$this->serverpath = $servPath;
			$this->tempfolder = $tempFolder;
			$this->realfolder = $realFolder;
			$this->files = $files;
			$this->filename = '';
			$this->originalfilename = '';
			$this->filetypes = '';
			$this->filesizes = '';
			$this->editmode = $_edmode;
			$this->newflag = $_newflag;
			parent::__construct($_st, $_et, $_tp, $_course, $_title, $_color, $_message, $_isAllDay, $_classarr, $_grouparr, $_studentarr);	
		}	
		
		public function init(){
			$count  = count($this->files);
			if($this->editmode == 1 && $this->newflag == 0){
				if($count > 0){
					for($i = 0; $i < $count; $i++){
						if($i + 1 != $count){
							$infoArr = explode('|', $this->files[$i]);	
							$this->filesizes .= $infoArr[0].",";
							$this->originalfilename .= $infoArr[1].",";
							$this->filename .= $infoArr[2].",";
							$pos = strrpos($this->originalfilename, '.');							
							$this->filetypes .= substr($this->originalfilename, $pos + 1).",";							
						}else{
							$infoArr = explode('|', $this->files[$i]);	
							$this->filesizes = $infoArr[0];
							$this->originalfilename = $infoArr[1];
							$this->filename = $infoArr[2];
							$pos = strrpos($this->originalfilename, '.');							
							$this->filetypes = substr($this->originalfilename, $pos + 1);
						}	
					}
				}
			}else{
				if($count > 0){
					for( $i = 0; $i < $count; $i++){ 
						$size_name_Filetype = explode("|", $this->files[$i]);
						$name_Filetype = $size_name_Filetype[1];
						$pos = strrpos($name_Filetype, '.');
						if(($i+1) != $count){
							$FilesSize .= $size_name_Filetype[0].",";
							$FilesType .= substr($name_Filetype, $pos+1).",";
							$name = $this->checkFilename($name_Filetype, $this->serverpath, substr($name_Filetype, $pos+1));
							$nameOrig = $name;
							$name = md5($name.date('Y-m-d H:i:s:u')).".".substr($name_Filetype, $pos+1);
							$this->filename .= $name.',';
							$this->originalfilename .= $nameOrig.',';
						}else{
							$this->filesizes .= $size_name_Filetype[0];
							$this->filetypes .= substr($name_Filetype, $pos+1);
							$name = $this->checkFilename($name_Filetype, $this->serverpath, substr($name_Filetype, $pos+1));
							$nameOrig = $name;
							$name = md5($name.date('Y-m-d H:i:s:u')).".".$this->filetypes;
							$this->filename = $name;
							$this->originalfilename = $nameOrig;
						}
						
						$source = $this->tempfolder.'/'.$name_Filetype; 
						$destination = $this->realfolder.'/'.$name;
						rename($source , $destination);
					}
				}					
			}			
		}
		
		public function checkFilename($aName,$aPath,$aType){
			$aName = str_replace(",", "_", $aName);
			$aName = str_replace(" ", "_", $aName);
			$aName = str_replace("~", "_", $aName);
			$aName = str_replace("`", "_", $aName);
			$aName = str_replace("\'", "_", $aName);
			$aName = str_replace("'", "_", $aName);
			$aName = str_replace("!", "_", $aName);
			$aName = str_replace("@", "_", $aName);
			$aName = str_replace("#", "_", $aName);
			$aName = str_replace("$", "_", $aName);
			$aName = str_replace("%", "_", $aName);
			$aName = str_replace("^", "_", $aName);
			$aName = str_replace("&", "_", $aName);
			$aName = str_replace("*", "_", $aName);
			$aName = str_replace("+", "_", $aName);
			$aName = str_replace("=", "_", $aName);
			$aName = str_replace("/", "_", $aName);
			$aName = str_replace(":", "_", $aName);
			$aName = str_replace(";", "_", $aName);
			$aName = str_replace("<", "_", $aName);
			$aName = str_replace(">", "_", $aName);
			$aName = str_replace("|", "_", $aName);
			$aName = str_replace("(", "_", $aName);
			$aName = str_replace(")", "_", $aName);
			$newaName = "";	
			$aLink = $aPath . $aName;
			$Rank = 1;
			$k=0;	
			while(file_exists($aLink)==true && is_file($aLink)==true){				
				$fname = explode(".", $aName);
				$newaName = $fname[0]."_$Rank";
				$aLink = $aPath.$newaName.".".$aType;
				$newaName=$newaName.".".$aType;
				$Rank++;
				$k++;
			}
			if($newaName!=''){
				$aName=$newaName;
			}
			return $aName;
		}
		
	};
	
	class RL extends Event{
		public $topicstr;	
		
		public function __construct($_st, $_et, $_tp, $_course, $_title, $_color, $_message, $_isAllDay, $_classarr, $_grouparr, $_studentarr, $_topicstr){
			$this->topicstr = $_topicstr;
			parent::__construct($_st, $_et, $_tp, $_course, $_title, $_color, $_message, $_isAllDay, $_classarr, $_grouparr, $_studentarr);	
		}
	};

	class EventManager{
		private $types = array('Announcement', 'Resources', 'Links', 'RL', 'Quiz', 'Worksheet', 'Journal', 'PMP', 'EA', 'AT', 'SV', 'Forum', 'MPG');
		private $mappings = array('Announcement'=>'AnnouncementAssignment', 
								  'Resources'=>'ResourcesAssignment', 
								  'Links'=>'LinksAssignment', 
								  'RL'=>'RLAssignment', 
								  'Quiz'=>'QuizAssignment', 
								  'Worksheet'=>'WorksheetAssignment', 
								  'Journal'=>'JournalAssignment', 
								  'PMP'=>'ZIntPrac_Assignment', 
								  'EA'=>'EA_Assignment',
								  'AT'=>'Annotations');
		private $sql;
		private $startTime;
		private $endTime;
		private $role;
		private $course;
		private $teacherids;
		private $classids;
		private $groupids;
		private $studentstr;
		private $events = array();
		private $event;
		
		public function __construct($st = '0000-00-00 00:00:00', $et = '0000-00-00 00:00:00', $course = 'All', $eventStr = 'All', $teacherids = 'All', $classids = '', $groupids = '', $studentstr = ''){
			$this->startTime = $st;
			$this->endTime = $et;
			$this->role = $_SESSION['SessionUserTypeID'] == 2 ? 'student' : 'teacher';
			$this->course = $course;
			if($eventStr != 'All'){
				$this->types = explode(',', $eventStr);	
			}
			$this->teacherids = $teacherids == 'Self' ? $_SESSION['SessionUserID'] : $teacherids;
			$this->classids = $classids;
			$this->groupids = $groupids;
			$this->studentstr = $studentstr;
		}
		
		protected function _getTopicsRL($agnlsnid){
			$topicStr = '';
			$agnLsnIdArr = explode(',', $agnlsnid);
			
			foreach($agnLsnIdArr as $alid){
				if(strpos($alid,'MATH') === false){
					$SyllabusIDRL = substr ($alid, 0, 5);
					$SectionIDRL = substr ($alid, 5, 2);
					$TopicIDRL = substr ($alid, 7, 2);
					$SubTopicIDRL = substr ($alid, 9, 2);
					$ContentPageIDRL = substr ($alid, 11, 1);					
					$CourseIDRL = substr($SyllabusIDRL, -2);
					$QuerySID = "SELECT SubjectID, CourseName FROM TBL_ModuleCourses WHERE CourseID='".$CourseIDRL."'";
					$ResultSID = mysql_query($QuerySID);
					if (mysql_errno() > 0 || !(mysql_affected_rows() > 0)) {
						continue;	
					}
					$RowSID = mysql_fetch_array($ResultSID, MYSQL_NUM);
					$SubjectID = $RowSID[0];
					
					$QueryRLName = "SELECT SubTopicName FROM TBL_SyllabusSubTopics WHERE SubjectID='".$SubjectID."' AND SectionID=".$SectionIDRL." AND TopicID=".$TopicIDRL." AND SubTopicID=".$SubTopicIDRL;
					$ResultRLName = mysql_query($QueryRLName);
					if (mysql_affected_rows() > 0) {
						$RowRLName = mysql_fetch_array($ResultRLName, MYSQL_NUM);
						if ($ContentPageIDRL == 1){
							$leftover = explode("_", $alid);
							$ChapterIDRL = $leftover[1];
							$CTopicID = $leftover[2];
							if($topicStr != ''){
								$topicStr .= '|';
							}
							$topicStr .= 'NCPA;'.$SyllabusIDRL.','.$ChapterIDRL.','.$CTopicID.','.$SectionIDRL.','.$TopicIDRL.','.$SubTopicIDRL.';'.$RowRLName[0];
						}else{
							if($topicStr != ''){
								$topicStr .= '|';
							}
							$topicStr .= 'NC;'.$SyllabusIDRL.','.$SectionIDRL.','.$TopicIDRL.','.$SubTopicIDRL.';'.$RowRLName[0];
						}
					}
				}else{
					$SubjectID = substr ($alid, 0, 4);
					$SectionIDRL = substr ($alid, 4, 2);
					$TopicIDRL = substr ($alid, 6, 2);
					$SubTopicIDRL = substr ($alid, 8, 2);
					$typeID = substr($alid,10,1);
					$CTopicID = substr($alid,11);
					$SyllTypeName = "";
					switch($typeID){
						case "d" : $SyllTypeName = "tid";  break;
						case "a" : $SyllTypeName = "atid"; break;
						case "t" : $SyllTypeName = "ttid"; break;
						default  : $SyllTypeName = "tid";  break;
					}
					if(isset($CTopicID)==false || strlen($CTopicID)<=0){
						$QueryRLName = "SELECT 
											TBL_SYST.SubTopicName ,
											TBL_MST.CTopicID
										FROM 
											TBL_ModuleCourses TBL_MC,
											TBL_SyllabusSubTopics TBL_SYST, 
											TBL_ModuleSubTopics TBL_MST
											LEFT JOIN
											(SELECT SyllabusID FROM TBL_StudentsSyllabus WHERE SyllabusID IS NOT NULL AND SyllabusID != '' AND UserID = '".$_SESSION['SessionUserID']."') AS TBL_SS
											ON ( 
												RIGHT(TBL_MST.SyllabusID,3)=RIGHT(TBL_SS.SyllabusID,3) AND
												SUBSTRING(TBL_MST.SyllabusID,0,2)<=SUBSTRING(TBL_SS.SyllabusID,0,2)
											)
										WHERE 
											CONCAT(TBL_SYST.SubjectID, TBL_SYST.SectionID ,TBL_SYST.TopicID ,TBL_SYST.SubTopicID) = '".$alid."' AND
											TBL_MC.CourseID    = RIGHT(TBL_MST.SyllabusID,2) AND 
											TBL_MC.SubjectID   = TBL_SYST.SubjectID AND
											TBL_MST.SectionID  = TBL_SYST.SectionID AND 
											TBL_MST.TopicID    = TBL_SYST.TopicID AND 
											TBL_MST.SubTopicID = TBL_SYST.SubTopicID
											ORDER BY
											TBL_SS.SyllabusID IS NOT NULL
											LIMIT 1";
						$ResultRLName = mysql_query($QueryRLName);
						if (mysql_errno() > 0 || !(mysql_affected_rows() > 0)) {
							continue;	
						}
						if($RowRLName = mysql_fetch_array($ResultRLName, MYSQL_NUM)){
							if($topicStr != ''){
								$topicStr .= '|';
							}
							$topicStr .= 'MCPV;'.$RowRLName[1].';'.$RowRLName[0];
						}
					}elseif($typeID=='d'){
						$QueryRLName = "SELECT 
											TBL_SYST.SubTopicName 
										FROM 							
											TBL_SyllabusSubTopics TBL_SYST,
											TBL_ModuleSubTopics TBL_MST,
											TBL_ModuleCourses TBL_MC
										WHERE 
											RIGHT(TBL_MST.SyllabusID,2) = TBL_MC.CourseID AND
											TBL_SYST.SubjectID = TBL_MC.SubjectID AND
											TBL_SYST.SectionID = TBL_MST.SectionID AND
											TBL_SYST.TopicID = TBL_MST.TopicID AND
											TBL_SYST.SubTopicID = TBL_MST.SubTopicID AND
											TBL_MST.CTopicID='".$CTopicID."'
											LIMIT 
											1";
						$ResultRLName = mysql_query($QueryRLName);
						if (mysql_errno() > 0 || !(mysql_affected_rows() > 0)) {
							continue;	
						}
						if ($RowRLName = mysql_fetch_array($ResultRLName, MYSQL_NUM)) {
							if($topicStr != ''){
								$topicStr .= '|';
							}
							$topicStr .= 'MCPV;'.$CTopicID.';'.$RowRLName[0];
						}
					}elseif($typeID=='a'){
						$QueryRLName = "SELECT QRY_SCT.CTopicName FROM QRY_".$_SESSION['SessionSchoolID']."_SyllabusACETopic QRY_SCT WHERE QRY_SCT.CTopicID=".$CTopicID;
						$ResultRLName = mysql_query($QueryRLName);
						if (mysql_errno() > 0 || !(mysql_affected_rows() > 0)) {
							continue;	
						}
						if ($RowRLName = mysql_fetch_array($ResultRLName, MYSQL_NUM)) {
							if($topicStr != ''){
								$topicStr .= '|';
							}
							$topicStr .= 'MCPVA;'.$CTopicID.';'.$RowRLName[0];
						}
					}elseif($typeID=='t'){
						$QueryRLName = "SELECT QRY_SCT.CTopicName FROM QRY_".$_SESSION['SessionSchoolID']."_SyllabusTopic QRY_SCT WHERE QRY_SCT.CTopicID=".$CTopicID;					
						$ResultRLName = mysql_query($QueryRLName);
						if (mysql_errno() > 0 || !(mysql_affected_rows() > 0)) {
							continue;	
						}
						if ($RowRLName = mysql_fetch_array($ResultRLName, MYSQL_NUM)) {
							if($topicStr != ''){
								$topicStr .= '|';
							}
							$topicStr .= 'MCPVT;'.$CTopicID.';'.$RowRLName[0];
						}
					}
				}
			}
			return $topicStr;		
		}
		
		protected function quotElement($str, $sep = ','){
			$reStr = "";
			$eleArr = explode($sep, $str);
			$reStr .= "'".$eleArr[0]."'";		
			for($i = 1; $i < count($eleArr); $i++){
				$reStr .= ",'".$eleArr[$i]."'";			
			}
			return $reStr;
		}
		
		protected function isSameDay($dtstr1, $dtstr2){
			$dt1 = substr($dtstr1, 0, 10);
			$dt2 = substr($dtstr2, 0, 10);	
			return $dt1 != $dt2 ? 1 : 0;
		}
		
		protected function mySql2PhpTime($sqlDate){
			$arr = date_parse($sqlDate);
			return mktime($arr["hour"],$arr["minute"],$arr["second"],$arr["month"],$arr["day"],$arr["year"]);		
		}
		protected function php2JsTime($phpDate){
			return date("m/d/Y H:i", $phpDate);
		}
		protected function js2PhpTime($jsdate){
			if(preg_match('@(\d+)/(\d+)/(\d+)\s+(\d+):(\d+)@', $jsdate, $matches)==1){
				$ret = mktime($matches[4], $matches[5], 0, $matches[1], $matches[2], $matches[3]);
			}else if(preg_match('@(\d+)/(\d+)/(\d+)@', $jsdate, $matches)==1){
				$ret = mktime(0, 0, 0, $matches[1], $matches[2], $matches[3]);
			}
			return $ret;
		}
		protected function php2MySqlTime($phpDate){
			return date("Y-m-d H:i:s", $phpDate);
		}
		protected function ConvertToLocalTime($Timezone, $DateValue, $format){
			if($Timezone==""){
				$Timezone = "Asia/Singapore";
			}
			switch($format){
				case 1:
					$TimeFormat = "d M y";
					break;
				case 2:
					$TimeFormat = "d M y, h:i A";
					break;
				case 3:
					$TimeFormat = "Y-m-d H:i:s";
					break;
				case 4:
					$TimeFormat = "d M Y, h:i A";
					break;
				case 5:
					$TimeFormat = "d M Y, h:i:s A";
					break;
				case 6:
					$TimeFormat = "d M y, h:i A";
					break;
				case 7:
					$TimeFormat = "d/m/Y";
					break;
				case 8:
					$TimeFormat = "d M y, h:i:s A";
					break;
			}
			unset($dateTime);
			unset($dateTimeZone);
			$dateTime = new DateTime($DateValue);
			$dateTimeZone = new DateTimeZone($Timezone);
			$dateTime->setTimezone($dateTimeZone);
			$LocalTime = $dateTime->format($TimeFormat); 
			return $LocalTime;	
		}
		
		protected function sortEventsBU(){
			for($i = count($this->events) - 1; $i > 0; ){
				$c = $i - 1;
				for($j = 0; $j < $i; $j++){
					if($this->js2PhpTime($this->events[$j][2]) > $this->js2PhpTime($this->events[$j + 1][2])){
						$tmp = $this->events[$j];
						$this->events[$j] = $this->events[$j + 1];	
						$this->events[$j + 1] = $tmp;
						$c = $j;
					}	
				}	
				$i = $c;
			}	
		}
		
		public function removeEvents($type, $calids, $servpath = ''){
			$temparr = explode(',', $calids);
			$this->sql = "select ID from QRY_".$_SESSION['SessionSchoolID']."_".$this->mappings[$type]." where AssignID = ".$temparr[0]; 
			$tempres = mysql_query($this->sql);
			if(mysql_errno() > 0){
				return false;	
			}
			$temprow = mysql_fetch_row($tempres);
			$protoID = $temprow[0];
			$this->sql = "delete from QRY_".$_SESSION['SessionSchoolID']."_".$this->mappings[$type]." where AssignID in (".$calids.")";	
			mysql_query($this->sql);	
			if(mysql_errno() > 0){
				return false;	
			}
			$this->sql = "select count(AssignID) from QRY_".$_SESSION['SessionSchoolID']."_".$this->mappings[$type]." where ID = ".$protoID; 
			$tempres = mysql_query($this->sql);
			if(mysql_errno() > 0){
				return false;	
			}
			$temprow = mysql_fetch_row($tempres);
			if($temprow[0] == 0){
				if($type == 'Resources' && !empty($servpath)){
					$this->sql = "select FileName from QRY_".$_SESSION['SessionSchoolID']."_".$type." where ID = ".$protoID;					
					$rsret = mysql_query($this->sql);
					if(mysql_errno() > 0){
						return false;	
					}
					$rsrow = mysql_fetch_row($rsret);
					$attachmentURL = $servpath."/".$rsrow[0];
					if(file_exists($attachmentURL) == false && is_file($attachmentURL) == false){
						$attachmentURL = substr($attachmentURL, 0, strrpos($attachmentURL, '/') + 1);
						$attachmentURL .= $rsrow[0];
						if(file_exists($attachmentURL) == true && is_file($attachmentURL) == true){
							unlink($attachmentURL);
						}
					}else{
						unlink($attachmentURL);	
					}
				}
				$this->sql = "delete from QRY_".$_SESSION['SessionSchoolID']."_".$type." where ID = ".$protoID;
				mysql_query($this->sql);
				if(mysql_errno() > 0){
					return false;	
				}	
			}			
			return true;
		}
		
		public function updateEvents($type, $calids){
			$this->sql = "update QRY_".$_SESSION['SessionSchoolID']."_".$this->mappings[$type]." set `StartDate`='".$this->startTime."', "."`EndDate`='".$this->endTime."' "."where `AssignID` in (".$calids.")";
			mysql_query($this->sql);	
			if(mysql_errno() > 0){
				return false;	
			}
			return true;			
		}
		
		public function updateEventDetails($ids, $otype, &$event){
			$this->event = $event;
			if($otype == $this->event->type){
				$temparr = explode(',', $ids);
				$this->sql = "select ID, StartDate, EndDate from QRY_".$_SESSION['SessionSchoolID']."_".$this->mappings[$otype]." where AssignID = ".$temparr[0]; 
				$tempres = mysql_query($this->sql);
				if(mysql_errno() > 0){
					return false;	
				}
				$temprow = mysql_fetch_row($tempres);
				$protoID = $temprow[0];
				$protoSDate = $temprow[1];
				$protoEDate = $temprow[2];
				switch($otype){
					case 'Announcement':
						$this->sql = "update QRY_".$_SESSION['SessionSchoolID']."_".$otype." set Title = '".$this->event->title."', Message = '".$this->event->message."', CourseID = '".$this->event->course."' where ID = ".$protoID;
						break;
					case 'Links':
						$this->sql = "update QRY_".$_SESSION['SessionSchoolID']."_".$otype." set Title = '".$this->event->title."', URL = '".$this->event->url."', UserID = ".$_SESSION['SessionUserID'].", CourseID = '".$this->event->course."' where ID = ".$protoID;
						break;
					case 'Resources':
						$this->sql = "update QRY_".$_SESSION['SessionSchoolID']."_".$otype." set Title = '".$this->event->title."', UserID = ".$_SESSION['SessionUserID'].", CourseID = '".$this->event->course."', FileName = '".$this->event->filename."', OriginalName = '".$this->event->originalfilename."', FileType = '".$this->event->filetypes."', FileSize = ".$this->event->filesizes.", ShareID = 1"." where ID = ".$protoID;
						break;
					case 'RL':
						$this->sql = "update QRY_".$_SESSION['SessionSchoolID']."_".$otype." set Title = '".$this->event->title."', UserID = ".$_SESSION['SessionUserID'].", CourseID = '".$this->event->course."', RLID = '".substr(md5(time()), 0, 32)."', AssignedLessonsID = '".$this->event->topicstr."', Date = now() where ID = ".$protoID;
						break;	
				}
				mysql_query($this->sql);				
				if(mysql_errno() > 0){
					return false;
				}
				$deleteArr = array();
				$modifyArr = array();
				$insertArr = array();
				$destclassarr = array();
				$destmappingarr = array();
				$this->sql = "select AssignID, ClassID from QRY_".$_SESSION['SessionSchoolID']."_".$this->mappings[$otype]." where UserID = ".$_SESSION['SessionUserID']." and ID = ".$protoID." and StartDate = '".$protoSDate."' and EndDate = '".$protoEDate."' and (GpStuType = '' or GpStuType is NULL)";
				$resc = mysql_query($this->sql);
				if(mysql_errno() == 0){
					while($rowc = mysql_fetch_row($resc)){
						$destclassarr[] = $rowc[1];
						$destmappingarr[$rowc[1]] = $rowc[0];	
					}
				}
				foreach($destclassarr as $value){
					if(!in_array($value, $this->event->classarr)){
						$deleteArr[] = $destmappingarr[$value];	
					} 	
				}
				foreach($this->event->classarr as $value){
					if(in_array($value, $destclassarr)){
						$modifyArr[] = $destmappingarr[$value];
					}else{
						$insertArr[] = $value;
					}	
				}
				
				if($otype == 'Announcement'){
					if(!empty($modifyArr)){
						$this->sql = "update QRY_".$_SESSION['SessionSchoolID']."_".$this->mappings[$otype]." set UserID = ".$_SESSION['SessionUserID'].", StartDate = '".$this->event->starttime."', EndDate = '".$this->event->endtime."', IsAllDayEvent = ".$this->event->isallday.", Color = '".$this->event->color."' where AssignID in (".implode(',', $modifyArr).")";
						mysql_query($this->sql);
						if(mysql_errno() > 0){
							return false;
						}	
					}
					if(!empty($insertArr)){
						$this->sql = "insert into QRY_".$_SESSION['SessionSchoolID']."_".$this->mappings[$otype]."(UserID, ClassID, ID, StartDate, EndDate, IsAllDayEvent, Color) values";
						$values = '';
						foreach($insertArr as $value){
							$values .= $values == '' ? ("(".$_SESSION['SessionUserID'].", '".$value."', ".$protoID.", '".$this->event->starttime."', '".$this->event->endtime."', ".$this->event->isallday.", '".$this->event->color."')") : (", (".$_SESSION['SessionUserID'].", '".$value."', ".$protoID.", '".$this->event->starttime."', '".$this->event->endtime."', ".$this->event->isallday.", '".$this->event->color."')");	
						}
						$this->sql .= $values;
						mysql_query($this->sql);
						if(mysql_errno() > 0){
							return false;
						}		
					}
				}elseif($otype == 'Links' || $otype == 'Resources' || $otype == 'RL'){
					if(!empty($modifyArr)){
						$this->sql = "update QRY_".$_SESSION['SessionSchoolID']."_".$this->mappings[$otype]." set UserID = ".$_SESSION['SessionUserID'].", Message = '".$this->event->message."', StartDate = '".$this->event->starttime."', EndDate = '".$this->event->endtime."', IsAllDayEvent = ".$this->event->isallday.", Color = '".$this->event->color."' where AssignID in (".implode(',', $modifyArr).")";
						mysql_query($this->sql);
						if(mysql_errno() > 0){
							return false;
						}		
					}
					if(!empty($insertArr)){
						$this->sql = "insert into QRY_".$_SESSION['SessionSchoolID']."_".$this->mappings[$otype]."(UserID, ClassID, ID, Message, StartDate, EndDate, IsAllDayEvent, Color) values";
						$values = '';
						foreach($insertArr as $value){
							$values .= $values == '' ? ("(".$_SESSION['SessionUserID'].", '".$value."', ".$protoID.", '".$this->event->message."', '".$this->event->starttime."', '".$this->event->endtime."', ".$this->event->isallday.", '".$this->event->color."')") : (", (".$_SESSION['SessionUserID'].", '".$value."', ".$protoID.", '".$this->event->starttime."', '".$this->event->endtime."', ".$this->event->isallday.", '".$this->event->color."')");	
						}
						$this->sql .= $values;
						mysql_query($this->sql);
						if(mysql_errno() > 0){
							return false;
						}		
					}					
				}
				$modifyArr = array();
				$insertArr = array();
				$destgrouparr = array();
				$destmappingarr = array();
				$this->sql = "select AssignID, GpStuID from QRY_".$_SESSION['SessionSchoolID']."_".$this->mappings[$otype]." where UserID = ".$_SESSION['SessionUserID']." and ID = ".$protoID." and StartDate = '".$protoSDate."' and EndDate = '".$protoEDate."' and GpStuType = 'QAGRP'";
				$resg = mysql_query($this->sql);
				if(mysql_errno() == 0){
					while($rowg = mysql_fetch_row($resg)){
						$destgrouparr[] = $rowg[1];
						$destmappingarr[$rowg[1]] = $rowg[0];	
					}
				}
				foreach($destgrouparr as $value){
					if(!in_array($value, $this->event->grouparr)){
						$deleteArr[] = $destmappingarr[$value];	
					} 	
				}
				foreach($this->event->grouparr as $value){
					if(in_array($value, $destgrouparr)){
						$modifyArr[] = $destmappingarr[$value];
					}else{
						$insertArr[] = $value;
					}	
				}
					
				if($otype == 'Announcement'){
					if(!empty($modifyArr)){
						$this->sql = "update QRY_".$_SESSION['SessionSchoolID']."_".$this->mappings[$otype]." set UserID = ".$_SESSION['SessionUserID'].", StartDate = '".$this->event->starttime."', EndDate = '".$this->event->endtime."', IsAllDayEvent = ".$this->event->isallday.", Color = '".$this->event->color."' where AssignID in (".implode(',', $modifyArr).")";
						mysql_query($this->sql);
						if(mysql_errno() > 0){
							return false;
						}		
					}
					if(!empty($insertArr)){
						$this->sql = "insert into QRY_".$_SESSION['SessionSchoolID']."_".$this->mappings[$otype]."(UserID, GpStuType, GpStuID, ID, StartDate, EndDate, IsAllDayEvent, Color) values";
						$values = '';
						foreach($insertArr as $value){
							$values .= $values == '' ? ("(".$_SESSION['SessionUserID'].", 'QAGRP', ".$value.", ".$protoID.", '".$this->event->starttime."', '".$this->event->endtime."', ".$this->event->isallday.", '".$this->event->color."')") : (", (".$_SESSION['SessionUserID'].", 'QAGRP', ".$value.", ".$protoID.", '".$this->event->starttime."', '".$this->event->endtime."', ".$this->event->isallday.", '".$this->event->color."')");
						}
						$this->sql .= $values;
						mysql_query($this->sql);
						if(mysql_errno() > 0){
							return false;
						}		
					}					
				}elseif($otype == 'Links' || $otype == 'Resources' || $otype == 'RL'){
					if(!empty($modifyArr)){
						$this->sql = "update QRY_".$_SESSION['SessionSchoolID']."_".$this->mappings[$otype]." set UserID = ".$_SESSION['SessionUserID'].", Message = '".$this->event->message."', StartDate = '".$this->event->starttime."', EndDate = '".$this->event->endtime."', IsAllDayEvent = ".$this->event->isallday.", Color = '".$this->event->color."' where AssignID in (".implode(',', $modifyArr).")";
						mysql_query($this->sql);
						if(mysql_errno() > 0){
							return false;
						}
					}
					if(!empty($insertArr)){
						$this->sql = "insert into QRY_".$_SESSION['SessionSchoolID']."_".$this->mappings[$otype]."(UserID, GpStuType, GpStuID, ID, Message, StartDate, EndDate, IsAllDayEvent, Color) values";
						$values = '';
						foreach($insertArr as $value){
							$values .= $values == '' ? ("(".$_SESSION['SessionUserID'].", 'QAGRP', ".$value.", ".$protoID.", '".$this->event->message."', '".$this->event->starttime."', '".$this->event->endtime."', ".$this->event->isallday.", '".$this->event->color."')") : (", (".$_SESSION['SessionUserID'].", 'QAGRP', ".$value.", ".$protoID.", '".$this->event->message."', '".$this->event->starttime."', '".$this->event->endtime."', ".$this->event->isallday.", '".$this->event->color."')");	
						}
						$this->sql .= $values;
						mysql_query($this->sql);
						if(mysql_errno() > 0){
							return false;
						}
					}
				}
				$modifyArr = array();
				$insertArr = array();
				$deststudentarr = array();
				$destmappingarr = array();
				$this->sql = "select AssignID, concat(ClassID, '|', GpStuID) from QRY_".$_SESSION['SessionSchoolID']."_".$this->mappings[$otype]." where UserID = ".$_SESSION['SessionUserID']." and ID = ".$protoID." and StartDate = '".$protoSDate."' and EndDate = '".$protoEDate."' and GpStuType = 'QASTU'";
				$resc = mysql_query($this->sql);
				if(mysql_errno() == 0){
					while($rowc = mysql_fetch_row($resc)){
						$deststudentarr[] = $rowc[1];
						$destmappingarr[$rowc[1]] = $rowc[0];	
					}
				}
				foreach($deststudentarr as $value){
					if(!in_array($value, $this->event->studentarr)){
						$deleteArr[] = $destmappingarr[$value];	
					} 	
				}
				foreach($this->event->studentarr as $value){
					if(in_array($value, $deststudentarr)){
						$modifyArr[] = $destmappingarr[$value];
					}else{
						$insertArr[] = $value;
					}	
				}
				
				if($otype == 'Announcement'){
					if(!empty($modifyArr)){
						$this->sql = "update QRY_".$_SESSION['SessionSchoolID']."_".$this->mappings[$otype]." set UserID = ".$_SESSION['SessionUserID'].", StartDate = '".$this->event->starttime."', EndDate = '".$this->event->endtime."', IsAllDayEvent = ".$this->event->isallday.", Color = '".$this->event->color."' where AssignID in (".implode(',', $modifyArr).")";
						mysql_query($this->sql);
						if(mysql_errno() > 0){
							return false;
						}
					}
					if(!empty($insertArr)){
						$this->sql = "insert into QRY_".$_SESSION['SessionSchoolID']."_".$this->mappings[$otype]."(UserID, GpStuType, ClassID, GpStuID, ID, StartDate, EndDate, IsAllDayEvent, Color) values";
						$values = '';
						$tvaluearr = array();
						foreach($insertArr as $value){
							$tvaluearr = explode('|', $value);
							$values .= $values == '' ? ("(".$_SESSION['SessionUserID'].", 'QASTU', '".$tvaluearr[0]."', ".$tvaluearr[1].", ".$protoID.", '".$this->event->starttime."', '".$this->event->endtime."', ".$this->event->isallday.", '".$this->event->color."')") : (", (".$_SESSION['SessionUserID'].", 'QASTU', '".$tvaluearr[0]."', ".$tvaluearr[1].", ".$protoID.", '".$this->event->starttime."', '".$this->event->endtime."', ".$this->event->isallday.", '".$this->event->color."')");
						}
						$this->sql .= $values;
						mysql_query($this->sql);
						if(mysql_errno() > 0){
							return false;
						}
					}
				}elseif($otype == 'Links' || $otype == 'Resources' || $otype == 'RL'){
					if(!empty($modifyArr)){
						$this->sql = "update QRY_".$_SESSION['SessionSchoolID']."_".$this->mappings[$otype]." set UserID = ".$_SESSION['SessionUserID'].", Message = '".$this->event->message."', StartDate = '".$this->event->starttime."', EndDate = '".$this->event->endtime."', IsAllDayEvent = ".$this->event->isallday.", Color = '".$this->event->color."' where AssignID in (".implode(',', $modifyArr).")";
						mysql_query($this->sql);
						if(mysql_errno() > 0){
							return false;
						}	
					}
					if(!empty($insertArr)){
						$this->sql = "insert into QRY_".$_SESSION['SessionSchoolID']."_".$this->mappings[$otype]."(UserID, GpStuType, ClassID, GpStuID, ID, Message, StartDate, EndDate, IsAllDayEvent, Color) values";
						$values = '';
						$tvaluearr = array();
						foreach($insertArr as $value){
							$tvaluearr = explode('|', $value);
							$values .= $values == '' ? ("(".$_SESSION['SessionUserID'].", 'QASTU', '".$tvaluearr[0]."', ".$tvaluearr[1].", ".$protoID.", '".$this->event->message."', '".$this->event->starttime."', '".$this->event->endtime."', ".$this->event->isallday.", '".$this->event->color."')") : (", (".$_SESSION['SessionUserID'].", 'QASTU', '".$tvaluearr[0]."', ".$tvaluearr[1].", ".$protoID.", '".$this->event->message."', '".$this->event->starttime."', '".$this->event->endtime."', ".$this->event->isallday.", '".$this->event->color."')");	
						}
						$this->sql .= $values;
						mysql_query($this->sql);
						if(mysql_errno() > 0){
							return false;
						}	
					}
				}
				if(!empty($deleteArr)){
					$resrm = $this->removeEvents($otype, implode(',', $deleteArr), isset($this->event->serverpath) ? $this->event->serverpath : '');
					if(!$resrm){
						return false;	
					}	
				}
				return true;	
			}else{
				$resrm = $this->removeEvents($otype, $ids, isset($this->evnet->serverpath) ? $this->evnet->serverpath : '');
				if(!$resrm){
					return false;	
				}
				return $this->addEvent($this->event);	
			}	
		}
		
		public function addEvent(&$event){
			$this->event = $event;
			switch($this->event->type){
				case 'Announcement':
					$this->sql = "insert into QRY_".$_SESSION['SessionSchoolID']."_".$this->event->type."(Title, Message, UserID, CourseID) value('".$this->event->title."', '".$this->event->message."', ".$_SESSION['SessionUserID'].", '".$this->event->course."')";	
					break;	
				case "Links":
					$this->sql = "insert into QRY_".$_SESSION['SessionSchoolID']."_".$this->event->type."(Title, URL, UserID, CourseID) value('".$this->event->title."', '".$this->event->url."', ".$_SESSION['SessionUserID'].", '".$this->event->course."')";
					break;
				case 'Resources':
					$this->sql = "insert into QRY_".$_SESSION['SessionSchoolID']."_".$this->event->type."(Title, UserID, CourseID, FileName, OriginalName, FileType, FileSize, ShareID) value('".$this->event->title."', ".$_SESSION['SessionUserID'].", '".$this->event->course."', '".$this->event->filename."', '".$this->event->originalfilename."', '".$this->event->filetypes."', ".$this->event->filesizes.", 1)";						
					break;
				case 'RL':
					$this->sql = "insert into QRY_".$_SESSION['SessionSchoolID']."_".$this->event->type."(Title, UserID, CourseID, RLID, AssignedLessonsID, Date) value('".$this->event->title."', ".$_SESSION['SessionUserID'].", '".$this->event->course."', '".substr(md5(time()), 0, 32)."', '".$this->event->topicstr."', now())";						
					break;							
			}
			mysql_query($this->sql);				
			if(mysql_errno() > 0){
				return false;
			}else{
				$protoID = mysql_insert_id();
				for($i = 0; isset($this->event->classarr[$i]); $i++){
					if($this->event->type == 'Announcement'){
						$this->sql = "insert into QRY_".$_SESSION['SessionSchoolID']."_".$this->mappings[$this->event->type]."(UserID, ClassID, ID, StartDate, EndDate, IsAllDayEvent, Color) value(".$_SESSION['SessionUserID'].", '".$this->event->classarr[$i]."', ".$protoID.", '".$this->event->starttime."', '".$this->event->endtime."', ".$this->event->isallday.", '".$this->event->color."')";	
					}elseif($this->event->type == 'Links' || $this->event->type == 'Resources' || $this->event->type == 'RL'){
						$this->sql = "insert into QRY_".$_SESSION['SessionSchoolID']."_".$this->mappings[$this->event->type]."(UserID, ClassID, ID, Message, StartDate, EndDate, IsAllDayEvent, Color) value(".$_SESSION['SessionUserID'].", '".$this->event->classarr[$i]."', ".$protoID.", '".$this->event->message."', '".$this->event->starttime."', '".$this->event->endtime."', ".$this->event->isallday.", '".$this->event->color."')";	
					}	
					mysql_query($this->sql);
					if(mysql_errno() > 0){
						return false;
					}
				}
				for($i = 0; isset($this->event->grouparr[$i]); $i++){
					if($this->event->type == 'Announcement'){
						$this->sql = "insert into QRY_".$_SESSION['SessionSchoolID']."_".$this->mappings[$this->event->type]."(UserID, ID, StartDate, EndDate, IsAllDayEvent, Color, GpStuType, GpStuID) value(".$_SESSION['SessionUserID'].", ".$protoID.", '".$this->event->starttime."', '".$this->event->endtime."', ".$this->event->isallday.", '".$this->event->color."', 'QAGRP', ".$this->event->grouparr[$i].")";	
					}elseif($this->event->type == 'Links' || $this->event->type == 'Resources' || $this->event->type == 'RL'){
						$this->sql = "insert into QRY_".$_SESSION['SessionSchoolID']."_".$this->mappings[$this->event->type]."(UserID, ID, Message, StartDate, EndDate, IsAllDayEvent, Color, GpStuType, GpStuID) value(".$_SESSION['SessionUserID'].", ".$protoID.", '".$this->event->message."', '".$this->event->starttime."', '".$this->event->endtime."', ".$this->event->isallday.", '".$this->event->color."', 'QAGRP', ".$this->event->grouparr[$i].")";	
					}
					mysql_query($this->sql);
					if(mysql_errno() > 0){
						return false;
					}
				}
				for($i = 0; isset($this->event->studentarr[$i]); $i++){
					$temparr = explode('|', $this->event->studentarr[$i]);
					if($this->event->type == 'Announcement'){
						$this->sql = "insert into QRY_".$_SESSION['SessionSchoolID']."_".$this->mappings[$this->event->type]."(UserID, ClassID, ID, StartDate, EndDate, IsAllDayEvent, Color, GpStuType, GpStuID) value(".$_SESSION['SessionUserID'].", '".$temparr[0]."', ".$protoID.", '".$this->event->starttime."', '".$this->event->endtime."', ".$this->event->isallday.", '".$this->event->color."', 'QASTU', ".$temparr[1].")";	
					}elseif($this->event->type == 'Links' || $this->event->type == 'Resources' || $this->event->type == 'RL'){
						$this->sql = "insert into QRY_".$_SESSION['SessionSchoolID']."_".$this->mappings[$this->event->type]."(UserID, ClassID, ID, Message, StartDate, EndDate, IsAllDayEvent, Color, GpStuType, GpStuID) value(".$_SESSION['SessionUserID'].", '".$temparr[0]."', ".$protoID.", '".$this->event->message."', '".$this->event->starttime."', '".$this->event->endtime."', ".$this->event->isallday.", '".$this->event->color."', 'QASTU', ".$temparr[1].")";
					}
					mysql_query($this->sql);
					if(mysql_errno() > 0){
						return false;
					}
				}
				return true;
			}
		}
		
		public function retrieveEvents(){
			$_selectSQL = "select AA.UserID, AA.AssignID as Id, AA.ID as ProtoID, AA.StartDate, AA.EndDate, AA.IsAllDayEvent, AA.Color, AA.ClassID, AA.GpStuType, AA.GpStuID, A.Title as Subject, A.CourseID as Course, concat(U.FirstName, ' ', U.LastName) as Author";
			$_selectSQL2 = "select AA.UserID, AA.AssignID as Id, AA.StartDate, AA.EndDate, AA.IsAllDayEvent, AA.Color, AA.ClassID, AA.GpStuType, AA.GpStuID, A.CourseID as Course, concat(U.FirstName, ' ', U.LastName) as Author";
			$_selectSQLAT = "select A.DrawID as ProtoID, A.Title as Subject, A.CourseID as Course, SV.ID as VID, concat(SV.SectionID, ',', SV.TopicID, ',', SV.SubTopicID) as Catagery, concat(U.FirstName, ' ', U.LastName) as Author, AA.AssignID as Id, AA.UserID, AA.StartDate, AA.EndDate, AA.IsAllDayEvent, AA.Color, AA.ClassID, AA.GpStuType, AA.GpStuID, (AA.StartDate > Now()) as Future";
			$_fromSQL = " from QRY_".$_SESSION['SessionSchoolID']."_"."{table} A, QRY_".$_SESSION['SessionSchoolID']."_"."{table}Assignment AA, TBL_Users U";
			$_fromSQLAT = " from TBL_Annotations A, TBL_AnnotationsAssignment AA, TBL_SyllabusVideos SV, TBL_Users U";
			$_whereSQL = " where A.ID = AA.ID and U.UserID = AA.UserID and (AA.StartDate between '".$this->startTime."' and '".$this->endTime."' or AA.StartDate < '".$this->startTime."' and AA.EndDate > '".$this->startTime."')";
			$_whereSQL2 = " where A.{table}ID = AA.{table}ID and U.UserID = AA.UserID and (AA.StartDate between '".$this->startTime."' and '".$this->endTime."' or AA.StartDate < '".$this->startTime."' and AA.EndDate > '".$this->startTime."')";
			$_whereSQL3 = " where A.ID = AA.EAID and U.UserID = AA.UserID and (AA.StartDate between '".$this->startTime."' and '".$this->endTime."' or AA.StartDate < '".$this->startTime."' and AA.EndDate > '".$this->startTime."')";
			$_whereSQLAT = " where A.DrawID = AA.DrawID and AA.UserID = U.UserID and A.VideoID = SV.VideoID and A.SchoolID = '".$_SESSION['SessionSchoolID']."' and ((StartDate between '".$this->startTime."' and '".$this->endTime."') or (StartDate < '".$this->startTime."' and EndDate > '".$this->startTime."'))";
			$_orderby = " order by StartDate asc, Subject asc";
			$_swhereSQL = " where ((StartDate between '".$this->startTime."' and '".$this->endTime."') or (StartDate < '".$this->startTime."' and EndDate > '".$this->startTime."'))";
			$_groupids = "";
			if($this->role == 'student'){
				$_classid = "";
				$_groupids = "";
				$tempSql = "select ClassID from TBL_StudentsProfile where UserID = ".$_SESSION['SessionUserID'];
				$tempRet = mysql_query($tempSql);
				if(mysql_errno() != 0){
					return $this->events;
				}
				if($tRow = mysql_fetch_row($tempRet)){
					$_classid = $tRow[0];	
				}
				$tempSql = "select GroupID from QRY_".$_SESSION['SessionSchoolID']."_GroupMembers where UserID = ".$_SESSION['SessionUserID'];
				$tempRet = mysql_query($tempSql);
				if(mysql_errno() != 0){
					return $this->events;
				}
				while($tRow = mysql_fetch_row($tempRet)){
					$_groupids .= $_groupids == "" ? $tRow[0] : (", ".$tRow[0]);	
				}
				if($_groupids == ""){
					$_groupids = 0;	
				}
				
				$causeSQL = " and (((GpStuType = '' or GpStuType is NULL) and ClassID = '".$_classid."')";
				if(!empty($_groupids)){
					$causeSQL .= " or (GpStuType = 'QAGRP' and GpStuID in (".$_groupids."))";	
				}
				$causeSQL .= " or (GpStuType = 'QASTU' and ClassID = '".$_classid."' and GpStuID = ".$_SESSION['SessionUserID']."))";
				$_whereSQL .= $causeSQL;
				$_whereSQL2 .= $causeSQL;
				$_whereSQL3 .= $causeSQL;
				$_swhereSQL .= $causeSQL;
				$_fromSQLAT .= ", (select max(AssignID) as AssignID from TBL_AnnotationsAssignment group by DrawID) as DAA";
				$_whereSQLAT .= "and AA.AssignID = DAA.AssignID and AA.EndDate > Now()";
				$_whereSQLAT .= $causeSQL;
			}
			
			if($this->course != 'All'){
				$_whereSQL .= " and A.CourseID in (".$this->quotElement($this->course).")";
				$_whereSQL2 .= " and A.CourseID in (".$this->quotElement($this->course).")";
				$_whereSQL3 .= " and A.CourseID in (".$this->quotElement($this->course).")";	
				$_whereSQLAT .= " and A.CourseID in (".$this->quotElement($this->course).")";	
			}
			if($this->teacherids != 'All'){
				$_whereSQL .= " and AA.UserID in (".$this->teacherids.")";
				$_whereSQL2 .= " and AA.UserID in (".$this->teacherids.")";
				$_whereSQL3 .= " and AA.UserID in (".$this->teacherids.")";
				$_whereSQLAT .= " and AA.UserID in (".$this->teacherids.")";
			}
			if(!empty($this->classids) || !empty($this->groupids) || !empty($this->studentstr)){
				$_where_cause = " and (";
				if(!empty($this->classids)){
					$_where_cause .= "((AA.GpStuType = '' or AA.GpStuType is NULL) and AA.ClassID in (".$this->quotElement($this->classids)."))";	
				}
				if(!empty($this->groupids)){
					if(!empty($this->classids)){
						$_where_cause .= " or ";	
					}
					$_where_cause .= "(AA.GpStuType = 'QAGRP' and AA.GpStuID in (".$this->groupids."))";	
				}
				if(!empty($this->studentstr)){				
					$temparr = explode(',', $this->studentstr);
					$_where_student = '';
					foreach($temparr as $item){
						$tempinner = explode('|', $item);	
						$_where_student .= $_where_student == '' ? "(AA.GpStuType = 'QASTU' and AA.ClassID = '".$tempinner[0]."' and AA.GpStuID = ".$tempinner[1].")" : " or (AA.GpStuType = 'QASTU' and AA.ClassID = '".$tempinner[0]."' and AA.GpStuID = ".$tempinner[1].")";
					}
					if(!empty($this->classids) || !empty($this->groupids)){
						$_where_cause .= " or ";	
					}
					$_where_cause .= "(".$_where_student.")";
				}
				$_where_cause .= ")";
				$_whereSQL .= $_where_cause;
				$_whereSQL2 .= $_where_cause;
				$_whereSQL3 .= $_where_cause;
				$_whereSQLAT .= $_where_cause;
			}
			
			foreach($this->types as $type){
				switch($type){
					case 'Announcement':
						if($this->role == "student"){
							$this->sql = $_selectSQL.", A.Message".preg_replace('/{\w+}/', $type, $_fromSQL).$_whereSQL.' AND EndDate > Now()'.$_orderby;	
						}else{
							$this->sql = $_selectSQL.", A.Message".preg_replace('/{\w+}/', $type, $_fromSQL).$_whereSQL.$_orderby;
						}
						$handle = mysql_query($this->sql);
						if(mysql_errno() > 0){
							break;
						}
						$assignerArr = array();
						while($row = mysql_fetch_array($handle)){
							if(!isset($assignerArr[$row['UserID']])){
								$assignerArr[$row['UserID']] = array();
							}
							if(!isset($assignerArr[$row['UserID']][$row['ProtoID']])){
								$assignerArr[$row['UserID']][$row['ProtoID']] = array();	
							}
							$flag = $row['StartDate']."|".$row['EndDate']."|".$row['IsAllDayEvent']."|".$row['Color'];
							if(!isset($assignerArr[$row['UserID']][$row['ProtoID']][$flag])){
								$assignerArr[$row['UserID']][$row['ProtoID']][$flag] = array();	
							}
							array_push($assignerArr[$row['UserID']][$row['ProtoID']][$flag], $row);
						}
						foreach($assignerArr as $aitem){
							foreach($aitem as $pitem){
								foreach($pitem as $eitem){
									$assignids = '';
									$title = $eitem[0]['Subject'];
									$startdate = $this->ConvertToLocalTime($_SESSION['SessionTimezone'], $eitem[0]['StartDate'], 2);
									$enddate = $this->ConvertToLocalTime($_SESSION['SessionTimezone'], $eitem[0]['EndDate'], 2);
									$isalldyevent = $eitem[0]['IsAllDayEvent'];
									$iscrossevent = $this->isSameDay($startdate, $enddate);
									$color = $eitem[0]['Color'];
									$message = $eitem[0]['Message'];
									$courseid = $eitem[0]['Course'];
									$classidstr = '';
									$groupnamestr = '';
									$groupidstr = '';
									$studentnamestr = '';
									$studentidstr = '';
									$author = $eitem[0]['Author'];
									$assigner = $eitem[0]['UserID'];
									foreach($eitem as $item){
										$assignids .= $assignids == '' ? $item['Id'] : ','.$item['Id'];
										if($item['GpStuType'] == 'QAGRP'){
											$groupidstr .= $groupidstr == '' ? $item['GpStuID'] : ','.$item['GpStuID'];
										}elseif($item['GpStuType'] == 'QASTU'){
											$studentidstr .= $studentidstr == '' ? $item['GpStuID'] : ','.$item['GpStuID'];
										}else{
											$classidstr .= $classidstr == '' ? $item['ClassID'] : ','.$item['ClassID'];	
										}	
									}
									if($groupidstr != ''){
										$this->sql = "select concat(GroupID, '|', GroupName) from QRY_".$_SESSION['SessionSchoolID']."_Group where GroupID in (".$groupidstr.")";
										$gresult = mysql_query($this->sql);
										if(mysql_errno() > 0){
											continue;	
										}
										while($grow = mysql_fetch_array($gresult)){
											$groupnamestr .= $groupnamestr == '' ? $grow[0] : ','.$grow[0];
										}		
									}
									if($studentidstr != ''){
										$this->sql = "select concat(U.UserID, '|', U.FirstName, ' ', U.LastName, '(', SP.ClassID, ')') from (select UserID, ClassID from TBL_StudentsProfile where UserID in (".$studentidstr.")) as SP, (select UserID, FirstName, LastName from TBL_Users where UserID in (".$studentidstr.")) as U where SP.UserID = U.UserID";
										$sresult = mysql_query($this->sql);
										if(mysql_errno() > 0){
											continue;	
										}
										while($srow = mysql_fetch_array($sresult)){
											$studentnamestr .= $studentnamestr == '' ? $srow[0] : ','.$srow[0];
										}
									}
									$this->events[] = array(
										$assignids,
										$title,
										$this->php2JsTime($this->mySql2PhpTime($startdate)),
										$this->php2JsTime($this->mySql2PhpTime($enddate)),
										$isalldyevent,
										$iscrossevent,	//more than one day event
										0,	//Recurring event
										$color,
										1,	//editable
										'Announcement',
										$message,
										$courseid,
										$classidstr,
										$groupnamestr,
										$studentnamestr,
										$author,
										$assigner
									);	
								}
							}	
						}
						break;
					case 'Links':
						if($this->role == "student"){
							$this->sql = $_selectSQL.", A.URL, AA.Message".preg_replace('/{\w+}/', $type, $_fromSQL).$_whereSQL.' AND EndDate > Now()'.$_orderby;
						}else{
							$this->sql = $_selectSQL.", A.URL, AA.Message".preg_replace('/{\w+}/', $type, $_fromSQL).$_whereSQL.$_orderby;
						}
						$handle = mysql_query($this->sql);
						if(mysql_errno() > 0){
							break;
						}
						$assignerArr = array();
						while($row = mysql_fetch_array($handle)){
							if(!isset($assignerArr[$row['UserID']])){
								$assignerArr[$row['UserID']] = array();
							}
							if(!isset($assignerArr[$row['UserID']][$row['ProtoID']])){
								$assignerArr[$row['UserID']][$row['ProtoID']] = array();	
							}
							$flag = $row['StartDate']."|".$row['EndDate']."|".$row['IsAllDayEvent']."|".$row['Color'];
							if(!isset($assignerArr[$row['UserID']][$row['ProtoID']][$flag])){
								$assignerArr[$row['UserID']][$row['ProtoID']][$flag] = array();	
							}
							array_push($assignerArr[$row['UserID']][$row['ProtoID']][$flag], $row);
						}
						foreach($assignerArr as $aitem){
							foreach($aitem as $pitem){
								foreach($pitem as $eitem){
									$assignids = '';
									$title = $eitem[0]['Subject'];
									$startdate = $this->ConvertToLocalTime($_SESSION['SessionTimezone'], $eitem[0]['StartDate'], 2);
									$enddate = $this->ConvertToLocalTime($_SESSION['SessionTimezone'], $eitem[0]['EndDate'], 2);
									$isalldyevent = $eitem[0]['IsAllDayEvent'];
									$iscrossevent = $this->isSameDay($startdate, $enddate);
									$color = $eitem[0]['Color'];
									$message = $eitem[0]['Message'];
									$courseid = $eitem[0]['Course'];
									$classidstr = '';
									$groupnamestr = '';
									$groupidstr = '';
									$studentnamestr = '';
									$studentidstr = '';
									$author = $eitem[0]['Author'];
									$assigner = $eitem[0]['UserID'];
									$url = $eitem[0]['URL'];
									foreach($eitem as $item){
										$assignids .= $assignids == '' ? $item['Id'] : ','.$item['Id'];
										if($item['GpStuType'] == 'QAGRP'){
											$groupidstr .= $groupidstr == '' ? $item['GpStuID'] : ','.$item['GpStuID'];
										}elseif($item['GpStuType'] == 'QASTU'){
											$studentidstr .= $studentidstr == '' ? $item['GpStuID'] : ','.$item['GpStuID'];
										}else{
											$classidstr .= $classidstr == '' ? $item['ClassID'] : ','.$item['ClassID'];	
										}	
									}
									if($groupidstr != ''){
										$this->sql = "select concat(GroupID, '|', GroupName) from QRY_".$_SESSION['SessionSchoolID']."_Group where GroupID in (".$groupidstr.")";
										$gresult = mysql_query($this->sql);
										if(mysql_errno() > 0){
											continue;	
										}
										while($grow = mysql_fetch_array($gresult)){
											$groupnamestr .= $groupnamestr == '' ? $grow[0] : ','.$grow[0];
										}		
									}
									if($studentidstr != ''){
										$this->sql = "select concat(U.UserID, '|', U.FirstName, ' ', U.LastName, '(', SP.ClassID, ')') from (select UserID, ClassID from TBL_StudentsProfile where UserID in (".$studentidstr.")) as SP, (select UserID, FirstName, LastName from TBL_Users where UserID in (".$studentidstr.")) as U where SP.UserID = U.UserID";
										$sresult = mysql_query($this->sql);
										if(mysql_errno() > 0){
											continue;	
										}
										while($srow = mysql_fetch_array($sresult)){
											$studentnamestr .= $studentnamestr == '' ? $srow[0] : ','.$srow[0];
										}
									}
									$this->events[] = array(
										$assignids,
										$title,
										$this->php2JsTime($this->mySql2PhpTime($startdate)),
										$this->php2JsTime($this->mySql2PhpTime($enddate)),
										$isalldyevent,
										$iscrossevent,	//more than one day event
										0,	//Recurring event
										$color,
										1,	//editable
										'Links',
										$message,
										$courseid,
										$classidstr,
										$groupnamestr,
										$studentnamestr,
										$author,
										$assigner,
										$url
									);	
								}
							}	
						}
						break;
					case 'Resources':
						if($this->role == "student"){
							$this->sql = $_selectSQL.", A.FileName, A.OriginalName, A.FileSize, AA.Message".preg_replace('/{\w+}/', $type, $_fromSQL).$_whereSQL.' AND EndDate > Now()'.$_orderby;
						}else{
							$this->sql = $_selectSQL.", A.FileName, A.OriginalName, A.FileSize, AA.Message".preg_replace('/{\w+}/', $type, $_fromSQL).$_whereSQL.$_orderby;
						}
						$handle = mysql_query($this->sql);
						if(mysql_errno() > 0){
							break;
						}
						$assignerArr = array();
						while($row = mysql_fetch_array($handle)){
							if(!isset($assignerArr[$row['UserID']])){
								$assignerArr[$row['UserID']] = array();
							}
							if(!isset($assignerArr[$row['UserID']][$row['ProtoID']])){
								$assignerArr[$row['UserID']][$row['ProtoID']] = array();	
							}
							$flag = $row['StartDate']."|".$row['EndDate']."|".$row['IsAllDayEvent']."|".$row['Color'];
							if(!isset($assignerArr[$row['UserID']][$row['ProtoID']][$flag])){
								$assignerArr[$row['UserID']][$row['ProtoID']][$flag] = array();	
							}
							array_push($assignerArr[$row['UserID']][$row['ProtoID']][$flag], $row);
						}
						foreach($assignerArr as $aitem){
							foreach($aitem as $pitem){
								foreach($pitem as $eitem){
									$assignids = '';
									$title = $eitem[0]['Subject'];
									$startdate = $this->ConvertToLocalTime($_SESSION['SessionTimezone'], $eitem[0]['StartDate'], 2);
									$enddate = $this->ConvertToLocalTime($_SESSION['SessionTimezone'], $eitem[0]['EndDate'], 2);
									$isalldyevent = $eitem[0]['IsAllDayEvent'];
									$iscrossevent = $this->isSameDay($startdate, $enddate);
									$color = $eitem[0]['Color'];
									$message = $eitem[0]['Message'];
									$courseid = $eitem[0]['Course'];
									$classidstr = '';
									$groupnamestr = '';
									$groupidstr = '';
									$studentnamestr = '';
									$studentidstr = '';
									$author = $eitem[0]['Author'];
									$assigner = $eitem[0]['UserID'];
									$schoolid = $_SESSION['SessionSchoolID'];
									$filename = $eitem[0]['FileName'];
									$originalfilename = $eitem[0]['OriginalName'];
									$filesize = $eitem[0]['FileSize'];
									$protoid = $eitem[0]['ProtoID'];
									foreach($eitem as $item){
										$assignids .= $assignids == '' ? $item['Id'] : ','.$item['Id'];
										if($item['GpStuType'] == 'QAGRP'){
											$groupidstr .= $groupidstr == '' ? $item['GpStuID'] : ','.$item['GpStuID'];
										}elseif($item['GpStuType'] == 'QASTU'){
											$studentidstr .= $studentidstr == '' ? $item['GpStuID'] : ','.$item['GpStuID'];
										}else{
											$classidstr .= $classidstr == '' ? $item['ClassID'] : ','.$item['ClassID'];	
										}	
									}
									if($groupidstr != ''){
										$this->sql = "select concat(GroupID, '|', GroupName) from QRY_".$_SESSION['SessionSchoolID']."_Group where GroupID in (".$groupidstr.")";
										$gresult = mysql_query($this->sql);
										if(mysql_errno() > 0){
											continue;	
										}
										while($grow = mysql_fetch_array($gresult)){
											$groupnamestr .= $groupnamestr == '' ? $grow[0] : ','.$grow[0];
										}		
									}
									if($studentidstr != ''){
										$this->sql = "select concat(U.UserID, '|', U.FirstName, ' ', U.LastName, '(', SP.ClassID, ')') from (select UserID, ClassID from TBL_StudentsProfile where UserID in (".$studentidstr.")) as SP, (select UserID, FirstName, LastName from TBL_Users where UserID in (".$studentidstr.")) as U where SP.UserID = U.UserID";	
										$sresult = mysql_query($this->sql);
										if(mysql_errno() > 0){
											continue;	
										}
										while($srow = mysql_fetch_array($sresult)){
											$studentnamestr .= $studentnamestr == '' ? $srow[0] : ','.$srow[0];
										}
									}
									$this->events[] = array(
										$assignids,
										$title,
										$this->php2JsTime($this->mySql2PhpTime($startdate)),
										$this->php2JsTime($this->mySql2PhpTime($enddate)),
										$isalldyevent,
										$iscrossevent,	//more than one day event
										0,	//Recurring event
										$color,
										1,	//editable
										'Resources',
										$message,
										$courseid,
										$classidstr,
										$groupnamestr,
										$studentnamestr,
										$author,
										$assigner,
										$schoolid,
										$filename,
										$originalfilename,
										$filesize,
										$protoid
									);	
								}
							}	
						}
						break;
					case 'RL':
						if($this->role == "student"){
							$this->sql = $_selectSQL.", A.AssignedLessonsID, AA.Message".preg_replace('/{\w+}/', $type, $_fromSQL).$_whereSQL.' AND A.RLType = 0 AND EndDate > Now()'.$_orderby;
						}else{
							$this->sql = $_selectSQL.", A.AssignedLessonsID, AA.Message".preg_replace('/{\w+}/', $type, $_fromSQL).$_whereSQL.' AND A.RLType = 0'.$_orderby;
						}
						$handle = mysql_query($this->sql);
						if(mysql_errno() > 0){
							break;
						}
						$assignerArr = array();
						while($row = mysql_fetch_array($handle)){
							if(!isset($assignerArr[$row['UserID']])){
								$assignerArr[$row['UserID']] = array();
							}
							if(!isset($assignerArr[$row['UserID']][$row['ProtoID']])){
								$assignerArr[$row['UserID']][$row['ProtoID']] = array();	
							}
							$flag = $row['StartDate']."|".$row['EndDate']."|".$row['IsAllDayEvent']."|".$row['Color'];
							if(!isset($assignerArr[$row['UserID']][$row['ProtoID']][$flag])){
								$assignerArr[$row['UserID']][$row['ProtoID']][$flag] = array();	
							}
							array_push($assignerArr[$row['UserID']][$row['ProtoID']][$flag], $row);
						}
						foreach($assignerArr as $aitem){
							foreach($aitem as $pitem){
								foreach($pitem as $eitem){
									$assignids = '';
									$title = $eitem[0]['Subject'];
									$startdate = $this->ConvertToLocalTime($_SESSION['SessionTimezone'], $eitem[0]['StartDate'], 2);
									$enddate = $this->ConvertToLocalTime($_SESSION['SessionTimezone'], $eitem[0]['EndDate'], 2);
									$isalldyevent = $eitem[0]['IsAllDayEvent'];
									$iscrossevent = $this->isSameDay($startdate, $enddate);
									$color = $eitem[0]['Color'];
									$message = $eitem[0]['Message'];
									$courseid = $eitem[0]['Course'];
									$classidstr = '';
									$groupnamestr = '';
									$groupidstr = '';
									$studentnamestr = '';
									$studentidstr = '';
									$author = $eitem[0]['Author'];
									$assigner = $eitem[0]['UserID'];
									$topicStr = $this->_getTopicsRL($eitem[0]['AssignedLessonsID']);
									$assignedlessonsid = $eitem[0]['AssignedLessonsID'];
									foreach($eitem as $item){
										$assignids .= $assignids == '' ? $item['Id'] : ','.$item['Id'];
										if($item['GpStuType'] == 'QAGRP'){
											$groupidstr .= $groupidstr == '' ? $item['GpStuID'] : ','.$item['GpStuID'];
										}elseif($item['GpStuType'] == 'QASTU'){
											$studentidstr .= $studentidstr == '' ? $item['GpStuID'] : ','.$item['GpStuID'];
										}else{
											$classidstr .= $classidstr == '' ? $item['ClassID'] : ','.$item['ClassID'];	
										}	
									}
									if($groupidstr != ''){
										$this->sql = "select concat(GroupID, '|', GroupName) from QRY_".$_SESSION['SessionSchoolID']."_Group where GroupID in (".$groupidstr.")";
										$gresult = mysql_query($this->sql);
										if(mysql_errno() > 0){
											continue;	
										}
										while($grow = mysql_fetch_array($gresult)){
											$groupnamestr .= $groupnamestr == '' ? $grow[0] : ','.$grow[0];
										}		
									}
									if($studentidstr != ''){
										$this->sql = "select concat(U.UserID, '|', U.FirstName, ' ', U.LastName, '(', SP.ClassID, ')') from (select UserID, ClassID from TBL_StudentsProfile where UserID in (".$studentidstr.")) as SP, (select UserID, FirstName, LastName from TBL_Users where UserID in (".$studentidstr.")) as U where SP.UserID = U.UserID";
										$sresult = mysql_query($this->sql);
										if(mysql_errno() > 0){
											continue;	
										}
										while($srow = mysql_fetch_array($sresult)){
											$studentnamestr .= $studentnamestr == '' ? $srow[0] : ','.$srow[0];
										}
									}
									$this->events[] = array(
										$assignids,
										$title,
										$this->php2JsTime($this->mySql2PhpTime($startdate)),
										$this->php2JsTime($this->mySql2PhpTime($enddate)),
										$isalldyevent,
										$iscrossevent,	//more than one day event
										0,	//Recurring event
										$color,
										1,	//editable
										'RL',
										$message,
										$courseid,
										$classidstr,
										$groupnamestr,
										$studentnamestr,
										$author,
										$assigner,
										$topicStr,
										$assignedlessonsid
									);	
								}
							}	
						}
						break;
					case 'Quiz':
						if($this->role == "student"){
							$QuizIDAssigned= array();
							$AssignIDAssigned= array();		
							$QueryQA = "SELECT QuizID, MAX(AssignID) AssignID FROM QRY_".$_SESSION['SessionSchoolID']."_QuizAssignment".$_swhereSQL." GROUP BY QuizID";
							$resultQA = mysql_query($QueryQA);
							while ($RowQA = mysql_fetch_array($resultQA, MYSQL_NUM)){
								$QuizIDAssigned[]="'".$RowQA[0]."'";
								$AssignIDAssigned[]="'".$RowQA[1]."'";
							}
							$this->sql = "SELECT 
											QRY_SID_QA.UserID, 	
											QRY_SID_QA.QuizID as ProtoID, 
											QRY_SID_Q.QuizName as Subject, 
											QRY_SID_QA.StartDate,
											QRY_SID_QA.EndDate,
											QRY_SID_QA.IsAllDayEvent,
											QRY_SID_QA.Color,  
											QRY_SID_Q.CourseID as Course, 
											QRY_SID_QA.Message, 
											QRY_SID_QA.ClassID, 
											QRY_SID_QA.GpStuType , 
											QRY_SID_QA.GpStuID, 
											QRY_SID_QA.AssignID as Id,
											concat(U.FirstName, ' ', U.LastName) as Author,
											(QRY_SID_QA.StartDate > Now()) as Future
										FROM
											QRY_".$_SESSION['SessionSchoolID']."_Quiz QRY_SID_Q,
											(
												SELECT 
													COUNT(QuestionID) AS qC,
													GROUP_CONCAT(QuestionID ORDER By QuestionID) AS qL,
													QuizID 
												FROM 
													QRY_".$_SESSION['SessionSchoolID']."_QuizQuestions  
												WHERE
													QuizID IN (".implode(",",$QuizIDAssigned).")
												GROUP BY 
													QuizID
											)  AS QRY_SID_QQ	
												LEFT JOIN
											(
												SELECT 
													COUNT(QuestionID) AS qC,
													GROUP_CONCAT(QuestionID ORDER By QuestionID) AS qL,
													QuizID 
												FROM 
													QRY_".$_SESSION['SessionSchoolID']."_ResultsQuiz 
												WHERE 
													UserID=".$_SESSION['SessionUserID']." AND
													QuizID IN (".implode(",",$QuizIDAssigned).")
												GROUP BY 
													QuizID
											)  AS QRY_SID_RQ
												ON (QRY_SID_QQ.QuizID=QRY_SID_RQ.QuizID),
											
											QRY_".$_SESSION['SessionSchoolID']."_QuizAssignment QRY_SID_QA,
											TBL_Users AS U
										WHERE 
											QRY_SID_Q.QuizTypeID = 0 AND
											QRY_SID_QA.QuizID = QRY_SID_Q.QuizID AND 
											QRY_SID_Q.QuizID IN (".implode(",",$QuizIDAssigned).") AND
											QRY_SID_QA.AssignID IN (".implode(",",$AssignIDAssigned).") AND
											QRY_SID_QA.QuizID = QRY_SID_QQ.QuizID AND 
											(QRY_SID_QQ.qC > QRY_SID_RQ.qC OR QRY_SID_RQ.qC IS NULL) AND
											QRY_SID_QA.UserID = U.UserID AND
											QRY_SID_QA.EndDate > Now()
										ORDER BY 						
											(QRY_SID_QA.EndDate < Now()),
											(QRY_SID_QA.StartDate > Now()) ,
											QRY_SID_QA.AssignID";

						}else{
							$this->sql = $_selectSQL2.", A.QuizName as Subject, AA.QuizID as ProtoID, AA.Message, AA.Duration, AA.ReAttemptType, AA.ReadingAssignment, AA.DispQuestion, AA.DispSolution, AA.ReAttemptNo, AA.MustRead, AA.ShowAll, (AA.StartDate > Now()) as Future".preg_replace('/{\w+}/', $type, $_fromSQL).preg_replace('/{\w+}/', $type, $_whereSQL2)." AND A.QuizTypeID = 0".$_orderby;
						}
						
						$handle = mysql_query($this->sql);
						if(mysql_errno() > 0){
							break;
						}
						$assignerArr = array();
						while($row = mysql_fetch_array($handle)){
							if(!isset($assignerArr[$row['UserID']])){
								$assignerArr[$row['UserID']] = array();
							}
							if(!isset($assignerArr[$row['UserID']][$row['ProtoID']])){
								$assignerArr[$row['UserID']][$row['ProtoID']] = array();	
							}
							$flag = $row['StartDate']."|".$row['EndDate']."|".$row['IsAllDayEvent']."|".$row['Color']."|".$row['GpStuType']."|".$row['Message']."|".$row['Duration']."|".$row['ReAttemptType']."|"
									.$row['ReadingAssignment'].$row['DispQuestion'].$row['DispSolution'].$row['ReAttemptNo'].$row['MustRead'].$row['ShowAll'];
							if(!isset($assignerArr[$row['UserID']][$row['ProtoID']][$flag])){
								$assignerArr[$row['UserID']][$row['ProtoID']][$flag] = array();	
							}
							array_push($assignerArr[$row['UserID']][$row['ProtoID']][$flag], $row);
						}
						foreach($assignerArr as $aitem){
							foreach($aitem as $pitem){
								foreach($pitem as $eitem){
									$assignids = '';
									$title = $eitem[0]['Subject'];
									$startdate = $this->ConvertToLocalTime($_SESSION['SessionTimezone'], $eitem[0]['StartDate'], 2);
									$enddate = $this->ConvertToLocalTime($_SESSION['SessionTimezone'], $eitem[0]['EndDate'], 2);
									$isalldyevent = $eitem[0]['IsAllDayEvent'];
									$iscrossevent = $this->isSameDay($startdate, $enddate);
									$color = $eitem[0]['Color'];
									$message = $eitem[0]['Message'];
									$courseid = $eitem[0]['Course'];
									$classidstr = '';
									$groupnamestr = '';
									$groupidstr = '';
									$studentnamestr = '';
									$studentidstr = '';
									$author = $eitem[0]['Author'];
									$assigner = $eitem[0]['UserID'];
									$protoid = $eitem[0]['ProtoID'];
									$future = 0;
									foreach($eitem as $item){
										$assignids .= $assignids == '' ? $item['Id'] : ','.$item['Id'];
										$future = $item['Future'] ? 1 : 0;
										if($item['GpStuType'] == 'QAGRP'){
											$groupidstr .= $groupidstr == '' ? $item['GpStuID'] : ','.$item['GpStuID'];
										}elseif($item['GpStuType'] == 'QASTU'){
											$studentidstr .= $studentidstr == '' ? $item['GpStuID'] : ','.$item['GpStuID'];
										}else{
											$classidstr .= $classidstr == '' ? $item['ClassID'] : ','.$item['ClassID'];	
										}	
									}
									if($groupidstr != ''){
										$this->sql = "select concat(GroupID, '|', GroupName) from QRY_".$_SESSION['SessionSchoolID']."_Group where GroupID in (".$groupidstr.")";
										$gresult = mysql_query($this->sql);
										if(mysql_errno() > 0){
											continue;	
										}
										while($grow = mysql_fetch_array($gresult)){
											$groupnamestr .= $groupnamestr == '' ? $grow[0] : ','.$grow[0];
										}		
									}
									if($studentidstr != ''){
										$this->sql = "select concat(U.UserID, '|', U.FirstName, ' ', U.LastName, '(', SP.ClassID, ')') from (select UserID, ClassID from TBL_StudentsProfile where UserID in (".$studentidstr.")) as SP, (select UserID, FirstName, LastName from TBL_Users where UserID in (".$studentidstr.")) as U where SP.UserID = U.UserID";
										$sresult = mysql_query($this->sql);
										if(mysql_errno() > 0){
											continue;	
										}
										while($srow = mysql_fetch_array($sresult)){
											$studentnamestr .= $studentnamestr == '' ? $srow[0] : ','.$srow[0];
										}
									}
									$this->events[] = array(
										$assignids,
										$title,
										$this->php2JsTime($this->mySql2PhpTime($startdate)),
										$this->php2JsTime($this->mySql2PhpTime($enddate)),
										$isalldyevent,
										$iscrossevent,	//more than one day event
										0,	//Recurring event
										$color,
										1,	//editable
										'Quiz',
										$message,
										$courseid,
										$classidstr,
										$groupnamestr,
										$studentnamestr,
										$author,
										$assigner,
										$protoid,
										$future
									);	
								}
							}	
						}
						break;
					case 'Worksheet':
						if($this->role == "student"){
							$this->sql = "SELECT     
												QRY_SID_WA.UserID,             
												QRY_SID_WA.WSID as ProtoID, 
												QRY_SID_W.WSName as Subject,
												QRY_SID_WA.StartDate,
											   	QRY_SID_WA.EndDate,
												QRY_SID_WA.IsAllDayEvent,
												QRY_SID_WA.Color,  
												QRY_SID_W.CourseID as Course, 
												QRY_SID_WA.Message, 
												QRY_SID_WA.ClassID, 
												QRY_SID_WA.GpStuType, 
												QRY_SID_WA.GpStuID, 
												QRY_SID_WA.AssignID as Id,
												concat(U.FirstName, ' ', U.LastName) as Author,
												(QRY_SID_WA.StartDate > Now()) as Future
											FROM 
												(
													SELECT 
														WA.UserID,
														WA.WSID,
														MAX(WA.AssignID) as AssignID,
														WA.StartDate AS StartDate,
														WA.EndDate AS EndDate,
														WA.Message AS Message, 
														WA.ClassID AS ClassID, 
														WA.GpStuType AS GpStuType, 
														WA.GpStuID AS GpStuID,
														WA.IsAllDayEvent AS IsAllDayEvent,
														WA.Color AS Color
													FROM 
													(
														SELECT
															tb1.*
														FROM
															QRY_".$_SESSION['SessionSchoolID']."_WorksheetAssignment tb1,
															(
																SELECT
																	MAX(tb2a.AssignID) as AssignID
																FROM
																	QRY_".$_SESSION['SessionSchoolID']."_WorksheetAssignment tb2a".
																$_swhereSQL.
																	" GROUP BY
																		tb2a.WSID
																	  ORDER BY
																		tb2a.AssignID DESC
															) as tb2
														WHERE
															tb1.AssignID = tb2.AssignID
															GROUP BY
																tb1.WSID
															ORDER BY
																AssignID DESC
														) as WA 
														LEFT JOIN
														(
															SELECT 
																WT.WSID AS WSID 
															FROM 
																QRY_".$_SESSION['SessionSchoolID']."_WorksheetTotals WT 
															WHERE 
																WT.Submission=1 AND 
																WT.UserID='".$_SESSION['SessionUserID']."'
														) AS WTA ON (WTA.WSID=WA.WSID)
														WHERE 
															WTA.WSID IS NULL
													GROUP BY WA.WSID
												) QRY_SID_WA,
												QRY_".$_SESSION['SessionSchoolID']."_Worksheet QRY_SID_W,
												TBL_Users AS U
											WHERE
												QRY_SID_WA.WSID = QRY_SID_W.WSID AND
												QRY_SID_WA.UserID = U.UserID AND
												QRY_SID_WA.EndDate > Now()
											ORDER BY                         
												(QRY_SID_WA.EndDate < Now()),
												(QRY_SID_WA.StartDate > Now()),
												QRY_SID_WA.AssignID";
																	
						}else{
							$this->sql = $_selectSQL2.", A.WSName as Subject, AA.WSID as ProtoID, AA.Message, AA.DispSolution, AA.EditOption, AA.ReadingAssignment, AA.MustRead, (AA.StartDate > Now()) as Future".preg_replace('/{\w+}/', $type, $_fromSQL).preg_replace('/{\w+}/', 'WS', $_whereSQL2).$_orderby;							
						}

						$handle = mysql_query($this->sql);
						if(mysql_errno() > 0){
							break;
						}
						$assignerArr = array();
						while($row = mysql_fetch_array($handle)){
							if(!isset($assignerArr[$row['UserID']])){
								$assignerArr[$row['UserID']] = array();
							}
							if(!isset($assignerArr[$row['UserID']][$row['ProtoID']])){
								$assignerArr[$row['UserID']][$row['ProtoID']] = array();	
							}
							$flag = $row['StartDate']."|".$row['EndDate']."|".$row['IsAllDayEvent']."|".$row['Color']."|".$row['GpStuType']."|".$row['Message']."|".$row['DispSolution']."|".$row['EditOption']
									."|".$row['ReadingAssignment']."|".$row['MustRead'];
							if(!isset($assignerArr[$row['UserID']][$row['ProtoID']][$flag])){
								$assignerArr[$row['UserID']][$row['ProtoID']][$flag] = array();	
							}
							array_push($assignerArr[$row['UserID']][$row['ProtoID']][$flag], $row);
						}
						foreach($assignerArr as $aitem){
							foreach($aitem as $pitem){
								foreach($pitem as $eitem){
									$assignids = '';
									$title = $eitem[0]['Subject'];
									$startdate = $this->ConvertToLocalTime($_SESSION['SessionTimezone'], $eitem[0]['StartDate'], 2);
									$enddate = $this->ConvertToLocalTime($_SESSION['SessionTimezone'], $eitem[0]['EndDate'], 2);
									$isalldyevent = $eitem[0]['IsAllDayEvent'];
									$iscrossevent = $this->isSameDay($startdate, $enddate);
									$color = $eitem[0]['Color'];
									$message = $eitem[0]['Message'];
									$courseid = $eitem[0]['Course'];
									$classidstr = '';
									$groupnamestr = '';
									$groupidstr = '';
									$studentnamestr = '';
									$studentidstr = '';
									$author = $eitem[0]['Author'];
									$assigner = $eitem[0]['UserID'];
									$protoid = $eitem[0]['ProtoID'];
									$future = 0;
									foreach($eitem as $item){
										$assignids .= $assignids == '' ? $item['Id'] : ','.$item['Id'];
										$future = $item['Future'] ? 1 : 0;
										if($item['GpStuType'] == 'QAGRP'){
											$groupidstr .= $groupidstr == '' ? $item['GpStuID'] : ','.$item['GpStuID'];
										}elseif($item['GpStuType'] == 'QASTU'){
											$studentidstr .= $studentidstr == '' ? $item['GpStuID'] : ','.$item['GpStuID'];
										}else{
											$classidstr .= $classidstr == '' ? $item['ClassID'] : ','.$item['ClassID'];	
										}	
									}
									if($groupidstr != ''){
										$this->sql = "select concat(GroupID, '|', GroupName) from QRY_".$_SESSION['SessionSchoolID']."_Group where GroupID in (".$groupidstr.")";
										$gresult = mysql_query($this->sql);
										if(mysql_errno() > 0){
											continue;	
										}
										while($grow = mysql_fetch_array($gresult)){
											$groupnamestr .= $groupnamestr == '' ? $grow[0] : ','.$grow[0];
										}		
									}
									if($studentidstr != ''){
										$this->sql = "select concat(U.UserID, '|', U.FirstName, ' ', U.LastName, '(', SP.ClassID, ')') from (select UserID, ClassID from TBL_StudentsProfile where UserID in (".$studentidstr.")) as SP, (select UserID, FirstName, LastName from TBL_Users where UserID in (".$studentidstr.")) as U where SP.UserID = U.UserID";
										$sresult = mysql_query($this->sql);
										if(mysql_errno() > 0){
											continue;	
										}
										while($srow = mysql_fetch_array($sresult)){
											$studentnamestr .= $studentnamestr == '' ? $srow[0] : ','.$srow[0];
										}
									}
									$this->events[] = array(
										$assignids,
										$title,
										$this->php2JsTime($this->mySql2PhpTime($startdate)),
										$this->php2JsTime($this->mySql2PhpTime($enddate)),
										$isalldyevent,
										$iscrossevent,	//more than one day event
										0,	//Recurring event
										$color,
										1,	//editable
										'Worksheet',
										$message,
										$courseid,
										$classidstr,
										$groupnamestr,
										$studentnamestr,
										$author,
										$assigner,
										$protoid,
										$future
									);	
								}
							}	
						}
						break;
					case 'Journal':
						if($this->role == "student"){					
							$this->sql = "	SELECT 
												QRY_SID_JA.UserID,
												QRY_SID_JA.JournalID as ProtoID, 
												QRY_SID_J.JournalName as Subject,
												QRY_SID_JA.StartDate,
												QRY_SID_JA.EndDate,
												QRY_SID_JA.IsAllDayEvent,
												QRY_SID_JA.Color,  
												QRY_SID_J.CourseID as Course, 
												QRY_SID_JA.Message, 
												QRY_SID_JA.ClassID, 
												QRY_SID_JA.GpStuType , 
												QRY_SID_JA.GpStuID, 
												QRY_SID_JA.AssignID as Id,
												concat(U.FirstName, ' ', U.LastName) as Author,
												(QRY_SID_JA.StartDate > Now()) as Future
											FROM 
												(
													SELECT 
														JA.JournalID,
														MAX(JA.AssignID) AssignID
													FROM 
														QRY_".$_SESSION['SessionSchoolID']."_JournalAssignment JA
													WHERE 
														JA.JournalID NOT IN (SELECT JT.JournalID FROM QRY_".$_SESSION['SessionSchoolID']."_JournalTotals JT WHERE JT.Submission=1 AND JT.UserID='"
																			.$_SESSION['SessionUserID']."') AND
														((JA.ClassID='".$_SESSION['SessionClassID']."' AND (JA.GpStuType='' OR JA.GpStuType IS NULL)) OR 
														(JA.GpStuType='QAGRP' AND JA.GpStuID IN (".$_groupids.")) OR 
														(JA.GpStuType='QASTU' AND JA.GpStuID = '".$_SESSION['SessionUserID']."')) AND
														((StartDate between '".$this->startTime."' and '".$this->endTime."') or (StartDate < '".$this->startTime."' and EndDate > '".$this->startTime."'))
													GROUP BY JournalID
												) QRY_SID_JAA,
												QRY_".$_SESSION['SessionSchoolID']."_Journal QRY_SID_J,
												(
													SELECT COUNT(QuestionID) AS qC,GROUP_CONCAT(QuestionID ORDER By QuestionID) AS qL,JournalID 
													FROM QRY_".$_SESSION['SessionSchoolID']."_JournalQuestions  
													GROUP BY JournalID
												)  AS QRY_SID_JQ    LEFT JOIN
												(
													SELECT COUNT(JQnsID) AS qC,GROUP_CONCAT(JQnsID ORDER By JQnsID) AS qL,JournalID 
													FROM QRY_".$_SESSION['SessionSchoolID']."_JournalResults 
													WHERE UserID=".$_SESSION['SessionUserID']." 
													GROUP BY JournalID
												)  AS QRY_SID_RQ
												ON (QRY_SID_JQ.JournalID=QRY_SID_RQ.JournalID),
												QRY_".$_SESSION['SessionSchoolID']."_JournalAssignment QRY_SID_JA,
												TBL_Users as U
											WHERE 
												QRY_SID_J.JournalType = 0 AND
												QRY_SID_JAA.JournalID=QRY_SID_J.JournalID AND 
												QRY_SID_JAA.JournalID=QRY_SID_JA.JournalID AND 
												QRY_SID_JAA.AssignID=QRY_SID_JA.AssignID AND
												QRY_SID_JA.JournalID=QRY_SID_JQ.JournalID AND
												QRY_SID_JA.UserID = U.UserID AND
												QRY_SID_JA.EndDate > Now() And
												(QRY_SID_JQ.qC>=QRY_SID_RQ.qC OR QRY_SID_RQ.qC IS NULL)
											ORDER BY                         
												(QRY_SID_JA.EndDate<Now()),
												(QRY_SID_JA.StartDate>Now()) ,
												QRY_SID_JA.AssignID";	
						}else{
							$this->sql = $_selectSQL2.", A.JournalName as Subject, AA.JournalID as ProtoID, AA.Message, AA.EditOption, AA.ReadingAssignment, AA.MustRead, (AA.StartDate > Now()) as Future".preg_replace('/{\w+}/', $type, $_fromSQL).preg_replace('/{\w+}/', $type, $_whereSQL2)." AND A.JournalType = 0".$_orderby;								
						}
					
						$handle = mysql_query($this->sql);
						if(mysql_errno() > 0){
							break;
						}
						$assignerArr = array();
						while($row = mysql_fetch_array($handle)){
							if(!isset($assignerArr[$row['UserID']])){
								$assignerArr[$row['UserID']] = array();
							}
							if(!isset($assignerArr[$row['UserID']][$row['ProtoID']])){
								$assignerArr[$row['UserID']][$row['ProtoID']] = array();	
							}
							$flag = $row['StartDate']."|".$row['EndDate']."|".$row['IsAllDayEvent']."|".$row['Color']."|".$row['GpStuType']."|".$row['Message']."|".$row['EditOption']."|".$row['ReadingAssignment']
									.$row['MustRead'];
							if(!isset($assignerArr[$row['UserID']][$row['ProtoID']][$flag])){
								$assignerArr[$row['UserID']][$row['ProtoID']][$flag] = array();	
							}
							array_push($assignerArr[$row['UserID']][$row['ProtoID']][$flag], $row);
						}
						foreach($assignerArr as $aitem){
							foreach($aitem as $pitem){
								foreach($pitem as $eitem){
									$assignids = '';
									$title = $eitem[0]['Subject'];
									$startdate = $this->ConvertToLocalTime($_SESSION['SessionTimezone'], $eitem[0]['StartDate'], 2);
									$enddate = $this->ConvertToLocalTime($_SESSION['SessionTimezone'], $eitem[0]['EndDate'], 2);
									$isalldyevent = $eitem[0]['IsAllDayEvent'];
									$iscrossevent = $this->isSameDay($startdate, $enddate);
									$color = $eitem[0]['Color'];
									$message = $eitem[0]['Message'];
									$courseid = $eitem[0]['Course'];
									$classidstr = '';
									$groupnamestr = '';
									$groupidstr = '';
									$studentnamestr = '';
									$studentidstr = '';
									$author = $eitem[0]['Author'];
									$assigner = $eitem[0]['UserID'];
									$protoid = $eitem[0]['ProtoID'];
									$future = 0;
									foreach($eitem as $item){
										$assignids .= $assignids == '' ? $item['Id'] : ','.$item['Id'];
										$future = $item['Future'] ? 1 : 0;
										if($item['GpStuType'] == 'QAGRP'){
											$groupidstr .= $groupidstr == '' ? $item['GpStuID'] : ','.$item['GpStuID'];
										}elseif($item['GpStuType'] == 'QASTU'){
											$studentidstr .= $studentidstr == '' ? $item['GpStuID'] : ','.$item['GpStuID'];
										}else{
											$classidstr .= $classidstr == '' ? $item['ClassID'] : ','.$item['ClassID'];	
										}	
									}
									if($groupidstr != ''){
										$this->sql = "select concat(GroupID, '|', GroupName) from QRY_".$_SESSION['SessionSchoolID']."_Group where GroupID in (".$groupidstr.")";
										$gresult = mysql_query($this->sql);
										if(mysql_errno() > 0){
											continue;	
										}
										while($grow = mysql_fetch_array($gresult)){
											$groupnamestr .= $groupnamestr == '' ? $grow[0] : ','.$grow[0];
										}		
									}
									if($studentidstr != ''){
										$this->sql = "select concat(U.UserID, '|', U.FirstName, ' ', U.LastName, '(', SP.ClassID, ')') from (select UserID, ClassID from TBL_StudentsProfile where UserID in (".$studentidstr.")) as SP, (select UserID, FirstName, LastName from TBL_Users where UserID in (".$studentidstr.")) as U where SP.UserID = U.UserID";
										$sresult = mysql_query($this->sql);
										if(mysql_errno() > 0){
											continue;	
										}
										while($srow = mysql_fetch_array($sresult)){
											$studentnamestr .= $studentnamestr == '' ? $srow[0] : ','.$srow[0];
										}
									}
									$this->events[] = array(
										$assignids,
										$title,
										$this->php2JsTime($this->mySql2PhpTime($startdate)),
										$this->php2JsTime($this->mySql2PhpTime($enddate)),
										$isalldyevent,
										$iscrossevent,	//more than one day event
										0,	//Recurring event
										$color,
										1,	//editable
										'Journal',
										$message,
										$courseid,
										$classidstr,
										$groupnamestr,
										$studentnamestr,
										$author,
										$assigner,
										$protoid,
										$future
									);	
								}
							}	
						}
						break;
					case 'PMP':
						if($this->role == "student"){
							$AssignIDArr = array();
							$ZIDArr = array();
							$QueryZ = "SELECT
											MAX(QRY_SID_ZA.AssignID),
											QRY_SID_ZA.ZID
										FROM
											QRY_".$_SESSION['SessionSchoolID']."_ZIntPrac_Assignment QRY_SID_ZA".
										$_swhereSQL.
										" GROUP BY 
											QRY_SID_ZA.ZID";
							$ResultZ = mysql_query($QueryZ);
							while ($RowZ = mysql_fetch_array($ResultZ, MYSQL_NUM)) {
								$AssignIDArr[] = $RowZ[0];
								$ZIDArr[] = $RowZ[1];
							}
							$AssignIDList = implode(",",$AssignIDArr);
							$ZIDList = implode(",",$ZIDArr);
							$Query = "SELECT 
											QRY_SID_ZA.UserID, 
											QRY_SID_Z.ZID as ProtoID,
											QRY_SID_Z.PMPName as Subject,
											QRY_SID_ZA.StartDate,
											QRY_SID_ZA.EndDate,
											QRY_SID_ZA.IsAllDayEvent,
											QRY_SID_ZA.Color,  
											QRY_SID_Z.CourseID as Course, 
											QRY_SID_ZA.Message, 
											QRY_SID_ZA.ClassID, 
											QRY_SID_ZA.GpStuType, 
											QRY_SID_ZA.GpStuID, 
											QRY_SID_Z.IPID,
											QRY_SID_ZA.LevelID,
											QRY_SID_ZA.AssignID as Id,
											concat(U.FirstName, ' ', U.LastName) as Author,
											(QRY_SID_ZA.StartDate > Now()) as Future
										FROM 
											QRY_".$_SESSION['SessionSchoolID']."_ZIntPrac QRY_SID_Z, 
											QRY_".$_SESSION['SessionSchoolID']."_ZIntPrac_Assignment QRY_SID_ZA,
											TBL_Users as U
										WHERE 
											QRY_SID_Z.ZID=QRY_SID_ZA.ZID AND
											QRY_SID_ZA.AssignID IN (".$AssignIDList.") AND
											QRY_SID_ZA.AssignType!='3' AND
											QRY_SID_ZA.UserID = U.UserID AND
											QRY_SID_ZA.EndDate > Now()
										ORDER BY
											(QRY_SID_ZA.EndDate < Now()),
											(QRY_SID_ZA.StartDate > Now()) ,
											QRY_SID_ZA.StartDate
										LIMIT
											500";
											
							$Result = mysql_query($Query);			
							if(mysql_errno() > 0){
								break;
							}
							$assignList = array();
							if (mysql_affected_rows() > 0) {
								while ($Row = mysql_fetch_array($Result)) {
									$assignList[] = $Row;
									// AssignID, Title, IPID, LevelID, StartDate, EndDate collects non-due assignments only.
								}
							}
							foreach($assignList as $aData){
								$tAssignID = $aData["Id"]*1;
								$tZID = $aData["ProtoID"];
								$tTitle = $aData["Subject"];
								$tStartDate = $aData["StartDate"];
								$tEndDate = $aData["EndDate"];
								$tStartDate = $this->ConvertToLocalTime($_SESSION['SessionTimezone'], $aData['StartDate'], 2);
								$tEndDate = $this->ConvertToLocalTime($_SESSION['SessionTimezone'], $aData['EndDate'], 2);
								$isalldyevent = $aData["IsAllDayEvent"];
								$iscrossevent = $this->isSameDay($tStartDate, $tEndDate);
								$message = $aData["Message"];
								$color = $aData["Color"];
								$courseid = $aData["Course"];
								$classidstr = '';
								$groupnamestr = '';
								$groupidstr = '';
								$studentnamestr = '';
								$studentidstr = '';
								$tIPID = $aData["IPID"];
								$tLevelID_S = $aData["LevelID"];
								$tLevelID = explode(",",$aData["LevelID"]);
								$tLevelCount = count($tLevelID);
								$tAuthor = $aData["Author"];
								$assigner = $aData["UserID"];
								$tFuture = $aData["Future"] ? 1 : 0;
								$z = $ipid * $PMP_Key . md5($ipid);
								
								if($aData["GpStuType"] == 'QAGRP'){
									$groupidstr .= $groupidstr == '' ? $aData["GpStuID"] : ','.$aData["GpStuID"];
								}elseif($aData["GpStuType"] == 'QASTU'){
									$studentidstr .= $studentidstr == '' ? $aData["GpStuID"] : ','.$aData["GpStuID"];
								}else{
									$classidstr .= $classidstr == '' ? $aData["ClassID"] : ','.$aData["ClassID"];	
								}
								if($groupidstr != ''){
									$this->sql = "select concat(GroupID, '|', GroupName) from QRY_".$_SESSION['SessionSchoolID']."_Group where GroupID in (".$groupidstr.")";
									$gresult = mysql_query($this->sql);
									if(mysql_errno() > 0){
										continue;	
									}
									while($grow = mysql_fetch_array($gresult)){
										$groupnamestr .= $groupnamestr == '' ? $grow[0] : ','.$grow[0];
									}		
								}
								if($studentidstr != ''){
									$this->sql = "select concat(U.UserID, '|', U.FirstName, ' ', U.LastName, '(', SP.ClassID, ')') from (select UserID, ClassID from TBL_StudentsProfile where UserID in (".$studentidstr.")) as SP, (select UserID, FirstName, LastName from TBL_Users where UserID in (".$studentidstr.")) as U where SP.UserID = U.UserID";
									$sresult = mysql_query($this->sql);
									if(mysql_errno() > 0){
										continue;	
									}
									while($srow = mysql_fetch_array($sresult)){
										$studentnamestr .= $studentnamestr == '' ? $srow[0] : ','.$srow[0];
									}
								}
								
								$Query = "Select
												DISTINCT(QRY_SID_ZP_R.Level)
											From
												QRY_".$_SESSION['SessionSchoolID']."_ZIntPrac_Result QRY_SID_ZP_R
											Where
												QRY_SID_ZP_R.UserID = ".$_SESSION['SessionUserID']." AND
												QRY_SID_ZP_R.ZID = ".$tZID." AND
												QRY_SID_ZP_R.Level in (".$tLevelID_S.") AND
												QRY_SID_ZP_R.ToBeContinue = 0
											Order By
												QRY_SID_ZP_R.Level";
																									
								$Result = mysql_query($Query);
								if(mysql_errno() > 0){
									break;
								}
								$haveLevel = 0;
								if (mysql_affected_rows() >= $tLevelCount) {				
									while ($Row = mysql_fetch_array($Result, MYSQL_NUM)) {
										$tempLevel = $Row[0];
										for ($m=0; $m < $tLevelCount ;$m++){
											if ($tempLevel == $tLevelID[$m]){
												$haveLevel++;
												$m = $tLevelCount;
											}
										}
									}
								}
								if($tLevelCount != $haveLevel){
									$this->events[] = array(
										$tAssignID,
										$tTitle,
										$this->php2JsTime($this->mySql2PhpTime($tStartDate)),
										$this->php2JsTime($this->mySql2PhpTime($tEndDate)),
										$isalldyevent,
										$iscrossevent,	//more than one day event
										0,	//Recurring event
										$color,
										1,	//editable
										'PMP',
										$message,
										$courseid,
										$classidstr,
										$groupnamestr,
										$studentnamestr,
										$tAuthor,
										$assigner,
										$tZID,
										$z,
										$tFuture
									);	
								}
							}
						}else{
							$this->sql = $_selectSQL2.", A.PMPName as Subject, AA.ZID as ProtoID, AA.IPID, AA.Message, AA.MPQ, AA.NAPQ, AA.MNQAR, AA.MTMR, AA.MCCQR, AA.MustRead, AA.ReadingAssignment, (AA.StartDate > Now()) as Futrue from QRY_".$_SESSION['SessionSchoolID']."_ZIntPrac A, QRY_".$_SESSION['SessionSchoolID']."_ZIntPrac_Assignment AA, TBL_Users U".preg_replace('/{\w+}/', 'Z', $_whereSQL2).$_orderby;
							$handle = mysql_query($this->sql);
							if(mysql_errno() > 0){
								break;
							}
							$assignerArr = array();
							while($row = mysql_fetch_array($handle)){
								if(!isset($assignerArr[$row['UserID']])){
									$assignerArr[$row['UserID']] = array();
								}
								if(!isset($assignerArr[$row['UserID']][$row['ProtoID']])){
									$assignerArr[$row['UserID']][$row['ProtoID']] = array();	
								}
								$flag = $row['StartDate']."|".$row['EndDate']."|".$row['IsAllDayEvent']."|".$row['Color']."|".$row['GpStuType']."|".$row['MPQ']."|".$row['NAPQ']."|".$row['MNQAR']."|".$row['MTMR']."|".$row['MCCQR']."|".$row['MustRead']."|".$row['ReadingAssignment'];
								if(!isset($assignerArr[$row['UserID']][$row['ProtoID']][$flag])){
									$assignerArr[$row['UserID']][$row['ProtoID']][$flag] = array();	
								}
								array_push($assignerArr[$row['UserID']][$row['ProtoID']][$flag], $row);
							}
							foreach($assignerArr as $aitem){
								foreach($aitem as $pitem){
									foreach($pitem as $eitem){
										$assignids = '';
										$title = $eitem[0]['Subject'];
										$startdate = $this->ConvertToLocalTime($_SESSION['SessionTimezone'], $eitem[0]['StartDate'], 2);
										$enddate = $this->ConvertToLocalTime($_SESSION['SessionTimezone'], $eitem[0]['EndDate'], 2);
										$isalldyevent = $eitem[0]['IsAllDayEvent'];
										$iscrossevent = $this->isSameDay($startdate, $enddate);
										$color = $eitem[0]['Color'];
										$message = $eitem[0]['Message'];
										$courseid = $eitem[0]['Course'];
										$classidstr = '';
										$groupnamestr = '';
										$groupidstr = '';
										$studentnamestr = '';
										$studentidstr = '';
										$author = $eitem[0]['Author'];
										$assigner = $eitem[0]['UserID'];
										$protoid = $eitem[0]['ProtoID'];
										$ipid = $eitem[0]['IPID'];
										$z = $ipid * $PMP_Key . md5($ipid);
										$future = 0;
										foreach($eitem as $item){
											$assignids .= $assignids == '' ? $item['Id'] : ','.$item['Id'];
											$future = $item['Future'] ? 1 : 0;
											if($item['GpStuType'] == 'QAGRP'){
												$groupidstr .= $groupidstr == '' ? $item['GpStuID'] : ','.$item['GpStuID'];
											}elseif($item['GpStuType'] == 'QASTU'){
												$studentidstr .= $studentidstr == '' ? $item['GpStuID'] : ','.$item['GpStuID'];
											}else{
												$classidstr .= $classidstr == '' ? $item['ClassID'] : ','.$item['ClassID'];	
											}	
										}
										if($groupidstr != ''){
											$this->sql = "select concat(GroupID, '|', GroupName) from QRY_".$_SESSION['SessionSchoolID']."_Group where GroupID in (".$groupidstr.")";
											$gresult = mysql_query($this->sql);
											if(mysql_errno() > 0){
												continue;	
											}
											while($grow = mysql_fetch_array($gresult)){
												$groupnamestr .= $groupnamestr == '' ? $grow[0] : ','.$grow[0];
											}		
										}
										if($studentidstr != ''){
											$this->sql = "select concat(U.UserID, '|', U.FirstName, ' ', U.LastName, '(', SP.ClassID, ')') from (select UserID, ClassID from TBL_StudentsProfile where UserID in (".$studentidstr.")) as SP, (select UserID, FirstName, LastName from TBL_Users where UserID in (".$studentidstr.")) as U where SP.UserID = U.UserID";
											$sresult = mysql_query($this->sql);
											if(mysql_errno() > 0){
												continue;	
											}
											while($srow = mysql_fetch_array($sresult)){
												$studentnamestr .= $studentnamestr == '' ? $srow[0] : ','.$srow[0];
											}
										}
										$this->events[] = array(
											$assignids,
											$title,
											$this->php2JsTime($this->mySql2PhpTime($startdate)),
											$this->php2JsTime($this->mySql2PhpTime($enddate)),
											$isalldyevent,
											$iscrossevent,	//more than one day event
											0,	//Recurring event
											$color,
											1,	//editable
											'PMP',
											$message,
											$courseid,
											$classidstr,
											$groupnamestr,
											$studentnamestr,
											$author,
											$assigner,
											$protoid,
											$z,
											$future
										);	
									}
								}	
							}
						}					
						break;
					case 'EA':
						if($this->role == "student"){
							$QuizIDAssigned= array();
							$AssignIDAssigned= array();
							$QueryQA = "SELECT 
											EAID,
											MAX(AssignID) AssignID
										FROM 
											QRY_".$_SESSION['SessionSchoolID']."_EA_Assignment".
										$_swhereSQL.
										" GROUP BY 
											EAID";												
							$resultQA = mysql_query($QueryQA);
							while ($RowQA = mysql_fetch_array($resultQA, MYSQL_NUM)) {
								$QuizIDAssigned[]="'".$RowQA[0]."'";
								$AssignIDAssigned[]="'".$RowQA[1]."'";
							}
							$Query = "SELECT 
											QRY_SID_QA.EAID as ProtoID, 
											QRY_SID_Q.EAName as Subject, 
											QRY_SID_QA.StartDate, 
											QRY_SID_QA.EndDate,
											QRY_SID_QA.IsAllDayEvent,
											QRY_SID_QA.Color, 
											QRY_SID_Q.CourseID as Course, 
											QRY_SID_QA.Message, 
											QRY_SID_QA.ClassID, 
											QRY_SID_QA.GpStuType , 
											QRY_SID_QA.GpStuID, 
											QRY_SID_QA.AssignID as Id,
											QRY_SID_Q.AID,
											QRY_SID_QA.LevelID,
											QRY_SID_QA.Duration,
											QRY_SID_QA.NoOfQnsApp,
											QRY_SID_QA.UserID,
											concat(U.FirstName, ' ', U.LastName) as Author,
											concat(QRY_SID_Q.SectionID, ',', QRY_SID_Q.TopicID, ',', QRY_SID_Q.SubTopicID) as Catagery,
											(QRY_SID_QA.StartDate > Now()) as Future
										FROM
											QRY_".$_SESSION['SessionSchoolID']."_EA QRY_SID_Q,						
											QRY_".$_SESSION['SessionSchoolID']."_EA_Assignment QRY_SID_QA,
											TBL_Users as U
										WHERE 
											QRY_SID_QA.EAID=QRY_SID_Q.ID AND 
											QRY_SID_Q.ID IN (".implode(",",$QuizIDAssigned).") AND
											QRY_SID_QA.AssignID IN (".implode(",",$AssignIDAssigned).") AND
											QRY_SID_QA.UserID = U.UserID AND
											QRY_SID_QA.EndDate > Now()
										ORDER BY 						
											(QRY_SID_QA.EndDate < Now()),
											(QRY_SID_QA.StartDate > Now()),
											QRY_SID_QA.AssignID";	
										
							$Result = mysql_query($Query);
							if(mysql_errno()>0){
								break;	
							}
							while ($Row = mysql_fetch_array($Result)){
								$toUseMarker = 0;
								$assignids = '';
								$title = $Row['Subject'];
								$startdate = $Row['StartDate'];
								$enddate = $Row['EndDate'];
								$isalldyevent = $Row['IsAllDayEvent'];
								$iscrossevent = $this->isSameDay($startdate, $enddate);
								$color = $Row['Color'];
								$message = $Row['Message'];
								$courseid = $Row['Course'];
								$classidstr = '';
								$groupnamestr = '';
								$groupidstr = '';
								$studentnamestr = '';
								$studentidstr = '';
								$author = $Row['Author'];
								$assigner = $Row['UserID'];
								$protoid = $Row['ProtoID'];
								$encodeid = $Row['AID'];
								$catateryid = $Row['Catagery'];
								$future = $Row["Future"] ? 1 : 0;
								$assignids .= $assignids == '' ? $Row['Id'] : ','.$Row['Id'];
								if($Row['GpStuType'] == 'QAGRP'){
									$groupidstr .= $groupidstr == '' ? $Row['GpStuID'] : ','.$Row['GpStuID'];
								}elseif($item['GpStuType'] == 'QASTU'){
									$studentidstr .= $studentidstr == '' ? $Row['GpStuID'] : ','.$Row['GpStuID'];
								}else{
									$classidstr .= $classidstr == '' ? $Row['ClassID'] : ','.$Row['ClassID'];	
								}
								if($groupidstr != ''){
									$this->sql = "select concat(GroupID, '|', GroupName) from QRY_".$_SESSION['SessionSchoolID']."_Group where GroupID in (".$groupidstr.")";
									$gresult = mysql_query($this->sql);
									if(mysql_errno() > 0){
										continue;	
									}
									while($grow = mysql_fetch_array($gresult)){
										$groupnamestr .= $groupnamestr == '' ? $grow[0] : ','.$grow[0];
									}		
								}
								if($studentidstr != ''){
									$this->sql = "select concat(U.UserID, '|', U.FirstName, ' ', U.LastName, '(', SP.ClassID, ')') from (select UserID, ClassID from TBL_StudentsProfile where UserID in (".$studentidstr.")) as SP, (select UserID, FirstName, LastName from TBL_Users where UserID in (".$studentidstr.")) as U where SP.UserID = U.UserID";
									$sresult = mysql_query($this->sql);
									if(mysql_errno() > 0){
										continue;	
									}
									while($srow = mysql_fetch_array($sresult)){
										$studentnamestr .= $studentnamestr == '' ? $srow[0] : ','.$srow[0];
									}
								}
								
								$CheckResultsQuery = "
												SELECT
													SUM(QRY_EA_RD.AppMode='Quantity' and QRY_EA_RD.AppNoOfCorrectQns='1') AS CorrectCaptureQTY, 
													SUM(QRY_EA_RD.AppMode='Quantity') AS QTY,
													SUM(QRY_EA_RD.AppMode='TIME' and QRY_EA_RD.AppNoOfCorrectQns='1') AS CorrectCaptureTME, 
													SUM(QRY_EA_RD.AppMode='TIME') AS TME,
													SUM(QRY_EA_RD.AppDuration) AS AppDuration,
													QRY_EA_R.toUse,
													QRY_EA_R.CounterAT AS AppDuration
												FROM
													QRY_".$_SESSION['SessionSchoolID']."_EA_Assignment QRY_EA_A, 
													QRY_".$_SESSION['SessionSchoolID']."_EA_Report QRY_EA_R, 
													QRY_".$_SESSION['SessionSchoolID']."_EA_Report_Details QRY_EA_RD 
												WHERE 
													QRY_EA_A.AssignID='".$Row["Id"]."' AND 
													QRY_EA_A.AssignID=QRY_EA_R.AssignID AND 
													QRY_EA_RD.AssignID=QRY_EA_R.AssignID AND 
													QRY_EA_R.TID=QRY_EA_RD.TID AND 
													QRY_EA_R.UserID=QRY_EA_RD.UserID AND 
													QRY_EA_RD.UserID=".$_SESSION['SessionUserID']." 
												GROUP BY 
													QRY_EA_R.UserID 
												Order By 
													QRY_EA_R.TID DESC";
								$CheckResults = mysql_query($CheckResultsQuery);
								$LevelID=explode(",",$Row["LevelID"]);								
								$notdone = true;
								$NoOfQnsApp = $Row["NoOfQnsApp"];
								$Duration = $Row["Duration"];
								if(mysql_affected_rows() > 0){
									while ($RowCheck = mysql_fetch_array($CheckResults, MYSQL_NUM)){
										$arrayFlag = array();
										$toUseMarker += $RowCheck[5];	
										foreach($LevelID as $apptype){
											if($apptype == 1){
												if($RowCheck[1] >= $NoOfQnsApp){
													$arrayFlag[] = 1;
												}
											}
											if($apptype == 2){
												if(($RowCheck[6]/60) >= $Duration){
													$arrayFlag[] = 1;
												}
											}
										}
										if(array_sum($arrayFlag) >= count($LevelID) && $toUseMarker > 0){
											$notdone=false;
											break;
										}
									}	
								}
								if($notdone){
									$this->events[] = array(
										$assignids,
										$title,
										$this->php2JsTime($this->mySql2PhpTime($startdate)),
										$this->php2JsTime($this->mySql2PhpTime($enddate)),
										$isalldyevent,
										$iscrossevent,	//more than one day event
										0,	//Recurring event
										$color,
										1,	//editable
										'EA',
										$message,
										$courseid,
										$classidstr,
										$groupnamestr,
										$studentnamestr,
										$author,
										$assigner,
										$protoid,
										$encodeid,
										$catateryid,
										$future
									);		
								}	
							}
						}else{
							$this->sql = $_selectSQL2.", A.EAName as Subject, A.AID, A.ID as ProtoID, concat(A.SectionID, ',', A.TopicID, ',', A.SubTopicID) as Catagery, AA.Message, (AA.StartDate > Now()) as Futrue from QRY_".$_SESSION['SessionSchoolID']."_EA A, QRY_".$_SESSION['SessionSchoolID']."_EA_Assignment AA, TBL_Users U".$_whereSQL3.$_orderby;
							$handle = mysql_query($this->sql);
							if(mysql_errno() > 0){
								break;
							}
							$assignerArr = array();
							while($row = mysql_fetch_array($handle)){
								if(!isset($assignerArr[$row['UserID']])){
									$assignerArr[$row['UserID']] = array();
								}
								if(!isset($assignerArr[$row['UserID']][$row['ProtoID']])){
									$assignerArr[$row['UserID']][$row['ProtoID']] = array();	
								}
								$flag = $row['StartDate']."|".$row['EndDate']."|".$row['IsAllDayEvent']."|".$row['Color']."|".$row['GpStuType']."|".$row['Message'];
								if(!isset($assignerArr[$row['UserID']][$row['ProtoID']][$flag])){
									$assignerArr[$row['UserID']][$row['ProtoID']][$flag] = array();	
								}
								array_push($assignerArr[$row['UserID']][$row['ProtoID']][$flag], $row);
							}
							foreach($assignerArr as $aitem){
								foreach($aitem as $pitem){
									foreach($pitem as $eitem){
										$assignids = '';
										$title = $eitem[0]['Subject'];
										$startdate = $eitem[0]['StartDate'];
										$enddate = $eitem[0]['EndDate'];
										$isalldyevent = $eitem[0]['IsAllDayEvent'];
										$iscrossevent = $this->isSameDay($startdate, $enddate);
										$color = $eitem[0]['Color'];
										$message = $eitem[0]['Message'];
										$courseid = $eitem[0]['Course'];
										$classidstr = '';
										$groupnamestr = '';
										$groupidstr = '';
										$studentnamestr = '';
										$studentidstr = '';
										$author = $eitem[0]['Author'];
										$assigner = $eitem[0]['UserID'];
										$protoid = $eitem[0]['ProtoID'];
										$encodeid = $eitem[0]['AID'];
										$catateryid = $eitem[0]['Catagery'];
										$future = 0;
										foreach($eitem as $item){
											$assignids .= $assignids == '' ? $item['Id'] : ','.$item['Id'];
											$future = $eitem[0]['Future'] ? 1 : 0;
											if($item['GpStuType'] == 'QAGRP'){
												$groupidstr .= $groupidstr == '' ? $item['GpStuID'] : ','.$item['GpStuID'];
											}elseif($item['GpStuType'] == 'QASTU'){
												$studentidstr .= $studentidstr == '' ? $item['GpStuID'] : ','.$item['GpStuID'];
											}else{
												$classidstr .= $classidstr == '' ? $item['ClassID'] : ','.$item['ClassID'];	
											}	
										}
										if($groupidstr != ''){
											$this->sql = "select concat(GroupID, '|', GroupName) from QRY_".$_SESSION['SessionSchoolID']."_Group where GroupID in (".$groupidstr.")";
											$gresult = mysql_query($this->sql);
											if(mysql_errno() > 0){
												continue;	
											}
											while($grow = mysql_fetch_array($gresult)){
												$groupnamestr .= $groupnamestr == '' ? $grow[0] : ','.$grow[0];
											}		
										}
										if($studentidstr != ''){
											$this->sql = "select concat(U.UserID, '|', U.FirstName, ' ', U.LastName, '(', SP.ClassID, ')') from (select UserID, ClassID from TBL_StudentsProfile where UserID in (".$studentidstr.")) as SP, (select UserID, FirstName, LastName from TBL_Users where UserID in (".$studentidstr.")) as U where SP.UserID = U.UserID";
											$sresult = mysql_query($this->sql);
											if(mysql_errno() > 0){
												continue;	
											}
											while($srow = mysql_fetch_array($sresult)){
												$studentnamestr .= $studentnamestr == '' ? $srow[0] : ','.$srow[0];
											}
										}
										$this->events[] = array(
											$assignids,
											$title,
											$this->php2JsTime($this->mySql2PhpTime($startdate)),
											$this->php2JsTime($this->mySql2PhpTime($enddate)),
											$isalldyevent,
											$iscrossevent,	//more than one day event
											0,	//Recurring event
											$color,
											1,	//editable
											'EA',
											$message,
											$courseid,
											$classidstr,
											$groupnamestr,
											$studentnamestr,
											$author,
											$assigner,
											$protoid,
											$encodeid,
											$catateryid,
											$future
										);	
									}
								}	
							}
						}
						break;	
					case 'AT':
						$this->sql = $_selectSQLAT.$_fromSQLAT.$_whereSQLAT;
						$handle = mysql_query($this->sql);
						if(mysql_errno() > 0){
							break;
						}
						$assignerArr = array();
						while($row = mysql_fetch_array($handle)){
							if(!isset($assignerArr[$row['UserID']])){
								$assignerArr[$row['UserID']] = array();
							}
							if(!isset($assignerArr[$row['UserID']][$row['ProtoID']])){
								$assignerArr[$row['UserID']][$row['ProtoID']] = array();	
							}
							$flag = $row['StartDate']."|".$row['EndDate']."|".$row['IsAllDayEvent']."|".$row['Color']."|".$row['GpStuType'];
							if(!isset($assignerArr[$row['UserID']][$row['ProtoID']][$flag])){
								$assignerArr[$row['UserID']][$row['ProtoID']][$flag] = array();	
							}
							array_push($assignerArr[$row['UserID']][$row['ProtoID']][$flag], $row);
						}
						foreach($assignerArr as $aitem){
							foreach($aitem as $pitem){
								foreach($pitem as $eitem){
									$assignids = '';
									$title = $eitem[0]['Subject'];
									$startdate = $eitem[0]['StartDate'];
									$enddate = $eitem[0]['EndDate'];
									$isalldyevent = $eitem[0]['IsAllDayEvent'];
									$iscrossevent = $this->isSameDay($startdate, $enddate);
									$color = $eitem[0]['Color'];
									$message = '';
									$courseid = $eitem[0]['Course'];
									$classidstr = '';
									$groupnamestr = '';
									$groupidstr = '';
									$studentnamestr = '';
									$studentidstr = '';
									$author = $eitem[0]['Author'];
									$assigner = $eitem[0]['UserID'];
									$protoid = $eitem[0]['ProtoID'];
									$videoid = $eitem[0]['VID'];
									$catateryid = $eitem[0]['Catagery'];
									$future = 0;
									foreach($eitem as $item){
										$assignids .= $assignids == '' ? $item['Id'] : ','.$item['Id'];
										$future = $eitem[0]['Future'] ? 1 : 0;
										if($item['GpStuType'] == 'QAGRP'){
											$groupidstr .= $groupidstr == '' ? $item['GpStuID'] : ','.$item['GpStuID'];
										}elseif($item['GpStuType'] == 'QASTU'){
											$studentidstr .= $studentidstr == '' ? $item['GpStuID'] : ','.$item['GpStuID'];
										}else{
											$classidstr .= $classidstr == '' ? $item['ClassID'] : ','.$item['ClassID'];	
										}	
									}
									if($groupidstr != ''){
										$this->sql = "select concat(GroupID, '|', GroupName) from QRY_".$_SESSION['SessionSchoolID']."_Group where GroupID in (".$groupidstr.")";
										$gresult = mysql_query($this->sql);
										if(mysql_errno() > 0){
											continue;	
										}
										while($grow = mysql_fetch_array($gresult)){
											$groupnamestr .= $groupnamestr == '' ? $grow[0] : ','.$grow[0];
										}		
									}
									if($studentidstr != ''){
										$this->sql = "select concat(U.UserID, '|', U.FirstName, ' ', U.LastName, '(', SP.ClassID, ')') from (select UserID, ClassID from TBL_StudentsProfile where UserID in (".$studentidstr.")) as SP, (select UserID, FirstName, LastName from TBL_Users where UserID in (".$studentidstr.")) as U where SP.UserID = U.UserID";
										$sresult = mysql_query($this->sql);
										if(mysql_errno() > 0){
											continue;	
										}
										while($srow = mysql_fetch_array($sresult)){
											$studentnamestr .= $studentnamestr == '' ? $srow[0] : ','.$srow[0];
										}
									}
									$this->events[] = array(
										$assignids,
										$title,
										$this->php2JsTime($this->mySql2PhpTime($startdate)),
										$this->php2JsTime($this->mySql2PhpTime($enddate)),
										$isalldyevent,
										$iscrossevent,	//more than one day event
										0,	//Recurring event
										$color,
										1,	//editable
										'AT',
										$message,
										$courseid,
										$classidstr,
										$groupnamestr,
										$studentnamestr,
										$author,
										$assigner,
										$protoid,
										$videoid,
										$catateryid,
										$future
									);	
								}
							}	
						}
						break;		
					case 'SV':
						if($this->role == "student"){
							$GroupArray = array();
							$GroupString = "";
							$ClassID = "0";
							$GetGroup = "SELECT DISTINCT(GroupID) FROM QRY_".$_SESSION['SessionSchoolID']."_GroupMembers QRY_GM WHERE QRY_GM.UserID='".$_SESSION['SessionUserID']."'";
							$ResultG = mysql_query($GetGroup);
							while ($RowG = mysql_fetch_array($ResultG)) {
								$GroupArray[] = $RowG[0];
							}
							$GroupString = "'".implode("','", $GroupArray)."'";
							$GetClass = "SELECT ClassID FROM TBL_StudentsProfile TBL_SP WHERE TBL_SP.UserID='".$_SESSION['SessionUserID']."'";
							$ResultC = mysql_query($GetClass);
							while ($RowC = mysql_fetch_array($ResultC)){
								if($RowC[0]!=""){
									$ClassID=$RowC[0];
								}
							}
							$sidArray=array();
							$sidString="";
							$GetSurveyR = "SELECT DISTINCT(SID) FROM QRY_".$_SESSION['SessionSchoolID']."_SurveyResult QRY_SR WHERE QRY_SR.UserID='".$_SESSION['SessionUserID']."'";
							$ResultR = mysql_query($GetSurveyR);
							while ($RowR = mysql_fetch_array($ResultR)){
								$sidArray[]=$RowR[0];
							}
							if(count($sidArray)>0){
								$sidString = "'".implode("','", $sidArray)."'";
							}else{
								$sidString=0;
							}
							if($GroupString=="''"){
								$GroupString="'-1'";
							}
							$Query = "SELECT 
										QRY_SS.SID AS ProtoID, 
										QRY_SS.SurveyName AS Subject, 
										QRY_SA.StartDate,
										QRY_SA.EndDate,
										QRY_SA.IsAllDayEvent,
										QRY_SA.Color,
										QRY_SS.CourseID AS Course,
										QRY_SA.ClassID, 
										QRY_SA.GroupID, 
										QRY_SA.SAID as Id,
										QRY_SA.UserID,
										CONCAT(TBL_U.FirstName, ' ', TBL_U.LastName) AS Author,
										(QRY_SA.StartDate > Now()) as Future
									FROM
										QRY_".$_SESSION['SessionSchoolID']."_SurveyAssign QRY_SA,
										QRY_".$_SESSION['SessionSchoolID']."_SurveySetting QRY_SS
											LEFT JOIN
										TBL_Users TBL_U ON (TBL_U.UserID = QRY_SS.UserID)
									WHERE
										(QRY_SA.GroupID IN (".$GroupString.") OR QRY_SA.ClassID = '".$ClassID."') AND
										QRY_SA.StartDate<=Now() AND 
										QRY_SA.EndDate>=Now() AND
										QRY_SA.SID NOT IN (".$sidString.") AND
										QRY_SS.SID=QRY_SA.SID AND
										QRY_SS.SurveyType=0  
									GROUP BY
										QRY_SS.SID";
							/*				
							$hder = fopen('events.txt', 'a+');
							if($hder){
								if(chmod('events.txt', 0777)){
									fwrite($hder, $Query."\r\n ###");
								}
							}
							fclose($hder);		*/
							
							$handle = mysql_query($Query);
							if(mysql_errno()>0){
								break;	
							}
							$assignerArr = array();
							while($row = mysql_fetch_array($handle)){
								if(!isset($assignerArr[$row['UserID']])){
									$assignerArr[$row['UserID']] = array();
								}
								if(!isset($assignerArr[$row['UserID']][$row['ProtoID']])){
									$assignerArr[$row['UserID']][$row['ProtoID']] = array();	
								}
								$flag = $row['StartDate']."|".$row['EndDate']."|".$row['IsAllDayEvent']."|".$row['Color'];
								if(!isset($assignerArr[$row['UserID']][$row['ProtoID']][$flag])){
									$assignerArr[$row['UserID']][$row['ProtoID']][$flag] = array();	
								}
								array_push($assignerArr[$row['UserID']][$row['ProtoID']][$flag], $row);
							}
							foreach($assignerArr as $aitem){
								foreach($aitem as $pitem){
									foreach($pitem as $eitem){
										$assignids = '';
										$title = $eitem[0]['Subject'];
										$startdate = $eitem[0]['StartDate'];
										$enddate = $eitem[0]['EndDate'];
										$isalldyevent = $eitem[0]['IsAllDayEvent'];
										$iscrossevent = $this->isSameDay($startdate, $enddate);
										$color = $eitem[0]['Color'];
										$message = '';
										$courseid = $eitem[0]['Course'];
										$classidstr = '';
										$groupnamestr = '';
										$groupidstr = '';
										$author = $eitem[0]['Author'];
										$assigner = $eitem[0]['UserID'];
										$protoid = $eitem[0]['ProtoID'];
										$future = 0;
										foreach($eitem as $item){
											$assignids .= $assignids == '' ? $item['Id'] : ','.$item['Id'];
											$future = $eitem[0]['Future'] ? 1 : 0;
											$groupidstr .= $groupidstr == '' ? $item['GroupID'] : ','.$item['GroupID'];
											$classidstr .= $classidstr == '' ? $item['ClassID'] : ','.$item['ClassID'];	
										}
										if($groupidstr != ''){
											$this->sql = "select concat(GroupID, '|', GroupName) from QRY_".$_SESSION['SessionSchoolID']."_Group where GroupID in (".$groupidstr.")";
											$gresult = mysql_query($this->sql);
											if(mysql_errno() > 0){
												continue;	
											}
											while($grow = mysql_fetch_array($gresult)){
												$groupnamestr .= $groupnamestr == '' ? $grow[0] : ','.$grow[0];
											}		
										}
										$this->events[] = array(
											$assignids,
											$title,
											$this->php2JsTime($this->mySql2PhpTime($startdate)),
											$this->php2JsTime($this->mySql2PhpTime($enddate)),
											$isalldyevent,
											$iscrossevent,	//more than one day event
											0,	//Recurring event
											$color,
											1,	//editable
											'SV',
											'',
											$courseid,
											$classidstr,
											$groupnamestr,
											$protoid,
											$author,
											$assigner,
											$future
										);	
									}
								}	
							}
						}else{
							if($_SESSION['SessionSchoolID'] == "ZHS"){
								$Query = "SELECT 
												DISTINCT(QRY_SS.SID) AS ProtoID, 
												QRY_SS.SurveyName AS Subject, 
												QRY_SA.StartDate, 
												QRY_SA.EndDate,
												QRY_SA.IsAllDayEvent,
												QRY_SA.Color,
												QRY_SS.CourseID AS Course, 
												QRY_SA.ClassID, 
												QRY_SA.GroupID, 
												QRY_SA.SAID as Id, 
												QRY_SA.UserID,
												CONCAT(TBL_U.FirstName, ' ', TBL_U.LastName) AS Author,
												(QRY_SA.StartDate > Now()) as Future
											FROM
												QRY_".$_SESSION['SessionSchoolID']."_SurveyAssign QRY_SA,
												QRY_".$_SESSION['SessionSchoolID']."_SurveySetting QRY_SS
													LEFT JOIN
												TBL_Users TBL_U ON (TBL_U.UserID = QRY_SS.UserID)	
											WHERE
												QRY_SS.SID = QRY_SA.SID AND
												QRY_SS.SurveyType = 0 AND (
												QRY_SS.UserID = '".$_SESSION['SessionUserID']."' OR
												QRY_SS.UserID = '583442' OR 
												QRY_SS.UserID = '37821') AND
												QRY_SA.UserID = '".$_SESSION['SessionUserID']."' AND
												(QRY_SA.StartDate between '".$this->startTime."' and '".$this->endTime."' or QRY_SA.StartDate < '".$this->startTime."' and QRY_SA.EndDate > '".$this->startTime."')
											GROUP BY 
												QRY_SS.SID,
												QRY_SA.StartDate,
												QRY_SA.EndDate";
							}else{
								$Query = "SELECT 
												DISTINCT(QRY_SS.SID) AS ProtoID, 
												QRY_SS.SurveyName AS Subject, 
												QRY_SA.StartDate, 
												QRY_SA.EndDate,
												QRY_SA.IsAllDayEvent,
												QRY_SA.Color,
												QRY_SS.CourseID AS Course, 
												QRY_SA.ClassID, 
												QRY_SA.GroupID, 
												QRY_SA.SAID as Id, 
												QRY_SA.UserID,
												CONCAT(TBL_U.FirstName, ' ', TBL_U.LastName) AS Author,
												(QRY_SA.StartDate > Now()) as Future
											FROM
												QRY_".$_SESSION['SessionSchoolID']."_SurveyAssign QRY_SA,
												QRY_".$_SESSION['SessionSchoolID']."_SurveySetting QRY_SS
													LEFT JOIN
												TBL_Users TBL_U ON (TBL_U.UserID = QRY_SS.UserID)												
											WHERE
												QRY_SS.SID = QRY_SA.SID AND
												QRY_SS.SurveyType = 0 AND
												QRY_SA.UserID = '".$_SESSION['SessionUserID']."' AND
												(QRY_SA.StartDate between '".$this->startTime."' and '".$this->endTime."' or QRY_SA.StartDate < '".$this->startTime."' and QRY_SA.EndDate > '".$this->startTime."') 	
											GROUP BY 
												QRY_SS.SID,
												QRY_SA.StartDate,
												QRY_SA.EndDate";
							}
							$handle = mysql_query($Query);
							if(mysql_errno()>0){
								break;	
							}
							$assignerArr = array();
							
							if($_SESSION['SessionSchoolID']=="A0110"){
								$ArrayTempSID = array(
														"1"=>array(453885, 453878),
														"2"=>array(453885, 453872),
														"3"=>array(453885, 453873),
														"4"=>array(453885, 453888),
														"5"=>array(453885, 453877),
														"6"=>array(453885, 453886),
														"7"=>array(453885, 453887),
														"8"=>array(453885, 453881),
														"9"=>array(453885, 453885),
														"10"=>array(453885, 453875),
														"11"=>array(453885, 453880),
														"12"=>array(453885, 524133),
														"13"=>array(453885, 524134),
														"14"=>array(453885, 453885),
														"15"=>array(453885, 453875),
														"16"=>array(453885, 453880),
													 );
								while ($row = mysql_fetch_array($handle)) {
									$ShowSurvey = false;
									if(isset($ArrayTempSID[$row[0]])){
										$AllowedUserIDArray = $ArrayTempSID[$row[0]];
										if(in_array($_SESSION['SessionUserID'], $AllowedUserIDArray)){
											$ShowSurvey = true;
										}
									}else{
										$ShowSurvey = true;
									}
									
									if($ShowSurvey){
										if(!isset($assignerArr[$row['UserID']])){
											$assignerArr[$row['UserID']] = array();
										}
										if(!isset($assignerArr[$row['UserID']][$row['ProtoID']])){
											$assignerArr[$row['UserID']][$row['ProtoID']] = array();	
										}
										$flag = $row['StartDate']."|".$row['EndDate']."|".$row['IsAllDayEvent']."|".$row['Color'];
										if(!isset($assignerArr[$row['UserID']][$row['ProtoID']][$flag])){
											$assignerArr[$row['UserID']][$row['ProtoID']][$flag] = array();	
										}
										array_push($assignerArr[$row['UserID']][$row['ProtoID']][$flag], $row);
									}
								}						 
							}else{
								$SIDUserID = array();
								if($_SESSION['SessionSchoolID']=='A0066')								{
									$SIDUserID = array(
														1=>array(333709,333705),
														2=>array(333705,333716),
														3=>array(333705,525458),
														4=>array(333705,565161,333703),
														5=>array(333705,480205,525460),
														6=>array(333705,375857,333702),
														7=>array(333705,375857,466960),
														8=>array(333705,533350,466960),
														9=>array(333705,333713,466960),
														10=>array(333705,333701),
														11=>array(333705,333701),
														12=>array(333705,333708),
														13=>array(333705,333708),
														14=>array(333705,333700),
														15=>array(333705,333700),
														16=>array(333705,375856),
														17=>array(333705,375856),
														18=>array(333705,345424),
														19=>array(333705,466959),
														20=>array(333705,345424,333709,375856),
														21=>array(333705,375856,333709),
														22=>array(333705,375857,586229),
														23=>array(333705,333704,333701),
														24=>array(333705,375856),
														25=>array(333705),
														26=>array(333705,375856,466959),
														27=>array(333705,333704),
														28=>array(333705)
													  );
								}
								while ($row = mysql_fetch_array($handle)) {
									$ShowSurvey = false;
									if($_SESSION['SessionSchoolID'] == 'A0066'){
										if(in_array($_SESSION['SessionUserID'], $SIDUserID[$row[0]])){
											$ShowSurvey = true;
										}
									}else{
										$ShowSurvey = true;
									}
									if($ShowSurvey){
										if(!isset($assignerArr[$row['UserID']])){
											$assignerArr[$row['UserID']] = array();
										}
										if(!isset($assignerArr[$row['UserID']][$row['ProtoID']])){
											$assignerArr[$row['UserID']][$row['ProtoID']] = array();	
										}
										$flag = $row['StartDate']."|".$row['EndDate']."|".$row['IsAllDayEvent']."|".$row['Color'];
										if(!isset($assignerArr[$row['UserID']][$row['ProtoID']][$flag])){
											$assignerArr[$row['UserID']][$row['ProtoID']][$flag] = array();	
										}
										array_push($assignerArr[$row['UserID']][$row['ProtoID']][$flag], $row);
									}
								}
							}	
							foreach($assignerArr as $aitem){
								foreach($aitem as $pitem){
									foreach($pitem as $eitem){
										$assignids = '';
										$title = $eitem[0]['Subject'];
										$startdate = $eitem[0]['StartDate'];
										$enddate = $eitem[0]['EndDate'];
										$isalldyevent = $eitem[0]['IsAllDayEvent'];
										$iscrossevent = $this->isSameDay($startdate, $enddate);
										$color = $eitem[0]['Color'];
										$message = '';
										$courseid = $eitem[0]['Course'];
										$classidstr = '';
										$groupnamestr = '';
										$groupidstr = '';
										$author = $eitem[0]['Author'];
										$assigner = $eitem[0]['UserID'];
										$protoid = $eitem[0]['ProtoID'];
										$future = 0;
										foreach($eitem as $item){
											$assignids .= $assignids == '' ? $item['Id'] : ','.$item['Id'];
											$future = $eitem[0]['Future'] ? 1 : 0;
											$groupidstr .= $groupidstr == '' ? $item['GroupID'] : ','.$item['GroupID'];
											$classidstr .= $classidstr == '' ? $item['ClassID'] : ','.$item['ClassID'];	
										}
										if($groupidstr != ''){
											$this->sql = "select concat(GroupID, '|', GroupName) from QRY_".$_SESSION['SessionSchoolID']."_Group where GroupID in (".$groupidstr.")";
											$gresult = mysql_query($this->sql);
											if(mysql_errno() > 0){
												continue;	
											}
											while($grow = mysql_fetch_array($gresult)){
												$groupnamestr .= $groupnamestr == '' ? $grow[0] : ','.$grow[0];
											}		
										}
										$this->events[] = array(
											$assignids,
											$title,
											$this->php2JsTime($this->mySql2PhpTime($startdate)),
											$this->php2JsTime($this->mySql2PhpTime($enddate)),
											$isalldyevent,
											$iscrossevent,	//more than one day event
											0,	//Recurring event
											$color,
											1,	//editable
											'SV',
											'',
											$courseid,
											$classidstr,
											$groupnamestr,
											$protoid,
											$author,
											$assigner,
											$future
										);	
									}
								}	
							}		
						}
						break;
					case 'Forum':
						if($this->role == "student"){
							$GroupArray = array();
							$GroupString = "";
							$ClassID = "0";
							$GetGroup = "SELECT DISTINCT(GroupID) FROM QRY_".$_SESSION['SessionSchoolID']."_GroupMembers QRY_GM WHERE QRY_GM.UserID='".$_SESSION['SessionUserID']."'";
							$ResultG = mysql_query($GetGroup);
							while ($RowG = mysql_fetch_array($ResultG)) {
								$GroupArray[] = $RowG[0];
							}
							$GroupString = "'".implode("','", $GroupArray)."'";
							$GetClass = "SELECT ClassID FROM TBL_StudentsProfile TBL_SP WHERE TBL_SP.UserID='".$_SESSION['SessionUserID']."'";
							$ResultC = mysql_query($GetClass);
							while ($RowC = mysql_fetch_array($ResultC)){
								if($RowC[0]!=""){
									$ClassID=$RowC[0];
								}
							}
							$sidArray=array();
							$sidString="";
							$GetSurveyR = "SELECT DISTINCT(SID) FROM QRY_".$_SESSION['SessionSchoolID']."_SurveyResult QRY_SR WHERE QRY_SR.UserID='".$_SESSION['SessionUserID']."'";
							$ResultR = mysql_query($GetSurveyR);
							while ($RowR = mysql_fetch_array($ResultR)){
								$sidArray[]=$RowR[0];
							}
							if(count($sidArray)>0){
								$sidString = "'".implode("','", $sidArray)."'";
							}else{
								$sidString=0;
							}
							if($GroupString=="''"){
								$GroupString="'-1'";
							}
							$Query = "SELECT  
										QRY_FTA.UserID,
										QRY_FTA.AssignID as Id,
										QRY_FTA.StartDate,
										QRY_FTA.EndDate,
										QRY_FTA.IsAllDayEvent,
										QRY_FTA.Color,
										QRY_FTA.ClassID,
										QRY_FTA.GpStuType,
										QRY_FTA.GpStuID,
										QRY_FTA.FSID,
										QRY_FTA.FTID as ProtoID,
										QRY_FS.CourseID as Course,
										QRY_FT.Topic as Subject,
										QRY_FT.Message,
										CONCAT(TBL_U.FirstName, ' ', TBL_U.LastName) AS Author,
										(QRY_FTA.StartDate > Now()) as Future
									FROM 
										QRY_".$_SESSION['SessionSchoolID']."_ForumTopicAssignment QRY_FTA,
										QRY_".$_SESSION['SessionSchoolID']."_ForumTopic QRY_FT
											LEFT JOIN
										TBL_Users TBL_U ON (TBL_U.UserID = QRY_FT.UserID)
											LEFT JOIN
										QRY_".$_SESSION['SessionSchoolID']."_ForumSection QRY_FS ON (QRY_FT.FSID = QRY_FS.FSID)
									WHERE 
										QRY_FTA.FTID = QRY_FT.FTID AND
										QRY_FT.ForumType = 0 AND
										QRY_FTA.StartDate<=Now() AND 
										QRY_FTA.EndDate>=Now() AND (
										((QRY_FTA.GpStuType = '' OR QRY_FTA.GpStuType is NULL) AND QRY_FTA.ClassID = '".$ClassID."') OR
										(QRY_FTA.GpStuType = 'QAGRP' AND QRY_FTA.GpStuID IN (".$GroupString.")) OR
										(QRY_FTA.GpStuType = 'QASTU' AND QRY_FTA.GpStuID = ".$_SESSION['SessionUserID'].") )
									ORDER BY 
										QRY_FTA.StartDate DESC";
					
						}else{
							$Query = "SELECT  
											QRY_FTA.UserID,
											QRY_FTA.AssignID as Id,
											QRY_FTA.StartDate,
											QRY_FTA.EndDate,
											QRY_FTA.IsAllDayEvent,
											QRY_FTA.Color,
											QRY_FTA.ClassID,
											QRY_FTA.GpStuType,
											QRY_FTA.GpStuID,
											QRY_FTA.FSID,
											QRY_FTA.FTID as ProtoID,
											QRY_FS.CourseID as Course,
											QRY_FT.Topic as Subject,
											QRY_FT.Message,
											CONCAT(TBL_U.FirstName, ' ', TBL_U.LastName) AS Author,
											(QRY_FTA.StartDate > Now()) as Future
										FROM 
											QRY_".$_SESSION['SessionSchoolID']."_ForumTopicAssignment QRY_FTA,
											QRY_".$_SESSION['SessionSchoolID']."_ForumTopic QRY_FT
												LEFT JOIN
											TBL_Users TBL_U ON (TBL_U.UserID = QRY_FT.UserID)
												LEFT JOIN
											QRY_".$_SESSION['SessionSchoolID']."_ForumSection QRY_FS ON (QRY_FT.FSID = QRY_FS.FSID)	
										WHERE 
											QRY_FTA.FTID = QRY_FT.FTID AND
											QRY_FTA.UserID = '".$_SESSION['SessionUserID']."' AND
											QRY_FT.ForumType = 0 AND
											(QRY_FTA.StartDate between '".$this->startTime."' and '".$this->endTime."' or QRY_FTA.StartDate < '".$this->startTime."' and QRY_FTA.EndDate > '".$this->startTime."')
										ORDER BY 
											QRY_FTA.StartDate DESC";
						}
						
						$handle = mysql_query($Query);
						if(mysql_errno() > 0){
							break;
						}
						$assignerArr = array();
						while($row = mysql_fetch_array($handle)){
							if(!isset($assignerArr[$row['UserID']])){
								$assignerArr[$row['UserID']] = array();
							}
							if(!isset($assignerArr[$row['UserID']][$row['ProtoID']])){
								$assignerArr[$row['UserID']][$row['ProtoID']] = array();	
							}
							$flag = $row['StartDate']."|".$row['EndDate']."|".$row['IsAllDayEvent']."|".$row['Color']."|".$row['GpStuType']."|".$row['Message'];
							if(!isset($assignerArr[$row['UserID']][$row['ProtoID']][$flag])){
								$assignerArr[$row['UserID']][$row['ProtoID']][$flag] = array();	
							}
							array_push($assignerArr[$row['UserID']][$row['ProtoID']][$flag], $row);
						}
						foreach($assignerArr as $aitem){
							foreach($aitem as $pitem){
								foreach($pitem as $eitem){
									$assignids = '';
									$title = $eitem[0]['Subject'];
									$startdate = $this->ConvertToLocalTime($_SESSION['SessionTimezone'], $eitem[0]['StartDate'], 2);
									$enddate = $this->ConvertToLocalTime($_SESSION['SessionTimezone'], $eitem[0]['EndDate'], 2);
									$isalldyevent = $eitem[0]['IsAllDayEvent'];
									$iscrossevent = $this->isSameDay($startdate, $enddate);
									$color = $eitem[0]['Color'];
									$message = $eitem[0]['Message'];
									$courseid = $eitem[0]['Course'];
									$classidstr = '';
									$groupnamestr = '';
									$groupidstr = '';
									$studentnamestr = '';
									$studentidstr = '';
									$author = $eitem[0]['Author'];
									$assigner = $eitem[0]['UserID'];
									$protoid = $eitem[0]['ProtoID'];
									$forumsectionid = $eitem[0]['FSID'];
									$future = 0;
									foreach($eitem as $item){
										$assignids .= $assignids == '' ? $item['Id'] : ','.$item['Id'];
										$future = $item['Future'] ? 1 : 0;
										if($item['GpStuType'] == 'QAGRP'){
											$groupidstr .= $groupidstr == '' ? $item['GpStuID'] : ','.$item['GpStuID'];
										}elseif($item['GpStuType'] == 'QASTU'){
											$studentidstr .= $studentidstr == '' ? $item['GpStuID'] : ','.$item['GpStuID'];
										}else{
											$classidstr .= $classidstr == '' ? $item['ClassID'] : ','.$item['ClassID'];	
										}	
									}
									if($groupidstr != ''){
										$this->sql = "select concat(GroupID, '|', GroupName) from QRY_".$_SESSION['SessionSchoolID']."_Group where GroupID in (".$groupidstr.")";
										$gresult = mysql_query($this->sql);
										if(mysql_errno() > 0){
											continue;	
										}
										while($grow = mysql_fetch_array($gresult)){
											$groupnamestr .= $groupnamestr == '' ? $grow[0] : ','.$grow[0];
										}		
									}
									if($studentidstr != ''){
										$this->sql = "select concat(U.UserID, '|', U.FirstName, ' ', U.LastName, '(', SP.ClassID, ')') from (select UserID, ClassID from TBL_StudentsProfile where UserID in (".$studentidstr.")) as SP, (select UserID, FirstName, LastName from TBL_Users where UserID in (".$studentidstr.")) as U where SP.UserID = U.UserID";
										$sresult = mysql_query($this->sql);
										if(mysql_errno() > 0){
											continue;	
										}
										while($srow = mysql_fetch_array($sresult)){
											$studentnamestr .= $studentnamestr == '' ? $srow[0] : ','.$srow[0];
										}
									}
									$this->events[] = array(
										$assignids,
										$title,
										$this->php2JsTime($this->mySql2PhpTime($startdate)),
										$this->php2JsTime($this->mySql2PhpTime($enddate)),
										$isalldyevent,
										$iscrossevent,	//more than one day event
										0,	//Recurring event
										$color,
										1,	//editable
										'Forum',
										$message,
										$courseid,
										$classidstr,
										$groupnamestr,
										$studentnamestr,
										$author,
										$assigner,
										$protoid,
										$forumsectionid,
										$future
									);	
								}
							}	
						}
						break;	
					case 'MPG':
						$sql = preg_replace('/UserID/', 'AssignedBy as UserID', $_selectSQL2).', AA.Message, A.OGID as ProtoID, A.OGTitle as Subject, AA.ScenarioID, (AA.StartDate > Now()) as Future'." from QRY_".$_SESSION['SessionSchoolID']."_PMP_OG A, QRY_".$_SESSION['SessionSchoolID']."_PMP_OG_Assignment AA, TBL_Users U".preg_replace('/AA.UserID/', 'AA.AssignedBy', preg_replace('/{\w+}/', 'OG', $_whereSQL2));
						if($this->role == "student"){
							$sql .= ' AND AA.StartDate <= Now() AND AA.EndDate >= Now()';
					
						}
					
						$handle = mysql_query($sql);
						if(mysql_errno() > 0){
							break;
						}
						$assignerArr = array();
						while($row = mysql_fetch_array($handle)){
							if(!isset($assignerArr[$row['UserID']])){
								$assignerArr[$row['UserID']] = array();
							}
							if(!isset($assignerArr[$row['UserID']][$row['ProtoID']])){
								$assignerArr[$row['UserID']][$row['ProtoID']] = array();	
							}
							$flag = $row['StartDate']."|".$row['EndDate']."|".$row['IsAllDayEvent']."|".$row['Color']."|".$row['GpStuType']."|".$row['Message'];
							if(!isset($assignerArr[$row['UserID']][$row['ProtoID']][$flag])){
								$assignerArr[$row['UserID']][$row['ProtoID']][$flag] = array();	
							}
							array_push($assignerArr[$row['UserID']][$row['ProtoID']][$flag], $row);
						}
						foreach($assignerArr as $aitem){
							foreach($aitem as $pitem){
								foreach($pitem as $eitem){
									$assignids = '';
									$title = $eitem[0]['Subject'];
									$startdate = $this->ConvertToLocalTime($_SESSION['SessionTimezone'], $eitem[0]['StartDate'], 2);
									$enddate = $this->ConvertToLocalTime($_SESSION['SessionTimezone'], $eitem[0]['EndDate'], 2);
									$isalldyevent = $eitem[0]['IsAllDayEvent'];
									$iscrossevent = $this->isSameDay($startdate, $enddate);
									$color = $eitem[0]['Color'];
									$message = $eitem[0]['Message'];
									$courseid = $eitem[0]['Course'];
									$classidstr = '';
									$groupnamestr = '';
									$groupidstr = '';
									$studentnamestr = '';
									$studentidstr = '';
									$author = $eitem[0]['Author'];
									$assigner = $eitem[0]['UserID'];
									$protoid = $eitem[0]['ProtoID'];
									$scenarioid = $eitem[0]['ScenarioID'];
									$future = 0;
									foreach($eitem as $item){
										$assignids .= $assignids == '' ? $item['Id'] : ','.$item['Id'];
										$future = $item['Future'] ? 1 : 0;
										if($item['GpStuType'] == 'QAGRP'){
											$groupidstr .= $groupidstr == '' ? $item['GpStuID'] : ','.$item['GpStuID'];
										}elseif($item['GpStuType'] == 'QASTU'){
											$studentidstr .= $studentidstr == '' ? $item['GpStuID'] : ','.$item['GpStuID'];
										}else{
											$classidstr .= $classidstr == '' ? $item['ClassID'] : ','.$item['ClassID'];	
										}	
									}
									if($groupidstr != ''){
										$this->sql = "select concat(GroupID, '|', GroupName) from QRY_".$_SESSION['SessionSchoolID']."_Group where GroupID in (".$groupidstr.")";
										$gresult = mysql_query($this->sql);
										if(mysql_errno() > 0){
											continue;	
										}
										while($grow = mysql_fetch_array($gresult)){
											$groupnamestr .= $groupnamestr == '' ? $grow[0] : ','.$grow[0];
										}		
									}
									if($studentidstr != ''){
										$this->sql = "select concat(U.UserID, '|', U.FirstName, ' ', U.LastName, '(', SP.ClassID, ')') from (select UserID, ClassID from TBL_StudentsProfile where UserID in (".$studentidstr.")) as SP, (select UserID, FirstName, LastName from TBL_Users where UserID in (".$studentidstr.")) as U where SP.UserID = U.UserID";
										$sresult = mysql_query($this->sql);
										if(mysql_errno() > 0){
											continue;	
										}
										while($srow = mysql_fetch_array($sresult)){
											$studentnamestr .= $studentnamestr == '' ? $srow[0] : ','.$srow[0];
										}
									}
									$this->events[] = array(
										$assignids,
										$title,
										$this->php2JsTime($this->mySql2PhpTime($startdate)),
										$this->php2JsTime($this->mySql2PhpTime($enddate)),
										$isalldyevent,
										$iscrossevent,	//more than one day event
										0,	//Recurring event
										$color,
										1,	//editable
										'MPG',
										$message,
										$courseid,
										$classidstr,
										$groupnamestr,
										$studentnamestr,
										$author,
										$assigner,
										$protoid,
										$classidstr !== '' ? 1 : 2,
										$scenarioid,
										$future
									);	
								}
							}	
						}
						break;	
				}
			}
			
			$this->sortEventsBU();
			return $this->events;		
		}
		
	};
?>