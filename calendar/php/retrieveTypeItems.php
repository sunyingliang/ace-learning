<?php
	include_once('dbconfig.php');
    include ('../../includes/session.php');
	
	$result = array();
	if(isset($_SESSION['SessionUserID'])){
		if(isset($_POST['classIds']) && isset($_POST['type']) && isset($_POST['courseid'])){
			$classIds = $_POST['classIds'];
			$type = $_POST['type'];		
			$coursearr = array('AM' => 'Additional Mathematics', 'EM' => 'Elementary Mathematics');
			switch($type){
				case 'Class':
					$query = 'select ID, ClassID from QRY_'.$_SESSION['SessionSchoolID'].'_Classes where ClassID in (select distinct FS.ClassID from TBL_FacultySyllabus as FS where FS.UserID in (select FP.UserID from'.
								' TBL_FacultyProfile as FP, TBL_Users as U where FP.SchoolID = "'.$_SESSION['SessionSchoolID'].'" and FP.UserID = U.UserID) and right(FS.SyllabusID, 2) = "'.$_POST['courseid'].
								'") group by ClassID order by ClassID asc';
					$result['type'] = 'Class';
					break;	
				case 'Group':
					$query = 'select group_concat(right(SyllabusID, 2)) as CourseIDs, GM.GroupID, GroupName from QRY_'.$_SESSION['SessionSchoolID'].'_GroupMembers as GM, TBL_StudentsSyllabus as SS,'.
								' QRY_'.$_SESSION['SessionSchoolID'].'_Group as G where	GM.GroupID = G.GroupID and GM.UserID = SS.UserID group by GM.GroupID order by G.GroupName';
					$result['type'] = 'Group';
					break;	
				case 'Student':	
					$cidArr = explode(',', $classIds);
					$classIds = '"'.$cidArr[0].'"';
					for($i = 1; $i < count($cidArr); $i++){
						$classIds .= ', "'.$cidArr[$i].'"';	
					}
					$query = 'select U.UserID, concat(U.FirstName, " ", U.LastName) as StudentName, SP.ClassID from TBL_StudentsProfile SP, TBL_Users U where  SP.SchoolID = "'.$_SESSION['SessionSchoolID'].
							'" and SP.ClassID in ('.$classIds.') and SP.UserID = U.UserID order by SP.ClassID, SP.IndexID';
					$result['type'] = 'Student';
					break;
			}
			
			$db = new DBConnection();
			$db->getConnection();
			$res = mysql_query($query);
			if(mysql_errno() > 0){
				$result['error'] = 'm-0';	
			}elseif(mysql_affected_rows() > 0){
				$result['flag'] = true;
				if($type == "Class"){
					while($myrow = mysql_fetch_array($res)){
						$result[] = $myrow[0].','.$myrow[1];	
					}
				}elseif($type == "Student"){
					$result['multi'] = count(explode(',', $classIds)) > 1 ? true : false;
					while($myrow = mysql_fetch_array($res)){
						$result[] = $myrow[0].','.$myrow[1].','.$myrow[2];	
					}
				}elseif($type == "Group"){
					$num = 0;
					while($myrow = mysql_fetch_array($res)){
						$gpcrsnum = 0;
						$courseidarr = explode(',', $myrow['CourseIDs']);	
						foreach($courseidarr as $value){
							if($value == $_POST['courseid']){
								$gpcrsnum++;	
							}
						}
						$gmquery = 'select count(*) from QRY_'.$_SESSION['SessionSchoolID'].'_GroupMembers where GroupID = '.$myrow['GroupID'];
						$gmres = mysql_query($gmquery);
						if(mysql_errno() > 0){
							continue;	
						}
						$gmrow = mysql_fetch_array($gmres);
						if($gpcrsnum >= $gmrow[0]){
							$num++;
							$result[] = $myrow['GroupID'].','.$myrow['GroupName'];	
						}
					}	
					if($num == 0){
						$result['flag'] = false;
						$result['message'] = "No group is taking '".$coursearr[$_POST['courseid']]."'.";		
					}
				}	
			}elseif(mysql_affected_rows() == 0){
				$result['flag'] = false;
				$result['course'] = $_POST['courseid'];	
				switch($type){
					case "Class":
						$result['exist'] = 1;
						$result['message'] = "No class is taking '".$coursearr[$_POST['courseid']]."'.";		
						$query = 'select count(*) from QRY_'.$_SESSION['SessionSchoolID'].'_Classes';
						$res = mysql_query($query);	
						if(mysql_errno() == 0){
							$irow = mysql_fetch_array($res);	
							$result['exist'] = $irow[0] == 0 ? 0 : 1;
							if($result['exist'] == 0){
								$result['message'] = "No class found.";			
							}						
						}
						break;
					case "Group":
						$result['exist'] = 1;
						$result['message'] = "No group is taking '".$coursearr[$_POST['courseid']]."'.";		
						$query = 'select count(*) from QRY_'.$_SESSION['SessionSchoolID'].'_Group';
						$res = mysql_query($query);	
						if(mysql_errno() == 0){
							$irow = mysql_fetch_array($res);	
							$result['exist'] = $irow[0] == 0 ? 0 : 1;
							if($result['exist'] == 0){
								$result['message'] = "No group found.";			
							}						
						}
						break;
					case "Student":
						$result['message'] = "There are no Student records.";		
						break;
				}
			}
		}else{
			$result['error'] = 'p-0';	
		}
	}else{
		$result['error'] = 's-0';	
	}
	echo json_encode($result);
?>