<?php
include_once("dbconfig.php");
include_once("functions.php");
include_once('../../includes/session.php');
include_once('../../includes/mysql_connect.php');
include_once('event.php');

function addCalendar($st, $et, $sub, $ade){
  $ret = array();
  try{
    $db = new DBConnection();
    $db->getConnection();
    $sql = "insert into `jqcalendar` (`subject`, `starttime`, `endtime`, `isalldayevent`) values ('"
      .mysql_real_escape_string($sub)."', '"
      .php2MySqlTime(js2PhpTime($st))."', '"
      .php2MySqlTime(js2PhpTime($et))."', '"
      .mysql_real_escape_string($ade)."' )";
 
	if(mysql_query($sql)==false){
      $ret['IsSuccess'] = false;
      $ret['Msg'] = mysql_error();
    }else{
      $ret['IsSuccess'] = true;
      $ret['Msg'] = 'add success';
      $ret['Data'] = mysql_insert_id();
    }
	}catch(Exception $e){
     $ret['IsSuccess'] = false;
     $ret['Msg'] = $e->getMessage();
  }
  return $ret;
}


function addDetailedCalendar($event){
  $ret = array();
  try{
    $db = new DBConnection();
    $db->getConnection();
    $em = new EventManager();
	$res = $em->addEvent($event);

	if($res == false){
      $ret['IsSuccess'] = false;
      $ret['Msg'] = 'You have failed to add an event. Please try again or contact ACE-Learning Support.';
    }else{
      $ret['IsSuccess'] = true;
      $ret['Msg'] = 'You have added an event successfully.';
      $ret['Data'] = mysql_insert_id();
    }
  }catch(Exception $e){
     $ret['IsSuccess'] = false;
     $ret['Msg'] = $e->getMessage();
  }
  return $ret;
}

function listCalendarByRange($sd, $ed, $cs, $es, $tids, $cids, $gids, $stustr){
  $ret = array();
  $ret['events'] = array();
  $ret["issort"] = true;
  $ret["start"] = php2JsTime($sd);
  $ret["end"] = php2JsTime($ed);
  $ret['error'] = null;
  try{
    $db = new DBConnection();
    $db->getConnection();
	$em = new EventManager(php2MySqlTime($sd), php2MySqlTime($ed), $cs, $es, $tids, $cids, $gids, $stustr);  
	$ret['events'] = $em->retrieveEvents();
  }catch(Exception $e){
     $ret['error'] = $e->getMessage();
  }
  return $ret;
}

function listCalendar($day, $type, $courseStr = 'All', $eventStr = 'All', $teacherids = 'Self', $classids = '', $groupids = '', $studentstr = ''){
  $phpTime = js2PhpTime($day);
  switch($type){
    case "year":
      $st = mktime(0, 0, 0, date("m", $phpTime), 1, date("Y", $phpTime));
      $et = mktime(0, 0, -1, date("m", $phpTime), 2, date("Y", $phpTime));
      break;
    case "month":
	  $startday = 1;
	  $firstday = date("w", $phpTime) - 1;
	  $diffday = $startday - $firstday;
	  if($diffday > 0){
		  $diffday -= 7;
	  }
	  $st = mktime(0, 0, 0, date("m", $phpTime), 1, date("Y", $phpTime)) + $diffday * 24 * 60 * 60;
	  $et = mktime(0, 0, 0, date("m", $phpTime), 1, date("Y", $phpTime)) + $diffday * 24 * 60 * 60 + 34 * 24 * 60 * 60;
	  if(date('Y', $st) == date('Y', $et) && date('n', $st) == date('n', $et) && date('d', $et) < date('t', $st)){
		  $et += 7 * 24 * 60 * 60;	  
	  }
      break;
    case "week":
      $monday  =  date("d", $phpTime) - date('N', $phpTime) + 1;
      $st = mktime(0,0,0,date("m", $phpTime), $monday, date("Y", $phpTime));
      $et = mktime(0,0,-1,date("m", $phpTime), $monday+7, date("Y", $phpTime));
      break;
    case "day":
      $st = mktime(0, 0, 0, date("m", $phpTime), date("d", $phpTime), date("Y", $phpTime));
      $et = mktime(0, 0, -1, date("m", $phpTime), date("d", $phpTime)+1, date("Y", $phpTime));
      break;
  }
  return listCalendarByRange($st, $et, $courseStr, $eventStr, $teacherids, $classids, $groupids, $studentstr, $role);
}

function updateCalendar($type, $idStr, $st, $et){
  $ret = array();
  try{
    $db = new DBConnection();
    $db->getConnection();
    $em = new EventManager(php2MySqlTime(js2PhpTime($st)), php2MySqlTime(js2PhpTime($et)));
    $res = $em->updateEvents($type, $idStr);

	if($res == false){
      $ret['IsSuccess'] = false;
      $ret['Msg'] = 'You have failed to update an event. Please try again or contact ACE-Learning Support.';
    }else{
      $ret['IsSuccess'] = true;
      $ret['Msg'] = 'You have updated an event successfully.';
    }
	}catch(Exception $e){
     $ret['IsSuccess'] = false;
     $ret['Msg'] = $e->getMessage();
  }
  return $ret;
}

function updateDetailedCalendar($ids, $otype, $event){
  $ret = array();
  try{
    $db = new DBConnection();
    $db->getConnection();
    $em = new EventManager();
	$res = $em->updateEventDetails($ids, $otype, $event);
	if($res == false){
      $ret['IsSuccess'] = false;
      $ret['Msg'] = 'You have failed to update an event. Please try again or contact ACE-Learning Support.';
    }else{
      $ret['IsSuccess'] = true;
      $ret['Msg'] = 'You have updated an event successfully.';
    }
  }catch(Exception $e){
     $ret['IsSuccess'] = false;
     $ret['Msg'] = $e->getMessage();
  }
  return $ret;
}

function removeCalendar($type, $calid, $servpath){
  $ret = array();
  try{
    $db = new DBConnection();
    $db->getConnection();
	$em = new EventManager();
	$res = $em->removeEvents($type, $calid, $servpath);
	if($res == false){
      $ret['IsSuccess'] = false;
      $ret['Msg'] = 'You have failed to delete an event. Please try again or contact ACE-Learning Support.';
    }else{
      $ret['IsSuccess'] = true;
      $ret['Msg'] = 'You have deleted an event successfully.';
    }
	}catch(Exception $e){
     $ret['IsSuccess'] = false;
     $ret['Msg'] = $e->getMessage();
  }
  return $ret;
}

function formatDate($date){
	$pattern = '@(\d+)/(\d+)/(\d+)@';	
	return preg_replace($pattern, '${2}/${1}/${3}', $date);
}

$hder = fopen('event.txt', 'a+');
if($hder){
	if(chmod('event.txt', 0777)){
		fwrite($hder, 'entered here..');
	}
}
fclose($hder);


$method = $_GET["method"];
switch ($method) {
    case "add":
        $ret = addCalendar($_POST["CalendarStartTime"], $_POST["CalendarEndTime"], $_POST["CalendarTitle"], $_POST["IsAllDayEvent"]);
        break;
    case "list":
		if(isset($_POST["courseStr"])){
	        $ret = listCalendar($_POST["showdate"], $_POST["viewtype"], $_POST["courseStr"], $_POST['eventStr'], $_POST['teachers'], $_POST["classes"], $_POST["groups"], $_POST['students']);			
		}else{
		    $ret = listCalendar($_POST["showdate"], $_POST["viewtype"]);	
		}

        break;
    case "update":
        $ret = updateCalendar($_POST['type'], $_POST["calendarId"], $_POST["CalendarStartTime"], $_POST["CalendarEndTime"]);
        break; 
    case "remove":
		$servPath = '';
		if ($_SESSION['SessionUserTypeID'] == 2 || $_SESSION['SessionUserTypeID'] == 3){
			$servPath = "../../resources/schools/".$_SESSION['SessionSchoolID']."/".$_SESSION['SessionUserID'];
			$real_path = "/$SystemFolder/resources/schools/".$_SESSION['SessionSchoolID']."/".$_SESSION['SessionUserID'];
		}else{
			$servPath = "../../resources/ace/";
			$real_path = "/$SystemFolder/resources/ace";
		}
        $ret = removeCalendar($_POST["type"], $_POST['calendarId'], $servPath);
        break;
    case "adddetails":		
		$isallday = isset($_POST["IsAllDayEvent"]) ? 1 : 0;
		if($isallday === 1){
			$st = formatDate($_POST["stpartdate"] > $_POST["etpartdate"] ? $_POST["etpartdate"] : $_POST["stpartdate"]) . " 00:00";
			$et = formatDate($_POST["stpartdate"] > $_POST["etpartdate"] ? $_POST["stpartdate"] : $_POST["etpartdate"]) . " 23:59";	
		}else{
			$st = formatDate($_POST["stpartdate"]) . " " . $_POST["stparttime"];
	        $et = formatDate($_POST["etpartdate"]) . " " . $_POST["etparttime"];	
		}
		if($st > $et){
			$tmp = $st;
			$st = $et;
			$et = $tmp;	
		}
		$etype = $_POST['event'];
		$course = $_POST['course'];
		$title = $_POST['title'];
		$color = $_POST['colorvalue'];
		if($color == -1){
			switch($etype){
				case 'Announcement':
					$color = 22;
					break;
				case 'Links':
					$color = 23;
					break;
				case 'Resources':
					$color = 24;
					break;
				case 'RL':
					$color = 25;
					break;
			}
		}
		$message = $_POST['Message'];
		$isallday = isset($_POST["IsAllDayEvent"])?1:0;
		$aclass = isset($_POST['Class']) ? $_POST['Class'] : array();
		$agroup = isset($_POST['Group']) ? $_POST['Group'] : array();
		$astudent = isset($_POST['Student']) ? $_POST['Student'] : array();
		$url = $_POST['Url'];
		if(strpos($url, 'http://') === false && strpos($url, 'https://') === false){
			$url = 'http://'.$url;	
		}
		$editmode = isset($_POST['edmode']) ? 1 : 0;
		$newflag = $_POST['newFile'];
		$files = $_POST['uploadfiles'];
		$servPath = '';
		if ($_SESSION['SessionUserTypeID'] == 2 || $_SESSION['SessionUserTypeID'] == 3){
			$servPath = "../../resources/schools/".$_SESSION['SessionSchoolID']."/".$_SESSION['SessionUserID'];
			$real_path = "/$SystemFolder/resources/schools/".$_SESSION['SessionSchoolID']."/".$_SESSION['SessionUserID'];
		}else{
			$servPath = "../../resources/ace/";
			$real_path = "/$SystemFolder/resources/ace";
		}
		$documentRoot = $_SERVER['DOCUMENT_ROOT'];
		$temp_path = "/$SystemFolder/files";
		$tempFolder = $documentRoot.$temp_path;
		$realFolder = $documentRoot.$real_path;
		$topicCodeStr = $_POST['TopicCodeArr'];
		$event = '';
		
		switch($etype){
			case 'Announcement':
				$event = new Announcement(ConvertToServerTime($_SESSION['SessionTimezone'], php2MySqlTime(js2PhpTime($st)), 3), ConvertToServerTime($_SESSION['SessionTimezone'], php2MySqlTime(js2PhpTime($et)), 3), $etype, $course, $title, $color, $message, $isallday, $aclass, $agroup, $astudent); 
				break;
			case 'Links':
				$event = new Links(ConvertToServerTime($_SESSION['SessionTimezone'], php2MySqlTime(js2PhpTime($st)), 3), ConvertToServerTime($_SESSION['SessionTimezone'], php2MySqlTime(js2PhpTime($et)), 3), $etype, $course, $title, $color, $message, $isallday, $aclass, $agroup, $astudent, $url);
				break;	
			case 'Resources':
				$event = new Resources(ConvertToServerTime($_SESSION['SessionTimezone'], php2MySqlTime(js2PhpTime($st)), 3),ConvertToServerTime($_SESSION['SessionTimezone'], php2MySqlTime(js2PhpTime($et)), 3), $etype, $course, $title, $color, $message, $isallday, $aclass, $agroup, $astudent, $servPath, $tempFolder, $realFolder, $files, $newflag, $editmode);
				$event->init();
				break;
			case 'RL':
				$event = new RL(ConvertToServerTime($_SESSION['SessionTimezone'], php2MySqlTime(js2PhpTime($st)), 3),ConvertToServerTime($_SESSION['SessionTimezone'], php2MySqlTime(js2PhpTime($et)), 3), $etype, $course, $title, $color, $message, $isallday, $aclass, $agroup, $astudent, $topicCodeStr);
				break;	
		} 
		
		$hder = fopen('event.txt', 'a+');
		if($hder){
			if(chmod('event.txt', 0777)){
				fwrite($hder, var_export($event, true));
			}
		}
		fclose($hder);
		
        if(isset($_GET["ids"])){
			$ids = $_GET["ids"];
			$orgtype = $_POST['edtype'];
			$ret = updateDetailedCalendar($ids, $orgtype, $event);
        }else{
            $ret = addDetailedCalendar($event);
        }        
        break; 


}
echo json_encode($ret); 
?>