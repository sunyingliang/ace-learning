<?php
	include_once('dbconfig.php');
    include ('../../includes/session.php');
	
	$result = array();
	if(isset($_SESSION['SessionUserID'])){
		if(isset($_POST['classIds']) && isset($_POST['type'])){
			$classIds = $_POST['classIds'];
			$type = $_POST['type'];		
			
			switch($type){
				case 'Class':
					$query = 'select ID, ClassID from QRY_'.$_SESSION['SessionSchoolID'].'_Classes group by ClassID order by ClassID asc';
					$result['type'] = 'Class';
					break;	
				case 'Group':
					$query = 'select GroupID, GroupName from QRY_'.$_SESSION['SessionSchoolID'].'_Group order by GroupName';
					$result['type'] = 'Group';
					break;	
				case 'Student':	
					$cidArr = explode(',', $classIds);
					$classIds = '"'.$cidArr[0].'"';
					for($i = 1; $i < count($cidArr); $i++){
						$classIds .= ', "'.$cidArr[$i].'"';	
					}
					$query = 'select U.UserID, concat(U.FirstName, " ", U.LastName) as StudentName, SP.ClassID from TBL_StudentsProfile SP, TBL_Users U where  SP.SchoolID = "'.$_SESSION['SessionSchoolID'].
							'" and SP.ClassID in ('.$classIds.') and SP.UserID = U.UserID order by SP.ClassID, SP.IndexID';
					$result['type'] = 'Student';
					break;
			}
			
			$db = new DBConnection();
			$db->getConnection();
			$res = mysql_query($query);
			if(mysql_errno() > 0){
				$result['error'] = 'm-0';	
			}elseif(mysql_affected_rows() > 0){
				$result['success'] = true;
				if($result['type'] == 'Student'){
					$result['multi'] = count(explode(',', $classIds)) > 1 ? true : false;
					while($myrow = mysql_fetch_array($res)){
						$result[] = $myrow[0].','.$myrow[1].','.$myrow[2];	
					}					
				}else{
					while($myrow = mysql_fetch_array($res)){
						$result[] = $myrow[0].','.$myrow[1];	
					}	
				}	
			}
		}else{
			$result['error'] = 'p-0';	
		}
	}else{
		$result['error'] = 's-0';	
	}
	echo json_encode($result);
?>