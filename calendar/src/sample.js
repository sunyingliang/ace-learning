$(document).ready(function() {     
   var view="week";          
   
	var DATA_FEED_URL = "php/datafeed.php";
	var op = {
		view: view,
		theme:3,
		showday: new Date(),
		EditCmdhandler:Edit,
		DeleteCmdhandler:Delete,
		ViewCmdhandler:View,    
		onWeekOrMonthToDay:wtd,
		onBeforeRequestData: cal_beforerequest,
		onAfterRequestData: cal_afterrequest,
		onRequestDataError: cal_onerror, 
		autoload:true,
		url: DATA_FEED_URL + "?method=list",  
		quickAddUrl: DATA_FEED_URL + "?method=add", 
		quickUpdateUrl: DATA_FEED_URL + "?method=update",
		quickDeleteUrl: DATA_FEED_URL + "?method=remove"        
	};
	
	var $dv = $("#calhead");
	var _MH = document.documentElement.clientHeight;
	var dvH = $dv.height() + 2;
	op.height = _MH - dvH - 60;
	op.eventItems =[];

	var p = $("#gridcontainer").bcalendar(op).BcalGetOp();
	if (p && p.datestrshow) {
		$("#txtdatetimeshow").text(p.datestrshow);
	}
	$("#caltoolbar").noSelect();
	
	$("#hdtxtshow").datepicker({ picker: "#txtdatetimeshow", showtarget: $("#txtdatetimeshow"),
	onReturn:function(r){  
					$("#caltoolbar div.fcurrent").each(function() {
						$(this).removeClass("fcurrent");
					});
					$("#showdaybtn").addClass("fcurrent");                        
					var _param = $('#userType').val().split('|')[0] == 2 ? [] : settings;
					var p = $("#gridcontainer").gotoDate(_param, r).BcalGetOp();
					if (p && p.datestrshow) {
						$("#txtdatetimeshow").text(p.datestrshow);
					}
			 } 
	});
	function cal_beforerequest(type)
	{
		var t="Loading data...";
		switch(type)
		{
			case 1:
				t="Loading data...";
				break;
			case 2:                      
			case 3:  
			case 4:    
				t="The request is being processed ...";                                   
				break;
		}
		$("#errorpannel").hide();
		$("#loadingpannel").html(t).show();    
	}
	function cal_afterrequest(type)
	{
		switch(type)
		{
			case 1:
				$("#loadingpannel").hide();
				break;
			case 2:
			case 3:
			case 4:
				$("#loadingpannel").html("Success!");
				window.setTimeout(function(){ $("#loadingpannel").hide();},2000);
			break;
		}              
	   
	}
	function cal_onerror(type,data)
	{
		$("#errorpannel").show();
	}
	function Edit(data)
	{
		if(data)
		{
			var eurl="dialogs/eventForm.php?", _params, startdate = new Date(data[2]), enddate = new Date(data[3]),
				start = (startdate.getDate() < 10 ? '0' + startdate.getDate() : startdate.getDate()) + '/' + (startdate.getMonth() < 9 ? '0' + (startdate.getMonth() + 1) : (startdate.getMonth() + 1)) + '/' + startdate.getFullYear() + ' ' + (startdate.getHours() < 10 ? '0' + startdate.getHours() : startdate.getHours()) + ':' + (startdate.getMinutes() < 10 ? '0' + startdate.getMinutes() : startdate.getMinutes()),
				end = (enddate.getDate() < 10 ? '0' + enddate.getDate() : enddate.getDate()) + '/' + (enddate.getMonth() < 9 ? '0' + (enddate.getMonth() + 1) : (enddate.getMonth() + 1)) + '/' + enddate.getFullYear() + ' ' + (enddate.getHours() < 10 ? '0' + enddate.getHours() : enddate.getHours()) + ':' + (enddate.getMinutes() < 10 ? '0' + enddate.getMinutes() : enddate.getMinutes());   

			switch(data[9]){
				case 'Announcement':
					_params = 'type={9}&course={11}&title={1}&color={7}&message={10}&isallday={4}&classes={12}&groups={13}&students={14}&assignids={0}&start=' + start + '&end=' + end;
					eurl += _params;
					break;
				case 'Links':
					_params = 'type={9}&course={11}&title={1}&color={7}&url={17}&message={10}&isallday={4}&classes={12}&groups={13}&students={14}&assignids={0}&start=' + start + '&end=' + end;
					eurl += _params;
					break;
				case 'RL':
					_params = 'type={9}&course={11}&title={1}&color={7}&topicstr={17}&topicids={18}&message={10}&isallday={4}&classes={12}&groups={13}&students={14}&assignids={0}&start=' + start + '&end=' + end;
					eurl += _params;
					break;
				case 'Resources':
					_params = 'type={9}&course={11}&title={1}&color={7}&schoolid={17}&userid={16}&filename={18}&protoid={21}&originalfilename={19}&filesize={20}&' + 
								'message={10}&isallday={4}&classes={12}&groups={13}&students={14}&assignids={0}&start=' + start + '&end=' + end;
					eurl += _params;
					break;		
			}
			var url = StrFormat(eurl,data);
			OpenModelWindow(url,{ width: 816, height: 600, caption:"Edit Event",onclose:freshPage});
		}
	}    
	function View(data)
	{
		var str = "";
		$.each(data, function(i, item){
			str += "[" + i + "]: " + item + "\n";
		});
		alert(str);               
	}    
	function Delete(data,callback)
	{           
		$.alerts.okButton="Confirm";  
		$.alerts.cancelButton="Cancel";  
		hiConfirm("Are you sure you want to proceed?", 'Confirm',function(r){ r && callback(data);});           
	}
	function wtd(p)
	{
	   if (p && p.datestrshow) {
			$("#txtdatetimeshow").text(p.datestrshow);
		}
		$("#caltoolbar div.fcurrent").each(function() {
			$(this).removeClass("fcurrent");
		});
		$("#showdaybtn").addClass("fcurrent");
	}
	$("#showdaybtn").click(function(e) {
		$("#caltoolbar div.fcurrent").each(function() {
			$(this).removeClass("fcurrent");
		});
		$(this).addClass("fcurrent");
		var _param = $('#userType').val().split('|')[0] == 2 ? [] : settings;
		var p = $("#gridcontainer").swtichView("day", _param).BcalGetOp();
		if (p && p.datestrshow) {
			$("#txtdatetimeshow").text(p.datestrshow);
		}
	});
	$("#showweekbtn").click(function(e) {
		$("#caltoolbar div.fcurrent").each(function() {
			$(this).removeClass("fcurrent");
		});
		$(this).addClass("fcurrent");
		var _param = $('#userType').val().split('|')[0] == 2 ? [] : settings;
		var p = $("#gridcontainer").swtichView("week", _param).BcalGetOp();
		if (p && p.datestrshow) {
			$("#txtdatetimeshow").text(p.datestrshow);
		}

	});
	$("#showmonthbtn").click(function(e) {
		$("#caltoolbar div.fcurrent").each(function() {
			$(this).removeClass("fcurrent");
		});
		$(this).addClass("fcurrent");
		var _param = $('#userType').val().split('|')[0] == 2 ? [] : settings;
		var p = $("#gridcontainer").swtichView("month", _param).BcalGetOp();
		if (p && p.datestrshow) {
			$("#txtdatetimeshow").text(p.datestrshow);
		}
	});
	$("#showyearbtn").click(function(e) {
		$("#caltoolbar div.fcurrent").each(function() {
			$(this).removeClass("fcurrent");
		});
		$(this).addClass("fcurrent");
		var _param = $('#userType').val().split('|')[0] == 2 ? [] : settings;
		var p = $("#gridcontainer").swtichView("year", _param).BcalGetOp();
		if (p && p.datestrshow) {
			$("#txtdatetimeshow").text(p.datestrshow);
		}
	});
	$("#showreflashbtn").click(function(e){
		var _param = $('#userType').val().split('|')[0] == 2 ? [] : settings;
		$("#gridcontainer").reload(_param);
	});
	
	$("#faddbtn").click(function(e) {
		var url ="dialogs/eventForm.php";
		OpenModelWindow(url,{ width: i18n.xgcalendar.dialog.width, height: i18n.xgcalendar.dialog.height, caption: "New Event", onclose: freshPage});
		resetBcolor();
		$(".bbit-window-header .bbit-window-header-text").addClass("bbit-window-header-add-text");
	});
	$("#showtodaybtn").click(function(e) {
		$("#caltoolbar div.fcurrent").each(function() {
			$(this).removeClass("fcurrent");
		});
		$(this).addClass("fcurrent");
		var _param = $('#userType').val().split('|')[0] == 2 ? [] : settings;
		var p = $("#gridcontainer").gotoDate(_param).BcalGetOp();
		if (p && p.datestrshow) {
			$("#txtdatetimeshow").text(p.datestrshow);
		}


	});
	$("#sfprevbtn").click(function(e) {
		var _param = $('#userType').val().split('|')[0] == 2 ? [] : settings, p = $("#gridcontainer").previousRange(_param).BcalGetOp();
		if (p && p.datestrshow) {
			$("#txtdatetimeshow").text(p.datestrshow);
		}

	});
	$("#sfnextbtn").click(function(e) {
		var _param = $('#userType').val().split('|')[0] == 2 ? [] : settings, p = $("#gridcontainer").nextRange(_param).BcalGetOp();
		if (p && p.datestrshow) {
			$("#txtdatetimeshow").text(p.datestrshow);
		}
	});
	$("#fsettingbtn").click(function(e) {
		var url ="dialogs/settings.php";
		if(settings.length > 0){
			var _param = '?' + settings[0]['name'] + '=' + settings[0]['value'];	
			for(var i = 1; i < settings.length; i++){
				_param += '&' + settings[i]['name'] + '=' + settings[i]['value'];	
			}
			url += _param;	
		}
		OpenModelWindow(url,{ width: i18n.xgcalendar.dialog.width, height: i18n.xgcalendar.dialog.height, caption: "Settings", onclose: freshPage});
		resetBcolor();
		$(".bbit-window-header .bbit-window-header-text").addClass("bbit-window-header-set-text");
	});
	$("#colorlegend, #clcontent").mouseover(function(e) {
		$('#clcontent').css('display', 'block');
	}).mouseout(function(e){
		$('#clcontent').css('display', 'none');	
	});
	$("#showyearbtn").hide();
	$("#showyearbtn").next().hide();
	$.ajax({
		type: 'GET',
		url: 'php/userType.php',
		async: false,
		global: false,
		cache: false,
		success: function(data, status){
			var _arr = data.split('|');
			if(status && _arr[0] == 2){
				$('#fsettingbtn').next().remove();
				$('#fsettingbtn').remove();
				$('#faddbtn').next().remove();
				$('#faddbtn').remove();
			}	
			$('#userType').val(data);			
		}
	});
});
var settings = [];
function freshPage(){
	var _param = $('#userType').val().split('|')[0] == 2 ? [] : settings;
	$("#gridcontainer").reload(_param);
}
function initSettings(paramArr){
	settings = paramArr;
}
function refreshWindow(){
	window.location.reload();
}
function resetBcolor(){
	$(".bbit-window").prev().css({'background-color':'#000000', opacity:0.7});	
}