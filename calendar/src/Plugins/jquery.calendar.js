/**
  * @description {Class} wdCalendar
  * This is the main class of wdCalendar.
  */
; (function($) {
    var __WDAY = new Array(i18n.xgcalendar.dateformat.sun, i18n.xgcalendar.dateformat.mon, i18n.xgcalendar.dateformat.tue, i18n.xgcalendar.dateformat.wed, i18n.xgcalendar.dateformat.thu, i18n.xgcalendar.dateformat.fri, i18n.xgcalendar.dateformat.sat);
    var __MonthName = new Array(i18n.xgcalendar.dateformat.jan, i18n.xgcalendar.dateformat.feb, i18n.xgcalendar.dateformat.mar, i18n.xgcalendar.dateformat.apr, i18n.xgcalendar.dateformat.may, i18n.xgcalendar.dateformat.jun, i18n.xgcalendar.dateformat.jul, i18n.xgcalendar.dateformat.aug, i18n.xgcalendar.dateformat.sep, i18n.xgcalendar.dateformat.oct, i18n.xgcalendar.dateformat.nov, i18n.xgcalendar.dateformat.dec);
    if (!Clone || typeof (Clone) != "function") {
        var Clone = function(obj) {
            var objClone = new Object();
            if (obj.constructor == Object) {
                objClone = new obj.constructor();
            } else {
                objClone = new obj.constructor(obj.valueOf());
            }
            for (var key in obj) {
                if (objClone[key] != obj[key]) {
                    if (typeof (obj[key]) == 'object') {
                        objClone[key] = Clone(obj[key]);
                    } else {
                        objClone[key] = obj[key];
                    }
                }
            }
            objClone.toString = obj.toString;
            objClone.valueOf = obj.valueOf;
            return objClone;
        }
    }
    if (!dateFormat || typeof (dateFormat) != "function") {
        var dateFormat = function(format) {
            var o = {
                "M+": this.getMonth() + 1,
                "d+": this.getDate(),
                "h+": this.getHours(),
                "H+": this.getHours(),
                "m+": this.getMinutes(),
                "s+": this.getSeconds(),
                "q+": Math.floor((this.getMonth() + 3) / 3),
                "w": "0123456".indexOf(this.getDay()),
                "W": __WDAY[this.getDay()],
                "L": __MonthName[this.getMonth()] //non-standard
            };
            if (/(y+)/.test(format)) {
                format = format.replace(RegExp.$1, (this.getFullYear() + "").substr(4 - RegExp.$1.length));
            }
            for (var k in o) {
                if (new RegExp("(" + k + ")").test(format))
                    format = format.replace(RegExp.$1, RegExp.$1.length == 1 ? o[k] : ("00" + o[k]).substr(("" + o[k]).length));
            }
            return format;
        };
    }
    if (!DateAdd || typeof (DateAdd) != "function") {
        var DateAdd = function(interval, number, idate) {
            number = parseInt(number);
            var date;
            if (typeof (idate) == "string") {
                date = idate.split(/\D/);
                eval("var date = new Date(" + date.join(",") + ")");
            }

            if (typeof (idate) == "object") {
                date = new Date(idate.toString());
            }
            switch (interval) {
                case "y": date.setFullYear(date.getFullYear() + number); break;
                case "m": date.setMonth(date.getMonth() + number); break;
                case "d": date.setDate(date.getDate() + number); break;
                case "w": date.setDate(date.getDate() + 7 * number); break;
                case "h": date.setHours(date.getHours() + number); break;
                case "n": date.setMinutes(date.getMinutes() + number); break;
                case "s": date.setSeconds(date.getSeconds() + number); break;
                case "l": date.setMilliseconds(date.getMilliseconds() + number); break;
            }
            return date;
        }
    }
    if (!DateDiff || typeof (DateDiff) != "function") {
        var DateDiff = function(interval, d1, d2) {
            switch (interval) {
                case "d": //date
                case "w":
                    d1 = new Date(d1.getFullYear(), d1.getMonth(), d1.getDate());
                    d2 = new Date(d2.getFullYear(), d2.getMonth(), d2.getDate());
                    break;  //w
                case "h":
                    d1 = new Date(d1.getFullYear(), d1.getMonth(), d1.getDate(), d1.getHours());
                    d2 = new Date(d2.getFullYear(), d2.getMonth(), d2.getDate(), d2.getHours());
                    break; //h
                case "n":
                    d1 = new Date(d1.getFullYear(), d1.getMonth(), d1.getDate(), d1.getHours(), d1.getMinutes());
                    d2 = new Date(d2.getFullYear(), d2.getMonth(), d2.getDate(), d2.getHours(), d2.getMinutes());
                    break;
                case "s":
                    d1 = new Date(d1.getFullYear(), d1.getMonth(), d1.getDate(), d1.getHours(), d1.getMinutes(), d1.getSeconds());
                    d2 = new Date(d2.getFullYear(), d2.getMonth(), d2.getDate(), d2.getHours(), d2.getMinutes(), d2.getSeconds());
                    break;
            }
            var t1 = d1.getTime(), t2 = d2.getTime();
            var diff = NaN;
            switch (interval) {
                case "y": diff = d2.getFullYear() - d1.getFullYear(); break; //y
                case "m": diff = (d2.getFullYear() - d1.getFullYear()) * 12 + d2.getMonth() - d1.getMonth(); break;    //m
                case "d": diff = Math.floor(t2 / 86400000) - Math.floor(t1 / 86400000); break;
                case "w": diff = Math.floor((t2 + 345600000) / (604800000)) - Math.floor((t1 + 345600000) / (604800000)); break; //w
                case "h": diff = Math.floor(t2 / 3600000) - Math.floor(t1 / 3600000); break; //h
                case "n": diff = Math.floor(t2 / 60000) - Math.floor(t1 / 60000); break; //
                case "s": diff = Math.floor(t2 / 1000) - Math.floor(t1 / 1000); break; //s
                case "l": diff = t2 - t1; break;
            }
            return diff;

        }
    }
    if ($.fn.noSelect == undefined) {
        $.fn.noSelect = function(p) { //no select plugin by me :-)
            if (p == null)
                prevent = true;
            else
                prevent = p;
            if (prevent) {
                return this.each(function() {
                    if ($.browser.msie || $.browser.safari) $(this).bind('selectstart', function() { return false; });
                    else if ($.browser.mozilla) {
                        $(this).css('MozUserSelect', 'none');
                        $('body').trigger('focus');
                    }
                    else if ($.browser.opera) $(this).bind('mousedown', function() { return false; });
                    else $(this).attr('unselectable', 'on');
                });

            } else {
                return this.each(function() {
                    if ($.browser.msie || $.browser.safari) $(this).unbind('selectstart');
                    else if ($.browser.mozilla) $(this).css('MozUserSelect', 'inherit');
                    else if ($.browser.opera) $(this).unbind('mousedown');
                    else $(this).removeAttr('unselectable', 'on');
                });

            }
        }; //end noSelect
    }
    $.fn.bcalendar = function(option) {
        var def = {
            /**
             * @description {Config} view  
             * {String} Three calendar view provided, 'day','week','month'. 'week' by default.
             */
            view: "week", 
            /**
             * @description {Config} weekstartday  
             * {Number} First day of week 0 for Sun, 1 for Mon, 2 for Tue.
             */
            weekstartday: 1,  //start from Monday by default
            theme: 0, //theme no
            /**
             * @description {Config} height  
             * {Number} Calendar height, false for page height by default.
             */
            height: false, 
            /**
             * @description {Config} url  
             * {String} Url to request calendar data.
             */
            url: "", 
            /**
             * @description {Config} eventItems  
             * {Array} event items for initialization.
             */   
            eventItems: [], 
            method: "POST", 
            /**
             * @description {Config} showday  
             * {Date} Current date. today by default.
             */
            showday: new Date(), 
            /**
	 	         * @description {Event} onBeforeRequestData:function(stage)
	 	         * Fired before any ajax request is sent.
	 	         * @param {Number} stage. 1 for retrieving events, 2 - adding event, 3 - removiing event, 4 - update event.
	           */
            onBeforeRequestData: false, 
            /**
	 	         * @description {Event} onAfterRequestData:function(stage)
	 	         * Fired before any ajax request is finished.
	 	         * @param {Number} stage. 1 for retrieving events, 2 - adding event, 3 - removiing event, 4 - update event.
	           */
            onAfterRequestData: false, 
            /**
	 	         * @description {Event} onAfterRequestData:function(stage)
	 	         * Fired when some errors occur while any ajax request is finished.
	 	         * @param {Number} stage. 1 for retrieving events, 2 - adding event, 3 - removiing event, 4 - update event.
	           */
            onRequestDataError: false,              
            
            onWeekOrMonthToDay: false, 
            /**
	 	         * @description {Event} quickAddHandler:function(calendar, param )
	 	         * Fired when user quick adds an item. If this function is set, ajax request to quickAddUrl will abort. 
	 	         * @param {Object} calendar Calendar object.
	 	         * @param {Array} param Format [{name:"name1", value:"value1"}, ...]
	 	         * 	 	         
	           */
            quickAddHandler: false, 
            /**
             * @description {Config} quickAddUrl  
             * {String} Url for quick adding. 
             */
            quickAddUrl: "", 
            /**
             * @description {Config} quickUpdateUrl  
             * {String} Url for time span update.
             */
            quickUpdateUrl: "", 
            /**
             * @description {Config} quickDeleteUrl  
             * {String} Url for removing an event.
             */
            quickDeleteUrl: "", 
            /**
             * @description {Config} autoload  
             * {Boolean} If event items is empty, and this param is set to true. 
             * Event will be retrieved by ajax call right after calendar is initialized.
             */  
            autoload: false,
            /**
             * @description {Config} readonly  
             * {Boolean} Indicate calendar is readonly or editable 
             */
            readonly: false, 
            /**
             * @description {Config} extParam  
             * {Array} Extra params submitted to server. 
             * Sample - [{name:"param1", value:"value1"}, {name:"param2", value:"value2"}]
             */
            extParam: [], 
            /**
             * @description {Config} enableDrag  
             * {Boolean} Whether end user can drag event item by mouse. 
             */
            enableDrag: true, 
            loadDateR: [],
			/**
			* @description {settings} indicate how to show the event.
			 **/ 
			settings: [] 
        };
        var eventDiv = $("#gridEvent");
        if (eventDiv.length == 0) {
            eventDiv = $("<div id='gridEvent' style='display:none;'></div>").appendTo(document.body);
        }
        var gridcontainer = $(this);
        option = $.extend(def, option);
        //no quickUpdateUrl, dragging disabled.
        if (option.quickUpdateUrl == null || option.quickUpdateUrl == "") {
            option.enableDrag = false;
        }
        //template for month and date
		var __SCOLLEVENTTEMP = "<DIV style=\"WIDTH:${width};top:${top};left:${left};\" class=\"chip chip${i} ${drag}\"><div class=\"dhdV\" style=\"display:none\">${data}</div><DIV style=\"BORDER-BOTTOM-COLOR:${bgcolor1}\" class=ct>&nbsp;</DIV><DL style=\"BORDER-BOTTOM-COLOR:${bgcolor1}; BACKGROUND-COLOR:${bgcolor2}; BORDER-TOP-COLOR: ${bgcolor1}; HEIGHT: ${height}px; BORDER-RIGHT-COLOR:${bgcolor1}; BORDER-LEFT-COLOR:${bgcolor1}\"><DT style=\"BACKGROUND-COLOR:${bgcolor1}\">${starttime} - ${endtime} ${icon}</DT><DD style=\"BORDER-TOP:1px solid ${bgcolor1};\"><SPAN>${type}&nbsp;&nbsp;(${course})</SPAN></DD><DD style=\"BORDER-TOP:1px solid ${bgcolor1};\"><SPAN>${content}</SPAN></DD><DD style=\"BORDER-TOP:1px solid ${bgcolor1};\"><SPAN>By:&nbsp;${author}</SPAN></DD>${classes}<DIV class='resizer' style='display:${redisplay}'><DIV class=rszr_icon>&nbsp;</DIV></DIV></DL><DIV style=\"BORDER-BOTTOM-COLOR:${bgcolor1}; BACKGROUND-COLOR:${bgcolor1}; BORDER-TOP-COLOR: ${bgcolor1}; BORDER-RIGHT-COLOR: ${bgcolor1}; BORDER-LEFT-COLOR:${bgcolor1}\" class=cb1>&nbsp;</DIV><DIV style=\"BORDER-BOTTOM-COLOR:${bgcolor1}; BORDER-TOP-COLOR:${bgcolor1}; BORDER-RIGHT-COLOR:${bgcolor1}; BORDER-LEFT-COLOR:${bgcolor1}\" class=cb2>&nbsp;</DIV></DIV>";
		var __ALLDAYEVENTTEMP = '<div class="rb-o ${eclass}" id="${id}" style="color:${bgcolor1};"><div class="dhdV" style="display:none">${data}</div><div class="${extendClass} rb-m" style="background-color:${color}">${extendHTML}<div class="rb-i">${content}</div></div></div>';
        var __MonthDays = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];
        var __LASSOTEMP = "<div class='drag-lasso' style='left:${left}px;top:${top}px;width:${width}px;height:${height}px;'>&nbsp;</div>";
		//template for details view
		var __ANNOUNCEMENTVIEW = '<div id="bbit-cs-an-buddle" style="z-index: 180; width: 532px;visibility:visible;" class="bubble"><table class="bubble-table" cellSpacing="0" cellPadding="0"><tbody><tr><td class="bubble-cell-side"><div id="tl1" class="bubble-corner bubble-corner-tl"></div></td><td class="bubble-cell-main"><div class="bubble-top"></div></td><td class="bubble-cell-side"><div id="tr1" class="bubble-corner bubble-corner-tr"></div></td></tr><tr><td class="bubble-mid" colSpan="3"><div style="overflow: hidden" id="bubbleContent1"><div><div></div><div class="cb-root"><table class="cb-table" cellSpacing="0" cellPadding="0"><tbody><tr><td class="cb-value cb-gray" style="width:80px;">Start Date:</td><td class="cb-value" style="width:400px;"><div id="bbit-cs-an-start-time"></div></td></tr><tr><td class="cb-value cb-gray" style="width:80px;">End Date:</td><td class="cb-value" style="width:400px;"><div id="bbit-cs-an-end-time"></div></td></tr><tr><td class="cb-value cb-gray" style="width:80px;">Event:</td><td class="cb-value" style="width:400px;"><div id="bbit-cs-an-type"></div></td></tr><tr><td class="cb-value cb-gray" style="width:80px;vertical-align:top;">Title:</td><td class="cb-value" style="width:400px;"><div id="bbit-cs-an-title" title="' + i18n.xgcalendar.click_to_detail + '" class="textbox-fill-div"></div></td></tr><tr class="bbit-cs-message"><td class="cb-value cb-gray" style="width:80px;vertical-align:top;">Message:</td><td class="cb-value" style="width:400px;"><div id="bbit-cs-an-message" class="message-show-div"></div></td></tr><tr><td class="cb-value cb-gray" style="width:80px;">Assign By:</td><td class="cb-value" style="width:400px;"><div id="bbit-cs-an-author"></div></td></tr><tr class="bbit-cs-assignto"><td class="cb-value cb-gray" style="width:80px; vertical-align:top;">Assign To:</td><td class="cb-value" style="width:400px;"><div id="bbit-cs-an-assign" class="textbox-fill-div"></div></td></tr></tbody></table><div class="bbit-cs-split"><input id="bbit-cs-an-id" type="hidden" value=""/>&ensp;<span id="bbit-cs-an-delete" class="lk">' + i18n.xgcalendar.i_delete + '</span>&nbsp;|&ensp;<SPAN id="bbit-cs-an-edit" class="lk">' + i18n.xgcalendar.update + '</SPAN></div></div></div></div></td></tr><tr><td><div id="bl1" class="bubble-corner bubble-corner-bl"></div></td><td><div class="bubble-bottom"></div></td><td><div id="br1" class="bubble-corner bubble-corner-br"></div></td></tr></tbody></table><div id="bbit-cs-an-close" class="bubble-closebutton"></div><div id="bbit-cs-an-prong" class="prong"><div class="t-b tb-bg"></div><div class="t-b tb-bd"></div></div></div>';
		var __RESOURCESVIEW = '<div id="bbit-cs-rs-buddle" style="z-index: 180; width: 532px;visibility:visible;" class="bubble"><table class="bubble-table" cellSpacing="0" cellPadding="0"><tbody><tr><td class="bubble-cell-side"><div id="tl1" class="bubble-corner bubble-corner-tl"></div></td><td class="bubble-cell-main"><div class="bubble-top"></div></td><td class="bubble-cell-side"><div id="tr1" class="bubble-corner bubble-corner-tr"></div></td></tr><tr><td class="bubble-mid" colSpan="3"><div style="overflow: hidden" id="bubbleContent2"><div><div></div><div class="cb-root"><table class="cb-table" cellSpacing="0" cellPadding="0"><tbody><tr><td class="cb-value cb-gray" style="width:80px;">Start Date:</td><td class="cb-value" style="width:400px;"><div id="bbit-cs-rs-start-time"></div></td></tr><tr><td class="cb-value cb-gray" style="width:80px;">End Date:</td><td class="cb-value" style="width:400px;"><div id="bbit-cs-rs-end-time"></div></td></tr><tr><td class="cb-value cb-gray" style="width:80px;">Event:</td><td class="cb-value" style="width:400px;"><div id="bbit-cs-rs-type"></div></td></tr><tr><td class="cb-value cb-gray" style="width:80px;vertical-align:top;">Title:</td><td class="cb-value" style="width:400px;"><div id="bbit-cs-rs-title" class="textbox-fill-div"></div></td></tr><tr><td class="cb-value cb-gray" style="width:80px;">File:</td><td class="cb-value" style="width:400px;"><div class="textbox-fill-wrapper"><div class="textbox-fill-mid"><div id="bbit-cs-rs-file" title="' + i18n.xgcalendar.click_to_detail + '" class="textbox-fill-div lk" style="cursor:pointer;"></div></div></div></td></tr><tr class="bbit-cs-message"><td class="cb-value cb-gray" style="width:80px;vertical-align:top;">Message:</td><td class="cb-value" style="width:400px;"><div id="bbit-cs-rs-message" class="message-show-div"></div></td></tr><tr><td class="cb-value cb-gray" style="width:80px;">Assign By:</td><td class="cb-value" style="width:400px;"><div id="bbit-cs-rs-author"></div></td></tr><tr class="bbit-cs-assignto"><td class="cb-value cb-gray" style="width:80px; vertical-align:top;">Assign To:</td><td class="cb-value" style="width:400px;"><div id="bbit-cs-rs-assign" class="textbox-fill-div"></div></td></tr></tbody></table><div class="bbit-cs-split"><input id="bbit-cs-rs-id" type="hidden" value=""/><span id="bbit-cs-rs-delete" class="lk">' + i18n.xgcalendar.i_delete + '</span>&ensp;|&nbsp;<SPAN id="bbit-cs-rs-edit" class="lk">' + i18n.xgcalendar.update + '</SPAN></div></div></div></div></td></tr><tr><td><div id="bl1" class="bubble-corner bubble-corner-bl"></div></td><td><div class="bubble-bottom"></div></td><td><div id="br1" class="bubble-corner bubble-corner-br"></div></td></tr></tbody></table><div id="bbit-cs-rs-close" class="bubble-closebutton"></div><div id="bbit-cs-rs-prong" class="prong"><div class="t-b tb-bg"></div><div class="t-b tb-bd"></div></div></div>';
		var __LINKSVIEW = '<div id="bbit-cs-lk-buddle" style="z-index: 180; width: 532px;visibility:visible;" class="bubble"><table class="bubble-table" cellSpacing="0" cellPadding="0"><tbody><tr><td class="bubble-cell-side"><div id="tl1" class="bubble-corner bubble-corner-tl"></div></td><td class="bubble-cell-main"><div class="bubble-top"></div></td><td class="bubble-cell-side"><div id="tr1" class="bubble-corner bubble-corner-tr"></div></td></tr><tr><td class="bubble-mid" colSpan="3"><div style="overflow: hidden" id="bubbleContent3"><div><div></div><div class="cb-root"><table class="cb-table" cellSpacing="0" cellPadding="0"><tbody><tr><td class="cb-value cb-gray" style="width:80px;">Start Date:</td><td class="cb-value" style="width:400px;"><div id="bbit-cs-lk-start-time"></div></td></tr><tr><td class="cb-value cb-gray" style="width:80px;">End Date:</td><td class="cb-value" style="width:400px;"><div id="bbit-cs-lk-end-time"></div></td></tr><tr><td class="cb-value cb-gray" style="width:80px;">Event:</td><td class="cb-value" style="width:400px;"><div id="bbit-cs-lk-type"></div></td></tr><tr><td class="cb-value cb-gray" style="width:80px;vertical-align:top;">Title:</td><td class="cb-value" style="width:400px;"><div id="bbit-cs-lk-title" class="textbox-fill-div"></div></td></tr><tr><td class="cb-value cb-gray" style="width:80px;">URL:</td><td class="cb-value" style="width:400px;"><div class="textbox-fill-wrapper"><div class="textbox-fill-mid"><div id="bbit-cs-lk-url" title="' + i18n.xgcalendar.click_to_detail + '" class="textbox-fill-div lk" style="cursor:pointer;"></div></div></div></td></tr><tr class="bbit-cs-message"><td class="cb-value cb-gray" style="width:80px;vertical-align:top;">Message:</td><td class="cb-value" style="width:400px;"><div id="bbit-cs-lk-message" class="message-show-div"></div></td></tr><tr><td class="cb-value cb-gray" style="width:80px;">Assign By:</td><td class="cb-value" style="width:400px;"><div id="bbit-cs-lk-author"></div></td></tr><tr class="bbit-cs-assignto"><td class="cb-value cb-gray" style="width:80px; vertical-align:top;">Assign To:</td><td class="cb-value" style="width:400px;"><div id="bbit-cs-lk-assign" class="textbox-fill-div"></div></td></tr></tbody></table><div class="bbit-cs-split"><input id="bbit-cs-lk-id" type="hidden" value=""/><span id="bbit-cs-lk-delete" class="lk">' + i18n.xgcalendar.i_delete + '</span>&ensp;|&nbsp;<SPAN id="bbit-cs-lk-edit" class="lk">' + i18n.xgcalendar.update + '</SPAN></div></div></div></div></td></tr><tr><td><div id="bl1" class="bubble-corner bubble-corner-bl"></div></td><td><div class="bubble-bottom"></div></td><td><div id="br1" class="bubble-corner bubble-corner-br"></div></td></tr></tbody></table><div id="bbit-cs-lk-close" class="bubble-closebutton"></div><div id="bbit-cs-lk-prong" class="prong"><div class="t-b tb-bg"></div><div class="t-b tb-bd"></div></div></div>';
		var __RLVIEW = '<div id="bbit-cs-rl-buddle" style="z-index: 180; width: 532px;visibility:visible;" class="bubble"><table class="bubble-table" cellSpacing="0" cellPadding="0"><tbody><tr><td class="bubble-cell-side"><div id="tl1" class="bubble-corner bubble-corner-tl"></div></td><td class="bubble-cell-main"><div class="bubble-top"></div></td><td class="bubble-cell-side"><div id="tr1" class="bubble-corner bubble-corner-tr"></div></td></tr><tr><td class="bubble-mid" colSpan="3"><div style="overflow: hidden" id="bubbleContent4"><div><div></div><div class="cb-root"><table class="cb-table" cellSpacing="0" cellPadding="0"><tbody><tr><td class="cb-value cb-gray" style="width:80px;">Start Date:</td><td class="cb-value" style="width:400px;"><div id="bbit-cs-rl-start-time"></div></td></tr><tr><td class="cb-value cb-gray" style="width:80px;">End Date:</td><td class="cb-value" style="width:400px;"><div id="bbit-cs-rl-end-time"></div></td></tr><tr><td class="cb-value cb-gray" style="width:80px;">Event:</td><td class="cb-value" style="width:400px;"><div id="bbit-cs-rl-type"></div></td></tr><tr><td class="cb-value cb-gray" style="width:80px;vertical-align:top;">Title:</td><td class="cb-value" style="width:400px;"><div id="bbit-cs-rl-title" class="textbox-fill-div"></div></td></tr><tr><td class="cb-value cb-gray" style="width:80px; vertical-align:top;" id="bbit-cs-rl-buddle-topic">Topics:</td><td class="cb-value" style="width:400px;"><div id="bbit-cs-rl-topics" title="' + i18n.xgcalendar.click_to_detail + '" class="textbox-fill-div lk" style="cursor:pointer;"></div></td></tr><tr class="bbit-cs-message"><td class="cb-value cb-gray" style="width:80px;vertical-align:top;">Message:</td><td class="cb-value" style="width:400px;"><div id="bbit-cs-rl-message" class="message-show-div"></div></td></tr><tr><td class="cb-value cb-gray" style="width:80px;">Assign By:</td><td class="cb-value" style="width:400px;"><div id="bbit-cs-rl-author"></div></td></tr><tr class="bbit-cs-assignto"><td class="cb-value cb-gray" style="width:80px; vertical-align:top;">Assign To:</td><td class="cb-value" style="width:400px;"><div id="bbit-cs-rl-assign" class="textbox-fill-div"></div></td></tr></tbody></table><div class="bbit-cs-split"><input id="bbit-cs-rl-id" type="hidden" value=""/><span id="bbit-cs-rl-delete" class="lk">' + i18n.xgcalendar.i_delete + '</span>&ensp;|&nbsp;<SPAN id="bbit-cs-rl-edit" class="lk">' + i18n.xgcalendar.update + '</SPAN></div></div></div></div></td></tr><tr><td><div id="bl1" class="bubble-corner bubble-corner-bl"></div></td><td><div class="bubble-bottom"></div></td><td><div id="br1" class="bubble-corner bubble-corner-br"></div></td></tr></tbody></table><div id="bbit-cs-rl-close" class="bubble-closebutton"></div><div id="bbit-cs-rl-prong" class="prong"><div class="t-b tb-bg"></div><div class="t-b tb-bd"></div></div></div>';
		var __QUIZVIEW = '<div id="bbit-cs-qz-buddle" style="z-index: 180; width: 532px;visibility:visible;" class="bubble"><table class="bubble-table" cellSpacing="0" cellPadding="0"><tbody><tr><td class="bubble-cell-side"><div id="tl1" class="bubble-corner bubble-corner-tl"></div></td><td class="bubble-cell-main"><div class="bubble-top"></div></td><td class="bubble-cell-side"><div id="tr1" class="bubble-corner bubble-corner-tr"></div></td></tr><tr><td class="bubble-mid" colSpan="3"><div style="overflow: hidden" id="bubbleContent5"><div><div></div><div class="cb-root"><table class="cb-table" cellSpacing="0" cellPadding="0"><tbody><tr><td class="cb-value cb-gray" style="width:80px;">Start Date:</td><td class="cb-value" style="width:400px;"><div id="bbit-cs-qz-start-time"></div></td></tr><tr><td class="cb-value cb-gray" style="width:80px;">End Date:</td><td class="cb-value" style="width:400px;"><div id="bbit-cs-qz-end-time"></div></td></tr><tr><td class="cb-value cb-gray" style="width:80px;">Event:</td><td class="cb-value" style="width:400px;"><div id="bbit-cs-qz-type"></div></td></tr><tr><td class="cb-value cb-gray" style="width:80px;vertical-align:top;">Title:</td><td class="cb-value" style="width:400px;"><div id="bbit-cs-qz-title" title="' + i18n.xgcalendar.click_to_detail + '" class="textbox-fill-div lk" style="cursor:pointer;"></div></td></tr><tr class="bbit-cs-message"><td class="cb-value cb-gray" style="width:80px;vertical-align:top;">Message:</td><td class="cb-value" style="width:400px;"><div id="bbit-cs-qz-message" class="message-show-div"></div></td></tr><tr><td class="cb-value cb-gray" style="width:80px;">Assign By:</td><td class="cb-value" style="width:400px;"><div id="bbit-cs-qz-author"></div></td></tr><tr class="bbit-cs-assignto"><td class="cb-value cb-gray" style="width:80px; vertical-align:top;">Assign To:</td><td class="cb-value" style="width:400px;"><div id="bbit-cs-qz-assign" class="textbox-fill-div"></div></td></tr></tbody></table><div class="bbit-cs-split"><input id="bbit-cs-qz-id" type="hidden" value=""/><span id="bbit-cs-qz-delete" class="lk">' + i18n.xgcalendar.i_delete + '</span>&ensp;|&nbsp;<SPAN id="bbit-cs-qz-edit" class="lk">' + i18n.xgcalendar.update + '</SPAN></div></div></div></div></td></tr><tr><td><div id="bl1" class="bubble-corner bubble-corner-bl"></div></td><td><div class="bubble-bottom"></div></td><td><div id="br1" class="bubble-corner bubble-corner-br"></div></td></tr></tbody></table><div id="bbit-cs-qz-close" class="bubble-closebutton"></div><div id="bbit-cs-qz-prong" class="prong"><div class="t-b tb-bg"></div><div class="t-b tb-bd"></div></div></div>';
		var __WSVIEW = '<div id="bbit-cs-ws-buddle" style="z-index: 180; width: 532px;visibility:visible;" class="bubble"><table class="bubble-table" cellSpacing="0" cellPadding="0"><tbody><tr><td class="bubble-cell-side"><div id="tl1" class="bubble-corner bubble-corner-tl"></div></td><td class="bubble-cell-main"><div class="bubble-top"></div></td><td class="bubble-cell-side"><div id="tr1" class="bubble-corner bubble-corner-tr"></div></td></tr><tr><td class="bubble-mid" colSpan="3"><div style="overflow: hidden" id="bubbleContent6"><div><div></div><div class="cb-root"><table class="cb-table" cellSpacing="0" cellPadding="0"><tbody><tr><td class="cb-value cb-gray" style="width:80px;">Start Date:</td><td class="cb-value" style="width:400px;"><div id="bbit-cs-ws-start-time"></div></td></tr><tr><td class="cb-value cb-gray" style="width:80px;">End Date:</td><td class="cb-value" style="width:400px;"><div id="bbit-cs-ws-end-time"></div></td></tr><tr><td class="cb-value cb-gray" style="width:80px;">Event:</td><td class="cb-value" style="width:400px;"><div id="bbit-cs-ws-type"></div></td></tr><tr><td class="cb-value cb-gray" style="width:80px;vertical-align:top;">Title:</td><td class="cb-value" style="width:400px;"><div id="bbit-cs-ws-title" title="' + i18n.xgcalendar.click_to_detail + '" class="textbox-fill-div lk" style="cursor:pointer;"></div></td></tr><tr class="bbit-cs-message"><td class="cb-value cb-gray" style="width:80px;vertical-align:top;">Message:</td><td class="cb-value" style="width:400px;"><div id="bbit-cs-ws-message" class="message-show-div"></div></td></tr><tr><td class="cb-value cb-gray" style="width:80px;">Assign By:</td><td class="cb-value" style="width:400px;"><div id="bbit-cs-ws-author"></div></td></tr><tr class="bbit-cs-assignto"><td class="cb-value cb-gray" style="width:80px; vertical-align:top;">Assign To:</td><td class="cb-value" style="width:400px;"><div id="bbit-cs-ws-assign" class="textbox-fill-div"></div></td></tr></tbody></table><div class="bbit-cs-split"><input id="bbit-cs-ws-id" type="hidden" value=""/><span id="bbit-cs-ws-delete" class="lk">' + i18n.xgcalendar.i_delete + '</span>&ensp;|&nbsp;<SPAN id="bbit-cs-ws-edit" class="lk">' + i18n.xgcalendar.update + '</SPAN></div></div></div></div></td></tr><tr><td><div id="bl1" class="bubble-corner bubble-corner-bl"></div></td><td><div class="bubble-bottom"></div></td><td><div id="br1" class="bubble-corner bubble-corner-br"></div></td></tr></tbody></table><div id="bbit-cs-ws-close" class="bubble-closebutton"></div><div id="bbit-cs-ws-prong" class="prong"><div class="t-b tb-bg"></div><div class="t-b tb-bd"></div></div></div>';
		var __JOURNALVIEW = '<div id="bbit-cs-jn-buddle" style="z-index: 180; width: 532px;visibility:visible;" class="bubble"><table class="bubble-table" cellSpacing="0" cellPadding="0"><tbody><tr><td class="bubble-cell-side"><div id="tl1" class="bubble-corner bubble-corner-tl"></div></td><td class="bubble-cell-main"><div class="bubble-top"></div></td><td class="bubble-cell-side"><div id="tr1" class="bubble-corner bubble-corner-tr"></div></td></tr><tr><td class="bubble-mid" colSpan="3"><div style="overflow: hidden" id="bubbleContent7"><div><div></div><div class="cb-root"><table class="cb-table" cellSpacing="0" cellPadding="0"><tbody><tr><td class="cb-value cb-gray" style="width:80px;">Start Date:</td><td class="cb-value" style="width:400px;"><div id="bbit-cs-jn-start-time"></div></td></tr><tr><td class="cb-value cb-gray" style="width:80px;">End Date:</td><td class="cb-value" style="width:400px;"><div id="bbit-cs-jn-end-time"></div></td></tr><tr><td class="cb-value cb-gray" style="width:80px;">Event:</td><td class="cb-value" style="width:400px;"><div id="bbit-cs-jn-type"></div></td></tr><tr><td class="cb-value cb-gray" style="width:80px;vertical-align:top;">Title:</td><td class="cb-value" style="width:400px;"><div id="bbit-cs-jn-title" title="' + i18n.xgcalendar.click_to_detail + '" class="textbox-fill-div lk" style="cursor:pointer;"></div></td></tr><tr class="bbit-cs-message"><td class="cb-value cb-gray" style="width:80px;vertical-align:top;">Message:</td><td class="cb-value" style="width:400px;"><div id="bbit-cs-jn-message" class="message-show-div"></div></td></tr><tr><td class="cb-value cb-gray" style="width:80px;">Assign By:</td><td class="cb-value" style="width:400px;"><div id="bbit-cs-jn-author"></div></td></tr><tr class="bbit-cs-assignto"><td class="cb-value cb-gray" style="width:80px; vertical-align:top;">Assign To:</td><td class="cb-value" style="width:400px;"><div id="bbit-cs-jn-assign" class="textbox-fill-div"></div></td></tr></tbody></table><div class="bbit-cs-split"><input id="bbit-cs-jn-id" type="hidden" value=""/><span id="bbit-cs-jn-delete" class="lk">' + i18n.xgcalendar.i_delete + '</span>&ensp;|&nbsp;<SPAN id="bbit-cs-jn-edit" class="lk">' + i18n.xgcalendar.update + '</SPAN></div></div></div></div></td></tr><tr><td><div id="bl1" class="bubble-corner bubble-corner-bl"></div></td><td><div class="bubble-bottom"></div></td><td><div id="br1" class="bubble-corner bubble-corner-br"></div></td></tr></tbody></table><div id="bbit-cs-jn-close" class="bubble-closebutton"></div><div id="bbit-cs-jn-prong" class="prong"><div class="t-b tb-bg"></div><div class="t-b tb-bd"></div></div></div>';
		var __PMPVIEW = '<div id="bbit-cs-pmp-buddle" style="z-index: 180; width: 532px;visibility:visible;" class="bubble"><table class="bubble-table" cellSpacing="0" cellPadding="0"><tbody><tr><td class="bubble-cell-side"><div id="tl1" class="bubble-corner bubble-corner-tl"></div></td><td class="bubble-cell-main"><div class="bubble-top"></div></td><td class="bubble-cell-side"><div id="tr1" class="bubble-corner bubble-corner-tr"></div></td></tr><tr><td class="bubble-mid" colSpan="3"><div style="overflow: hidden" id="bubbleContent8"><div><div></div><div class="cb-root"><table class="cb-table" cellSpacing="0" cellPadding="0"><tbody><tr><td class="cb-value cb-gray" style="width:80px;">Start Date:</td><td class="cb-value" style="width:400px;"><div id="bbit-cs-pmp-start-time"></div></td></tr><tr><td class="cb-value cb-gray" style="width:80px;">End Date:</td><td class="cb-value" style="width:400px;"><div id="bbit-cs-pmp-end-time"></div></td></tr><tr><td class="cb-value cb-gray" style="width:80px;">Event:</td><td class="cb-value" style="width:400px;"><div id="bbit-cs-pmp-type"></div></td></tr><tr><td class="cb-value cb-gray" style="width:80px;vertical-align:top;">Title:</td><td class="cb-value" style="width:400px;"><div id="bbit-cs-pmp-title" title="' + i18n.xgcalendar.click_to_detail + '" class="textbox-fill-div lk" style="cursor:pointer;"></div></td></tr><tr class="bbit-cs-message"><td class="cb-value cb-gray" style="width:80px;vertical-align:top;">Message:</td><td class="cb-value" style="width:400px;"><div id="bbit-cs-pmp-message" class="message-show-div"></div></td></tr><tr><td class="cb-value cb-gray" style="width:80px;">Assign By:</td><td class="cb-value" style="width:400px;"><div id="bbit-cs-pmp-author"></div></td></tr><tr class="bbit-cs-assignto"><td class="cb-value cb-gray" style="width:80px; vertical-align:top;">Assign To:</td><td class="cb-value" style="width:400px;"><div id="bbit-cs-pmp-assign" class="textbox-fill-div"></div></td></tr></tbody></table><div class="bbit-cs-split"><input id="bbit-cs-pmp-id" type="hidden" value=""/><span id="bbit-cs-pmp-delete" class="lk">' + i18n.xgcalendar.i_delete + '</span>&ensp;|&nbsp;<SPAN id="bbit-cs-pmp-edit" class="lk">' + i18n.xgcalendar.update + '</SPAN></div></div></div></div></td></tr><tr><td><div id="bl1" class="bubble-corner bubble-corner-bl"></div></td><td><div class="bubble-bottom"></div></td><td><div id="br1" class="bubble-corner bubble-corner-br"></div></td></tr></tbody></table><div id="bbit-cs-pmp-close" class="bubble-closebutton"></div><div id="bbit-cs-pmp-prong" class="prong"><div class="t-b tb-bg"></div><div class="t-b tb-bd"></div></div></div>';
		var __EAVIEW = '<div id="bbit-cs-ea-buddle" style="z-index: 180; width: 532px;visibility:visible;" class="bubble"><table class="bubble-table" cellSpacing="0" cellPadding="0"><tbody><tr><td class="bubble-cell-side"><div id="tl1" class="bubble-corner bubble-corner-tl"></div></td><td class="bubble-cell-main"><div class="bubble-top"></div></td><td class="bubble-cell-side"><div id="tr1" class="bubble-corner bubble-corner-tr"></div></td></tr><tr><td class="bubble-mid" colSpan="3"><div style="overflow: hidden" id="bubbleContent9"><div><div></div><div class="cb-root"><table class="cb-table" cellSpacing="0" cellPadding="0"><tbody><tr><td class="cb-value cb-gray" style="width:80px;">Start Date:</td><td class="cb-value" style="width:400px;"><div id="bbit-cs-ea-start-time"></div></td></tr><tr><td class="cb-value cb-gray" style="width:80px;">End Date:</td><td class="cb-value" style="width:400px;"><div id="bbit-cs-ea-end-time"></div></td></tr><tr><td class="cb-value cb-gray" style="width:80px;">Event:</td><td class="cb-value" style="width:400px;"><div id="bbit-cs-ea-type"></div></td></tr><tr><td class="cb-value cb-gray" style="width:80px;vertical-align:top;">Title:</td><td class="cb-value" style="width:400px;"><div id="bbit-cs-ea-title" title="' + i18n.xgcalendar.click_to_detail + '" class="textbox-fill-div lk" style="cursor:pointer;"></div></td></tr><tr class="bbit-cs-message"><td class="cb-value cb-gray" style="width:80px;vertical-align:top;">Message:</td><td class="cb-value" style="width:400px;"><div id="bbit-cs-ea-message" class="message-show-div"></div></td></tr><tr><td class="cb-value cb-gray" style="width:80px;">Assign By:</td><td class="cb-value" style="width:400px;"><div id="bbit-cs-ea-author"></div></td></tr><tr class="bbit-cs-assignto"><td class="cb-value cb-gray" style="width:80px; vertical-align:top;">Assign To:</td><td class="cb-value" style="width:400px;"><div id="bbit-cs-ea-assign" class="textbox-fill-div"></div></td></tr></tbody></table><div class="bbit-cs-split"><input id="bbit-cs-ea-id" type="hidden" value=""/><span id="bbit-cs-ea-delete" class="lk">' + i18n.xgcalendar.i_delete + '</span>&ensp;|&nbsp;<SPAN id="bbit-cs-ea-edit" class="lk">' + i18n.xgcalendar.update + '</SPAN></div></div></div></div></td></tr><tr><td><div id="bl1" class="bubble-corner bubble-corner-bl"></div></td><td><div class="bubble-bottom"></div></td><td><div id="br1" class="bubble-corner bubble-corner-br"></div></td></tr></tbody></table><div id="bbit-cs-ea-close" class="bubble-closebutton"></div><div id="bbit-cs-ea-prong" class="prong"><div class="t-b tb-bg"></div><div class="t-b tb-bd"></div></div></div>';
		var __ATVIEW = '<div id="bbit-cs-at-buddle" style="z-index: 180; width: 532px;visibility:visible;" class="bubble"><table class="bubble-table" cellSpacing="0" cellPadding="0"><tbody><tr><td class="bubble-cell-side"><div id="tl1" class="bubble-corner bubble-corner-tl"></div></td><td class="bubble-cell-main"><div class="bubble-top"></div></td><td class="bubble-cell-side"><div id="tr1" class="bubble-corner bubble-corner-tr"></div></td></tr><tr><td class="bubble-mid" colSpan="3"><div style="overflow: hidden" id="bubbleContent9"><div><div></div><div class="cb-root"><table class="cb-table" cellSpacing="0" cellPadding="0"><tbody><tr><td class="cb-value cb-gray" style="width:80px;">Start Date:</td><td class="cb-value" style="width:400px;"><div id="bbit-cs-at-start-time"></div></td></tr><tr><td class="cb-value cb-gray" style="width:80px;">End Date:</td><td class="cb-value" style="width:400px;"><div id="bbit-cs-at-end-time"></div></td></tr><tr><td class="cb-value cb-gray" style="width:80px;">Event:</td><td class="cb-value" style="width:400px;"><div id="bbit-cs-at-type"></div></td></tr><tr><td class="cb-value cb-gray" style="width:80px;vertical-align:top;">Title:</td><td class="cb-value" style="width:400px;"><div id="bbit-cs-at-title" title="' + i18n.xgcalendar.click_to_detail + '" class="textbox-fill-div lk" style="cursor:pointer;"></div></td></tr><tr class="bbit-cs-message"><td class="cb-value cb-gray" style="width:80px;vertical-align:top;">Message:</td><td class="cb-value" style="width:400px;"><div id="bbit-cs-at-message" class="message-show-div"></div></td></tr><tr><td class="cb-value cb-gray" style="width:80px;">Assign By:</td><td class="cb-value" style="width:400px;"><div id="bbit-cs-at-author"></div></td></tr><tr class="bbit-cs-assignto"><td class="cb-value cb-gray" style="width:80px; vertical-align:top;">Assign To:</td><td class="cb-value" style="width:400px;"><div id="bbit-cs-at-assign" class="textbox-fill-div"></div></td></tr></tbody></table><div class="bbit-cs-split"><input id="bbit-cs-at-id" type="hidden" value=""/><span id="bbit-cs-at-delete" class="lk">' + i18n.xgcalendar.i_delete + '</span><SPAN id="bbit-cs-at-edit" class="lk">&ensp;|&nbsp;' + i18n.xgcalendar.update + '</SPAN></div></div></div></div></td></tr><tr><td><div id="bl1" class="bubble-corner bubble-corner-bl"></div></td><td><div class="bubble-bottom"></div></td><td><div id="br1" class="bubble-corner bubble-corner-br"></div></td></tr></tbody></table><div id="bbit-cs-at-close" class="bubble-closebutton"></div><div id="bbit-cs-at-prong" class="prong"><div class="t-b tb-bg"></div><div class="t-b tb-bd"></div></div></div>';
		var __SVVIEW = '<div id="bbit-cs-sv-buddle" style="z-index: 180; width: 532px;visibility:visible;" class="bubble"><table class="bubble-table" cellSpacing="0" cellPadding="0"><tbody><tr><td class="bubble-cell-side"><div id="tl1" class="bubble-corner bubble-corner-tl"></div></td><td class="bubble-cell-main"><div class="bubble-top"></div></td><td class="bubble-cell-side"><div id="tr1" class="bubble-corner bubble-corner-tr"></div></td></tr><tr><td class="bubble-mid" colSpan="3"><div style="overflow: hidden" id="bubbleContent9"><div><div></div><div class="cb-root"><table class="cb-table" cellSpacing="0" cellPadding="0"><tbody><tr><td class="cb-value cb-gray" style="width:80px;">Start Date:</td><td class="cb-value" style="width:400px;"><div id="bbit-cs-sv-start-time"></div></td></tr><tr><td class="cb-value cb-gray" style="width:80px;">End Date:</td><td class="cb-value" style="width:400px;"><div id="bbit-cs-sv-end-time"></div></td></tr><tr><td class="cb-value cb-gray" style="width:80px;">Event:</td><td class="cb-value" style="width:400px;"><div id="bbit-cs-sv-type"></div></td></tr><tr><td class="cb-value cb-gray" style="width:80px;vertical-align:top;">Title:</td><td class="cb-value" style="width:400px;"><div id="bbit-cs-sv-title" title="' + i18n.xgcalendar.click_to_detail + '" class="textbox-fill-div lk" style="cursor:pointer;"></div></td></tr><tr class="bbit-cs-message"><td class="cb-value cb-gray" style="width:80px;vertical-align:top;">Message:</td><td class="cb-value" style="width:400px;"><div id="bbit-cs-sv-message" class="message-show-div"></div></td></tr><tr><td class="cb-value cb-gray" style="width:80px;">Assign By:</td><td class="cb-value" style="width:400px;"><div id="bbit-cs-sv-author"></div></td></tr><tr class="bbit-cs-assignto"><td class="cb-value cb-gray" style="width:80px; vertical-align:top;">Assign To:</td><td class="cb-value" style="width:400px;"><div id="bbit-cs-sv-assign" class="textbox-fill-div"></div></td></tr></tbody></table><div class="bbit-cs-split"><input id="bbit-cs-sv-id" type="hidden" value=""/><span id="bbit-cs-sv-delete" class="lk">' + i18n.xgcalendar.i_delete + '</span><SPAN id="bbit-cs-sv-edit" class="lk">&ensp;|&nbsp;' + i18n.xgcalendar.update + '</SPAN></div></div></div></div></td></tr><tr><td><div id="bl1" class="bubble-corner bubble-corner-bl"></div></td><td><div class="bubble-bottom"></div></td><td><div id="br1" class="bubble-corner bubble-corner-br"></div></td></tr></tbody></table><div id="bbit-cs-sv-close" class="bubble-closebutton"></div><div id="bbit-cs-sv-prong" class="prong"><div class="t-b tb-bg"></div><div class="t-b tb-bd"></div></div></div>';
		var __FMVIEW = '<div id="bbit-cs-fm-buddle" style="z-index: 180; width: 532px;visibility:visible;" class="bubble"><table class="bubble-table" cellSpacing="0" cellPadding="0"><tbody><tr><td class="bubble-cell-side"><div id="tl1" class="bubble-corner bubble-corner-tl"></div></td><td class="bubble-cell-main"><div class="bubble-top"></div></td><td class="bubble-cell-side"><div id="tr1" class="bubble-corner bubble-corner-tr"></div></td></tr><tr><td class="bubble-mid" colSpan="3"><div style="overflow: hidden" id="bubbleContent9"><div><div></div><div class="cb-root"><table class="cb-table" cellSpacing="0" cellPadding="0"><tbody><tr><td class="cb-value cb-gray" style="width:80px;">Start Date:</td><td class="cb-value" style="width:400px;"><div id="bbit-cs-fm-start-time"></div></td></tr><tr><td class="cb-value cb-gray" style="width:80px;">End Date:</td><td class="cb-value" style="width:400px;"><div id="bbit-cs-fm-end-time"></div></td></tr><tr><td class="cb-value cb-gray" style="width:80px;">Event:</td><td class="cb-value" style="width:400px;"><div id="bbit-cs-fm-type"></div></td></tr><tr><td class="cb-value cb-gray" style="width:80px;vertical-align:top;">Title:</td><td class="cb-value" style="width:400px;"><div id="bbit-cs-fm-title" title="' + i18n.xgcalendar.click_to_detail + '" class="textbox-fill-div lk" style="cursor:pointer;"></div></td></tr><tr class="bbit-cs-message"><td class="cb-value cb-gray" style="width:80px;vertical-align:top;">Message:</td><td class="cb-value" style="width:400px;"><div id="bbit-cs-fm-message" class="message-show-div"></div></td></tr><tr><td class="cb-value cb-gray" style="width:80px;">Assign By:</td><td class="cb-value" style="width:400px;"><div id="bbit-cs-fm-author"></div></td></tr><tr class="bbit-cs-assignto"><td class="cb-value cb-gray" style="width:80px; vertical-align:top;">Assign To:</td><td class="cb-value" style="width:400px;"><div id="bbit-cs-fm-assign" class="textbox-fill-div"></div></td></tr></tbody></table><div class="bbit-cs-split"><input id="bbit-cs-fm-id" type="hidden" value=""/><span id="bbit-cs-fm-delete" class="lk">' + i18n.xgcalendar.i_delete + '</span><SPAN id="bbit-cs-fm-edit" class="lk">&ensp;|&nbsp;' + i18n.xgcalendar.update + '</SPAN></div></div></div></div></td></tr><tr><td><div id="bl1" class="bubble-corner bubble-corner-bl"></div></td><td><div class="bubble-bottom"></div></td><td><div id="br1" class="bubble-corner bubble-corner-br"></div></td></tr></tbody></table><div id="bbit-cs-fm-close" class="bubble-closebutton"></div><div id="bbit-cs-fm-prong" class="prong"><div class="t-b tb-bg"></div><div class="t-b tb-bd"></div></div></div>';
		var __MPGVIEW = '<div id="bbit-cs-mpg-buddle" style="z-index: 180; width: 532px;visibility:visible;" class="bubble"><table class="bubble-table" cellSpacing="0" cellPadding="0"><tbody><tr><td class="bubble-cell-side"><div id="tl1" class="bubble-corner bubble-corner-tl"></div></td><td class="bubble-cell-main"><div class="bubble-top"></div></td><td class="bubble-cell-side"><div id="tr1" class="bubble-corner bubble-corner-tr"></div></td></tr><tr><td class="bubble-mid" colSpan="3"><div style="overflow: hidden" id="bubbleContent9"><div><div></div><div class="cb-root"><table class="cb-table" cellSpacing="0" cellPadding="0"><tbody><tr><td class="cb-value cb-gray" style="width:80px;">Start Date:</td><td class="cb-value" style="width:400px;"><div id="bbit-cs-mpg-start-time"></div></td></tr><tr><td class="cb-value cb-gray" style="width:80px;">End Date:</td><td class="cb-value" style="width:400px;"><div id="bbit-cs-mpg-end-time"></div></td></tr><tr><td class="cb-value cb-gray" style="width:80px;">Event:</td><td class="cb-value" style="width:400px;"><div id="bbit-cs-mpg-type"></div></td></tr><tr><td class="cb-value cb-gray" style="width:80px;vertical-align:top;">Title:</td><td class="cb-value" style="width:400px;"><div id="bbit-cs-mpg-title" title="' + i18n.xgcalendar.click_to_detail + '" class="textbox-fill-div lk" style="cursor:pointer;"></div></td></tr><tr class="bbit-cs-message"><td class="cb-value cb-gray" style="width:80px;vertical-align:top;">Message:</td><td class="cb-value" style="width:400px;"><div id="bbit-cs-mpg-message" class="message-show-div"></div></td></tr><tr><td class="cb-value cb-gray" style="width:80px;">Assign By:</td><td class="cb-value" style="width:400px;"><div id="bbit-cs-mpg-author"></div></td></tr><tr class="bbit-cs-assignto"><td class="cb-value cb-gray" style="width:80px; vertical-align:top;">Assign To:</td><td class="cb-value" style="width:400px;"><div id="bbit-cs-mpg-assign" class="textbox-fill-div"></div></td></tr></tbody></table><div class="bbit-cs-split"><input id="bbit-cs-mpg-id" type="hidden" value=""/><span id="bbit-cs-mpg-delete" class="lk">' + i18n.xgcalendar.i_delete + '</span><SPAN id="bbit-cs-mpg-edit" class="lk">&ensp;|&nbsp;' + i18n.xgcalendar.update + '</SPAN></div></div></div></div></td></tr><tr><td><div id="bl1" class="bubble-corner bubble-corner-bl"></div></td><td><div class="bubble-bottom"></div></td><td><div id="br1" class="bubble-corner bubble-corner-br"></div></td></tr></tbody></table><div id="bbit-cs-mpg-close" class="bubble-closebutton"></div><div id="bbit-cs-mpg-prong" class="prong"><div class="t-b tb-bg"></div><div class="t-b tb-bd"></div></div></div>';
	
		var _typeViews = {'Announcement':__ANNOUNCEMENTVIEW, 'Resources':__RESOURCESVIEW, 'Links':__LINKSVIEW, 'RL':__RLVIEW, 'Quiz':__QUIZVIEW, 'Worksheet':__WSVIEW, 'Journal':__JOURNALVIEW, 'PMP':__PMPVIEW, 'EA':__EAVIEW, 'AT':__ATVIEW, 'SV':__SVVIEW, 'Forum':__FMVIEW, 'MPG':__MPGVIEW},
		_typeNames = {'Announcement':'Announcement', 'Resources':'Documents', 'Links':'Link', 'RL':'Reading Assignment', 'Quiz':'Quiz', 'Worksheet':'Worksheet', 'Journal':'Journal', 'PMP':'Progressive Mastery Programme', 'EA':'Exploratory Activity', 'AT':'Annotation', 'SV':'Survey', 'Forum':'Forum', 'MPG':'Multiplayer Game'},
		_typeTags = {'Announcement':'an', 'Resources':'rs', 'Links':'lk', 'RL':'rl', 'Quiz':'qz', 'Worksheet':'ws', 'Journal':'jn', 'PMP':'pmp', 'EA':'ea', 'AT':'at', 'SV':'sv', 'Forum':'fm', 'MPG':'mpg'},
		_docroot = '/' + document.getElementById('sysFolder').value.toString(),
		_existTypes = {
						'New':{'Create':{'url':'dialogs/eventForm.php', 'option':{'width':i18n.xgcalendar.dialog.width, 'height':i18n.xgcalendar.dialog.height, 'caption':'New Event', onclose:freshPage}}},
						'Quiz':{
							Delete:{
								'addressC':_docroot + '/admin/qms/QADelC_md.php?AssignID=', 
								'addressG':_docroot + '/admin/qms/QADelG_md.php?AssignID=', 
								'addressS':_docroot + '/admin/qms/QADelS_md.php?AssignID=', 'param':'&QuizID='
							},
							Edit:{'address':_docroot + '/admin/qms/QAEdit_md.php?AssignID=', 'param':'&QuizID='},
							SView:{url:_docroot + '/homework/QCall.php', param:{CourseID:'?CourseID=', QuizID:'&QuizID=', AssignID:'&AssignID='}},
							TView:{url:_docroot + '/admin/qms/QView.php', param:{QuizID:'?QuizID='}}	
						},
						'Worksheet':{
							Delete:{
								addressC:_docroot + '/admin/wms/WADelC_md.php?AssignID=', 
								addressG:_docroot + '/admin/wms/WADelG_md.php?AssignID=', 
								addressS:_docroot + '/admin/wms/WADelS_md.php?AssignID=', param:'&WSID='
							},
							Edit:{address:_docroot + '/admin/wms/WAEdit_md.php?AssignID=', param:'&WSID='},
							SView:{url:_docroot + '/homework/WCall.php', param:{CourseID:'?CourseID=', WSID:'&WSID=', AssignID:'&AssignID='}},
							TView:{url:_docroot + '/admin/wms/WView.php', param:{CourseID:'?CourseID=', WSID:'&WSID='}}	
						},
						'Journal':{
							Delete:{
								addressC:_docroot + '/admin/jms/JADelC_md.php?AssignID=', 
								addressG:_docroot + '/admin/jms/JADelG_md.php?AssignID=', 
								addressS:_docroot + '/admin/jms/JADelS_md.php?AssignID=', param:'&JournalID='
							},
							Edit:{address:_docroot + '/admin/jms/JAEdit_md.php?AssignID=', param:'&JournalID='},
							SView:{url:_docroot + '/homework/JCall.php', param:{CourseID:'?CourseID=', JournalID:'&JournalID=', AssignID:'&AssignID='}},
							TView:{url:_docroot + '/admin/jms/JView.php', param:{JournalID:'?JournalID='}}
						},
						'PMP':{
							Delete:{
								addressC:_docroot + '/admin/pmp/PADel_md.php?AssignID=', 
								addressG:_docroot + '/admin/pmp/PADel_md.php?AssignID=', 
								addressS:_docroot + '/admin/pmp/PADel_md.php?AssignID=', param:'&ZID='
							},
							Edit:{address:_docroot + '/admin/pmp/PAEdit_md.php?AssignID=', param:'&ZID='},
							SView:{url:_docroot + '/homework/PCall.php', param:{CourseID:'?CourseID=', AssignID:'&AssignID='}},
							TView:{url:_docroot + '/admin/pmp/PPreview.php', param:{Z:'?w='}}
						},
						'EA':{
							Delete:{
								addressC:_docroot + '/admin/eams/EADel_md.php?AssignID=', 
								addressG:_docroot + '/admin/eams/EADel_md.php?AssignID=', 
								addressS:_docroot + '/admin/eams/EADel_md.php?AssignID=', param:'&ID='
							},
							Edit:{address:_docroot + '/admin/eams/EAEdit_md.php?AssignID=', param:'&ID='},
							SView:{url:_docroot + '/homework/EACall.php', param:{CourseID:'?CourseID=', EAID:'&EAID=', AssignID:'&AssignID=', AID:'&AID='}},
							TView:{url:_docroot + '/courses/DisplayEA.php', param:{TypeID:'?TypeID=', AID:'&ID=', SectionID:'&SectionID=', TopicID:'&TopicID=', SubTopicID:'&SubTopicID='}}
						},
						'AT':{
							Delete:{
								addressC:_docroot + '/admin/vms/VADelC_md.php?AssignID=', param:'&DrawID=',
								addressG:_docroot + '/admin/vms/VADelG_md.php?AssignID=', param:'&DrawID=',
								addressS:_docroot + '/admin/vms/VADelS_md.php?AssignID=', param:'&DrawID='
							}, 
							/*Edit:{address:_docroot + '/admin/eams/EAEdit_md.php?AssignID=', param:'&ID='},	*/
							SView:{url:_docroot + '/courses/Display.php', param:{TypeID:'?TypeID=', VID:'&ID='}},
							TView:{url:_docroot + '/courses/DisplayT.php', param:{TypeID:'?TypeID=', VID:'&ID=', SectionID:'&SectionID=', TopicID:'&TopicID=', SubTopicID:'&SubTopicID='}}
						},
						'SV':{
							SView:{url:_docroot + '/homework/Survey.php?SID='},
							TView:{url:_docroot + '/admin/sms/SurveyResult.php?SID='}
						},
						'Forum':{
							Delete:{address:_docroot + '/forum/topic.php?t=3&ForumID=1&CalendarForumDelete=1', param:{FSID:'&FSID=', FTID:'&FTID='}}, 
							Edit:{address:_docroot + '/forum/topic_edit.php?t=3&ForumID=1', param:{FSID:'&FSID=', FTID:'&FTID='}},
							SView:{url:_docroot + '/forum/topic.php?t=3&ForumID=1&AddNo=1', param:{FSID:'&FSID=', FTID:'&FTID='}},
							TView:{url:_docroot + '/forum/topic.php?t=3&ForumID=1&AddNo=1', param:{FSID:'&FSID=', FTID:'&FTID='}}
						},
						'MPG':{
							Delete:{address:_docroot + '/Games/MPGameAssignDel.php?AssignID=', param:{OGID:'&OGID=', DelType:'&DelType='}}, 
							Edit:{address:_docroot + '/Games/MPGameAssignEdit.php?AssignID=', param:{OGID:'&OGID='}},
							SView:{url:_docroot + '/Games/MPGameStartDetails.php?AssignID=', param:{GameID:'&GameID=', ScenarioID:'&ScenarioID='}},
							TView:{url:_docroot + '/Games/MPGameAssignDetails.php?OGID='}
						}
					  };
        //for dragging var
        var _dragdata;
        var _dragevent;
		var months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];

        //clear DOM
        clearcontainer();

        //no height specified in options, we get page height.
        if (!option.height) {
            option.height = document.documentElement.clientHeight - 30;
        }
        //gridcontainer.css("overflow-y", "visible").height(option.height - 8);
        gridcontainer.css("overflow-y", "auto").height(option.height - 8);

        //populate events data for first display.
        if (option.url && option.autoload) {
            populate(); 
        }
        else {
            //contruct HTML          
            render();
            //get date range
            var d = getRdate();
            pushER(d.start, d.end);
        }

        //clear DOM
        function clearcontainer() {
            gridcontainer.empty();
        }
        //get range
        function getRdate() {
            return { start: option.vstart, end: option.vend };
        }
        //add date range to cache.
        function pushER(start, end) {
            var ll = option.loadDateR.length;
            if (!end) {
                end = start;
            }
            if (ll == 0) {
                option.loadDateR.push({ startdate: start, enddate: end });
            }
            else {
                for (var i = 0; i < ll; i++) {
                    var dr = option.loadDateR[i];
                    var diff = DateDiff("d", start, dr.startdate);
                    if (diff == 0 || diff == 1) {
                        if (dr.enddate < end) {
                            dr.enddate = end;
                        }
                        break;
                    }
                    else if (diff > 1) {
                        var d2 = DateDiff("d", end, dr.startdate);
                        if (d2 > 1) {
                            option.loadDateR.splice(0, 0, { startdate: start, enddate: end });
                        }
                        else {
                            dr.startdate = start;
                            if (dr.enddate < end) {
                                dr.enddate = end;
                            }
                        }
                        break;
                    }
                    else {
                        var d3 = DateDiff("d", end, dr.startdate);

                        if (dr.enddate < end) {
                            if (d3 < 1) {
                                dr.enddate = end;
                                break;
                            }
                            else {
                                if (i == ll - 1) {
                                    option.loadDateR.push({ startdate: start, enddate: end });
                                }
                            }
                        }
                    }
                }
                //end for
                //clear
                ll = option.loadDateR.length;
                if (ll > 1) {
                    for (var i = 0; i < ll - 1; ) {
                        var d1 = option.loadDateR[i];
                        var d2 = option.loadDateR[i + 1];

                        var diff1 = DateDiff("d", d2.startdate, d1.enddate);
                        if (diff1 <= 1) {
                            d1.startdate = d2.startdate > d1.startdate ? d1.startdate : d2.startdate;
                            d1.enddate = d2.enddate > d1.enddate ? d2.enddate : d1.enddate;
                            option.loadDateR.splice(i + 1, 1);
                            ll--;
                            continue;
                        }
                        i++;
                    }
                }
            }
        }
        //contruct DOM 
        function render() {
            //params needed
            //viewType, showday, events, config		
            var showday = new Date(option.showday.getFullYear(), option.showday.getMonth(), option.showday.getDate());
            var events = option.eventItems;
            var config = { view: option.view, weekstartday: option.weekstartday, theme: option.theme };
            if (option.view == "day" || option.view == "week") {
                var $dvtec = $("#dvtec");
                if ($dvtec.length > 0) {
                    option.scoll = $dvtec.attr("scrollTop"); //get scroll bar position
                }
            }
            switch (option.view) {
                case "day":
                    BuildDaysAndWeekView(showday, 1, events, config);
                    break;
                case "week":
                    BuildDaysAndWeekView(showday, 7, events, config);
                    break;
                case "month":
                    BuildMonthView(showday, events, config);
                    break;
                case "year":
                    BuildYearView(showday, events, config);
                    break;
                default:
                    alert(i18n.xgcalendar.no_implement);
                    break;
            }
            initevents(option.view); 
            ResizeView();
        }

        //build day view
        function BuildDaysAndWeekView(startday, l, events, config) {
            var days = [];
            if (l == 1) {
                var show = dateFormat.call(startday, i18n.xgcalendar.dateformat.dM);
                days.push({ display: show, date: startday, day: startday.getDate(), year: startday.getFullYear(), month: startday.getMonth() + 1 });
                option.datestrshow = CalDateShow(days[0].date);
                option.vstart = days[0].date;
                option.vend = days[0].date;
            }
            else {
                var w = 0;
                if (l == 7) {
                    w = config.weekstartday - startday.getDay();
                    if (w > 0) w = w - 7;
                }
                var ndate;
                for (var i = w, j = 0; j < l; i = i + 1, j++) {
                    ndate = DateAdd("d", i, startday);
                    var show = dateFormat.call(ndate, i18n.xgcalendar.dateformat.dM);
                    days.push({ display: show, date: ndate, day: ndate.getDate(), year: ndate.getFullYear(), month: ndate.getMonth() + 1 });
                }
                option.vstart = days[0].date;
                option.vend = days[l - 1].date;
                option.datestrshow = CalDateShow(days[0].date, days[l - 1].date);
            }

            var allDayEvents = [];
            var scollDayEvents = [];
            //get number of all-day events, including more-than-one-day events.
            var dM = PropareEvents(days, events, allDayEvents, scollDayEvents);
            var html = [];
            html.push("<div id=\"dvwkcontaienr\" class=\"wktopcontainer\">");
            html.push("<table class=\"wk-top\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">");
            BuildWT(html, days, allDayEvents, dM);
            html.push("</table>");
            html.push("</div>");

            //onclick=\"javascript:FunProxy('rowhandler',event,this);\"
            html.push("<div id=\"dvtec\"  class=\"scolltimeevent\"><table style=\"table-layout: fixed;", jQuery.browser.msie ? "" : "width:100%", "\" cellspacing=\"0\" cellpadding=\"0\"><tbody><tr><td>");
            html.push("<table style=\"height: 1008px\" id=\"tgTable\" class=\"tg-timedevents\" cellspacing=\"0\" cellpadding=\"0\"><tbody>");
            BuildDayScollEventContainer(html, days, scollDayEvents);
            html.push("</tbody></table></td></tr></tbody></table></div>");
            gridcontainer.html(html.join(""));
            html = null;
            //TODO event handlers
            //$("#weekViewAllDaywk").click(RowHandler);
        }
        //build month view
        function BuildMonthView(showday, events, config) {
            var cc = "<div id='cal-month-cc' class='cc'><div id='cal-month-cc-header'><div class='cc-close' id='cal-month-closebtn'></div><div id='cal-month-cc-title' class='cc-title'></div></div><div id='cal-month-cc-body' class='cc-body'><div id='cal-month-cc-content' class='st-contents'><table class='st-grid' cellSpacing='0' cellPadding='0'><tbody></tbody></table></div></div></div>";
            var html = [];
            html.push(cc);
            //build header
            html.push("<div id=\"mvcontainer\" class=\"mv-container\">");
            html.push("<table id=\"mvweek\" class=\"mv-daynames-table\" cellSpacing=\"0\" cellPadding=\"0\"><tbody><tr>");
            for (var i = config.weekstartday, j = 0; j < 7; i++, j++) {
                if (i > 6) i = 0;
                var p = { dayname: __WDAY[i] };
                html.push("<th class=\"mv-dayname\" title=\"", __WDAY[i], "\">", __WDAY[i], "");
            }
            html.push("</tr></tbody></table>");
            html.push("</div>");
            var bH = GetMonthViewBodyHeight() - GetMonthViewHeaderHeight();

            html.push("<div id=\"mvEventContainer\" class=\"mv-event-container\" style=\"height:", bH, "px;", "\">");
            BuilderMonthBody(html, showday, config.weekstartday, events, bH);
            html.push("</div>");
            gridcontainer.html(html.join(""));
            html = null;
            $("#cal-month-closebtn").click(closeCc);
        }
		//build year view
        function BuildYearView(showday, events, config) {
			option.datestrshow = CalYearShow(showday);
            var html = [];
            //build header
			html.push("<div id='yv-bg' class='yv-bg'>");
			BuilderYearBody(html, showday);
            html.push("</div>");
			
            gridcontainer.html(html.join(""));
            html = null;
        }
		function BuilderYearBody(html, showday){
			html.push("<div id='yv-container' class='yv-container'>");	
			html.push("<table id='yvmonth' class='yv-monthnames-table' cellSpacing='0' cellPadding='0'><tbody>");
			var titletemp = "<td class='yv-detail' abbr='${abbr}' axis='00:00' title='${title}'><span class='monthdayshow'>${month}</span></td>", year = showday.getFullYear(), o = {};
			for(var i = 0; i < 3; i++){
				html.push("<tr>");
				for(var j = 0; j < 4; j++){
					o.abbr = dateFormat.call(new Date(year, i*4 + j, 1), i18n.xgcalendar.dateformat.fulldayvalue);	
					o.title = months[i*4 + j] + " " + year;
					o.month = months[i*4 + j];
					html.push(Tp(titletemp, o));
				}
				
				html.push("</tr>");	
			}
			
			
			html.push("</tbody></table>");
            html.push("</div>");
		}
        function closeCc() {
            //$("#cal-month-cc").css({"visibility":"hidden"});
			$("#cal-month-cc").css({"display":"none"});
        }
        
        //all-day event, including more-than-one-day events 
        function PropareEvents(dayarrs, events, aDE, sDE) {
            var l = dayarrs.length;
            var el = events.length;
            var fE = [];
            var deB = aDE;
            var deA = sDE;
            for (var j = 0; j < el; j++) {
                var sD = events[j][2];
                var eD = events[j][3];
                var s = {};
                s.event = events[j];
                s.day = sD.getDate();
                s.year = sD.getFullYear();
                s.month = sD.getMonth() + 1;
                s.allday = events[j][4] == 1;
                s.crossday = events[j][5] == 1;
                s.reevent = events[j][6] == 1; //Recurring event
                s.daystr = [s.year, s.month, s.day].join("/");
                s.st = {};
				s.st.yearidx = sD.getFullYear();
				s.st.monthidx = sD.getMonth();
				s.st.dayidx = sD.getDate();
                s.st.hour = sD.getHours();
                s.st.minute = sD.getMinutes();
                s.st.p = s.st.hour * 60 + s.st.minute; // start time
                s.et = {};
				s.et.yearidx = eD.getFullYear();
				s.et.monthidx = eD.getMonth();
				s.et.dayidx = eD.getDate();
                s.et.hour = eD.getHours();
                s.et.minute = eD.getMinutes();
                s.et.p = s.et.hour * 60 + s.et.minute; // end time
                fE.push(s);
            }
            var dMax = 0;
            for (var i = 0; i < l; i++) {
                var da = dayarrs[i];
                deA[i] = []; deB[i] = [];
                da.daystr = da.year + "/" + da.month + "/" + da.day;
                for (var j = 0; j < fE.length; j++) {
                    if (!fE[j].crossday && !fE[j].allday) {
                        if (da.daystr == fE[j].daystr)
                            deA[i].push(fE[j]);
                    }
                    else {
                        if (da.daystr == fE[j].daystr) {
                            deB[i].push(fE[j]);
                            dMax++;
                        }
                        else {
                            if (i == 0 && da.date >= fE[j].event[2] && da.date <= fE[j].event[3])//first more-than-one-day event
                            {
                                deB[i].push(fE[j]);
                                dMax++;
                            }
                        }
                    }
                }
            }
            var lrdate = dayarrs[l - 1].date;
            for (var i = 0; i < l; i++) { //to deal with more-than-one-day event
                var de = deB[i];
                if (de.length > 0) { //           
                    for (var j = 0; j < de.length; j++) {
                        var end = DateDiff("d", lrdate, de[j].event[3]) > 0 ? lrdate : de[j].event[3];
                        de[j].colSpan = DateDiff("d", dayarrs[i].date, end) + 1
                    }
                }
                de = null;
            }
            //for all-day events
            for (var i = 0; i < l; i++) {
                var de = deA[i];
                if (de.length > 0) { 
                    var x = []; 
                    var y = []; 
                    var D = [];
                    var dl = de.length;
                    var Ia;
                    for (var j = 0; j < dl; ++j) {
                        var ge = de[j];
                        for (var La = ge.st.p, Ia = 0; y[Ia] > La; ) Ia++;
                        ge.PO = Ia; ge.ne = []; //PO is how many events before this one
                        y[Ia] = ge.et.p || 1440;
                        x[Ia] = ge;
                        if (!D[Ia]) {
                            D[Ia] = [];
                        }
                        D[Ia].push(ge);
                        if (Ia != 0) {
                            ge.pe = [x[Ia - 1]]; //previous event
                            x[Ia - 1].ne.push(ge); //next event
                        }
                        for (Ia = Ia + 1; y[Ia] <= La; ) Ia++;
                        if (x[Ia]) {
                            var k = x[Ia];
                            ge.ne.push(k);
                            k.pe.push(ge);
                        }
                        ge.width = 1 / (ge.PO + 1);
                        ge.left = 1 - ge.width;
                    }
                    var k = Array.prototype.concat.apply([], D);
                    x = y = D = null;
                    var t = k.length;
                    for (var y = t; y--; ) {
                        var H = 1;
                        var La = 0;
                        var x = k[y];
                        for (var D = x.ne.length; D--; ) {
                            var Ia = x.ne[D];
                            La = Math.max(La, Ia.VL);
                            H = Math.min(H, Ia.left)
                        }
                        x.VL = La + 1;
                        x.width = H / (x.PO + 1);
                        x.left = H - x.width;
                    }
                    for (var y = 0; y < t; y++) {
                        var x = k[y];
                        x.left = 0;
                        if (x.pe) for (var D = x.pe.length; D--; ) {
                            var H = x.pe[D];
                            x.left = Math.max(x.left, H.left + H.width);
                        }
                        var p = (1 - x.left) / x.VL;
                        x.width = Math.max(x.width, p);
                        x.aQ = Math.min(1 - x.left, x.width + 0.7 * p); //width offset
                    }
                    de = null;
                    deA[i] = k;
                }
            }
            return dMax;
        }

        function BuildWT(ht, dayarrs, events, dMax) {
            //1:
            ht.push("<tr>", "<th width=\"60\" rowspan=\"3\">&nbsp;</th>");
            for (var i = 0; i < dayarrs.length; i++) {
                var ev, title, cl;
                if (dayarrs.length == 1) {
                    ev = "";
                    title = "";
                    cl = "";
                }
                else {
                    ev = ""; // "onclick=\"javascript:FunProxy('week2day',event,this);\"";
                    title = dateFormat.call(dayarrs[i].date, i18n.xgcalendar.dateformat.dM);
                    cl = "wk-daylink";
                }
                ht.push("<th abbr='", dateFormat.call(dayarrs[i].date, i18n.xgcalendar.dateformat.fulldayvalue), "' class='gcweekname' scope=\"col\"><div title='", title, "' ", ev, " class='wk-dayname'><span class='", cl, "'>", dayarrs[i].display, "</span></div></th>");

            }
            ht.push("</tr>"); //end tr1;
            //2:          
            ht.push("<tr>");
            ht.push("<td class=\"wk-allday\"");

            if (dayarrs.length > 1) {
                ht.push(" colSpan='", dayarrs.length, "'");
            }
            //onclick=\"javascript:FunProxy('rowhandler',event,this);\"
            ht.push("><div id=\"weekViewAllDaywk\" ><table class=\"st-grid\" cellpadding=\"0\" cellspacing=\"0\"><tbody>");

            if (dMax == 0) {
                ht.push("<tr>");
                for (var i = 0; i < dayarrs.length; i++) {
                    ht.push("<td class=\"st-c st-s\"", " ch='qkadd' abbr='", dateFormat.call(dayarrs[i].date, "yyyy-M-d"), "' axis='00:00'>&nbsp;</td>");
                }
                ht.push("</tr>");
            }
            else {
                var l = events.length;
                var el = 0;
                var x = [];
                for (var j = 0; j < l; j++) {
                    x.push(0);
                }
                //var c = tc();
                for (var j = 0; el < dMax; j++) {
                    ht.push("<tr>");
                    for (var h = 0; h < l; ) {
                        var e = events[h][x[h]];
                        ht.push("<td class='st-c");
                        if (e) { //if exists
                            x[h] = x[h] + 1;
                            ht.push("'");
                            var t = BuildMonthDayEvent(e, dayarrs[h].date, l - h);
                            if (e.colSpan > 1) {
                                ht.push(" colSpan='", e.colSpan, "'");
                                h += e.colSpan;
                            }
                            else {
                                h++;
                            }
                            ht.push(" ch='show'>", t);
                            t = null;
                            el++;
                        }
                        else {
                            ht.push(" st-s' ch='qkadd' abbr='", dateFormat.call(dayarrs[h].date, i18n.xgcalendar.dateformat.fulldayvalue), "' axis='00:00'>&nbsp;");
                            h++;
                        }
                        ht.push("</td>");
                    }
                    ht.push("</tr>");
                }
                ht.push("<tr>");
                for (var h = 0; h < l; h++) {
                    ht.push("<td class='st-c st-s' ch='qkadd' abbr='", dateFormat.call(dayarrs[h].date, i18n.xgcalendar.dateformat.fulldayvalue), "' axis='00:00'>&nbsp;</td>");
                }
                ht.push("</tr>");
            }
            ht.push("</tbody></table></div></td></tr>"); // stgrid end //wvAd end //td2 end //tr2 end
            //3:
            ht.push("<tr>");

            ht.push("<td style=\"height: 5px;\"");
            if (dayarrs.length > 1) {
                ht.push(" colSpan='", dayarrs.length, "'");
            }
            ht.push("></td>");
            ht.push("</tr>");
        }

        function BuildDayScollEventContainer(ht, dayarrs, events) {
            //1:
            ht.push("<tr>");
            ht.push("<td style='width:60px;'></td>");
            ht.push("<td");
            if (dayarrs.length > 1) {
                ht.push(" colSpan='", dayarrs.length, "'");
            }
            ht.push("><div id=\"tgspanningwrapper\" class=\"tg-spanningwrapper\"><div style=\"font-size: 20px\" class=\"tg-hourmarkers\">");
            for (var i = 0; i < 24; i++) {
                ht.push("<div class=\"tg-dualmarker\"></div>");
            }
            ht.push("</div></div></td></tr>");

            //2:
            ht.push("<tr>");
            ht.push("<td style=\"width: 60px\" class=\"tg-times\">");

            //get current time 
            var now = new Date(); var h = now.getHours(); var m = now.getMinutes();
            var mHg = gP(h, m) - 4; //make middle alignment vertically
            ht.push("<div id=\"tgnowptr\" class=\"tg-nowptr\" style=\"left:0px;top:", mHg, "px\"></div>");
            var tmt = "";
            for (var i = 0; i < 24; i++) {
                tmt = fomartTimeShow(i);
                ht.push("<div style=\"height: 41px\" class=\"tg-time\">", tmt, "</div>");
            }
            ht.push("</td>");

            var l = dayarrs.length;
            for (var i = 0; i < l; i++) {
                ht.push("<td class=\"tg-col\" ch='qkadd' abbr='", dateFormat.call(dayarrs[i].date, i18n.xgcalendar.dateformat.fulldayvalue), "'>");
                var istoday = dateFormat.call(dayarrs[i].date, "yyyyMMdd") == dateFormat.call(new Date(), "yyyyMMdd");
                // Today
                if (istoday) {
                    ht.push("<div style=\"margin-bottom: -1008px; height:1008px\" class=\"tg-today\">&nbsp;</div>");
                }
                //var eventC = $(eventWrap);
                //onclick=\"javascript:FunProxy('rowhandler',event,this);\"
                ht.push("<div  style=\"margin-bottom: -1008px; height: 1008px\" id='tgCol", i, "' class=\"tg-col-eventwrapper\">");
                BuildEvents(ht, events[i], dayarrs[i]);
                ht.push("</div>");

                ht.push("<div class=\"tg-col-overlaywrapper\" id='tgOver", i, "'>");
                if (istoday) {
                    var mhh = mHg + 4;
                    ht.push("<div id=\"tgnowmarker\" class=\"tg-hourmarker tg-nowmarker\" style=\"left:0px;top:", mhh, "px\"></div>");
                }
                ht.push("</div>");
                ht.push("</td>");
            }
            ht.push("</tr>");
        }
        //show events to calendar
        function BuildEvents(hv, events, sday) {
            for (var i = 0; i < events.length; i++) {
                var c;
                if (events[i].event[7] && events[i].event[7] >= 0) {
                    c = tc(events[i].event[7]); //theme
                }
                else {
                    c = tc(); //default theme
                }
                var tt = BuildDayEvent(c, events[i], i);
                hv.push(tt);
            }
        }
        function getTitle(event) {			
            var timeshow, eventshow;
            var showtime = event[4] != 1;
            eventshow = event[1];
            var startformat = getymformat(event[2], null, showtime, true);
            var endformat = getymformat(event[3], event[2], showtime, true);
            timeshow = dateFormat.call(event[2], startformat) + " - " + dateFormat.call(event[3], endformat);
            var ret = [];
            if (event[4] == 1) {
                ret.push("[" + i18n.xgcalendar.allday_event + "]",$.browser.mozilla?"\r\n":"\r\n" );
            }
            else {
                if (event[5] == 1) {
                    ret.push("[" + i18n.xgcalendar.repeat_event + "]",$.browser.mozilla?"\r\n":"\r\n");
                }
            }
            ret.push(i18n.xgcalendar.time + ": ", timeshow, $.browser.mozilla?"\r\n":"\r\n", i18n.xgcalendar.event + ": ", eventshow,$.browser.mozilla?"\r\n":"\r\n");
            return ret.join("");
        }
        function BuildDayEvent(theme, e, index) {
            var p = { bdcolor: theme[0], bgcolor1: theme[1], bgcolor2: theme[2], width: "70%", icon: "", title: "", data: "" };
            p.starttime = pZero(e.st.hour) + ":" + pZero(e.st.minute);
            p.endtime = pZero(e.et.hour) + ":" + pZero(e.et.minute);
			(e.st.yearidx != e.et.yearidx || e.st.monthidx != e.et.monthidx || e.st.dayidx != e.et.dayidx) && (p.starttime = e.st.dayidx + "&ensp;" + months[e.st.monthidx] + "&ensp;" + p.starttime, p.endtime = e.et.dayidx + "&ensp;" + months[e.et.monthidx] + "&ensp;" + p.endtime);
            p.content = e.event[1];
            p.title = getTitle(e.event);
            p.data = e.event.join("$");
            var icons = [];
            icons.push("<I class=\"cic cic-tmr\">&nbsp;</I>");
            if (e.reevent) {
                icons.push("<I class=\"cic cic-spcl\">&nbsp;</I>");
            }
            p.icon = icons.join("");
            var sP = gP(e.st.hour, e.st.minute);
            var eP = gP(e.et.hour, e.et.minute);
            p.top = sP + "px";
            p.left = (e.left * 100) + "%";
            p.width = (e.aQ * 100) + "%";
            p.height = (eP - sP - 4);
            p.i = index;
            if (option.enableDrag && e.event[8] == 1) {
                p.drag = "drag";
                p.redisplay = "block";
            }
            else {
                p.drag = "";
                p.redisplay = "none";
            }
			/**
			*added to satisfy the need of new fields showed in event.
			 **/
			p.startdate = formatDate(e.event[2]); 
			p.enddate = formatDate(e.event[3]); 
			p.type = _typeNames[e.event[9]];
			p.message = e.event[10];
			p.course = e.event[11];
			p.author = e.event[15];
			p.classes = Tp('<DD style=\"BORDER-TOP:1px solid ${bgcolor1};\"><SPAN>', p) + e.event[12] + '</SPAN></DD>'; 
            var newtemp = Tp(__SCOLLEVENTTEMP, p);
            p = null;
            return newtemp;
        }

        //get body height in month view
        function GetMonthViewBodyHeight() {
            return option.height;
        }
        function GetMonthViewHeaderHeight() {
			return 22; //21
        }
        function BuilderMonthBody(htb, showday, startday, events, bodyHeight) {
            var firstdate = new Date(showday.getFullYear(), showday.getMonth(), 1);
            var diffday = startday - firstdate.getDay();
            var showmonth = showday.getMonth();
            if (diffday > 0) {
                diffday -= 7;
            }
            var startdate = DateAdd("d", diffday, firstdate);
            var enddate = DateAdd("d", 34, startdate);
            var rc = 5;

            if (enddate.getFullYear() == showday.getFullYear() && enddate.getMonth() == showday.getMonth() && enddate.getDate() < __MonthDays[showmonth]) {
                enddate = DateAdd("d", 7, enddate);
                rc = 6;
            }
            option.vstart = startdate;
            option.vend = enddate;
            option.datestrshow = CalDateShow(startdate, enddate);
            bodyHeight = bodyHeight - 18 * rc;
            var rowheight = bodyHeight / rc;
            var roweventcount = parseInt(rowheight / 21);
            if (rowheight % 21 > 15) {
                roweventcount++;
            }
            var p = 100 / rc;
            var formatevents = [];
            var hastdata = formartEventsInHashtable(events, startday, 7, startdate, enddate);
            var B = [];	//B[i]: the number of events in ith row
            var C = [];	//C[i]: the date string of ith day
            for (var j = 0; j < rc; j++) {
                var k = 0;
                formatevents[j] = b = [];	//formatevents[j][i]: 第j行第i列的hastdata元素
                for (var i = 0; i < 7; i++) {
                    var newkeyDate = DateAdd("d", j * 7 + i, startdate);
                    C[j * 7 + i] = newkeyDate;
                    var newkey = dateFormat.call(newkeyDate, i18n.xgcalendar.dateformat.fulldaykey);
                    b[i] = hastdata[newkey];
                    if (b[i] && b[i].length > 0) {
                        k += b[i].length;
                    }
                }
                B[j] = k;
            }
            //var c = tc();
            eventDiv.data("mvdata", formatevents);
            for (var j = 0; j < rc; j++) {
                //onclick=\"javascript:FunProxy('rowhandler',event,this);\"
                htb.push("<div id='mvrow_", j, "' style=\"HEIGHT:", p, "%; TOP:", p * j, "%\"  class=\"month-row\">");
                htb.push("<table class=\"st-bg-table\" cellSpacing=\"0\" cellPadding=\"0\"><tbody><tr>");
                var dMax = B[j];	//第j行所有event的总数

                for (var i = 0; i < 7; i++) {
                    var day = C[j * 7 + i];
                    htb.push("<td abbr='", dateFormat.call(day, i18n.xgcalendar.dateformat.fulldayvalue), "' ch='qkadd' axis='00:00' title=''");

                    if (dateFormat.call(day, "yyyyMMdd") == dateFormat.call(new Date(), "yyyyMMdd")) {
                        htb.push(" class=\"st-bg st-bg-today\">");
                    }
                    else {
                        htb.push(" class=\"st-bg\">");
                    }
                    htb.push("&nbsp;</td>");
                }
                //bgtable
                htb.push("</tr></tbody></table>");

                //stgrid
                htb.push("<table class=\"st-grid\" cellpadding=\"0\" cellspacing=\"0\"><tbody>");

                //title tr
                htb.push("<tr>");
                //var titletemp = "<td class=\"st-dtitle${titleClass}\" ch='qkadd' abbr='${abbr}' axis='00:00' title=\"${title}\"><a><span class='monthdayshow'>${dayshow}</span></a></td>";
				var titletemp = "<td class=\"st-dtitle${titleClass}\" ch='qkadd' abbr='${abbr}' axis='00:00' title=\"${title}\"><span class='monthdayshow'>${dayshow}</span></td>";

                for (var i = 0; i < 7; i++) {
                    var o = { titleClass: "", dayshow: "" };
                    var day = C[j * 7 + i];
                    if (dateFormat.call(day, "yyyyMMdd") == dateFormat.call(new Date(), "yyyyMMdd")) {
                        o.titleClass = " st-dtitle-today";
                    }
                    if (day.getMonth() != showmonth) {
                        o.titleClass = " st-dtitle-nonmonth";
                    }
                    o.title = dateFormat.call(day, i18n.xgcalendar.dateformat.dM);
                    if (day.getDate() == 1) {
                        o.dayshow = dateFormat.call(day, i18n.xgcalendar.dateformat.Md3);
                    }else {
                        o.dayshow = day.getDate();
                    }
                    o.abbr = dateFormat.call(day, i18n.xgcalendar.dateformat.fulldayvalue);
                    htb.push(Tp(titletemp, o));
                }
                htb.push("</tr>");
                var sfirstday = C[j * 7];
                BuildMonthRow(htb, formatevents[j], dMax, roweventcount, sfirstday);
                //htb=htb.concat(rowHtml); rowHtml = null;  

                htb.push("</tbody></table>");
                //month-row
                htb.push("</div>");
            }

            formatevents = B = C = hastdata = null;
            //return htb;
        }
        
        //formate datetime 
        function formartEventsInHashtable(events, startday, daylength, rbdate, redate) {
            var hast = new Object();
            var l = events.length;
            for (var i = 0; i < l; i++) {
                var sD = events[i][2];
                var eD = events[i][3];
                var diff = DateDiff("d", sD, eD);
                var s = {};
                s.event = events[i];
                s.day = sD.getDate();
                s.year = sD.getFullYear();
                s.month = sD.getMonth() + 1;
                s.allday = events[i][4] == 1;
                s.crossday = events[i][5] == 1;
                s.reevent = events[i][6] == 1; //Recurring event
                s.daystr = s.year + "/" + s.month + "/" + s.day;
                s.st = {};
				s.st.yearidx = sD.getFullYear();
				s.st.monthidx = sD.getMonth();
				s.st.dayidx = sD.getDate();
                s.st.hour = sD.getHours();
                s.st.minute = sD.getMinutes();
                s.st.p = s.st.hour * 60 + s.st.minute; // start time position
                s.et = {};
				s.et.yearidx = eD.getFullYear();
				s.et.monthidx = eD.getMonth();
				s.et.dayidx = eD.getDate();
                s.et.hour = eD.getHours();
                s.et.minute = eD.getMinutes();
                s.et.p = s.et.hour * 60 + s.et.minute; // end time postition

                if (diff > 0) {
                    if (sD < rbdate) { //start date out of range
                        sD = rbdate;
                    }
                    if (eD > redate) { //end date out of range
                        eD = redate;
                    }
                    var f = startday - sD.getDay();
                    if (f > 0) { f -= daylength; }
                    var sdtemp = DateAdd("d", f, sD);
                    for (; new Date(sdtemp.getFullYear(), sdtemp.getMonth(), sdtemp.getDate()).getTime() <= new Date(eD).getTime(); sD = sdtemp = DateAdd("d", daylength, sdtemp)) {
                        var d = Clone(s);
                        var key = dateFormat.call(sD, i18n.xgcalendar.dateformat.fulldaykey);
                        var x = DateDiff("d", sdtemp, eD);
                        if (hast[key] == null) {
                            hast[key] = [];
                        }
                        d.colSpan = (x >= daylength) ? daylength - DateDiff("d", sdtemp, sD) : DateDiff("d", sD, eD) + 1;
                        hast[key].push(d);
                        d = null;
                    }
                }
                else {
                    var key = dateFormat.call(events[i][2], i18n.xgcalendar.dateformat.fulldaykey);
                    if (hast[key] == null) {
                        hast[key] = [];
                    }
                    s.colSpan = 1;
                    hast[key].push(s);
                }
                s = null;
            }
            return hast;
        }
        function BuildMonthRow(htr, events, dMax, sc, day) {
			//alert("events is: " + events + "|dMax is: " + dMax + "|sc is: " + sc + "|day is: " + day);
            var x = []; 
            var y = []; //月份中某行某一单元格（日）中的事件数
            var z = [];	//月份中某行某一单元格（日）中显示的事件数 
            var cday = [];  
            var l = events.length;
            var el = 0;
            //var c = tc();
            for (var j = 0; j < l; j++) {
                x.push(0);
                y.push(0);
                z.push(0);
                cday.push(DateAdd("d", j, day));
            }
            for (var j = 0; j < l; j++) {
                var ec = events[j] ? events[j].length : 0;
                y[j] += ec;
                for (var k = 0; k < ec; k++) {
                    var e = events[j][k];
                    if (e && e.colSpan > 1) {
                        for (var m = 1; m < e.colSpan; m++) {
                            y[j + m]++;
                        }
                    }
                }
            }
            //var htr=[];
            var tdtemp = "<td class='${cssclass}' axis='${axis}' ch='${ch}' abbr='${abbr}' title='${title}' ${otherAttr}>${html}</td>";
            for (var j = 0; j < sc && el < dMax; j++) {
                htr.push("<tr>");
                //var gridtr = $(__TRTEMP);
                for (var h = 0; h < l; ) {
                    var e = events[h] ? events[h][x[h]] : undefined;
                    var tempdata = { "class": "", axis: "", ch: "", title: "", abbr: "", html: "", otherAttr: "", click: "javascript:void(0);" };
                    var tempCss = ["st-c"];

                    if (e) { 
                        x[h] = x[h] + 1;
                        //last event of the day
                        var bs = false;
                        if (z[h] + 1 == y[h] && e.colSpan == 1) {
                            bs = true;
                        }
                        if (!bs && j == (sc - 1) && z[h] < y[h]) {
                            el++;
                            $.extend(tempdata, { "axis": h, ch: "more", "abbr": dateFormat.call(cday[h], i18n.xgcalendar.dateformat.fulldayvalue), html: "+" + (y[h] - z[h]) + i18n.xgcalendar.item + i18n.xgcalendar.others, click: "javascript:alert('more event');" });
                            tempCss.push("st-more st-moreul");
                            h++;
                        }
                        else {
                            tempdata.html = BuildMonthDayEvent(e, cday[h], l - h);
                            tempdata.ch = "show";
                            if (e.colSpan > 1) {
                                tempdata.otherAttr = " colSpan='" + e.colSpan + "'";
                                for (var m = 0; m < e.colSpan; m++) {
                                    z[h + m] = z[h + m] + 1;
                                }
                                h += e.colSpan;

                            }
                            else {
                                z[h] = z[h] + 1;
                                h++;
                            }
                            el++;
                        }
                    }
                    else {
                        if (j == (sc - 1) && z[h] < y[h] && y[h] > 0) {
                            $.extend(tempdata, { "axis": h, ch: "more", "abbr": dateFormat.call(cday[h], i18n.xgcalendar.dateformat.fulldayvalue), html: "+" + (y[h] - z[h]) + i18n.xgcalendar.item + i18n.xgcalendar.others, click: "javascript:alert('more event');" });
                            tempCss.push("st-more st-moreul");
                            h++;
                        }
                        else {
                            $.extend(tempdata, { html: "&nbsp;", ch: "qkadd", "axis": "00:00", "abbr": dateFormat.call(cday[h], i18n.xgcalendar.dateformat.fulldayvalue), title: "" });
                            tempCss.push("st-s");
                            h++;
                        }
                    }
                    tempdata.cssclass = tempCss.join(" ");
                    tempCss = null;
                    htr.push(Tp(tdtemp, tempdata));
                    tempdata = null;
                }
                htr.push("</tr>");
            }
            x = y = z = cday = null;
            //return htr;
        }
        function BuildMonthDayEvent(e, cday, length, lmtlength) {
            var theme;
            if (e.event[7] && e.event[7] >= 0) {
                theme = tc(e.event[7]);
            }
            else {
                theme = tc();
            }
            var p = {bgcolor1: theme[1], color: theme[2], title: "", extendClass: "", extendHTML: "", data: "" };

            p.title = getTitle(e.event);
            p.id = "bbit_cal_event_" + e.event[0];
            if (option.enableDrag && e.event[8] == 1) {
                p.eclass = "drag";
            }
            else {
                p.eclass = "cal_" + e.event[0];
            }
            p.data = e.event.join("$");
            var sp = "<span style=\"cursor: pointer\">${content}</span>";
            var i = "<I class=\"cic cic-tmr\">&nbsp;</I>";
            var i2 = "<I class=\"cic cic-rcr\">&nbsp;</I>";
            var ml = "<div class=\"st-ad-ml\"></div>";
            var mr = "<div class=\"st-ad-mr\"></div>";
            var arrm = [];
            var sf = e.event[2] < cday;
            var ef = DateDiff("d", cday, e.event[3]) >= length;  //e.event[3] >= DateAdd("d", 1, cday);
            if (sf || ef) {
                if (sf) {
                    arrm.push(ml);
                    p.extendClass = "st-ad-mpad ";
                }
                if (ef)
                { arrm.push(mr); }
                p.extendHTML = arrm.join("");

            }
            var cen;
            if (!e.allday && !sf) {
                cen = "(" + pZero(e.st.hour) + ":" + pZero(e.st.minute) + ")&ensp;" + e.event[1];
            }
            else {
                cen = e.event[1];
            }
			if(!!lmtlength && !!cen && cen.length > lmtlength){	//确保month中弹出框内的行长度适中
				cen = cen.substr(0, lmtlength) + '...';	
			}
            var content = [];
            content.push(Tp(sp, { content: cen }));
			
            content.push(i);
            if (e.reevent)
            { content.push(i2); }
			
            p.content = content.join("");
            return Tp(__ALLDAYEVENTTEMP, p);
        }
        //to populate the data 
        function populate() {
            if (option.isloading) {
                return true;
            }
            if (option.url && option.url != "") {
                option.isloading = true;
                //clearcontainer();
                if (option.onBeforeRequestData && $.isFunction(option.onBeforeRequestData)) {
                    option.onBeforeRequestData(1);
                }
                var zone = new Date().getTimezoneOffset() / 60 * -1;
                var param = [
                { name: "showdate", value: dateFormat.call(option.showday, i18n.xgcalendar.dateformat.fulldayvalue) },
                { name: "viewtype", value: option.view },
				 { name: "timezone", value: zone }
                ];
                if (option.extParam) {
                    for (var pi = 0; pi < option.extParam.length; pi++) {
                        param[param.length] = option.extParam[pi];
                    }
                }
				if (option.settings) {
                    for (var pe = 0; pe < option.settings.length; pe++) {
                        param[param.length] = option.settings[pe];
                    }
                }	
                $.ajax({
                    type: option.method, //
                    url: option.url,
					async: false,	//to get the datastrtime in time
                    data: param,				   
			        //dataType: "text",  // fixed jquery 1.4 not support Ms Date Json Format /Date(@Tickets)/
                    dataType: "json",
                    dataFilter: function(data, type) { 
                        //return data.replace(/"\\\/(Date\([0-9-]+\))\\\/"/gi, "new $1");
                        
                        return data;
                      },
                    success: function(data) {//function(datastr) {									
						//datastr =datastr.replace(/"\\\/(Date\([0-9-]+\))\\\/"/gi, 'new $1');						
                        //var data = (new Function("return " + datastr))();
                        if (data != null && data.error != null) {
                            if (option.onRequestDataError) {
                                option.onRequestDataError(1, data);
                            }
                        }
                        else {
							option.eventItems = [];
                            data["start"] = parseDate(data["start"]);
                            data["end"] = parseDate(data["end"]);
                            $.each(data.events, function(index, value) { 
                                value[2] = parseDate(value[2]);
                                value[3] = parseDate(value[3]); 
                            });
                            responseData(data, data.start, data.end);
                            pushER(data.start, data.end);
                        }
                        if (option.onAfterRequestData && $.isFunction(option.onAfterRequestData)) {
                            option.onAfterRequestData(1);
                        }
                        option.isloading = false;
                    },
                    error: function(data) {	
						try {							
                            if (option.onRequestDataError) {
                                option.onRequestDataError(1, data);
                            } else {
                                alert(i18n.xgcalendar.get_data_exception);
                            }
                            if (option.onAfterRequestData && $.isFunction(option.onAfterRequestData)) {
                                option.onAfterRequestData(1);
                            }
                            option.isloading = false;
                        } catch (e) { }
                    }
                });
            }
            else {
                alert("url" + i18n.xgcalendar.i_undefined);
            }
        }
        function responseData(data, start, end) {
            var events;
            if (data.issort == false) {
                if (data.events && data.events.length > 0) {
                    events = data.events.sort(function(l, r) { return l[2] > r[2] ? 1 : -1; });
                }
                else {
                    events = [];
                }
            }
            else {
                events = data.events;
            }
            ConcatEvents(events, start, end);
            render();

        }
        function clearrepeat(events, start, end) {
            var jl = events.length;
            if (jl > 0) {
                var es = events[0][2];
                var el = events[jl - 1][2];
                for (var i = 0, l = option.eventItems.length; i < l; i++) {

                    if (option.eventItems[i][2] > el || jl == 0) {
                        break;
                    }
                    if (option.eventItems[i][2] >= es) {
                        for (var j = 0; j < jl; j++) {
                            if (option.eventItems[i][0] == events[j][0] && option.eventItems[i][2] < start) {
                                events.splice(j, 1); //for duplicated event
                                jl--;
                                break;
                            }
                        }
                    }
                }
            }
        }
        function ConcatEvents(events, start, end) {
            if (!events) {
                events = [];
            } 
            if (events) {
                if (option.eventItems.length == 0) {
                    option.eventItems = events;
                }
                else {
                    //remove duplicated one
                    clearrepeat(events, start, end);
                    var l = events.length;
                    var sl = option.eventItems.length;
                    var sI = -1;
                    var eI = sl;
                    var s = start;
                    var e = end;
                    if (option.eventItems[0][2] > e)
                    {
                        option.eventItems = events.concat(option.eventItems);
                        return;
                    }
                    if (option.eventItems[sl - 1][2] < s) 
                    {
                        option.eventItems = option.eventItems.concat(events);
                        return;
                    }
                    for (var i = 0; i < sl; i++) {
                        if (option.eventItems[i][2] >= s && sI < 0) {
                            sI = i;
                            continue;
                        }
                        if (option.eventItems[i][2] > e) {
                            eI = i;
                            break;
                        }
                    }

                    var e1 = sI <= 0 ? [] : option.eventItems.slice(0, sI);
                    var e2 = eI == sl ? [] : option.eventItems.slice(eI);
                    option.eventItems = [].concat(e1, events, e2);
                    events = e1 = e2 = null;
                }
            }
        }
        //utils goes here
		function yeartomonth(e){
			$("#caltoolbar div.fcurrent").each(function() {
				$(this).removeClass("fcurrent");
			})
			$("#showmonthbtn").addClass("fcurrent");	
			var th = $(this);
            var daystr = th.attr("abbr");
            option.showday = strtodate(daystr + " 00:00");
            option.view = "month";
            render();
			dochange();
            return false;
		}
        function weekormonthtoday(e) {
			$("#caltoolbar div.fcurrent").each(function() {
				$(this).removeClass("fcurrent");
			})
			$("#showdaybtn").addClass("fcurrent");
            var th = $(this);
            var daystr = th.attr("abbr");
            option.showday = strtodate(daystr + " 00:00");
            option.view = "day";
            render();
            if (option.onweekormonthtoday) {
                option.onweekormonthtoday(option);
            }
            return false;
        }
        function parseDate(str){
            return new Date(Date.parse(str));
        }
        function gP(h, m) {
            return h * 42 + parseInt(m / 60 * 42);
        }
        function gW(ts1, ts2) {
            var t1 = ts1 / 42;
            var t2 = parseInt(t1);
            var t3 = t1 - t2 >= 0.5 ? 30 : 0;
            var t4 = ts2 / 42;
            var t5 = parseInt(t4);
            var t6 = t4 - t5 >= 0.5 ? 30 : 0;
            return { sh: t2, sm: t3, eh: t5, em: t6, h: ts2 - ts1 };
        }
        function gH(y1, y2, pt) {
            var sy1 = Math.min(y1, y2);
            var sy2 = Math.max(y1, y2);
            var t1 = (sy1 - pt) / 42;
            var t2 = parseInt(t1);
            var t3 = t1 - t2 >= 0.5 ? 30 : 0;
            var t4 = (sy2 - pt) / 42;
            var t5 = parseInt(t4);
            var t6 = t4 - t5 >= 0.5 ? 30 : 0;
            return { sh: t2, sm: t3, eh: t5, em: t6, h: sy2 - sy1 };
        }
        function pZero(n) {
            return n < 10 ? "0" + n : "" + n;
        }
        //to get color list array
        function tc(d) {
            function zc(c, i) {
                //var d = "666666888888aaaaaabbbbbbdddddda32929cc3333d96666e69999f0c2c2b1365fdd4477e67399eea2bbf5c7d67a367a994499b373b3cca2cce1c7e15229a36633cc8c66d9b399e6d1c2f029527a336699668cb399b3ccc2d1e12952a33366cc668cd999b3e6c2d1f01b887a22aa9959bfb391d5ccbde6e128754e32926265ad8999c9b1c2dfd00d78131096184cb05288cb8cb8e0ba52880066aa008cbf40b3d580d1e6b388880eaaaa11bfbf4dd5d588e6e6b8ab8b00d6ae00e0c240ebd780f3e7b3be6d00ee8800f2a640f7c480fadcb3b1440edd5511e6804deeaa88f5ccb8865a5aa87070be9494d4b8b8e5d4d47057708c6d8ca992a9c6b6c6ddd3dd4e5d6c6274878997a5b1bac3d0d6db5a69867083a894a2beb8c1d4d4dae54a716c5c8d8785aaa5aec6c3cedddb6e6e41898951a7a77dc4c4a8dcdccb8d6f47b08b59c4a883d8c5ace7dcce4f991f7bd1487bd1487bd1487bd1487998faa4bcfca4bcfca4bcfca4bcfcf8bb2bfbd75bfbd75bfbd75bfbd75bc47fffdbadffdbadffdbadffdbadffb2b2b2e1e1e1e1e1e1e1e1e1e1e1e110adb746d6d646d6d646d6d646d6d6";
				var d = "666666bbbcbef8f8f8bbbbbbdddddda32929f58d8efeeeefe69999f0c2c2b1365ff8a0d0fef1f8eea2bbf5c7d67a367ae1a0e2fbf1facca2cce1c7e15229a3c28df5f6eefdb399e6d1c2f029527a8dc2e2eef5fb99b3ccc2d1e12952a38dc2f4edf6fd99b3e6c2d1f01b887a77f3ebecfcfb91d5ccbde6e128754e8bdfbdeefaf699c9b1c2dfd00d7813f67ec5feedf788cb8cb8e0ba528800c2ea4bf7fce5b3d580d1e6b388880ee9ea62fdfce8d5d588e6e6b8ab8b00f7dc47fefce5ebd780f3e7b3be6d00fcc954fef9e5f7c480fadcb3b1440ef8b161fff3e7eeaa88f5ccb8865a5ae9c9cafdf7f7d4b8b8e5d4d4705770ddc7dcf9f7fac6b6c6ddd3dd4e5d6cbdcddaf4f8f9b1bac3d0d6db5a6986c9d6e9f8f9fdb8c1d4d4dae54a716cb9ddd9f5f9f8aec6c3cedddb6e6e41dbdaaef9f8f3c4c4a8dcdccb8d6f47ebdbb7fdfaf5d8c5ace7dcce95cb5395cb53f0f7e5f0f7e5f0f7e5f89c35f89c35fef1e1fef1e1fef1e1efa7f9a7a8acf5f5f5fef2fefef2feefa7f9efa7f9fef2fefef2fefef2fe4f991ffad65bfffae77bd1487bd148f8bb2bf3767afbddddfbd75bfbd75b7998fa61e97bf7fce5a4bcfca4bcfcc47fffd2b593f8f5f0dbadffdbadffb2b2b28dccebeef7fce1e1e1e1e1e110adb7ddbc53fbf5e546d6d646d6d610adb7d5de23fafade46d6d646d6d6acc5c2acc5c2f4f5f7f4f5f7f4f5f7acc5c220e0c9defdf8f4f5f7f4f5f7";
                return "#" + d.substring(c * 30 + i * 6, c * 30 + (i + 1) * 6);
            }
            var c = d != null && d != undefined ? d : option.theme;
            return [zc(c, 0), zc(c, 1), zc(c, 2), zc(c, 3)];
        }
        function Tp(temp, dataarry) {
            return temp.replace(/\$\{([\w]+)\}/g, function(s1, s2) { var s = dataarry[s2]; if (typeof (s) != "undefined") { return s; } else { return s1; } });
        }
        function Ta(temp, dataarry) {
            return temp.replace(/\{([\d])\}/g, function(s1, s2) { var s = dataarry[s2]; if (typeof (s) != "undefined") { return encodeURIComponent(s); } else { return ""; } });
        }
        function fomartTimeShow(h) {
            return h < 10 ? "0" + h + ":00" : h + ":00";
        }
        function getymformat(date, comparedate, isshowtime, isshowweek, showcompare) {
            var showyear = isshowtime != undefined ? (date.getFullYear() != new Date().getFullYear()) : true;
            var showmonth = true;
            var showday = true;
            var showtime = isshowtime || false;
            var showweek = isshowweek || false;
            var a = [];
            if (showyear) {
                a.push(i18n.xgcalendar.dateformat.fulldayshow1)
            } else if (showmonth) {
                a.push(i18n.xgcalendar.dateformat.dM3)
            } else if (showday) {
                a.push(i18n.xgcalendar.dateformat.day);
            }
            a.push(showweek ? " (W)" : "", showtime ? " HH:mm" : "");
            return a.join("");
        }
        function CalDateShow(startday, endday, isshowtime, isshowweek) {
            if (!endday) {
                return dateFormat.call(startday, getymformat(startday,null,isshowtime));
            } else {
                var strstart= dateFormat.call(startday, getymformat(startday, null, isshowtime, isshowweek));
				var strend=dateFormat.call(endday, getymformat(endday, startday, isshowtime, isshowweek));
				var join = (strend!=""? " - ":"");
				return [strstart,strend].join(join);
            }
        }
		function CalYearShow(sd){
			return "Year " + sd.getFullYear();	
		}

        function dochange() {
			populate();
        }

        function checkInEr(start, end) {
            var ll = option.loadDateR.length;
			var str = '';
			for(var i = 0; i < ll; i++){
				str += option.loadDateR[i].startdate + '--' + option.loadDateR[i].enddate + ';';
			}
            if (ll == 0) {
                return false;
            }
            var r = false;
            var r2 = false;
            for (var i = 0; i < ll; i++) {
                r = false, r2 = false;
                var dr = option.loadDateR[i];
                if (start >= dr.startdate && start <= dr.enddate) {
                    r = true;
                }
                if (dateFormat.call(start, "yyyyMMdd") == dateFormat.call(dr.startdate, "yyyyMMdd") || dateFormat.call(start, "yyyyMMdd") == dateFormat.call(dr.enddate, "yyyyMMdd")) {
                    r = true;
                }
                if (!end)
                { r2 = true; }
                else {
                    if (end >= dr.startdate && end <= dr.enddate) {
                        r2 = true;
                    }
                    if (dateFormat.call(end, "yyyyMMdd") == dateFormat.call(dr.startdate, "yyyyMMdd") || dateFormat.call(end, "yyyyMMdd") == dateFormat.call(dr.enddate, "yyyyMMdd")) {
                        r2 = true;
                    }
                }
                if (r && r2) {
                    break;
                }
            }
            return r && r2;
        }
		
		function getDateByObj(info){
			var dateObj = {};
			switch(info.proto.type){
				case 4:
					if(info.proto.cpwrap) {
						var _start = DateAdd("d", info.proto.cdi, option.vstart), _end = DateAdd("d", info.proto.cdi, option.vstart);
						dateObj.startdate = formatDate(_start);
						dateObj.enddate = formatDate(_end);
					}else{
						dateObj.startdate = formatDate(info.startTime);
						dateObj.enddate = formatDate(info.endTime);	
					}
					break;
				case 5:
					if(!!info.startTime){
						dateObj.startdate = formatDate(info.startTime);	
					}else{
						dateObj.startdate = '';	
					}
					if(!!info.endTime){
						dateObj.enddate = formatDate(info.endTime);
					}else{
						dateObj.enddate = '';	
					}
					break;		
			}	
			return dateObj;	
		}
		
		function formatDate(str){
			var dateStr = {}, _date = new Date(str.toString());
			dateStr = _date.getDate() + ' ' + months[_date.getMonth()] + ' ' + String(_date.getFullYear()).substr(2);	
			return dateStr;
		}

        function buildtempdayevent(sh, sm, eh, em, h, infoObj, title, w, resize, thindex) {
			var dateObj = getDateByObj(infoObj);
            var theme = thindex != undefined && thindex >= 0 ? tc(thindex) : tc();
            var newtemp = Tp(__SCOLLEVENTTEMP, {
                bdcolor: theme[0],
                bgcolor1: theme[1],
                bgcolor2: theme[2],
                data: "",
                starttime: [pZero(sh), pZero(sm)].join(":"),
                endtime: [pZero(eh), pZero(em)].join(":"),
                content: title ? title : i18n.xgcalendar.new_event,
                title: title ? title : i18n.xgcalendar.new_event,
                icon: "<I class=\"cic cic-tmr\">&nbsp;</I>",
                top: "0px",
                left: "",
                width: w ? w : "100%",
                height: h - 4,
                i: "-1",
                drag: "drag-chip",
                redisplay: resize ? "block" : "none",
				type: infoObj.type ? _typeNames[infoObj.type] : i18n.xgcalendar.event_type,
				course: infoObj.course ? infoObj.course : i18n.xgcalendar.event_course,
				message: infoObj.message,
				author: infoObj.author ? infoObj.author : i18n.xgcalendar.event_author,
				classes: infoObj.classes ? '<DD style=\"BORDER-TOP:1px solid ' + theme[0] + ';\"><SPAN>' + infoObj.classes + '</SPAN></DD>' : i18n.xgcalendar.event_classes,
				startdate: dateObj.startdate,
				enddate: dateObj.enddate
            });
            return newtemp;
        }

        function getdata(chip) {
            var hddata = chip.find("div.dhdV");
            if (hddata.length == 1) {
				var str = hddata.html();	//use 'html' to take place of 'text' to show images.
                return parseED(str.split("$"));
            }
            return null;
        }
        function parseED(data) {
            if (data.length > 6) {
                var e = [];
				e.push(data[0], data[1], new Date(data[2]), new Date(data[3]), parseInt(data[4]), parseInt(data[5]), parseInt(data[6]), data[7] != undefined ? parseInt(data[7]) : -1, data[8] != undefined ? parseInt(data[8]) : 0, data[9], data[10]);
				for(var i = 11; i < data.length; i++){
					e.push(data[i]);
				}
                return e;
            }
            return null;

        }
        function quickd(param) {
            $("#bbit-cs-" + _typeTags[param[9]] + "-buddle").css("visibility", "hidden");
            var calid = $("#bbit-cs-" + _typeTags[param[9]] + "-id").val();
            var param = [{ "name": "calendarId", value: calid },
                        { "name": "type", value: param[9]}];
            var de = rebyKey(calid, true);
            option.onBeforeRequestData && option.onBeforeRequestData(3);
            $.post(option.quickDeleteUrl, param, function(data) {
                if (data) {
                    if (data.IsSuccess) {
                        de = null;
                        option.onAfterRequestData && option.onAfterRequestData(3);
                    }
                    else {
                        option.onRequestDataError && option.onRequestDataError(3, data);
                        Ind(de);
                        render();
                        option.onAfterRequestData && option.onAfterRequestData(3);
                    }
                }
            }, "json");
            render();
        }
		/*
        function getbuddlepos(x, y, h) {
            var tleft = x - 110; 
            var ttop = y - h - 62; 
            var maxLeft = document.documentElement.clientWidth;
            var maxTop = document.documentElement.clientHeight;
            var ishide = false;
			if (tleft < 0 || ttop < 0 || tleft + 532 > maxLeft) {
				ttop = y - 159 <= 0 ? 10 : y - 159;		
				if(tleft < 0){
					tleft = 10;		
				}
                if (tleft + 532 >= maxLeft) {
                    tleft = maxLeft - 542;
                }
				if(ttop + h > maxTop){
					ttop = maxTop - h;	
				}
				ishide = true;
            }
            return { left: tleft, top: ttop, hide: ishide };
        }	*/
		function getbuddlepos(x, y, h) {
            var tleft = x - 266; 
            var ttop = y - h - 10; 
            var maxLeft = document.documentElement.clientWidth;
            var maxTop = document.documentElement.clientHeight;
            var ishide = false;
			if(tleft < 0){
				tleft = 10;		
			}
			if (tleft + 532 >= maxLeft) {
				tleft = maxLeft - 532;
			}
			if(ttop < 0){
				ttop = 10;
				ishide = true;	
			}
			if(ttop + h > maxTop){
				ttop = maxTop - h;
				ishide = true;	
			}
            return { left: tleft, top: ttop, hide: ishide };
        }
        function dayshow(e, data) {
            if (data == undefined) {
                data = getdata($(this));
            }
            if (data != null) {
                if (option.quickDeleteUrl != "" && data[8] == 1 && option.readonly != true) {
					var bud = $('#bbit-cs-' + _typeTags[data[9]] + '-buddle'), 
						prongdiv = $('#bbit-cs-' + _typeTags[data[9]] + '-prong'), 
						stdiv = $('#bbit-cs-' + _typeTags[data[9]] + '-start-time'), 
						etdiv = $('#bbit-cs-' + _typeTags[data[9]] + '-end-time'), 
						typediv = $('#bbit-cs-' + _typeTags[data[9]] + '-type'), 
						titlediv = $('#bbit-cs-' + _typeTags[data[9]] + '-title'),
						messagediv = $('#bbit-cs-' + _typeTags[data[9]] + '-message'), 
						assigndiv = $('#bbit-cs-' + _typeTags[data[9]] + '-assign'), 
						authordiv = $('#bbit-cs-' + _typeTags[data[9]] + '-author');
				
                    if (bud.length == 0) {
						//bud = $(_typeViews[data[9]]).appendTo(document.body).click(function(e){return false});
                        bud = $(_typeViews[data[9]]).appendTo(document.body).click(function(e){
							!$(e.target).hasClass('message-show-div') && e.stopPropagation();
						});
						prongdiv = $('#bbit-cs-' + _typeTags[data[9]] + '-prong'); 
						iddiv = $('#bbit-cs-' + _typeTags[data[9]] + '-id'); 
						stdiv = $('#bbit-cs-' + _typeTags[data[9]] + '-start-time'); 
						etdiv = $('#bbit-cs-' + _typeTags[data[9]] + '-end-time'); 
						typediv = $('#bbit-cs-' + _typeTags[data[9]] + '-type'); 
						titlediv = $('#bbit-cs-' + _typeTags[data[9]] + '-title');
						messagediv = $('#bbit-cs-' + _typeTags[data[9]] + '-message'); 
						assigndiv = $('#bbit-cs-' + _typeTags[data[9]] + '-assign'); 
						authordiv = $('#bbit-cs-' + _typeTags[data[9]] + '-author');
                    }
					//make some changes according to different user account's types
					var _useridstr = $('#userType').val(), _uidarr = _useridstr.split('|');
					if(_uidarr[0] == 2 || _uidarr[1] != data[16]){
						bud.find('.bbit-cs-split').hide();	
					}else{
						bud.find('.bbit-cs-split').show();		
						(data[9] == 'AT') && bud.find('#bbit-cs-at-edit').hide();
						data[9] == 'SV' && bud.find('.bbit-cs-split').hide();		
					}
					var deletebtn = $('#bbit-cs-' + _typeTags[data[9]] + '-delete').unbind('click').click(function() {					
																						if(data[9] == 'Quiz' || data[9] == 'Worksheet' || data[9] == 'Journal' || data[9] == 'PMP' || data[9] == 'EA' || data[9] == 'AT'){
																							var _url;
																							data[12] != '' && (_url = _existTypes[data[9]].Delete.addressC + data[0] + _existTypes[data[9]].Delete.param + data[17]);
																							data[13] != '' && (_url = _existTypes[data[9]].Delete.addressG + data[0] + _existTypes[data[9]].Delete.param + data[17]);
																							data[14] != '' && (_url = _existTypes[data[9]].Delete.addressS + data[0] + _existTypes[data[9]].Delete.param + data[17]);
																							OpenModelWindow(_url,{ width: 816, height: 600, caption:"Delete Event",onclose:freshPage});	
																							bud.css("visibility", "hidden");
																							return false;
																						}else if(data[9] == 'Forum'){
																							var _url = _existTypes[data[9]].Delete.address + _existTypes[data[9]].Delete.param.FSID + data[18] + 
																										_existTypes[data[9]].Delete.param.FTID + data[17],
																										_wname = 'FMWindow';
																							openWindowFS(_url, _wname);
																							bud.css("visibility", "hidden");
																							return false;
																						}else if(data[9] == 'MPG'){
																							var _url = _existTypes[data[9]].Delete.address + data[0] + _existTypes[data[9]].Delete.param.OGID + 
																										data[17] + _existTypes[data[9]].Delete.param.DelType + data[18],
																										_wname = 'MPGWindow';
																							openWindowFS(_url, _wname);
																							bud.css("visibility", "hidden");
																							return false;
																						}else{
																							if (option.DeleteCmdhandler && $.isFunction(option.DeleteCmdhandler)) {
																								option.DeleteCmdhandler.call(this, data, quickd);
																							}
																							else {
																								if (confirm(i18n.xgcalendar.confirm_delete_event + "?")) {
																									var s = 0; //0 single event , 1 for Recurring event
																									if (data[6] == 1) {
																										if (confirm(i18n.xgcalendar.confrim_delete_event_or_all)) {
																											s = 0;
																										}
																										else {
																											s = 1;
																										}
																									}
																									else {
																										s = 0;
																									}
																									quickd(s);
																								}
																							}																								
																						}
																					}), 
						editbtn = $('#bbit-cs-' + _typeTags[data[9]] + '-edit').unbind('click').click(function(e) {
																					if(data[9] == 'Quiz' || data[9] == 'Worksheet' || data[9] == 'Journal' || data[9] == 'PMP' || data[9] == 'EA' || data[9] == 'AT'){
																						if(data[9] == 'AT'){return false;}																																										
																						var _url = _existTypes[data[9]].Edit.address + data[0] + _existTypes[data[9]].Edit.param + data[17]; 
																						OpenModelWindow(_url,{ width: 816, height: 600, caption:"Edit Event",onclose:freshPage});	
																					}else if(data[9] == 'Forum'){
																							var _url = _existTypes[data[9]].Edit.address + _existTypes[data[9]].Edit.param.FSID + data[18] + 
																										_existTypes[data[9]].Edit.param.FTID + data[17],
																										_wname = 'FMWindow';
																							openWindowFS(_url, _wname);
																							bud.css("visibility", "hidden");
																							return false;
																					}else if(data[9] == 'MPG'){
																							var _url = _existTypes[data[9]].Edit.address + data[0] + _existTypes[data[9]].Edit.param.OGID + data[17],
																										_wname = 'MPGWindow';
																							openWindowFS(_url, _wname);
																							bud.css("visibility", "hidden");
																							return false;
																					}else{
																						if (!option.EditCmdhandler) {
																							alert("EditCmdhandler" + i18n.xgcalendar.i_undefined);
																						}
																						else {
																							if (option.EditCmdhandler && $.isFunction(option.EditCmdhandler)) {
																								option.EditCmdhandler.call(this, bud.data("cdata"));
																							}
																						}																								
																					}			
																					bud.css("visibility", "hidden");
																					return false;
																				}), 
						closebtn = $('#bbit-cs-' + _typeTags[data[9]] + '-close').unbind('click').click(function() {
																						bud.css("visibility", "hidden");
																					});

					if(data[9] == 'Resources'){
						var filediv = $('#bbit-cs-' + _typeTags[data[9]] + '-file').unbind('click').click(function(e) {
																						if (!option.ViewCmdhandler) {
																							alert("ViewCmdhandler" + i18n.xgcalendar.i_undefined);
																						}
																						else {
																							var _url = _docroot + '/includes/file.php?type=file&file=' + data[17] + ';' + data[18] + ';' + data[16], _wname = 'RSWindow';
																							openWindowFS(_url, _wname);
																						}
																						bud.css("visibility", "hidden");
																						return false;
																				 }).html(data[19]);
					}else if(data[9] == 'Links'){
						var urldiv = $('#bbit-cs-' + _typeTags[data[9]] + '-url').unbind('click').click(function(e) {
																					if (!option.ViewCmdhandler) {
																						alert("ViewCmdhandler" + i18n.xgcalendar.i_undefined);
																					}
																					else {
																						var _url = data[17], _wname = 'LKWindow';
																						if(_url.indexOf('http') < 0 && _url.indexOf('https') < 0){
																							_url = 'http://' + _url; 	
																						}
																						openWindowFS(_url, _wname);
																					}
																					bud.css("visibility", "hidden");
																					return false;
																				}).html(data[17]);
					}else if(data[9] == 'RL'){
						if(data[17] != ''){
							var topicsdiv = $('#bbit-cs-' + _typeTags[data[9]] + '-topics').empty(), $dl = $('<dl></dl>').css({margin:'0px', padding:'0px'}).appendTo(topicsdiv), _topicArr = data[17].split('|');
							_topicArr.length == 1 ? $("#bbit-cs-"+ _typeTags[data[9]] + "-buddle-topic").html('Topic:') : $("#bbit-cs-"+ _typeTags[data[9]] + "-buddle-topic").html('Topics:');
							for(var i = 0; i < _topicArr.length; i++){
								var _tempArr = _topicArr[i].split(';');
								$dt = $('<dt></dt>').css({'margin':'0px', 'padding':'0px', 'text-decoration':'underline'})
													.attr({'url':_tempArr[0], 'param':_tempArr[1]})
													.unbind('click')
													.click(function(){
														var $me = $(this), _flag = $me.attr('url'), _paramarr = $me.attr('param').split(','), _url, _wname = 'RLWindow';	
														switch(_flag){
															case 'NCPA':
																_url = _docroot + '/courses/preview.php?SyllabusID=' + _paramarr[0] + '&ChapterID=' + _paramarr[1] + '&CTopicID=' + _paramarr[2]
																		 + '&SectionID=' + _paramarr[3] + '&TopicID=' + _paramarr[4] + '&SubTopicID=' + _paramarr[5];
																break;
															case 'NC':
																_url = _docroot + '/courses/preview.php?SyllabusID=' + _paramarr[0] + '&SectionID=' + _paramarr[1] + '&TopicID=' + _paramarr[2] + '&SubTopicID=' + _paramarr[3];
																break;
															case 'MCPV':
																_url = _docroot + '/courses/preview.php?tid=' + _paramarr[0];
																break;
															case 'MCPVA':
																_url = _docroot + '/courses/preview.php?atid=' + _paramarr[0];
																break;
															case 'MCPVT':
																_url = _docroot + '/courses/preview.php?ttid=' + _paramarr[0];
																break;
														}
														openWindowFS(_url, _wname);
														bud.css("visibility", "hidden");
													})
													.html((i + 1) + ". " + _tempArr[2]).appendTo($dl);
							}
						}	
					}else if(data[9] == 'Quiz'){
						if(data[18] == 0 || _uidarr[0] != 2){
							titlediv.unbind('click').click(function(e) {
								if (!option.ViewCmdhandler) {
									alert("ViewCmdhandler" + i18n.xgcalendar.i_undefined);
								}
								else {
									if(_uidarr[0] == 2){
										var _url = _existTypes[data[9]]['SView']['url'] + _existTypes[data[9]]['SView']['param']['CourseID'] + data[11] + _existTypes[data[9]]['SView']['param']['QuizID'] + data[17]
													+ _existTypes[data[9]]['SView']['param']['AssignID'] + data[0];			
									}else{
										var _url = _existTypes[data[9]]['TView']['url'] + _existTypes[data[9]]['TView']['param']['QuizID'] + data[17];	
									}
									var _wname = 'QZWindow';	
									openWindowFS(_url, _wname);
								}
								bud.css("visibility", "hidden");
								return false;
							});		
						}else{
							titlediv.unbind('click');	
						}
					}else if(data[9] == 'Worksheet'){
						if(data[18] == 0 || _uidarr[0] != 2){
							titlediv.unbind('click').click(function(e) {
								if (!option.ViewCmdhandler) {
									alert("ViewCmdhandler" + i18n.xgcalendar.i_undefined);
								}
								else {
									if(_uidarr[0] == 2){
										var _url = _existTypes[data[9]]['SView']['url'] + _existTypes[data[9]]['SView']['param']['CourseID'] + data[11] + _existTypes[data[9]]['SView']['param']['WSID'] + data[17]
													+ _existTypes[data[9]]['SView']['param']['AssignID'] + data[0];			
									}else{
										var _url = _existTypes[data[9]]['TView']['url'] + _existTypes[data[9]]['TView']['param']['CourseID'] + data[11] + _existTypes[data[9]]['TView']['param']['WSID'] + data[17];	
									}
									var _wname = 'WSWindow';		
									openWindowFS(_url, _wname);
								}
								bud.css("visibility", "hidden");
								return false;
							});	
						}else{
							titlediv.unbind('click');	
						}
					}else if(data[9] == 'Journal'){
						if(data[18] == 0 || _uidarr[0] != 2){
							titlediv.unbind('click').click(function(e) {
								if (!option.ViewCmdhandler) {
									alert("ViewCmdhandler" + i18n.xgcalendar.i_undefined);
								}
								else {	
									if(_uidarr[0] == 2){
										var _url = _existTypes[data[9]]['SView']['url'] + _existTypes[data[9]]['SView']['param']['CourseID'] + data[11] + _existTypes[data[9]]['SView']['param']['JournalID'] + data[17]
													+ _existTypes[data[9]]['SView']['param']['AssignID'] + data[0];			
									}else{
										var _url = _existTypes[data[9]]['TView']['url'] + _existTypes[data[9]]['TView']['param']['JournalID'] + data[17];	
									}
									var _wname = 'JNWindow';
									openWindowFS(_url, _wname);
								}
								bud.css("visibility", "hidden");
								return false;
							});	
						}else{
							titlediv.unbind('click');	
						}						
					}else if(data[9] == 'PMP'){
						if(data[19] == 0 || _uidarr[0] != 2){
							titlediv.unbind('click').click(function(e) {
								if (!option.ViewCmdhandler) {
									alert("ViewCmdhandler" + i18n.xgcalendar.i_undefined);
								}
								else {	
									if(_uidarr[0] == 2){
										var _url = _existTypes[data[9]]['SView']['url'] + _existTypes[data[9]]['SView']['param']['CourseID'] + data[11] + _existTypes[data[9]]['SView']['param']['AssignID'] + data[0];
									}else{
										var _url = _existTypes[data[9]]['TView']['url'] + _existTypes[data[9]]['TView']['param']['Z'] + data[18];	
									}
									var _wname = 'PMPWindow';		
									openWindowFS(_url, _wname);
								}
								bud.css("visibility", "hidden");
								return false;
							});		
						}else{
							titlediv.unbind('click');	
						}						
					}else if(data[9] == 'EA'){
						if(data[20] == 0 || _uidarr[0] != 2){
							titlediv.unbind('click').click(function(e) {
								if (!option.ViewCmdhandler) {
									alert("ViewCmdhandler" + i18n.xgcalendar.i_undefined);
								}
								else {
									if(_uidarr[0] == 2){
										var _url = _existTypes[data[9]]['SView']['url'] + _existTypes[data[9]]['SView']['param']['CourseID'] + data[11] + _existTypes[data[9]]['SView']['param']['EAID'] + data[17]
													+ _existTypes[data[9]]['SView']['param']['AssignID'] + data[0] + _existTypes[data[9]]['SView']['param']['AID'] + data[18];			
									}else{
										var _catageryarr = data[19].split(',');
										var _url = _existTypes[data[9]]['TView']['url'] + _existTypes[data[9]]['TView']['param']['TypeID'] + 'EA' + _existTypes[data[9]]['TView']['param']['AID'] + data[18]
													+ _existTypes[data[9]]['TView']['param']['SectionID'] + _catageryarr[0] + _existTypes[data[9]]['TView']['param']['TopicID'] + _catageryarr[1]
													+ _existTypes[data[9]]['TView']['param']['SubTopicID'] + _catageryarr[2];	
									}
									var _wname = 'EAWindow';		
									openWindowFS(_url, _wname);
								}
								bud.css("visibility", "hidden");
								return false;
							});	
						}else{
							titlediv.unbind('click');	
						}						
					}else if(data[9] == 'AT'){
						if(data[20] == 0 || _uidarr[0] != 2){
							titlediv.unbind('click').click(function(e) {
								if (!option.ViewCmdhandler) {
									alert("ViewCmdhandler" + i18n.xgcalendar.i_undefined);
								}
								else {
									if(_uidarr[0] == 2){
										var _url = _existTypes[data[9]]['SView']['url'] + _existTypes[data[9]]['SView']['param']['TypeID'] + 'Videos' + _existTypes[data[9]]['SView']['param']['VID'] + data[18];			
									}else{
										var _catageryarr = data[19].split(',');
										var _url = _existTypes[data[9]]['TView']['url'] + _existTypes[data[9]]['TView']['param']['TypeID'] + 'Videos' + _existTypes[data[9]]['TView']['param']['VID'] + data[18]
													+ _existTypes[data[9]]['TView']['param']['SectionID'] + _catageryarr[0] + _existTypes[data[9]]['TView']['param']['TopicID'] + _catageryarr[1]
													+ _existTypes[data[9]]['TView']['param']['SubTopicID'] + _catageryarr[2];	
									}
									var _wname = 'ATWindow';		
									openWindowFS(_url, _wname);
								}
								bud.css("visibility", "hidden");
								return false;
							});	
						}else{
							titlediv.unbind('click');	
						}						
					}else if(data[9] == 'SV'){
						if(data[17] == 0 || _uidarr[0] != 2){
							titlediv.unbind('click').click(function(e) {
								if (!option.ViewCmdhandler) {
									alert("ViewCmdhandler" + i18n.xgcalendar.i_undefined);
								}
								else {
									if(_uidarr[0] == 2){
										var _url = _existTypes[data[9]]['SView']['url'] + data[14];			
									}else{
										var _url = _existTypes[data[9]]['TView']['url'] + data[14];			
									}
									var _wname = 'SVWindow';	
									openWindowFS(_url, _wname);
								}
								bud.css("visibility", "hidden");
								return false;
							});	
						}else{
							titlediv.unbind('click');	
						}						
					}else if(data[9] == 'Forum'){
						if(data[19] == 0 || _uidarr[0] != 2){
							titlediv.unbind('click').click(function(e) {
								if (!option.ViewCmdhandler) {
									alert("ViewCmdhandler" + i18n.xgcalendar.i_undefined);
								}
								else {
									var _url = _existTypes[data[9]]['SView']['url'] + _existTypes[data[9]]['SView']['param']['FSID'] + data[18] + _existTypes[data[9]]['SView']['param']['FTID'] + data[17];			
									var _wname = 'FMWindow';	
									openWindowFS(_url, _wname);
								}
								bud.css("visibility", "hidden");
								return false;
							});	
						}else{
							titlediv.unbind('click');	
						}						
					}else if(data[9] == 'MPG'){
						if(data[20] == 0 || _uidarr[0] != 2){
							titlediv.unbind('click').click(function(e) {
								if (!option.ViewCmdhandler) {
									alert("ViewCmdhandler" + i18n.xgcalendar.i_undefined);
								}
								else {
									if(_uidarr[0] == 2){
										var _url = _existTypes[data[9]]['SView']['url'] + data[0] + _existTypes[data[9]]['SView']['param']['GameID'] + data[17] + 
													_existTypes[data[9]]['SView']['param']['ScenarioID'] + data[19];					
									}else{
										var _url = _existTypes[data[9]]['TView']['url'] + parseInt(data[0].split(',')[0], 10);			
									}
									var _wname = 'MPGWindow';	
									openWindowFS(_url, _wname);
								}
								bud.css("visibility", "hidden");
								return false;
							});	
						}else{
							titlediv.unbind('click');	
						}						
					}					
                    var st = [], et = [];
					st.push(formatDate(data[2]));
					et.push(formatDate(data[3]));
					data[4] != 1 && (st.push(", ", dateFormat.call(data[2], "HH:mm")), et.push(", ", dateFormat.call(data[3], "HH:mm")));
                    stdiv.html(st.join("")); 
					etdiv.html(et.join(""));
					var typestr = data[11] ? (_typeNames[data[9]] + '&nbsp;(' + data[11] + ')') : _typeNames[data[9]]; 
					typediv.html(typestr);
					titlediv.html(data[1]);
					if(_uidarr[0] == 2){
						if(data[9] == "Quiz" || data[9] == "Worksheet" || data[9] == "Journal"){
							data[18] == 1 ? titlediv.css("color", "#666666") : titlediv.css("color", "#112abb");	
						}else if(data[9] == "PMP" || data[9] == "Forum"){
							data[19] == 1 ? titlediv.css("color", "#666666") : titlediv.css("color", "#112abb");		
						}else if(data[9] == "EA" || data[9] == "AT"){
							data[20] == 1 ? titlediv.css("color", "#666666") : titlediv.css("color", "#112abb");		
						}else if(data[9] == "SV"){
							data[17] == 1 ? titlediv.css("color", "#666666") : titlediv.css("color", "#112abb");		
						}		
					}					
					data[9] == 'Links' ? titlediv.attr({title:data[1]}) : (data[9] == 'AT' ? titlediv.attr({title:"View Lesson"}) : titlediv.removeAttr('title'));	
					messagediv.html(data[10]);
					data[10] == '' ? bud.find('.bbit-cs-message').hide() : bud.find('.bbit-cs-message').show();
					//construct the string of assign content accoding to the assign type
					var as = [];
					if(data[12] != ''){
						as.push('<dl style="margin:0px; padding:0px;">');
						var _aa = data[12].split(',');
						_aa.sort();	
						as.push('<dt style="margin:0px; padding:0px; text-decoration:underline;">', _aa.length > 1 ? 'Classes' : 'Class', '</dt>');
						for(var i = 0; i < _aa.length; i++){
							as.push('<dt style="margin:0px; padding:0px;">&ndash;&ensp;', _aa[i], '</dt>');	
						}
						as.push('</dl>');	
					}
					if(data[13] != ''){
						as.push('<dl style="margin:0px; padding:0px;">');
						var _aa = data[13].split(',');
						_aa.sort();	
						as.push('<dt style="margin:0px; padding:0px; text-decoration:underline;">', _aa.length > 1 ? 'Groups' : 'Group', '</dt>');
						var _tpitem = [];
						for(var i = 0; i < _aa.length; i++){
							_tpitem = _aa[i].split('|');
							as.push('<dt style="margin:0px; padding:0px;">&ndash;&ensp;', _tpitem[1], '</dt>');	
						}
						as.push('</dl>');	
					}
					if(data[9] != 'SV' && data[14] != ''){
						as.push('<dl style="margin:0px; padding:0px;">');
						var _aa = data[14].split(',');
						_aa.sort();	
						as.push('<dt style="margin:0px; padding:0px; text-decoration:underline;">', _aa.length > 1 ? 'Students' : 'Student', '</dt>');
						var _tpitem = [];
						for(var i = 0; i < _aa.length; i++){
							_tpitem = _aa[i].split('|');
							as.push('<dt style="margin:0px; padding:0px;">&ndash;&ensp;', _tpitem[1], '</dt>');	
						}
						as.push('</dl>');	
					}
					assigndiv.html(as.join(""));
					_uidarr[0] == 2 && bud.find(".bbit-cs-assignto").hide(); 
					authordiv.html(data[15]);
                    iddiv.val(data[0]);
                    bud.data("cdata", data);
					
                    var pos = getbuddlepos(e.pageX, e.pageY, bud.height());
					var _left = e.pageX - 10 < pos.left ? 0 : e.pageX - 10 - pos.left;							
					prongdiv.children().each(function(e){
						$(this).hasClass("t-b") && $(this).css("left", _left);	
					});
                    if (pos.hide) {
                        prongdiv.hide()
                    }
                    else {
                        prongdiv.show()
                    }
					//hide all buds first.
					for(var p in _typeTags){
						var $tpbud = $("#bbit-cs-" + _typeTags[p] + "-buddle");	
						$tpbud.length > 0 && $tpbud.css("visibility", "hidden");
					}
                    bud.css({ "visibility": "visible", left: pos.left, top: pos.top });

                    $(document).one("click", function() {
                        bud.css("visibility", "hidden");
                    });
                }
                else {
                    if (!option.ViewCmdhandler) {
                        alert("ViewCmdhandler" + i18n.xgcalendar.i_undefined);
                    }
                    else {
                        if (option.ViewCmdhandler && $.isFunction(option.ViewCmdhandler)) {
                            option.ViewCmdhandler.call(this, data);
                        }
                    }
                }
            }
            else {
                alert(i18n.xgcalendar.data_format_error);
            }
            return false;
        }

        function moreshow(mv) {
            var me = $(this);
            var divIndex = mv.id.split('_')[1];
            var pdiv = $(mv);
			var rc = pdiv.parent().children().length;
            var offsetMe = me.position();
            var offsetP = pdiv.position();
            var width = (me.width() + 2) * 1.5;
            var top = offsetP.top + 15;
            var left = offsetMe.left;
			
            var daystr = this.abbr;
            var arrdays = daystr.split('/');	//eg: 11/26/2012
            var day = new Date(arrdays[2], parseInt(arrdays[0] - 1), arrdays[1]);
            var cc = $("#cal-month-cc").css({top:0, height:"auto", "max-height":pdiv.height() * rc, display:"none"});
			cc.find(".cc-body").css({height:"auto"});
			//fill values into moreshow.
            var ccontent = $("#cal-month-cc-content table tbody");
            var ctitle = $("#cal-month-cc-title");
            ctitle.html(dateFormat.call(day, i18n.xgcalendar.dateformat.dM));
			ccontent.empty();
            //var c = tc()[2];
            var edata = $("#gridEvent").data("mvdata");
            var events = edata[divIndex];
            var index = parseInt(this.axis);
            var htm = [];
            for (var i = 0; i <= index; i++) {
                var ec = events[i] ? events[i].length : 0;
                for (var j = 0; j < ec; j++) {
                    var e = events[i][j];
                    if (e) {
                        if ((e.colSpan + i - 1) >= index) {
                            htm.push("<tr><td class='st-c'>");
                            htm.push(BuildMonthDayEvent(e, day, 1, 25));
                            htm.push("</td></tr>");
                        }
                    }
                }
            }
            ccontent.html(htm.join(""));
            //click
            ccontent.find("div.rb-o").each(function(i) {
                $(this).click(dayshow);
            });

            edata = events = null;
            var height = cc.height() > pdiv.height() * rc ? pdiv.height() * rc : ((cc.height() + 4 > pdiv.height() * rc) ? pdiv.height() * rc : (cc.height() + 4));
            var maxleft = document.documentElement.clientWidth;
            var maxtop = document.documentElement.clientHeight;
			//ajust the position of cc
			if (left + width >= maxleft) {
                left = (height == pdiv.height() * rc) ? (maxleft - width - 15) : (maxleft - width - 15);
            }
			if (top + height >= pdiv.height() * rc + 15) {
                top = pdiv.height() * rc + 15 - height - 5;
            }
			var newOff = { left: left, top: top, "z-index": 180, width: width, height: height, "display": "block" };
            cc.css(newOff);
			cc.find(".cc-body").css({height:height-24});
            $(document).one("click", closeCc);
            return false;
        }
        function dayupdate(data, start, end) {
            if (option.quickUpdateUrl != "" && data[8] == 1 && option.readonly != true) {
                if (option.isloading) {
                    return false;
                }
                option.isloading = true;
                var id = data[0];
                var os = data[2];
                var od = data[3];
                var zone = new Date().getTimezoneOffset() / 60 * -1;
                var param = [{ "name": "type", value: data[9] },
							 { "name": "calendarId", value: id },
							 { "name": "CalendarStartTime", value: dateFormat.call(start, i18n.xgcalendar.dateformat.fulldayvalue + " HH:mm") },
							 { "name": "CalendarEndTime", value: dateFormat.call(end, i18n.xgcalendar.dateformat.fulldayvalue + " HH:mm") },
							 { "name": "timezone", value: zone }
						   ];
                var d;
                if (option.quickUpdateHandler && $.isFunction(option.quickUpdateHandler)) {
                    option.quickUpdateHandler.call(this, param);
                }
                else {
                    option.onBeforeRequestData && option.onBeforeRequestData(4);
                    $.post(option.quickUpdateUrl, param, function(data) {
                        if (data) {
                            if (data.IsSuccess == true) {
                                option.isloading = false;
                                option.onAfterRequestData && option.onAfterRequestData(4);
                            }
                            else {
                                option.onRequestDataError && option.onRequestDataError(4, data);
                                option.isloading = false;									
                                d = rebyKey(id, true);
                                d[2] = os;
                                d[3] = od;
                                Ind(d);
                                render();
                                d = null;
                                option.onAfterRequestData && option.onAfterRequestData(4);
                            }
                        }
                    }, "json");					
                    d = rebyKey(id, true);
                    if (d) {
                        d[2] = start;
                        d[3] = end;
                    }
                    Ind(d);
                    render();
                }
            }
        }
        function quickadd(start, end, isallday, pos) {
            if ((!option.quickAddHandler && option.quickAddUrl == "") || option.readonly) {
                return;
            }
            var buddle = $("#bbit-cal-buddle");
            if (buddle.length == 0) {
                var temparr = [];
                temparr.push('<div id="bbit-cal-buddle" style="z-index: 180; width: 400px;visibility:hidden;" class="bubble">');
                temparr.push('<table class="bubble-table" cellSpacing="0" cellPadding="0"><tbody><tr><td class="bubble-cell-side"><div id="tl1" class="bubble-corner"><div class="bubble-sprite bubble-tl"></div></div>');
                temparr.push('<td class="bubble-cell-main"><div class="bubble-top"></div><td class="bubble-cell-side"><div id="tr1" class="bubble-corner"><div class="bubble-sprite bubble-tr"></div></div>  <tr><td class="bubble-mid" colSpan="3"><div style="overflow: hidden" id="bubbleContent1"><div><div></div><div class="cb-root">');
				temparr.push('<table class="cb-table" cellSpacing="0" cellPadding="0"><tbody>');
				temparr.push('<tr><td><div class="cb-tips">', i18n.xgcalendar.tips_create, '</div></td></tr>');
				temparr.push('<tr><td><div><div id="cre-Announcement" class="cb-tube-up">Announcement</div><div id="cre-RL" class="cb-tube-up">Reading Assignment</div></div></td></tr>');
				temparr.push('<tr><td><div><div id="cre-Links" class="cb-tube-up">Link</div><div id="cre-Resources" class="cb-tube-up">Resource</div></div></td></tr>');
				temparr.push('<tr><td><div style="border-top:2px solid #88a0be;"><div id="cre-Quiz" class="cb-tube-down">Quiz</div><div id="cre-Worksheet" class="cb-tube-down">Worksheet</div><div id="cre-Journal" class="cb-tube-down">Journal</div><div id="cre-PMP" class="cb-tube-down">PMP</div><div id="cre-EA" class="cb-tube-down">EA</div></div></td></tr>');
				temparr.push('</tbody></table>');
				temparr.push('<input id="bbit-cal-start" type="hidden"/><input id="bbit-cal-end" type="hidden"/><input id="bbit-cal-allday" type="hidden"/>');
				temparr.push('<tr><td><div id="bl1" class="bubble-corner"><div class="bubble-sprite bubble-bl"></div></div><td><div class="bubble-bottom"></div><td><div id="br1" class="bubble-corner"><div class="bubble-sprite bubble-br"></div></div></tr></tbody></table><div id="bubbleClose1" class="bubble-closebutton"></div><div id="prong2" class="prong"><div class=bubble-sprite></div></div></div>');
				
                var tempquickAddHanler = temparr.join("");
                temparr = null;
                $(document.body).append(tempquickAddHanler);
                buddle = $("#bbit-cal-buddle");
                var closebtn = $("#bubbleClose1").click(function() {
                    $("#bbit-cal-buddle").css("visibility", "hidden");
                    realsedragevent();
                });
                
                buddle.mousedown(function(e) { return false });
				buddle.find('.cb-tube-up').mouseenter(function(){
											$(this).addClass('btn-on');
										}).mouseleave(function(){
											$(this).removeClass('btn-on');	
										}).each(function(){
											$(this).click(function(){													
													var _tp = $(this).attr('id').slice(4),
													_url = _existTypes[_tp].Create.url, _option = _existTypes[_tp].Create.option;
													OpenModelWindow(_url, _option);
									                $("#bbit-cal-buddle").css("visibility", "hidden");
													realsedragevent();
												});
										});
				buddle.find('.cb-tube-down').mouseenter(function(){
											$(this).addClass('btn-on');
										}).mouseleave(function(){
											$(this).removeClass('btn-on');	
										}).each(function(){
											$(this).click(function(){													
													var _tp = $(this).attr('id').slice(4),
													_url = _existTypes[_tp].Create.address, _wname = _typeTags[_tp] + 'window';
													openWindowFS(_url, _wname);
									                $("#bbit-cal-buddle").css("visibility", "hidden");
													realsedragevent();
												});
										});
										
            }
			
            
            
            $("#bbit-cal-allday").val(isallday ? "1" : "0");
            $("#bbit-cal-start").val(dateFormat.call(start, i18n.xgcalendar.dateformat.fulldayvalue + " HH:mm"));
            $("#bbit-cal-end").val(dateFormat.call(end, i18n.xgcalendar.dateformat.fulldayvalue + " HH:mm"));
			var off = getbuddlepos(pos.left, pos.top, buddle.height());
            if (off.hide) {
                $("#prong2").hide()
            }
            else {
                $("#prong2").show()
            }
			var _url = _existTypes['New'].Create.url, _option = _existTypes['New'].Create.option, param = 'sdt=' + $("#bbit-cal-start").val() + '&edt=' + $("#bbit-cal-end").val(), _url = _url + '?' + param;
			OpenModelWindow(_url, _option);
			$("#bbit-cal-buddle").css("visibility", "hidden");
			realsedragevent();			
            $(document).one("mousedown", function() {
                $("#bbit-cal-buddle").css("visibility", "hidden");
                realsedragevent();
            });
            return false;
        }
        //format datestring to Date Type
        function strtodate(str) {

            var arr = str.split(" ");
            var arr2 = arr[0].split(i18n.xgcalendar.dateformat.separator);
            var arr3 = arr[1].split(":");

            var y = arr2[i18n.xgcalendar.dateformat.year_index];
            var m = arr2[i18n.xgcalendar.dateformat.month_index].indexOf("0") == 0 ? arr2[i18n.xgcalendar.dateformat.month_index].substr(1, 1) : arr2[i18n.xgcalendar.dateformat.month_index];
            var d = arr2[i18n.xgcalendar.dateformat.day_index].indexOf("0") == 0 ? arr2[i18n.xgcalendar.dateformat.day_index].substr(1, 1) : arr2[i18n.xgcalendar.dateformat.day_index];
            var h = arr3[0].indexOf("0") == 0 ? arr3[0].substr(1, 1) : arr3[0];
            var n = arr3[1].indexOf("0") == 0 ? arr3[1].substr(1, 1) : arr3[1];
            return new Date(y, parseInt(m) - 1, d, h, n);
        }

        function rebyKey(key, remove) {
            if (option.eventItems && option.eventItems.length > 0) {
                var sl = option.eventItems.length;
                var i = -1;
                for (var j = 0; j < sl; j++) {
                    if (option.eventItems[j][0] == key) {
                        i = j;
                        break;
                    }
                }
                if (i >= 0) {
                    var t = option.eventItems[i];
                    if (remove) {
                        option.eventItems.splice(i, 1);
                    }
                    return t;
                }
            }
            return null;
        }
        function Ind(event, i) {
            var d = 0;
            if (!i) {
                if (option.eventItems && option.eventItems.length > 0) {
                    var sl = option.eventItems.length;
                    var s = event[2];
                    var d1 = s.getTime() - option.eventItems[0][2].getTime();
                    var d2 = option.eventItems[sl - 1][2].getTime() - s.getTime();
                    var diff = d1 - d2;
                    if (d1 < 0 || diff < 0) {
                        for (var j = 0; j < sl; j++) {
                            if (option.eventItems[j][2] >= s) {
                                i = j;
                                break;
                            }
                        }
                    }
                    else if (d2 < 0) {
                        i = sl;
                    }
                    else {
                        for (var j = sl - 1; j >= 0; j--) {
                            if (option.eventItems[j][2] < s) {
                                i = j + 1;
                                break;
                            }
                        }
                    }
                }
                else {
                    i = 0;
                }
            }
            else {
                d = 1;
            }
            if (option.eventItems && option.eventItems.length > 0) {
                if (i == option.eventItems.length) {
                    option.eventItems.push(event);
                }
                else { option.eventItems.splice(i, d, event); }
            }
            else {
                option.eventItems = [event];
            }
            return i;
        }
        
        
        function ResizeView() {
            var _MH = document.documentElement.clientHeight;
            var _viewType = option.view;
            if (_viewType == "day" || _viewType == "week") {
                var $dvwkcontaienr = $("#dvwkcontaienr");
                var $dvtec = $("#dvtec");
                if ($dvwkcontaienr.length == 0 || $dvtec.length == 0) {
                    alert(i18n.xgcalendar.view_no_ready); return;
                }
                if (typeof (option.scoll) == "undefined") {
                    var currentday = new Date();
                    var h = currentday.getHours();
                    var m = currentday.getMinutes();
                    var th = gP(h, m);
                    var ch = $dvtec.attr("clientHeight");
                    var sh = th - 0.5 * ch;
                    var ph = $dvtec.attr("scrollHeight");
                    if (sh < 0) sh = 0;
                    if (sh > ph - ch) sh = ph - ch - 10 * (23 - h);
                    $dvtec.attr("scrollTop", sh);
                }
                else {
                    $dvtec.attr("scrollTop", option.scoll);
                }
            }
            else if (_viewType == "month") {
                //Resize GridContainer
            }
        }
        function returnfalse() {
            return false;
        }
        function initevents(viewtype) {
			var _useridstr = $('#userType').val(), _uidarr = _useridstr.split('|');
            if (viewtype == "week" || viewtype == "day") {
                $("div.chip", gridcontainer).each(function(i) {
                    var chip = $(this);
                    chip.click(dayshow);
					chip.find("div.resizer").css({'visibility':'hidden'});
					chip.mousedown(returnfalse);
                });
                $("div.rb-o", gridcontainer).each(function(i) {	//alldayevent bind handler here.
                    var chip = $(this);
                    chip.click(dayshow);
					chip.mousedown(returnfalse);
                });
                if (option.readonly == false) {
                    $("td.tg-col", gridcontainer).each(function(i) {
						if(_uidarr[0] != 2){
	                        $(this).mousedown(function(e) { dragStart.call(this, "dw1", e); return false; });							
						}
                    });
					if(_uidarr[0] != 2){
	                    $("#weekViewAllDaywk").mousedown(function(e) { dragStart.call(this, "dw2", e); return false; });
					}
                }

                if (viewtype == "week") {
                    $("#dvwkcontaienr th.gcweekname").each(function(i) {
                        $(this).click(weekormonthtoday);
                    });
                }
            }else if (viewtype == "month") {
                $("div.rb-o", gridcontainer).each(function(i) {
                    var chip = $(this);
                    chip.click(dayshow);
					var _data = getdata(chip);
                    if (chip.hasClass("drag") && _uidarr[0] != 2 && _uidarr[1] == _data[16]) {
                        //drag;
                        chip.mousedown(function(e) { dragStart.call(this, "m2", e); return false; });
                    }
                    else {
                        chip.mousedown(returnfalse)
                    }
                });
                $("td.st-more", gridcontainer).each(function(i) {

                    $(this).click(function(e) {
                        moreshow.call(this, $(this).parent().parent().parent().parent()[0]); return false;
                    }).mousedown(function() { return false; });
                });
                if (option.readonly == false) {
					if(_uidarr[0] != 2){
	                    $("#mvEventContainer").mousedown(function(e) { dragStart.call(this, "m1", e); return false; });
					}
                }
				$("td.st-dtitle", gridcontainer).each(function(){
					$(this).click(function(e){
						var source = e.srcElement || e.target;
						if($(source).hasClass("monthdayshow")){
							weekormonthtoday.call($(source).parent()[0],e);	
						}	
					});	
				});
            }else if(viewtype == "year"){
				$("td.yv-detail", gridcontainer).each(function(){
					$(this).click(function(e){
						var source = e.srcElement || e.target;                       
						if ($(source).hasClass("monthdayshow")){	
							yeartomonth.call($(source).parent()[0],e);
						}		
					});	
				});	
			}

        }
        function realsedragevent() {
            if (_dragevent) {
                _dragevent();
                _dragevent = null;
            }
        }
        function dragStart(type, e) {
            var obj = $(this);
            var source = e.srcElement || e.target;
            realsedragevent();
            switch (type) {
                case "dw1": 
                    _dragdata = { type: 1, target: obj, sx: e.pageX, sy: e.pageY };
                    break;
                case "dw2": 
                    var w = obj.width();
                    var h = obj.height();
                    var offset = obj.offset();
                    var left = offset.left;
                    var top = offset.top;
                    var l = option.view == "day" ? 1 : 7;
                    var py = w % l;
                    var pw = parseInt(w / l);
                    if (py > l / 2 + 1) {
                        pw++;
                    }
                    var xa = [];
                    var ya = [];
                    for (var i = 0; i < l; i++) {
                        xa.push({ s: i * pw + left, e: (i + 1) * pw + left });
                    }
                    ya.push({ s: top, e: top + h });
                    _dragdata = { type: 2, target: obj, sx: e.pageX, sy: e.pageY, pw: pw, xa: xa, ya: ya, h: h };
                    w = left = l = py = pw = xa = null;
                    break;
                case "dw3": //change day time in week or day view.
                    var evid = obj.parent().attr("id").replace("tgCol", "");
                    var p = obj.parent();
                    var pos = p.offset();
                    var w = p.width() + 10;
                    var h = obj.height();
                    var data = getdata(obj);
                    _dragdata = { type: 4, target: obj, sx: e.pageX, sy: e.pageY,
                        pXMin: pos.left, pXMax: pos.left + w, pw: w, h: h,
                        cdi: parseInt(evid), fdi: parseInt(evid), data: data
                    };
                    break;
                case "dw4": //change hour and minutes time in the same day by being resized;
                    var h = obj.height();
                    var data = getdata(obj);
                    _dragdata = { type: 5, target: obj, sx: e.pageX, sy: e.pageY, h: h, data: data };
                    break;
                case "dw5":
                    var con = $("#weekViewAllDaywk");
                    var w = con.width();
                    var h = con.height();
                    var offset = con.offset();
                    var moffset = obj.offset();
                    var left = offset.left;
                    var top = offset.top;
                    var l = 7;
                    var py = w % l;
                    var pw = parseInt(w / l);
                    if (py > l / 2 + 1) {
                        pw++;
                    }
                    var xa = [];
                    var ya = [];
                    var di = 0;
                    for (var i = 0; i < l; i++) {
                        xa.push({ s: i * pw + left, e: (i + 1) * pw + left });
                        if (moffset.left >= xa[i].s && moffset.left < xa[i].e) {
                            di = i;
                        }
                    }
                    var fdi = { x: di, y: 0, di: di };
                    ya.push({ s: top, e: top + h });
                    var data = getdata(obj);
                    var dp = DateDiff("d", data[2], data[3]) + 1;
                    _dragdata = { type: 6, target: obj, sx: e.pageX, sy: e.pageY, data: data, xa: xa, ya: ya, fdi: fdi, h: h, dp: dp, pw: pw };
                    break;
                case "m1": 
                    var w = obj.width();
                    var offset = obj.offset();
                    var left = offset.left;
                    var top = offset.top;
                    var l = 7;
                    var yl = obj.children().length;
                    var py = w % l;
                    var pw = parseInt(w / l);
                    if (py > l / 2 + 1) {
                        pw++;
                    }
                    var h = $("#mvrow_0").height();
                    var xa = [];
                    var ya = [];
                    for (var i = 0; i < l; i++) {
                        xa.push({ s: i * pw + left, e: (i + 1) * pw + left });
                    }
                    var xa = [];
                    var ya = [];
                    for (var i = 0; i < l; i++) {
                        xa.push({ s: i * pw + left, e: (i + 1) * pw + left });
                    }
                    for (var i = 0; i < yl; i++) {
                        ya.push({ s: i * h + top, e: (i + 1) * h + top });
                    }
                    _dragdata = { type: 3, target: obj, sx: e.pageX, sy: e.pageY, pw: pw, xa: xa, ya: ya, h: h };
                    break;
                case "m2": 
                    var row0 = $("#mvrow_0");
                    var row1 = $("#mvrow_1");
                    var w = row0.width();
                    var offset = row0.offset();
                    var diffset = row1.offset();
                    var moffset = obj.offset();
                    var h = diffset.top - offset.top;
                    var left = offset.left;
                    var top = offset.top;
                    var l = 7;
                    var yl = row0.parent().children().length;
                    var py = w % l;
                    var pw = parseInt(w / l);
                    if (py > l / 2 + 1) {
                        pw++;
                    }
                    var xa = [];
                    var ya = [];
                    var xi = 0;
                    var yi = 0;
                    for (var i = 0; i < l; i++) {
                        xa.push({ s: i * pw + left, e: (i + 1) * pw + left });
                        if (moffset.left >= xa[i].s && moffset.left < xa[i].e) {
                            xi = i;
                        }
                    }
                    for (var i = 0; i < yl; i++) {
                        ya.push({ s: i * h + top, e: (i + 1) * h + top });
                        if (moffset.top >= ya[i].s && moffset.top < ya[i].e) {
                            yi = i;
                        }
                    }
                    var fdi = { x: xi, y: yi, di: yi * 7 + xi };
                    var data = getdata(obj);
                    var dp = DateDiff("d", data[2], data[3]) + 1;
                    _dragdata = { type: 7, target: obj, sx: e.pageX, sy: e.pageY, data: data, xa: xa, ya: ya, fdi: fdi, h: h, dp: dp, pw: pw };
                    break;
            }
            $('body').noSelect();
        }
		function getInfoObj($data){
			var obj = {};
			obj.proto = $data;
			if(!!$data.data){
				obj.startTime = $data.data[2];
				obj.endTime = $data.data[3];
				obj.type = $data.data[9];
				obj.message = $data.data[10];
				obj.course = $data.data[11];
				obj.classes = $data.data[12];
				obj.author = $data.data[15];							
			}
			return obj;
		}
        function dragMove(e) {
            if (_dragdata) {
                if (e.pageX < 0 || e.pageY < 0
					|| e.pageX > document.documentElement.clientWidth
					|| e.pageY >= document.documentElement.clientHeight) {
                    dragEnd(e);
                    return false;
                }
                var d = _dragdata, info = getInfoObj(d);
				var _useridstr = $('#userType').val(), _uidarr = _useridstr.split('|');
                switch (d.type) {
                    case 1:
                        var sy = d.sy;
                        var y = e.pageY;
                        var diffy = y - sy;
                        if (diffy > 11 || diffy < -11 || d.cpwrap) {
                            if (diffy == 0) { diffy = 21; }
                            var dy = diffy % 21;
                            if (dy != 0) {
                                diffy = dy > 0 ? diffy + 21 - dy : diffy - 21 - dy;
                                y = d.sy + diffy;
                                if (diffy < 0) {
                                    sy = sy + 21;
                                }
                            }
                            if (!d.tp) {
                                d.tp = $(d.target).offset().top;
                            }
                            var gh = gH(sy, y, d.tp);
                            var ny = gP(gh.sh, gh.sm);
                            var tempdata;
                            if (!d.cpwrap) {
                                tempdata = buildtempdayevent(gh.sh, gh.sm, gh.eh, gh.em, gh.h, info);
                                var cpwrap = $("<div class='ca-evpi drag-chip-wrapper' style='top:" + ny + "px'/>").html(tempdata);
                                $(d.target).find("div.tg-col-overlaywrapper").append(cpwrap);
                                d.cpwrap = cpwrap;
                            }
                            else {
                                if (d.cgh.sh != gh.sh || d.cgh.eh != gh.eh || d.cgh.sm != gh.sm || d.cgh.em != gh.em) {
                                    tempdata = buildtempdayevent(gh.sh, gh.sm, gh.eh, gh.em, gh.h, info);
                                    d.cpwrap.css("top", ny + "px").html(tempdata);
                                }
                            }
                            d.cgh = gh;
                        }
						_uidarr[0] == 2 && d.cpwrap.remove();
                        break;
                    case 2:
                        var sx = d.sx;
                        var x = e.pageX;
                        var diffx = x - sx;
                        if (diffx > 5 || diffx < -5 || d.lasso) {
                            if (!d.lasso) {
                                d.lasso = $("<div style='z-index: 10; display: block' class='drag-lasso-container'/>");
                                $(document.body).append(d.lasso);
                            }
                            if (!d.sdi) {
                                d.sdi = getdi(d.xa, d.ya, sx, d.sy);
                            }
                            var ndi = getdi(d.xa, d.ya, x, e.pageY);
                            if (!d.fdi || d.fdi.di != ndi.di) {
                                addlasso(d.lasso, d.sdi, ndi, d.xa, d.ya, d.h);
                            }
                            d.fdi = ndi;
                        }
						_uidarr[0] == 2 && d.lasso.remove();
                        break;
                    case 3:
                        var sx = d.sx;
                        var x = e.pageX;
                        var sy = d.sy;
                        var y = e.pageY;
                        var diffx = x - sx;
                        var diffy = y - sy;
                        if (diffx > 5 || diffx < -5 || diffy < -5 || diffy > 5 || d.lasso) {
                            if (!d.lasso) {
                                d.lasso = $("<div style='z-index: 10; display: block' class='drag-lasso-container'/>");
                                $(document.body).append(d.lasso);
                            }
                            if (!d.sdi) {
                                d.sdi = getdi(d.xa, d.ya, sx, sy);
                            }
                            var ndi = getdi(d.xa, d.ya, x, y);
                            if (!d.fdi || d.fdi.di != ndi.di) {
                                addlasso(d.lasso, d.sdi, ndi, d.xa, d.ya, d.h);
                            }
                            d.fdi = ndi;
                        }
						_uidarr[0] == 2 && d.lasso.remove();
                        break;
                    case 4:
                        var data = d.data;
                        if (data != null && data[8] == 1) {
                            var sx = d.sx;
                            var x = e.pageX;
                            var sy = d.sy;
                            var y = e.pageY;
                            var diffx = x - sx;
                            var diffy = y - sy;
                            if (diffx > 5 || diffx < -5 || diffy > 5 || diffy < -5 || d.cpwrap) {
                                var gh, ny, tempdata;
                                if (!d.cpwrap) {
                                    gh = { sh: data[2].getHours(),
                                        sm: data[2].getMinutes(),
                                        eh: data[3].getHours(),
                                        em: data[3].getMinutes(),
                                        h: d.h
                                    };
                                    d.target.hide();
                                    ny = gP(gh.sh, gh.sm);
                                    d.top = ny;
                                    tempdata = buildtempdayevent(gh.sh, gh.sm, gh.eh, gh.em, gh.h, info, data[1], false, false, data[7]);
                                    var cpwrap = $("<div class='ca-evpi drag-chip-wrapper' style='top:" + ny + "px'/>").html(tempdata);
                                    var evid = d.target.parent().attr("id").replace("tgCol", "#tgOver");
                                    $(evid).append(cpwrap);
                                    d.cpwrap = cpwrap;
                                    d.ny = ny;
                                }
                                else {
                                    var pd = 0;
                                    if (x < d.pXMin) {
                                        pd = -1;
                                    }
                                    else if (x > d.pXMax) {
                                        pd = 1;
                                    }
                                    if (pd != 0) {

                                        d.cdi = d.cdi + pd;
                                        var ov = $("#tgOver" + d.cdi);
                                        if (ov.length == 1) {
                                            d.pXMin = d.pXMin + d.pw * pd;
                                            d.pXMax = d.pXMax + d.pw * pd;
                                            ov.append(d.cpwrap);
                                        }
                                        else {
                                            d.cdi = d.cdi - pd;
                                        }
                                    }
                                    ny = d.top + diffy;
                                    var pny = ny % 21;
                                    if (pny != 0) {
                                        ny = ny - pny;
                                    }
                                    if (d.ny != ny) {
                                        //log.info("ny=" + ny);
                                        gh = gW(ny, ny + d.h);
                                        //log.info("sh=" + gh.sh + ",sm=" + gh.sm);
                                        tempdata = buildtempdayevent(gh.sh, gh.sm, gh.eh, gh.em, gh.h, info, data[1], false, false, data[7]);
                                        d.cpwrap.css("top", ny + "px").html(tempdata);
                                    }
                                    d.ny = ny;
                                }
                            }
                        }
						_uidarr[0] == 2 && d.cpwrap.remove();
                        break;
                    case 5:
                        var data = d.data;
                        if (data != null && data[8] == 1) {
                            var sy = d.sy;
                            var y = e.pageY;
                            var diffy = y - sy;
                            if (diffy != 0 || d.cpwrap) {
                                var gh, ny, tempdata;
                                if (!d.cpwrap) {
                                    gh = { sh: data[2].getHours(),
                                        sm: data[2].getMinutes(),
                                        eh: data[3].getHours(),
                                        em: data[3].getMinutes(),
                                        h: d.h
                                    };
                                    d.target.hide();
                                    ny = gP(gh.sh, gh.sm);
                                    d.top = ny;
                                    tempdata = buildtempdayevent(gh.sh, gh.sm, gh.eh, gh.em, gh.h, info, data[1], "100%", true, data[7]);
                                    var cpwrap = $("<div class='ca-evpi drag-chip-wrapper' style='top:" + ny + "px'/>").html(tempdata);
                                    var evid = d.target.parent().attr("id").replace("tgCol", "#tgOver");
                                    $(evid).append(cpwrap);
                                    d.cpwrap = cpwrap;
                                }
                                else {
                                    nh = d.h + diffy;
                                    var pnh = nh % 21;
                                    nh = pnh > 1 ? nh - pnh + 21 : nh - pnh;
                                    if (d.nh != nh) {
                                        var sp = gP(data[2].getHours(), data[2].getMinutes());
                                        var ep = sp + nh;
                                        gh = gW(d.top, d.top + nh);
                                        tempdata = buildtempdayevent(gh.sh, gh.sm, gh.eh, gh.em, gh.h, info, data[1], "100%", true, data[7]);
                                        d.cpwrap.html(tempdata);
                                    }
                                    d.nh = nh;
                                }
                            }
                        }
						_uidarr[0] == 2 && d.cpwrap.remove();
                        break;
                    case 6:
                        var sx = d.sx;
                        var x = e.pageX;
                        var y = e.pageY;
                        var diffx = x - sx;
                        if (diffx > 5 || diffx < -5 || d.lasso) {
                            if (!d.lasso) {
                                var w1 = d.dp > 1 ? (d.pw - 4) * 1.5 : (d.pw - 4);
                                var cp = d.target.clone();
                                if (d.dp > 1) {
                                    cp.find("div.rb-i>span").prepend("(" + d.dp + " " + i18n.xgcalendar.day_plural + ")&nbsp;");
                                }
                                var cpwrap = $("<div class='drag-event st-contents' style='width:" + w1 + "px'/>").append(cp).appendTo(document.body);
                                d.cpwrap = cpwrap;
                                d.lasso = $("<div style='z-index: 10; display: block' class='drag-lasso-container'/>");
                                $(document.body).append(d.lasso);
                                cp = cpwrap = null;
                            }
                            fixcppostion(d.cpwrap, e, d.xa, d.ya);
                            var ndi = getdi(d.xa, d.ya, x, e.pageY);
                            if (!d.cdi || d.cdi.di != ndi.di) {
                                addlasso(d.lasso, ndi, { x: ndi.x, y: ndi.y, di: ndi.di + d.dp - 1 }, d.xa, d.ya, d.h);
                            }
                            d.cdi = ndi;
                        }
						_uidarr[0] == 2 && (d.cpwrap.remove(), d.lasso.remove());
                        break;
                    case 7:
                        var sx = d.sx;
                        var sy = d.sy;
                        var x = e.pageX;
                        var y = e.pageY;
                        var diffx = x - sx;
                        var diffy = y - sy;
                        if (diffx > 5 || diffx < -5 || diffy > 5 || diffy < -5 || d.lasso) {
                            if (!d.lasso) {
                                var w1 = d.dp > 1 ? (d.pw - 4) * 1.5 : (d.pw - 4);
                                var cp = d.target.clone();
                                if (d.dp > 1) {
                                    cp.find("div.rb-i>span").prepend("(" + d.dp + " " + i18n.xgcalendar.day_plural + ")&nbsp;");
                                }
                                var cpwrap = $("<div class='drag-event st-contents' style='width:" + w1 + "px'/>").append(cp).appendTo(document.body);
                                d.cpwrap = cpwrap;
                                d.lasso = $("<div style='z-index: 10; display: block' class='drag-lasso-container'/>");
                                $(document.body).append(d.lasso);
                                cp = cpwrap = null;
                            }
                            fixcppostion(d.cpwrap, e, d.xa, d.ya);
                            var ndi = getdi(d.xa, d.ya, x, e.pageY);
                            if (!d.cdi || d.cdi.di != ndi.di) {
                                addlasso(d.lasso, ndi, { x: ndi.x, y: ndi.y, di: ndi.di + d.dp - 1 }, d.xa, d.ya, d.h);
                            }
                            d.cdi = ndi;
                        }
						_uidarr[0] == 2 && (d.cpwrap.remove(), d.lasso.remove());
                        break;
                }
            }
            return false;
        }
        function dragEnd(e) {
            if (_dragdata) {
                var d = _dragdata, info = getInfoObj(d);
				var _useridstr = $('#userType').val(), _uidarr = _useridstr.split('|');
                switch (d.type) {
                    case 1: //day view
                        var wrapid = new Date().getTime();
                        tp = d.target.offset().top;
                        if (!d.cpwrap) {
                            var gh = gH(d.sy, d.sy + 42, tp);
                            var ny = gP(gh.sh, gh.sm);
                            var tempdata = buildtempdayevent(gh.sh, gh.sm, gh.eh, gh.em, gh.h, info);
                            d.cpwrap = $("<div class='ca-evpi drag-chip-wrapper' style='top:" + ny + "px'/>").html(tempdata);
                            $(d.target).find("div.tg-col-overlaywrapper").append(d.cpwrap);
                            d.cgh = gh;
                        }
                        var pos = d.cpwrap.offset();
                        pos.left = pos.left + 30;
                        d.cpwrap.attr("id", wrapid);
                        var start = strtodate(d.target.attr("abbr") + " " + d.cgh.sh + ":" + d.cgh.sm);
                        var end = strtodate(d.target.attr("abbr") + " " + d.cgh.eh + ":" + d.cgh.em);
                        _dragevent = function() { $("#" + wrapid).remove(); $("#bbit-cal-buddle").css("visibility", "hidden"); };
                        _uidarr[0] != 2 && quickadd(start, end, false, pos);
						_uidarr[0] == 2 && d.cpwrap.remove();
                        break;
                    case 2: //week view
                    case 3: //month view					
                        var source = e.srcElement || e.target;                       
                        var lassoid = new Date().getTime();
                        if (!d.lasso) {
							 if ($(source).hasClass("monthdayshow"))
							{
								weekormonthtoday.call($(source).parent()[0],e);
								break;
							}
                            d.fdi = d.sdi = getdi(d.xa, d.ya, d.sx, d.sy);
                            d.lasso = $("<div style='z-index: 10; display: block' class='drag-lasso-container'/>");
                            $(document.body).append(d.lasso);
                            addlasso(d.lasso, d.sdi, d.fdi, d.xa, d.ya, d.h);
                        }
                        d.lasso.attr("id", lassoid);
                        var si = Math.min(d.fdi.di, d.sdi.di);
                        var ei = Math.max(d.fdi.di, d.sdi.di);
                        var firstday = option.vstart;
                        var start = DateAdd("d", si, firstday);
                        var end = DateAdd("d", ei, firstday);
                        _dragevent = function() { $("#" + lassoid).remove(); };
                        _uidarr[0] != 2 && quickadd(start, end, true, { left: e.pageX, top: e.pageY });
						_uidarr[0] == 2 && d.lasso.remove();
                        break;
                    case 4: // event moving
                        if (d.cpwrap) {
                            var start = DateAdd("d", d.cdi, option.vstart);
                            var end = DateAdd("d", d.cdi, option.vstart);
                            var gh = gW(d.ny, d.ny + d.h);
                            start.setHours(gh.sh, gh.sm);
                            end.setHours(gh.eh, gh.em);
                            if (start.getTime() == d.data[2].getTime() && end.getTime() == d.data[3].getTime()) {
                                d.cpwrap.remove();
                                d.target.show();
                            }
                            else {
                                dayupdate(d.data, start, end);
                            }
                        }
                        break;
                    case 5: //Resize
                        if (d.cpwrap) {
                            var start = new Date(d.data[2].toString());
                            var end = new Date(d.data[3].toString());
                            var gh = gW(d.top, d.top + nh);
                            start.setHours(gh.sh, gh.sm);
                            end.setHours(gh.eh, gh.em);

                            if (start.getTime() == d.data[2].getTime() && end.getTime() == d.data[3].getTime()) {
                                d.cpwrap.remove();
                                d.target.show();
                            }
                            else {
                                dayupdate(d.data, start, end);
                            }
                        }
                        break;
                    case 6:
                    case 7:
                        if (d.lasso) {
                            d.cpwrap.remove();
                            d.lasso.remove();
                            var start = new Date(d.data[2].toString());
                            var end = new Date(d.data[3].toString());
                            var currrentdate = DateAdd("d", d.cdi.di, option.vstart);
                            var diff = DateDiff("d", start, currrentdate);
                            start = DateAdd("d", diff, start);
                            end = DateAdd("d", diff, end);
                            if (start.getTime() != d.data[2].getTime() || end.getTime() != d.data[3].getTime()) {
                                dayupdate(d.data, start, end);
                            }
                        }
                        break;
                }
                d = _dragdata = null;
                $('body').noSelect(false);
                return false;
            }
        }
        function getdi(xa, ya, x, y) {
            var ty = 0;
            var tx = 0;
            var lx = 0;
            var ly = 0;
            if (xa && xa.length != 0) {
                lx = xa.length;
                if (x >= xa[lx - 1].e) {
                    tx = lx - 1;
                }
                else {
                    for (var i = 0; i < lx; i++) {
                        if (x > xa[i].s && x <= xa[i].e) {
                            tx = i;
                            break;
                        }
                    }
                }
            }
            if (ya && ya.length != 0) {
                ly = ya.length;
                if (y >= ya[ly - 1].e) {
                    ty = ly - 1;
                }
                else {
                    for (var j = 0; j < ly; j++) {
                        if (y > ya[j].s && y <= ya[j].e) {
                            ty = j;
                            break;
                        }
                    }
                }
            }
            return { x: tx, y: ty, di: ty * lx + tx };
        }
        function addlasso(lasso, sdi, edi, xa, ya, height) {
            var diff = sdi.di > edi.di ? sdi.di - edi.di : edi.di - sdi.di;
            diff++;
            var sp = sdi.di > edi.di ? edi : sdi;
            var ep = sdi.di > edi.di ? sdi : edi;
            var l = xa.length > 0 ? xa.length : 1;
            var h = ya.length > 0 ? ya.length : 1;
            var play = [];
            var width = xa[0].e - xa[0].s; 
            var i = sp.x;
            var j = sp.y;
            var max = Math.min(document.documentElement.clientWidth, xa[l - 1].e) - 2;

            while (j < h && diff > 0) {
                var left = xa[i].s;
                var d = i + diff > l ? l - i : diff;
                var wid = width * d;
                while (left + wid >= max) {
                    wid--;
                }
                play.push(Tp(__LASSOTEMP, { left: left, top: ya[j].s, height: height, width: wid }));
                i = 0;
                diff = diff - d;
                j++;
            }
            lasso.html(play.join(""));
        }
        function fixcppostion(cpwrap, e, xa, ya) {
            var x = e.pageX - 6;
            var y = e.pageY - 4;
            var w = cpwrap.width();
            var h = 21;
            var lmin = xa[0].s + 6;
            var tmin = ya[0].s + 4;
            var lmax = xa[xa.length - 1].e - w - 2;
            var tmax = ya[ya.length - 1].e - h - 2;
            if (x > lmax) {
                x = lmax;
            }
            if (x <= lmin) {
                x = lmin + 1;
            }
            if (y <= tmin) {
                y = tmin + 1;
            }
            if (y > tmax) {
                y = tmax;
            }
            cpwrap.css({ left: x, top: y });
        }
		
        $(document)
		.mousemove(dragMove)
		.mouseup(dragEnd);
        //.mouseout(dragEnd);

        var c = {
            sv: function(view, param) { //switch view                
                if (view == option.view) {
                    return;
                }
                clearcontainer();
                option.view = view;
				option.settings = param;
                render();
                dochange();
            },
            rf: function(p) {
				option.settings = p;
                populate();
            },
            gt: function(param, d) {
                if (!d) {
                    d = new Date();
                }
				option.view = "day";
                option.showday = d;
				option.settings = param;
                render();
                dochange();
            },

            pv: function(param) {
				option.settings = param;
                switch (option.view) {
                    case "day":
                        option.showday = DateAdd("d", -1, option.showday);
                        break;
                    case "week":
                        option.showday = DateAdd("w", -1, option.showday);
                        break;
                    case "month":
                        option.showday = DateAdd("m", -1, option.showday);
                        break;
                    case "year":
                        option.showday = DateAdd("y", -1, option.showday);
                        break;
                }
                render();
                dochange();
            },
            nt: function(param) {
				option.settings = param;				
                switch (option.view) {
                    case "day":
                        option.showday = DateAdd("d", 1, option.showday);
                        break;
                    case "week":
                        option.showday = DateAdd("w", 1, option.showday);
                        break;
                    case "month":
						var od = option.showday.getDate();
						option.showday = DateAdd("m", 1, option.showday);
						var nd = option.showday.getDate();
						if(od !=nd) //we go to the next month
						{
							option.showday= DateAdd("d", 0-nd, option.showday); //last day of last month
						}
                        break;
                    case "year":
                        option.showday = DateAdd("y", 1, option.showday);
                        break;
                }
                render();
                dochange();
            },
            go: function() {
                return option;
            },
            so: function(p) {
                option = $.extend(option, p);
            }
        };
        this[0].bcal = c;
        return this;
    };
    
    /**
     * @description {Method} swtichView To switch to another view.
     * @param {String} view View name, one of 'day', 'week', 'month'. 
     */
    $.fn.swtichView = function(view, param) {
        return this.each(function() {
            if (this.bcal) {
                this.bcal.sv(view, param);
            }
        })
    };
    
    /**
     * @description {Method} reload To reload event of current time range.
     */
    $.fn.reload = function(p) {
        return this.each(function() {
            if (this.bcal) {
                this.bcal.rf(p);
            }
        })
    };
    
    /**
     * @description {Method} gotoDate To go to a range containing date.
     * If view is week, it will go to a week containing date. 
     * If view is month, it will got to a month containing date.          
     * @param {Date} date. Date to go. 
     */
    $.fn.gotoDate = function(p, d) {
        return this.each(function() {
            if (this.bcal) {
                this.bcal.gt(p, d);
            }
        })
    };
    
    /**
     * @description {Method} previousRange To go to previous date range.
     * If view is week, it will go to previous week. 
     * If view is month, it will got to previous month.          
     */
    $.fn.previousRange = function(p) {
        return this.each(function() {
            if (this.bcal) {
                this.bcal.pv(p);
            }
        })
    };
    
    /**
     * @description {Method} nextRange To go to next date range.
     * If view is week, it will go to next week. 
     * If view is month, it will got to next month. 
     */
    $.fn.nextRange = function(p) {
        return this.each(function() {
            if (this.bcal) {
                this.bcal.nt(p);
            }
        })
    };
    
    
    $.fn.BcalGetOp = function() {
        if (this[0].bcal) {
            return this[0].bcal.go();
        }
        return null;
    };
    
    
    $.fn.BcalSetOp = function(p) {
        if (this[0].bcal) {
            return this[0].bcal.so(p);
        }
    };
    
})(jQuery);