(function($) {
    $.fn.DhoverClass = function(className) {
        return $(this).hover(function() { $(this).addClass(className); }, function() { $(this).removeClass(className); });
    }
    function getDulyOffset(target, w, h) {
        var pos = target.offset();
        var height = target.outerHeight();
        var newpos = { left: pos.left, top: pos.top + height - 1 }
        var bw = $(document).width();
        var bh = $(document).height();
        if ((newpos.left + w) >= bw) {
            newpos.left = bw - w - 2;
        }
        if ((newpos.top + h) >= bh && bw > newpos.top) {
            newpos.top = pos.top - h - 2;
        }
        return newpos;
    }
    $.fn.colorselect = function(option) {        
        var options = $.extend({
            hiddenid: null,
            title: "Click to select color",
            width: 90,
            height: 90,
            //defaultcolor: "CF9CB5",
            index: "-1",
            cssClass: "bbit-dropdown"
        }, option);
        var me = $(this);
        var id = me.attr("id");
        if (id == null || id == "")
            id = new Date();
        me.addClass("containtdiv");
        var leftId = "divleft" + id;
        var leftdiv = $("<div/>").addClass("leftdiv").attr("id", leftId).appendTo(me);
        var aitem = '<a href="#" id="{1}-{0}" key="{2}" hidefocus="on"><em><span style="background:#{0};border:solid 1px #{3}" unselectable="on">&#160;</span></em></a>';



        var colors = [

        // "F7B281", "EA878B", "FDFC77", "8DADE1", "9CDA95", "CF9CB5"

    ];
        var borders = [];        
		var d = "666666bbbcbef8f8f8bbbbbbdddddda32929f58d8efeeeefe69999f0c2c2b1365ff8a0d0fef1f8eea2bbf5c7d67a367ae1a0e2fbf1facca2cce1c7e15229a3c28df5f6eefdb399e6d1c2f029527a8dc2e2eef5fb99b3ccc2d1e12952a38dc2f4edf6fd99b3e6c2d1f01b887a77f3ebecfcfb91d5ccbde6e128754e8bdfbdeefaf699c9b1c2dfd00d7813f67ec5feedf788cb8cb8e0ba528800c2ea4bf7fce5b3d580d1e6b388880ee9ea62fdfce8d5d588e6e6b8ab8b00f7dc47fefce5ebd780f3e7b3be6d00fcc954fef9e5f7c480fadcb3b1440ef8b161fff3e7eeaa88f5ccb8865a5ae9c9cafdf7f7d4b8b8e5d4d4705770ddc7dcf9f7fac6b6c6ddd3dd4e5d6cbdcddaf4f8f9b1bac3d0d6db5a6986c9d6e9f8f9fdb8c1d4d4dae54a716cb9ddd9f5f9f8aec6c3cedddb6e6e41dbdaaef9f8f3c4c4a8dcdccb8d6f47ebdbb7fdfaf5d8c5ace7dcce";
        var theme = 1;
        var init = theme * 6;
        for (var i = init; i < d.length; i = i + 30) {
            colors.push(d.substr(i, 6));
            borders.push(d.substr(i - 6, 6));
        }

        var html = [];
        for (var i = 0; i < colors.length; i++) {
            var atemp = [];
            var cucolor = colors[i];
            atemp.push(cucolor);
            atemp.push(id);
            atemp.push(i);
            atemp.push(borders[i]);
            var ahtml = StrFormatNoEncode(aitem, atemp);
            html.push(ahtml);
        }
        var blanka = '<a href="#" id="{1}-{0}" key="{2}" hidefocus="on"><em><span style=" text-align:center; width:46px;padding-top:2px; height:11px;border:solid 1px #8B7B8B;" unselectable="on">none</span></em></a>'
        var blank = [];
        blank.push("ffffff");
        blank.push(id);
        blank.push(-1);

        html.push(StrFormatNoEncode(blanka, blank));
        var result = html.join('');
        var div = $("<div />").addClass("x-color-palette").css({
            position: "absolute",
            "z-index": "999",
            "overflow": "auto",
            width: options.width,
            height: options.height,
            display: "none"

        }).attr("id", "div" + id).html(result).appendTo("body");
        $("#div" + id + "  a").click(function(e) {
            //debugger;
            var co = $(this).attr("id");

            var selectcolor = co.split('-')[1];
            leftdiv.css("background", "#" + selectcolor);
            var key = $(this).attr("key");
            //if (key != -1) {
            leftdiv.css("border", $(this).find("span").css("border"));
            if (options.hiddenid != null && options.hiddenid != "") {
                $("#" + options.hiddenid).val(key);
            }
            div.hide();
            return false;
        });
        me.click(function() {
            var pos = getDulyOffset(me, 120, 200);
            div.css(pos);
            div.show();
            $(document).one("click", function(event) { div.hide(); });
            return false;
        });
        function getcolorbyindex(index) {
            if (index >= 0 && index < colors.length)
                return colors[index];
            return "ffffff";
        }

        leftdiv.css("background", "#" + getcolorbyindex(options.index));
        if (options.index != -1) {
            leftdiv.css("border", " solid 1px " + "#" + borders[options.index]);
        }
        else {
            leftdiv.css("border", "solid 1px #8B7B8B");
        }
        if (options.hiddenid != null && options.hiddenid != "") {
            $("#" + options.hiddenid).val(options.index);
        }
        /*     */
        me.attr("title", options.title);
        return me;

    }
})(jQuery);