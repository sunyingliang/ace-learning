<div id="container" class="wrapper">
    <div id="tabs-classes">
        <div id="classes" class="module">
            <div id="cls_top" class="mod_top" style="margin:0px;">
                <!--span class="mod_label">Classes</span-->
                <span class="mod_all">
                    <input type="checkbox" id="cls_all" name="cls_all" />
                    <label for="cls_all" style="padding:0;">&ensp;All</label>
                </span>
            </div>
            <div id="cls_content" class="mod_content"></div>
        </div>
    </div>
    
    <div id="tabs-groups">
        <div id="groups" class="module">
            <div id="grp_top" class="mod_top" style="margin:0px;">
                <!--span class="mod_label">Groups</span-->
                <span class="mod_all">
                    <input type="checkbox" id="grp_all" name="grp_all" />                
                    <label for="grp_all" style="padding:0;">&ensp;All</label>
                </span>
            </div>
            <div id="grp_content" class="mod_content"></div>
        </div>
    </div>
    
    <div       id="tabs-students">
        <div id="individuals" class="module">
            <div id="ivd_top" class="mod_top">
                <!--span class="mod_label">Students</span-->                    
            </div>
            <div id="ivd_content" class="module">
                <div id="filter" class="clm_modudle" style="width:15%;">
                    <div id="flt_header" class="clm_mod_top">
                        <span>Classes</span>
                    </div>
                    <div id="flt_content" class="clm_mod_content">
                    </div>
                </div>
                <div id="source" class="clm_modudle clm_modudle_wid">
                    <div id="src_header" class="clm_mod_top">
                        <!--input id="search_str" class="default_search" type="text" name="search" default="1" value="Search Students" maxlength="100" /-->
                        <input id="search_str" class="FieldInput" type="text" name="search" default="1" value="Search Students" maxlength="100" />
                    </div>
                    <div id="src_content" class="clm_mod_content"></div>
                </div>
                <div id="destination" class="clm_modudle clm_modudle_wid">
                    <div id="des_header" class="clm_mod_top">
                        <span>No. of Selected Students: </span><span id="sltd_stu_num">0</span>
                    </div>
                    <div id="des_content" class="clm_mod_content">
                        <ul class="ui-items"></ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


