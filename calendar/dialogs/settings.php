<?php
	include ('../../includes/session.php');

	$paramarr = array();
	$first = 1;
	if(isset($_GET['courseStr'])){
		$paramarr['courseStr'] = $_GET['courseStr'];
		$paramarr['eventStr'] = $_GET['eventStr'];
		$paramarr['teachers'] = $_GET['teachers'];
		$paramarr['classes'] = $_GET['classes'];
		$paramarr['groups'] = $_GET['groups'];
		$paramarr['students'] = $_GET['students'];	
		$first = 0;
	}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link type="text/css" rel="stylesheet" href="settings.css" />
<link type="text/css" rel="stylesheet" href="/sys/stylesheets/calendar_1.css" />
<link type="text/css" rel="stylesheet" href="../../css/jqueryui/jqueryui.css" />
<script type="text/javascript" src="../src/jquery.js"></script>
<script type="text/javascript" src="../../js/jquery-1.9.1.js"></script>
<script type="text/javascript" src="../../js/jqueryui/jquery-ui-1.10.3.js"></script>
<script type="text/javascript" src="transfer.js"></script>
<script type="text/javascript" src="../src/Plugins/Common.js"></script>
<script type="text/javascript" src="settings.js"></script>
<title>settings</title>
</head>

<body>
<div id="swrapper">
<div class="wrapper">
	<input type="hidden" id="userid" value="<?php echo $_SESSION['SessionUserID']; ?>" />
	<input type="hidden" id="first" name="first" value="<?php echo $first; ?>" />
    <div id="div_course" class="item">
        <div class="label label_section">Course</div>
        <div id="div_cos_content" class="content"></div>
        <?php
			if(isset($paramarr['courseStr']) && !empty($paramarr['courseStr'])){
		?>
		<input type="hidden" id="ecourse" name="ecourse" value="<?php echo $paramarr['courseStr']; ?>" />
		<?php			
			}
		?>
    </div>
    <div id="div_type" class="item">
        <div class="label label_section">Event</div>
        <div id="div_et_content" class="content">
            <ul class="ul">
                <li>
                    <input type="checkbox" id="event_an" value="Announcement" />
                    <span><label for="event_an">Announcement</label></span>
                </li>
                <li>
                    <input type="checkbox" id="event_rl" value="RL" />
                    <span><label for="event_rl">Reading Assignment</label></span>
                </li>
                <li>
                    <input type="checkbox" id="event_rs" value="Resources" />
                    <span><label for="event_rs">Documents</label></span>
                </li>
                <li>
                    <input type="checkbox" id="event_lk" value="Links" />
                    <span><label for="event_lk">Link</label></span>
                </li>
                <li>
                    <input type="checkbox" id="event_qz" value="Quiz" />
                    <span><label for="event_qz">Quiz</label></span>
                </li>
                <li>
                    <input type="checkbox" id="event_ws" value="Worksheet" />
                    <span><label for="event_ws">Worksheet</label></span>
                </li>
                <li>
                    <input type="checkbox" id="event_jn" value="Journal" />
                    <span><label for="event_jn">Journal</label></span>
                </li>
                <li>
                    <input type="checkbox" id="event_pmp" value="PMP" />
                    <span><label for="event_pmp">Progressive Mastery Programme</label></span>
                </li>
                <li>
                    <input type="checkbox" id="event_ea" value="EA" />
                    <span><label for="event_ea">Exploratory Activity</label></span>
                </li>
                <li>
                    <input type="checkbox" id="event_at" value="AT" />
                    <span><label for="event_at">Annotation</label></span>
                </li>
                <li>
                    <input type="checkbox" id="event_sv" value="SV" />
                    <span><label for="event_sv">Survey</label></span>
                </li>
                <li>
                    <input type="checkbox" id="event_forum" value="Forum" />
                    <span><label for="event_forum">Forum</label></span>
                </li>
                <li>
                    <input type="checkbox" id="event_mpg" value="MPG" />
                    <span><label for="event_mpg">Multiplayer Game</label></span>
                </li>
            </ul>
        </div>
        <?php
			if(isset($paramarr['eventStr']) && !empty($paramarr['eventStr'])){
		?>
		<input type="hidden" id="eevent" name="eevent" value="<?php echo $paramarr['eventStr']; ?>" />
		<?php			
			}
		?>
    </div>
    <div id="div_assignby" class="item">
    	<div class="label label_section" style="width:100%;">Assign by</div>
        <div id="div_assignby_content" class="content" style="width:100%;">
        	<div id="assignby" class="module">
            	<div id="assignby_top" class="mod_top" style="margin:0px;">
                	<div class="mod_all">
                    	<input type="checkbox" id="div_assignby_content_all" name="cls_all" />
	                    <label for="div_assignby_content_all">All</label>
                    </div>
                </div>
                <div id="assignby_content" class="mod_content"></div>
            </div>
        </div>
        <?php
			if(isset($paramarr['teachers']) && !empty($paramarr['teachers'])){
		?>
		<input type="hidden" id="eteacher" name="eteacher" value="<?php echo $paramarr['teachers']; ?>" />
		<?php			
			}
		?>
    </div>
    <div id="div_view" class="item">
        <div class="label label_section" style="width:100%;">
        	Assign to
        	<span class="tips">(<i>Note: The assignments to all classes, groups and individual students will be shown by default.</i>)</span>
        </div>
        <div id="tabs" style="margin-top:5px;">
            <ul>
                <li>
                    <a href="#tabs-classes">Classes</a>
                </li>
                <li>
                    <a href="#tabs-groups">Groups</a>
                </li>
                <li>
                    <a href="#tabs-students">Students</a>
                </li>
            </ul>
            <?php
                include("assigntypes_f.php");
				if(isset($paramarr['classes']) && !empty($paramarr['classes'])){
            ?>
			<input type="hidden" id="eclass" name="eclass" value="<?php echo $paramarr['classes']; ?>" />
			<?php			
				}
				if(isset($paramarr['groups']) && !empty($paramarr['groups'])){
			?>
			<input type="hidden" id="egroup" name="egroup" value="<?php echo $paramarr['groups']; ?>" />
			<?php			
				}
                if(isset($paramarr['students']) && !empty($paramarr['students'])){
			?>
            <input type="hidden" id="estudent" name="estudent" value="<?php echo $paramarr['students']; ?>" />
            <?php			
                }
            ?>
        </div> 
    </div> 
    <div class="item">
    	<div class="btns1">
        	<input type="button" id="btn_confirm" value="Confirm" />
        </div>
    	<div class="btns2">
        	<input type="button" id="btn_cancel" value="Cancel" />
        </div>
    </div>       
</div>
</div>
</body>
</html>