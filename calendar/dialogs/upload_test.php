
<?php
// Include SESSION.PHP
	include ('../../includes/session.php');
  	// function for rename
	require ('../../includes/renameFile.php');
	require ('../../includes/mysql_connect.php');
	require ('FileUploadConfig.php');
	
	$FileType = explode(".", $_FILES['filefieldname']['name']);
	$FileType = $FileType[count($FileType)-1];
	$orgFileName =  $_FILES['filefieldname']['name'];
	$filename_show = basename($_FILES['filefieldname']['name']);
	$FileName = checkFilename($orgFileName,"../../files/",$FileType);
	$TotalFileSize=0;
?>


<?php 

$errors = array(1 => 'php.ini max file size exceeded', 
                2 => 'File uploaded exceeds the maximum file size.', 
                3 => 'file upload was only partial', 
                4 => 'no file was attached');
/*
	check file size of  uploaded file if its zero then dont continue
*/
if($_FILES['filefieldname']['size']==0)
{
	echo "<script>";
	echo "parent.document.getElementById('uploadfilefield').innerHTML = parent.document.getElementById('uploadfilefield').innerHTML;";
	echo "parent.document.getElementById('pb_outer').style.display = 'none';";
	echo "alert('Invalid file uploaded. 0 byte files are not allowed');";
	echo "</script>";
	exit;
}
if($_FILES['filefieldname']['error'] != 0)
{
	
	$index = $_FILES['filefieldname']['error'];
	echo "<script>";
	echo "parent.document.getElementById('uploadfilefield').innerHTML = parent.document.getElementById('uploadfilefield').innerHTML;";
	echo "parent.document.getElementById('pb_outer').style.display = 'none';";
	echo "alert('".$errors[$index]."');";
	echo "</script>";
	exit;
}
else
{
	echo "<script>";
	$TotalFileSize = $_GET['totalfiles']+$_FILES['filefieldname']['size'];
	$originalfilesize = $_FILES['filefieldname']['size'];
	$showFileSize = maxSizeForShow($_FILES['filefieldname']['size']);
	echo "parent.document.getElementById('TOTAL_FILE_SIZE').value ='".$TotalFileSize."';";
	
	echo "</script>";
}
if($TotalFileSize>$maxTotalFileSize)
{ //just in case it exceeds.. but basically it wont. 
	echo "<script>";
	echo "alert('Total maximum file size exceeded.');";
	//echo "parent.document.getElementById('uploadfilefield').innerHTML= parent.document.getElementById('uploadfilefield').innerHTML";
	echo "parent.document.getElementById('pb_outer').style.display = 'none';";
	echo "</script>";
	exit;

}else{
//does not exceeed total continue to upload
//$fileNamePre = $FileName; //filename is full path of file
$fileNamePre = basename($FileName); //filename is full path of file

//$divName = basename($_FILES['filefieldname']['name'])."div";
$divName =  basename($fileNamePre)."div";

$path_target = "../../files/".$fileNamePre; //path where file to be uploaded should be temp placed
$temp_path_link = "../../files/".$fileNamePre; //actual path in the server.
$filePath = "javascript:openWindowFS('../../includes/file.php?type=file&file=".$_SESSION['SessionSchoolID'].";".$fileNamePre.";".$_SESSION['SessionUserID']."','DLwinodw')"; 
$flag = 0;
if($flag = move_uploaded_file($_FILES['filefieldname']['tmp_name'], $path_target)) {

    echo "The file ".  basename( $_FILES['filefieldname']['name']). " has been uploaded";
	
	try{
		@chmod( $path_target, 0777);
	}catch(Exception $e){
		echo "<script>";
	echo "alert('Error in uploading file. \n $e.');";
	echo "</script>";
	}
} else{
   echo "<script>";
	echo "alert('Error in uploading file. Please try again.');";
	echo "</script>";
	exit;
}
}
?>
<script>
parent.document.getElementById('uploadedfile').innerHTML += "<div id=\"<?php echo $divName ?>\" class=\"FieldLabel\" ><img src=\"images/delete.gif\" border=\"0\" width=\"16\" height=\"16\"  onclick=\"removeFile('<?php echo $fileNamePre ?>','<?php echo $divName ?>')\" style=\"vertical-align:middle; cursor:pointer\"><input type=\"checkbox\" id=\"<?php echo $fileNamePre ?>\" name=\"<?php echo $fileNamePre ?>\"  style=\"display:none;\" value=\"<?php echo $originalfilesize; ?>\" checked>&nbsp;<a href=\"<?php echo $filePath; ?>\"><?php echo $orgFileName; ?></a></div>";

parent.window.restoreFile();
parent.document.getElementById('uploadfilefield').innerHTML = parent.document.getElementById('uploadfilefield').innerHTML; //change the upload box to empty
parent.document.getElementById('pb_outer').style.display = 'none';
parent.document.getElementById('newFile').value = '1';
</script>