<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Assign Types</title>
<link type="text/css" rel="stylesheet" href="assigntypes.css" />
<script src="../src/jquery.js" type="text/javascript"></script>
<script type="text/javascript" src="assigntypes.js"></script>
</head>

<body>
<div id="container" class="wrapper">
	<div id="classes" class="module">
    	<div id="cls_top" class="mod_top" style="margin:0px;">
        	<span class="mod_label">Classes</span>
            <span class="mod_all">
            	<input type="checkbox" id="cls_all" name="cls_all" />
                <label for="cls_all">&ensp;All</label>
            </span>
        </div>
        <div id="cls_content" class="mod_content"></div>
    </div>
    <div id="groups" class="module">
    	<div id="grp_top" class="mod_top">
        	<span class="mod_label">Groups</span>
            <span class="mod_all">
            	<input type="checkbox" id="grp_all" name="grp_all" />
                <label for="grp_all">&ensp;All</label>
            </span>
        </div>
        <div id="grp_content" class="mod_content"></div>
    </div>
    <div id="individuals" class="module">
    	<div id="ivd_top" class="mod_top">
        	<span class="mod_label">Students</span>
        </div>
        <div id="ivd_content" class="module">
            <div id="filter" class="clm_modudle" style="width:19%;">
                <div id="flt_header" class="clm_mod_top">
                    <span>Classes</span>
                </div>
                <div id="flt_content" class="clm_mod_content">
                </div>
            </div>
            <div id="source" class="clm_modudle clm_modudle_wid">
                <div id="src_header" class="clm_mod_top">
                    <input id="search_str" class="default_search" type="text" name="search" default="1" value="Search Students" maxlength="100" />
                </div>
                <div id="src_content" class="clm_mod_content">
                </div>
            </div>
            <div id="destination" class="clm_modudle clm_modudle_wid">
                <div id="des_header" class="clm_mod_top">
                    <span>No. of Selected Students: </span><span id="sltd_stu_num">0</span>
                </div>
                <div id="des_content" class="clm_mod_content">
                	<ul class="ui-items"></ul>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>