<?php
	include ('../../includes/session.php');
	require ('../../includes/general_functions.php');
	require ('../../includes/mysql_connect.php'); 
	 
	if(isset($_GET['RSID']))
	{
	
		$SchoolID = $_SESSION['SessionSchoolID'];
		$RSID = $_GET['RSID'];
		//resources/schools/CHHS/test.txt
		$attachmentURL = "../../resources/schools/".$SchoolID."/".$_GET[fname];
		
		if(file_exists($attachmentURL)==false && is_file($attachmentURL)==false)
		{
			$attachmentURL = "../../resources/schools/".$SchoolID."/".$_SESSION['SessionUserID']."/".$_GET[fname];
			if(file_exists($attachmentURL)==false && is_file($attachmentURL)==false)
			{
				echo  "<div>Could not find the file to delete.</div>";
			}
		}
		unlink($attachmentURL);
		
		//update database
		
		$query = "SELECT 
					FileName,
					OriginalName,
					FileType,
					FileSize
				  FROM 
					QRY_".$_SESSION['SessionSchoolID']."_Resources 
				  WHERE 
					ID='$RSID'";
		
		$result=mysql_query($query);
		 if (mysql_affected_rows() > 0) 
		 {
			$row=mysql_fetch_array($result, MYSQL_NUM);
		 }
		 else
		 {
			if(mysql_errno()>0)
			{
					echo "<div>$Query</div>";
					echo "<div>".mysql_errno().":".mysql_error()."</div>";
			}
		 }
		
		$fileArray=explode_string($row[0]);
		$OfileArray=explode_string($row[1]);
		$FileType=explode_string($row[2]);
		//$FileSize=explode_string($row[3]); //for multiple uploads.. make sure column datatype is chance
		if(in_array($_GET[fname], $fileArray))
		{
			$key = array_search($_GET[fname], $fileArray);
			unset($fileArray[$key]);
			unset($OfileArray[$key]);
			unset($FileType[$key]);
			$FileSize=0;
			
		}
		
		$filenames=implode(",",$fileArray);
		$Origfilenames=implode(",",$OfileArray);
		$OFileType=implode(",",$FileType);
		//$OFileSize=implode(",",$FileSize); //fr multiple upload need to change database datatype for filesize 
		
		$queryUpdate ="UPDATE 
						QRY_".$_SESSION['SessionSchoolID']."_Resources 
					SET 
						FileName='$filenames',
						OriginalName='$Origfilenames', 
						FileType='$OFileType',
						FileSize='$FileSize'
					WHERE 
						ID='$RSID'";
			
		$result=mysql_query($queryUpdate);	
		
		if(mysql_errno()>0)
		{
					echo "<div>$Query</div>";
					echo "<div>".mysql_errno().":".mysql_error()."</div>";
		}
		
	}else{
		$filename=$_GET[fname];
		unlink("../../files/".$filename);
	}
?>