<?php
	class TempForm{
		public $type;
		public $course;
		public $title;
		public $color;
		public $message;
		public $startdate;
		public $enddate;
		public $startday;
		public $starttime;
		public $endday;
		public $endtime;
		public $isallday;
		public $assignids;
		public $classes;
		public $groupstr;
		public $groups;
		public $studentstr;
		public $students;
		
		public function __construct($_tp, $_cs, $_tt, $_cl, $_msg, $_sd, $_ed, $_iad, $_aids, $_cls, $_grp, $_stu){
			$this->type = $_tp;
			$this->course = $_cs;
			$this->title = $_tt;
			$this->color = $_cl;
			$this->message = $_msg;
			$this->startdate = $_sd;
			$this->enddate = $_ed;
			$this->isallday = $_iad;
			$this->assignids = $_aids;
			$this->classes = $_cls;
			$this->groupstr = $_grp;
			$this->studentstr = $_stu;
			$this->groups = '';
			$this->students = '';
		}
		
		public function init(){
			$temparr = explode(" ", $this->startdate);
			$this->startday = $temparr[0];
			$this->starttime = $temparr[1];
			unset($temparr);
			$temparr = explode(" ", $this->enddate);
			$this->endday = $temparr[0];
			$this->endtime = $temparr[1];
			//init groups and students
			$temparr = explode(",", $this->groupstr);
			foreach($temparr as $item){
				$inarr = explode("|", $item);	
				$this->groups .= $this->groups == '' ? $inarr[0] : ','.$inarr[0];
			}
			$temparr = explode(",", $this->studentstr);
			foreach($temparr as $item){
				$inarr = explode("|", $item);
				$student = substr($inarr[1], strpos($inarr[1], "(") + 1, -1)."|".$inarr[0];
				$this->students .= $this->students == '' ? $student : ','.$student;
			}
		}
	}
	
	class TpLinks extends TempForm{
		public $url;
		
		public function __construct($_tp, $_cs, $_tt, $_cl, $_msg, $_sd, $_ed, $_iad, $_aids, $_cls, $_grp, $_stu, $_url){
			$this->url = $_url;
			parent::__construct($_tp, $_cs, $_tt, $_cl, $_msg, $_sd, $_ed, $_iad, $_aids, $_cls, $_grp, $_stu);	
		}	
	}
	
	class TpRL extends TempForm{
		public $topicnames;
		public $topicids;
		private $topicstr;
		
		public function __construct($_tp, $_cs, $_tt, $_cl, $_msg, $_sd, $_ed, $_iad, $_aids, $_cls, $_grp, $_stu, $_tpstr, $_tpids){
			$this->topicstr = $_tpstr;
			$this->topicids = $_tpids;
			parent::__construct($_tp, $_cs, $_tt, $_cl, $_msg, $_sd, $_ed, $_iad, $_aids, $_cls, $_grp, $_stu);	
			$temparr = array();
			$tpotarr = explode('|', $this->topicstr);	
			foreach($tpotarr as $value){
				$tpinarr = explode(';', $value);	
				$temparr[] = $tpinarr[2];
				unset($tpinarr);
			}
			$this->topicnames = implode(',', $temparr);
		}	
	}
	
	class TpResources extends TempForm{
		public $schoolid;
		public $userid;
		public $protoid;
		public $filename;
		public $origfilename;
		public $filesizes;
		
		public function __construct($_tp, $_cs, $_tt, $_cl, $_msg, $_sd, $_ed, $_iad, $_aids, $_cls, $_grp, $_stu, $_schoolid, $_userid, $_pid, $_fname, $_ofname, $_fsize){
			$this->schoolid = $_schoolid;
			$this->userid = $_userid;
			$this->protoid = $_pid;
			$this->filename = $_fname;
			$this->origfilename = $_ofname;
			$this->filesizes = $_fsize;
			parent::__construct($_tp, $_cs, $_tt, $_cl, $_msg, $_sd, $_ed, $_iad, $_aids, $_cls, $_grp, $_stu);		
		}	
	}
?>