if (!DateAdd || typeof (DateDiff) != "function") {
	var DateAdd = function(interval, number, idate) {
		number = parseInt(number);
		var date;
		if (typeof (idate) == "string") {
			date = idate.split(/\D/);
			eval("var date = new Date(" + date.join(",") + ")");
		}
		if (typeof (idate) == "object") {
			date = new Date(idate.toString());
		}
		switch (interval) {
			case "y": date.setFullYear(date.getFullYear() + number); break;
			case "m": date.setMonth(date.getMonth() + number); break;
			case "d": date.setDate(date.getDate() + number); break;
			case "w": date.setDate(date.getDate() + 7 * number); break;
			case "h": date.setHours(date.getHours() + number); break;
			case "n": date.setMinutes(date.getMinutes() + number); break;
			case "s": date.setSeconds(date.getSeconds() + number); break;
			case "l": date.setMilliseconds(date.getMilliseconds() + number); break;
		}
		return date;
	}
}
function getHM(date)
{
	 var hour =date.getHours();
	 var minute= date.getMinutes();
	 var ret= (hour>9?hour:"0"+hour)+":"+(minute>9?minute:"0"+minute) ;
	 return ret;
}
function retrieveCourses($form){
	var courseObj = {
		url: '../php/retrieveCourses.php',	
		type: 'GET',
		async: false,
		cache: false,
		dataType: 'json',
		success: function(responseObj, status){
			if(!!responseObj['error'] === false){
				var $courses = $form.find('#icourse'), $option, tempArr;
				for(p in responseObj){
					tempArr = responseObj[p].split('|');
					$option = $('<option></option>').attr('value', tempArr[0]).text(tempArr[1]).appendTo($courses);
				}		
			}
		}
	};	
	$.ajax(courseObj);
}
function getLiName($li){
	return $li.find('label').html().toLowerCase();	
}
function attachEventAll($chk){
	$chk.click(function(){
			var $destdiv = $chk.parent().parent().next(), _chked = $chk.attr('checked');
			$destdiv.find(':checkbox').each(function(){
									  	  $(this).attr('checked', _chked);
									  });
		});	
}
function attachEventFilter(){
	$('#flt_content').find(':checkbox').click(function(){
											$("#src_content ul [info='1']").length > 0 && $("#src_content ul [info='1']").remove();
											$('#search_str').val('').attr({'default':1}).val('Search Students').addClass('default_search');
											$('#src_content').find('ul [show="1"]').css('display', 'block');
											var clsid = $(this).val(), display = $(this).attr('checked') == true ? 'block' : 'none', show = $(this).attr('checked') == true ? 1 : 0;
											$('#src_content ul li').each(function(){
																			if($(this).children(':checkbox').attr('belongs') == clsid){
																				$(this).css('display', display).attr('show', show);
																			}
																		})
											$("#flt_content :checked").length > 0 && $("#src_content ul [show='1']").length == 0 && $("<li info='1' style='display: block;'>No student found.</li>").appendTo($("#src_content ul"));							
										});
}
function attachEventSelect(){
	$('#src_content').find(':checkbox').click(function(){
											moveItems();
										});								
}
function moveItems(){
	var $items = $('#src_content').find(':checked'), $destul = $('#des_content').find('ul'), $li, $chkbox, $span;
	$('#sltd_stu_num').text($items.length);
	$destul.empty();
	$items.each(function(){
			  $li = $(this).parent().clone();
			  $chkbox = $li.children(':checkbox');
			  $chkbox.attr({'id':$chkbox.val(), 'name':'Student[]', 'checked':true}).val($chkbox.attr('belongs') + '|' + $chkbox.val());
			  $li.find('label').attr('for', $chkbox.attr('id'));
			  $li.css('display', 'block').appendTo($destul);					 
		  });
	$destul.find(':checkbox').click(function(){
									var _itemval = $(this).attr('id');
									$('#src_content').find(':checked').each(function(){
																			if($(this).val() == _itemval){
																				$(this).attr('checked', false);	
																				moveItems();
																			}
																		});
								});	  
}
function retrieveTypes($droot, vtype, $clsdiv){
	var cid = '', divid = $droot.attr('id'), courseID = $("#icourse").val();
	if(vtype == 'Student'){
		$clsdiv.find(':checkbox').each(function(){
									 cid += cid == '' ? $(this).val() : ',' + $(this).val();
								 });
	}												
	var	typeObj = {
		url: '../php/retrieveTypeItems.php',	
		type: 'POST',
		async: false,
		cache: false,
		data: {classIds: cid, type: vtype, courseid: courseID},
		dataType: 'json',
		success: function(responseObj, status){
			$droot.empty();	
			if(responseObj['flag']){
				var $ui = $('<ul></ul>').addClass('ui-items').appendTo($droot), $li, $chkbox, $span, _item;
				switch(vtype){
					case 'Class':
						var clsType = $droot.attr('id') == 'cls_content' ? 'Class' : 'StuClass';
						for(var i = 0; !!responseObj[i]; i++){
							_item = responseObj[i].split(',');	
							$li = $('<li></li>').appendTo($ui);
							$chkbox = $droot.attr('id') == 'cls_content' ? $('<input type="checkbox" id="' + divid + '_' + _item[0].toLowerCase() + '" value="' + _item[1] + '" name="Class[]" />').appendTo($li) : $('<input type="checkbox" id="' + divid + '_' + _item[0].toLowerCase() + '" value="' + _item[1] + '" />').appendTo($li);
							$span = $('<span><label for="' + divid + '_' + _item[0].toLowerCase() + '">&ensp;' + _item[1] + '</label></span>').appendTo($li);	
						}
						if($droot.attr('id') == 'cls_content'){
							attachEventAll($('#cls_all'));
						}else{
							retrieveTypes($('#src_content'), 'Student', $droot);
							attachEventFilter();
						}
						break;
					case 'Group':
						for(var i = 0; !!responseObj[i]; i++){
							_item = responseObj[i].split(',');	
							$li = $('<li></li>').appendTo($ui.removeClass('ui-items').addClass('ul-items'));
							$chkbox = $('<input type="checkbox" id="' + divid + '_' + _item[0].toLowerCase() + '" value="' + _item[0] + '" name="Group[]" />').appendTo($li);
							$span = $('<span><label for="' + divid + '_' + _item[0].toLowerCase() + '">&ensp;' + _item[1] + '</label></span>').appendTo($li);	
						}
						attachEventAll($('#grp_all'));
						break;
					case 'Student':
						for(var i = 0; !!responseObj[i]; i++){
							_item = responseObj[i].split(',');	
							$li = $('<li show="0"></li>').appendTo($ui);
							$chkbox = $('<input type="checkbox" id="' + divid + '_' + _item[0].toLowerCase() + '" value="' + _item[0] + '" belongs="' + _item[2] + '" />').appendTo($li);
							$span = $('<span><label for="' + divid + '_' + _item[0].toLowerCase() + '">&ensp;' + _item[1] + '&ensp;(' + _item[2] + ')</label></span>').appendTo($li);	
						}
						$('#src_content').find('ul li').css('display', 'none');
						$('#src_content').find(':checkbox').click(function(){
																moveItems();
 														   });	
						break;	
				}	
			}else{
				if($droot.attr("id") == "cls_content" || $droot.attr("id") == "grp_content"){
					$droot.html(responseObj['message']);			
				}else if($droot.attr("id") == "flt_content"){
					var _studentcln = $droot.parent().parent().clone().attr('id', 'ivd_content_clone').empty();
					$droot.hide();
					_studentcln.insertBefore($droot);
					responseObj['exist'] == 1 ? _studentcln.html("No student is taking '" + responseObj['course'] + "'.") : _studentcln.html("No student found.");
				}
			}
		}
	};	
	$.ajax(typeObj);	
}
function retrieveItems(){
	retrieveTypes($('#cls_content'), 'Class');
	retrieveTypes($('#grp_content'), 'Group');
	retrieveTypes($('#flt_content'), 'Class');			
}
var MaxFileUpload = 1, patt1 = /[~!@#^*<>?\/|"']/, TopicID = new Array(), TopicName = new Array();	
function verifyLink(){
	var _url = $('#Url').val();
	if(_url != ''){
		if(_url.indexOf('http://') < 0 && _url.indexOf('https://') < 0){
			_url = 'http://' + _url;
		}
		openWindowFS(_url, 'vlink');
	}else{
		alert('Invalid URL title. Please try again');			
	}
}
function checkBeforeUpload(){
	var NoOfFileUpload = 0;
	var elem = document.getElementById('uploadedfile').getElementsByTagName("*");
	for(var i = 0; i < elem.length; i++){
		if(elem[i].type == "checkbox"){
			NoOfFileUpload++;
		}
	}
	if(NoOfFileUpload >= MaxFileUpload){
		alert("Maximum number of file upload exceeded.");
		return false;
	}
}
function showAttach(){	
	$('#Attach').css('visibility', 'visible');
}
function uploadFile(){
	var forward = LimitAttach(document.getElementById('filefieldname').value);
	if(forward == true){	
		var totalfilesize = document.getElementById('TOTAL_FILE_SIZE').value,
			$tempform = $('<form></form>').attr({	
							'action':'upload_test.php?totalfiles = ' + totalfilesize, 
							'target':'hiddenframe',
							'method':'post',
							'enctype':'multipart/form-data',
							'id':'tempform'
						}).css('display', 'none').appendTo($('body')),			
			$clone = $('#filefieldname').clone(true).attr({id:'filefieldname_clone', name:'filefieldname_clone'});								
		$clone.insertAfter($('#filefieldname'));
		$('#filefieldname').appendTo($tempform);
		submitme();
	}else{
		document.getElementById('uploadfilefield').innerHTML=document.getElementById('uploadfilefield').innerHTML;
		return false;
	}
}
function restoreFile(){
	$('#filefieldname').insertBefore($('#filefieldname_clone'));
	$('#filefieldname_clone').remove();
	$('#tempform').remove();
	checkResources();
}
function LimitAttach(file) {			
	var allowedfiletypes = document.getElementById('AllowFileExtensions').value;
	var extArray = allowedfiletypes.split('|');
	allowSubmit = false;
	if (!file) return;
	while (file.indexOf("\\") != -1){
		file = file.slice(file.indexOf("\\") + 1);
	}
	ext = file.slice(file.lastIndexOf(".")).toLowerCase();
	for(var i = 0; i < extArray.length; i++){
		if(extArray[i] == ext){ allowSubmit = true; break; }
	}
	if(allowSubmit){
		return true;
	}else{
		alert("Please only upload files that end in types:  " + (extArray.join("  ")) + "\n\nPlease select a new " + "file to upload and submit again.");
	}
	document.getElementById('Attach').style.visibility = 'hidden';	
	return false;
}
function submitme(){
	startProgress();
	$('#tempform').submit();
}
function startProgress() {
	document.getElementById('pb_outer').style.display = 'block';
	document.getElementById('Attach').style.visibility = 'hidden';
}
function removeFile(check_id, div_id){
	document.getElementById(check_id).checked = false;
	var Node = document.getElementById(div_id), url;
	Node.parentNode.removeChild(Node);
	$('#DeleteFile').val($('#DeleteFile').val() + check_id + ',');
	url = arguments[2] != undefined ? ('removefile.php?fname=' + check_id + '&RSID=' + arguments[2]) : ('removefile.php?fname=' + check_id);
	
	$.ajax({
		'url':	url,
		'type': 'GET',
		'async': true,
		'cache': false,
		'success': function(data, textStatus){
			alert('File remove!');	
		}
	});
}
function validWName(){
	var teststr = document.getElementById('ititle').value;
	if(patt1.test(teststr)==true){
		alert('Invalid characters : ~ ! @ # ^ * < > ? / | \\ \" \' ');
		return false;
	}else{
		return true;
	}
}
function validAddW(){
	rValue=true;
	if (document.getElementById('ititle').value == "") {
		alert("Please enter Resource Name.");
		document.getElementById('ititle').focus();
		rValue=false;
	} else if (document.getElementById('icourse').value == "0") {
		alert("Please select a Course.");
		document.getElementById('icourse').focus();
		rValue=false;
	}
	return rValue;
}
function checkTitle(){
	if(!$("#ititle").val()){
		$("#ititle").addClass("errorbdr");
		$("#titleerror").html("Please enter title.").css("display", "block");		
		return false;
	}else{
		if(patt1.test($("#ititle").val()) == true){
			$("#ititle").addClass("errorbdr");
			$("#titleerror").html("Invalid characters : ~ ! @ # ^ * < > ? / | \\ \" \'").css("display", "block");				
			return false;			
		}
		var _flag = false;
		if($("#sevent").val() == 'Links' || $("#sevent").val() == 'Resources'){
			if($("#edtype").length > 0 && $("#ititle").val() == $("#otitle").val()){
				_flag = false; 
			}else{
				var _qobj = {
					url: '../php/checkTitle.php',
					type: 'POST',
					async: false,
					cache: false,
					data: {type:$("#sevent").val(), title:$("#ititle").val()},
					success: function(text, status){
						_flag = text == 0 ? false : true;	
					}
				};
				$.ajax(_qobj);							
			}
		}
		if(_flag){
			$("#ititle").addClass("errorbdr");
			$("#titleerror").html("The title has been used, please try again.").css("display", "block");					
			return false;
		}else{
			$("#ititle").removeClass("errorbdr");
			$("#titleerror").html("you an use this title.").css("display", "none");						
			return true;
		}
	}	
}
function checkURL(){
	if(!$("#Url").val()){
		$("#Url").addClass("errorbdr");
		$("#urlerror").html("Please enter a URL.").css("display", "block");			
		return false;
	}else{
		var _flag = /^(https?|ftp):\/\/(((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:)*@)?(((\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5]))|((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?)(:\d*)?)(\/((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)+(\/(([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)*)*)?)?(\?((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|[\uE000-\uF8FF]|\/|\?)*)?(\#((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|\/|\?)*)?$/i.test($("#Url").val());
		if(!_flag){
			$("#Url").addClass("errorbdr");
			$("#urlerror").html("Please enter a valid URL. <i>e.g. http://www.google.com</i>").css("display", "block");		
		}else{
			$("#Url").removeClass("errorbdr");
			$("#urlerror").html("This url can be used.").css("display", "none");	
		}
		return _flag;
	}	
}
function checkResources(){///pass to uploadfiles
	var checkBoxcount = 0, str = 1;
	if(document.getElementById('uploadedfile').getElementsByTagName("*").length>0){
		var elem = document.getElementById('uploadedfile').getElementsByTagName("*");
		for(var i = 0; i < elem.length; i++){	
			if(elem[i].type == "checkbox"){
				document.getElementById('hiddenfield').innerHTML +='<input name="uploadfiles[]" type="hidden" id="'+elem[i].name+'" value="'+elem[i].value+"|"+elem[i].name+'">';
				checkBoxcount++;
			}
		}
	}
	if(checkBoxcount > 0){
		$("#uploadfilefield").removeClass("errorbdr");
		$("#fileerror").html("File has been uploaded successfully.").css("display", "none");
		return true;
	}else{
		$("#uploadfilefield").addClass("errorbdr");
		$("#fileerror").html("No file uploaded. Please try agian!").css("display", "block");		
		return false;
	}
}
function checkTopics(){
	var $lis = $("#disList ul li");
	if($lis.length == 1){
		$lis.css("color", "red");	
		$("#errtopics").length == 0 && $("<span id='errtopics'>&nbsp;&nbsp; Please select at least one topic.</span>").appendTo($lis);
		return false;
	}else{
		$lis.css("color", "black");
		$("#errtopics").remove();	
		return true;
	}	
}
function checkFromTime(){
	if(!$("#stpartdate").val() || !$("#stparttime").val()){
		!$("#stpartdate").val() && $("#stpartdate").addClass("errorbdr");
		!$("#stparttime").val() && $("#stparttime").addClass("errorbdr");
		$("#ddserror").html("Please select a date and time.").css("display", "block");	
		return false;	
	}
	var arrs = $("#stpartdate").val().split(i18n.datepicker.dateformat.separator);
	var year = arrs[i18n.datepicker.dateformat.year_index];
	var month = arrs[i18n.datepicker.dateformat.month_index];
	var day = arrs[i18n.datepicker.dateformat.day_index];
	var standvalue = [year,month,day].join("-");
	var _flag = /^(?:(?:1[6-9]|[2-9]\d)?\d{2}[\/\-\.](?:0?[1,3-9]|1[0-2])[\/\-\.](?:29|30))(?: (?:0?\d|1\d|2[0-3])\:(?:0?\d|[1-5]\d)\:(?:0?\d|[1-5]\d)(?: \d{1,3})?)?$|^(?:(?:1[6-9]|[2-9]\d)?\d{2}[\/\-\.](?:0?[1,3,5,7,8]|1[02])[\/\-\.]31)(?: (?:0?\d|1\d|2[0-3])\:(?:0?\d|[1-5]\d)\:(?:0?\d|[1-5]\d)(?: \d{1,3})?)?$|^(?:(?:1[6-9]|[2-9]\d)?(?:0[48]|[2468][048]|[13579][26])[\/\-\.]0?2[\/\-\.]29)(?: (?:0?\d|1\d|2[0-3])\:(?:0?\d|[1-5]\d)\:(?:0?\d|[1-5]\d)(?: \d{1,3})?)?$|^(?:(?:16|[2468][048]|[3579][26])00[\/\-\.]0?2[\/\-\.]29)(?: (?:0?\d|1\d|2[0-3])\:(?:0?\d|[1-5]\d)\:(?:0?\d|[1-5]\d)(?: \d{1,3})?)?$|^(?:(?:1[6-9]|[2-9]\d)?\d{2}[\/\-\.](?:0?[1-9]|1[0-2])[\/\-\.](?:0?[1-9]|1\d|2[0-8]))(?: (?:0?\d|1\d|2[0-3])\:(?:0?\d|[1-5]\d)\:(?:0?\d|[1-5]\d)(?:\d{1,3})?)?$/.test(standvalue);	
	if(!_flag){
		$("#stpartdate").addClass("errorbdr");
		$("#ddserror").html("Invalid date.").css("display", "block");
		return false;	
	}else{
		$("#stpartdate").removeClass("errorbdr");
		$("#ddserror").html("Date format is correct.").css("display", "none");		
		return true;
	}
}
function checkEndTime(){
	if(!$("#etpartdate").val() || !$("#etparttime").val()){
		!$("#etpartdate").val() && $("#etpartdate").addClass("errorbdr");
		!$("#etparttime").val() && $("#etparttime").addClass("errorbdr");
		$("#ddeerror").html("Please select a date and time.").css("display", "block");		
		return false;	
	}
	var arrs = $("#etpartdate").val().split(i18n.datepicker.dateformat.separator);
	var year = arrs[i18n.datepicker.dateformat.year_index];
	var month = arrs[i18n.datepicker.dateformat.month_index];
	var day = arrs[i18n.datepicker.dateformat.day_index];
	var standvalue = [year,month,day].join("-");
	var _flag = /^(?:(?:1[6-9]|[2-9]\d)?\d{2}[\/\-\.](?:0?[1,3-9]|1[0-2])[\/\-\.](?:29|30))(?: (?:0?\d|1\d|2[0-3])\:(?:0?\d|[1-5]\d)\:(?:0?\d|[1-5]\d)(?: \d{1,3})?)?$|^(?:(?:1[6-9]|[2-9]\d)?\d{2}[\/\-\.](?:0?[1,3,5,7,8]|1[02])[\/\-\.]31)(?: (?:0?\d|1\d|2[0-3])\:(?:0?\d|[1-5]\d)\:(?:0?\d|[1-5]\d)(?: \d{1,3})?)?$|^(?:(?:1[6-9]|[2-9]\d)?(?:0[48]|[2468][048]|[13579][26])[\/\-\.]0?2[\/\-\.]29)(?: (?:0?\d|1\d|2[0-3])\:(?:0?\d|[1-5]\d)\:(?:0?\d|[1-5]\d)(?: \d{1,3})?)?$|^(?:(?:16|[2468][048]|[3579][26])00[\/\-\.]0?2[\/\-\.]29)(?: (?:0?\d|1\d|2[0-3])\:(?:0?\d|[1-5]\d)\:(?:0?\d|[1-5]\d)(?: \d{1,3})?)?$|^(?:(?:1[6-9]|[2-9]\d)?\d{2}[\/\-\.](?:0?[1-9]|1[0-2])[\/\-\.](?:0?[1-9]|1\d|2[0-8]))(?: (?:0?\d|1\d|2[0-3])\:(?:0?\d|[1-5]\d)\:(?:0?\d|[1-5]\d)(?:\d{1,3})?)?$/.test(standvalue);	
	if(!_flag){
		$("#etpartdate").addClass("errorbdr");
		$("#ddeerror").html("Invalid date.").css("display", "block");	
		return false;
	}else{
		$("#etpartdate").removeClass("errorbdr");
		$("#ddeerror").html("Date format is correct.").css("display", "none");		
		return true;
	}
}
function checkOrderTime(){
	var _flag = checkFromTime() & checkEndTime();
	if(_flag){
		var _stdate = $("#stpartdate").val(), _sttime = $("#stparttime").val(),
			_etdate = $("#etpartdate").val(), _ettime = $("#etparttime").val(),
			_regexp = /\d+/g, _sdarr = _stdate.match(_regexp), _edarr = _etdate.match(_regexp),	
			_starr = _sttime.match(_regexp), _etarr = _ettime.match(_regexp),
			_sdatetime = new Date(), _edatetime = new Date();	
		_sdatetime.setFullYear(_sdarr[2], _sdarr[1] - 1, _sdarr[0]);
		_sdatetime.setHours(_starr[0], _starr[1]);
		_edatetime.setFullYear(_edarr[2], _edarr[1] - 1, _edarr[0]);
		_edatetime.setHours(_etarr[0], _etarr[1]);
		
		_flag &= _sdatetime.getTime() <= _edatetime.getTime();
		if(_flag){
			$("#ddserror").html("Datetime Order is correct.").css("display", "none");
		}else{
			$("#ddserror").html("You cannot create an event that ends before it starts.").css("display", "block");
		}
	}
	return _flag;
}
function checkTime(){
	checkFromTime();
	checkEndTime();
	checkOrderTime();
}
function checkAssign(){
	var $chks = $("#cls_content :checked, #grp_content :checked, #src_content :checked");
	if($chks.length == 0){
		$("#assignerror").html("Please select at least a class, a group, or a student.").css("display", "block");		
		return false;
	}else{
		$("#assigntolbl").removeClass("errorbdr");
		$("#assignerror").html("The assign part is ok.").css("display", "none");	
		return true;	
	}
}
function validateFields(){
	var _flag = true;
	_flag &= checkTitle();
	_flag &= checkFromTime();
	_flag &= checkEndTime();
	_flag &= checkOrderTime();
	_flag &= checkAssign();
	switch($('#sevent').val()){
		case 'Links':
			_flag &= checkURL();
			break;
		case 'RL':
			_flag &= checkTopics();
			break;
		case 'Resources':
			_flag &= checkResources();
			break;	
	}
	return _flag;
}
function formSubmit(){
	if(validateFields()){
		$('#fmEdit').submit();	
	}else{
		window.location.hash = "top";	
	}
}
function openTopicsWindow(){
	var _url = '../../includes/RTopic.php?&a=' + new Date().getTime(), wname = 'QuizRTopic';
	openWindowFS(_url, wname);	
}
function runText(nameArr,idArr){
	TopicID = new Array()
	TopicName = new Array()
	for (var i =0; i<idArr.length; i++){
		TopicID.push(idArr[i]);
		TopicName.push(nameArr[i]);
	}
	var $displayList = $("#disList").empty(), TopicCodeArr = TopicID.toString(), $ul = $("<ul></ul>"), $li = $("<li><a href='javascript:openTopicsWindow();'><u>Select Topics</u></a></li>");
	$("#TopicCodeArr").val(TopicCodeArr);
	if(TopicID.length > 0){
		$li.children().first().children().first().text("Edit Topics");	
	}
	for(var i = 0; i < TopicID.length; i++){
		$("<li></li>").text((i + 1) + ". " + TopicName[i]).appendTo($ul);	
	}
	$li.appendTo($ul);
	$ul.appendTo($displayList);
}
function hideItems(){
	$(".item, .spliter, .wrapper, .phony").each(function(){
		var _flag = $(this).attr('id') == 'devent' ? 'visible' : 'hidden';
		$(this).css('visibility', _flag);	
	});	
} 
function showItems(){
	$(".item, .spliter, .wrapper, .phony").each(function(){
		$(this).css('visibility', 'visible');	
	});	
}
function initForm(type){
	hideItems();
	var $form = $('.fform');	
	retrieveCourses($form);									
	retrieveItems();	
	$form.find('#sevent').change(function(){
							var $slt = $(this), $default = $slt.children('[value="select"]');
							if($default.length > 0){
								$default.remove();	
							}
							if($slt.val() != 'select'){
								showItems();
								//$('#ititle').focus();	
							}
							if($slt.val() != 'Announcement'){
								$('.spliter.optional').css('display', 'block');
							}else{
								$('.spliter.optional').css('display', 'none');
							}
							
							$form.find(".errordiv").each(function(){
															$(this).css("display", "none");
														});
							$form.find(".errorbdr").each(function(){
															$(this).removeClass("errorbdr");
														});
							$("#errtopics").length > 0 && $("#errtopics").remove();														
							$form.find('.item.optional').each(function(){
															var $me = $(this), display;
															switch($me.attr("event")){
																case "Links":
																	if($slt.val() == "Links"){
																		$.browser.msie ? $me.css("display", "block") : $me.css("display", "table");			
																	}else{
																		$me.css("display", "none");
																	}
																	break;
																case "Resources":
																	if($slt.val() == "Resources"){
																		$.browser.msie ? $me.css("display", "block") : $me.css("display", "table");
																	}else{
																		$me.css("display", "none");
																	}
																	break;
																case "RL":
																	if($slt.val() == "RL"){
																		$.browser.msie ? $me.css("display", "block") : $me.css("display", "table");															
																	}else{
																		$me.css("display", "none");
																	}
																	break;			 
															}	
														 });									
						 });									
	$("#icourse").change(function(){retrieveItems();});
	$('#search_str').focus(function(){
						if($(this).attr('default') == 1){
							$(this).val('');	
							$(this).removeClass('default_search');
						}
					})
					.blur(function(){
						if($(this).val() == ''){
							$(this).attr({'default':1})	
								   .val('Search Students')
								   .addClass('default_search');
						}else{
							$(this).attr({'default':0})
								   .removeClass('default_search');		
						}	
					})
					.keyup(function(){
						var $srcdiv = $('#src_content').find('ul [show="1"]'), searchStr = $(this).val().toLowerCase();	
						if(searchStr == ''){
							$srcdiv.each(function(){
										$(this).css('display', 'block');
									});
						}else{
							$srcdiv.each(function(){
										getLiName($(this)).indexOf(searchStr) > -1 ? $(this).css('display', 'block') : $(this).css('display', 'none');
									});	
						}
					});		
	$("#classes :checkbox, #groups :checkbox, #src_content :checkbox").each(function(){
		$(this).click(function(){checkAssign();});
	});
	$('.urlimg').click(function(){
				if(checkURL()){
					var _url = $('#Url').val();	
					openWindowFS(_url, 'vlink');	
				}
				return false;
			});	
	if($("#sdatetime").length == 1){
		var _sdatetime = $("#sdatetime").val(), _edatetime = $("#edatetime").val();	
		var datetimeformat = function(dt){
			var _dtobject = {};
			var _dtarr = dt.split(' ');
			var _d = _dtarr[0], _t = _dtarr[1];	
			_dtobject.time = _t;
			var _darr = _d.split('/');
			var _m = _darr[0], _d = _darr[1], _y = _darr[2];
			parseInt(_m, 10) < 10 && (_m = '0' + _m);
			parseInt(_d, 10) < 10 && (_d = '0' + _d);
			_dtobject.date = _d + '/' + _m + '/' + _y;
			return _dtobject;
		};
		var _sdtobject = datetimeformat(_sdatetime);
		var _edtobject = datetimeformat(_edatetime);
		$("#stpartdate").val(_sdtobject.date);
		$("#stparttime").val(_sdtobject.time);
		$("#etpartdate").val(_edtobject.date);
		$("#etparttime").val(_edtobject.time);
		if($("#stparttime").val() == '00:00' && $("#etparttime").val() == '00:00'){
			$("#stparttime").hide();
			$("#etparttime").hide();
			$("#IsAllDayEvent").attr('checked', true);	
		}
	}														
} 
function initEditOptionFields(){
	switch($('#edtype').val()){
		case 'RL':
			var _topics = new Array(), _topicstr = '', _topicids = '', $ul = $('<ul></ul>');
			_topicids = $('#TopicCodeArr').val();
			_topicstr = $('#edtopicnames').val();
			TopicID = _topicids.split(',');
			TopicName = _topicstr.split(',');
			_topics = TopicName;
			_topics.length > 0 && $("#disList ul li").remove();
			for(var i = 0; i < _topics.length; i++){
				$('<li></li>').text((i + 1) + ". " + _topics[i]).appendTo($ul);	
			}
			$("<li><a href='javascript:openTopicsWindow();'><u>Edit Topics</u></a></li>").appendTo($ul);
			$ul.appendTo($('#disList'));
			break;
		case 'Resources':
			var tempArr = new Array();
			tempArr.push('<div id="{fname}div" class="FieldLabel">');
			tempArr.push('<img width="16" height="16" border="0" style="vertical-align:middle; cursor:pointer" onclick="remove(\'{fname}\',\'{fname}div\', ', $('#edprotoid').val(), ')" src="images/delete.gif">');
			tempArr.push('<input id="{fname}" type="checkbox" checked="" value="', $('#edfilesize').val(),'|', $('#edorgfilename').val(), '" style="display:none;" name="{fname}">');
			tempArr.push('<a href="javascript:openWindowFS(\'../../includes/file.php?type=file&file=', $('#edschoolid').val(), ';{fname};', $('#eduserid').val(), '\',\'DLwinodw\')">', $('#edorgfilename').val(), '</a>');
			tempArr.push('</div>');
			tempStr = tempArr.join('');
			tempStr = tempStr.replace(/\{fname\}/gi, $('#edfilename').val());
			$(tempStr).appendTo($('#uploadedfile'));
			break;		
	}	
}
function in_array(a, arr){
	if((typeof(arr)).toLowerCase() != 'object'){
		return false;	
	}	
	for(var i = 0; i < arr.length; i++){
		if(a == arr[i]){return true;}
	}
	return false;
}
function initEditForm(){
	showItems();
	$('#sevent').children().each(function(){
										if($(this).val() == 'select'){
											$(this).remove();
										}else if($(this).val() == $('#edtype').val()){
											$(this).attr({'selected':true});
											$(this).parent().change();	
										}
									});
	$('#icourse').children().each(function(){
										if($(this).val() == $('#edcourse').val()){
											$(this).attr({'selected':true});	
										}
									});	
	retrieveItems();								
	initEditOptionFields();	
	if(CKEDITOR.instances['Message']){
		var _msg = decodeURIComponent($('#edmessage').val());	
		CKEDITOR.on('instanceReady', function(e){
			e.editor.setData(_msg.replace(/20%/g, ' '));
		});
	}else{
		var tmr = setInterval(function(){
								if(CKEDITOR.instances['Message']){
									var _msg = decodeURIComponent($('#edmessage').val()), _editor = CKEDITOR.instances['Message'];	
									CKEDITOR.on('instanceReady', function(e){
										e.editor.setData(_msg.replace(/20%/g, ' '));
										clearInterval(tmr);
									});
								}
							}, 300);
	}
	if($('#edisallday').val() > 0){
		$('#IsAllDayEvent').attr("checked", true); 
		$("#stparttime").val("00:00").hide();
		$("#etparttime").val("00:00").hide();
	}
	var $container = $('#cls_content'), destArr = ($('#edaclass').val()).split(','), origArr = [], tempArr = [];							
	$container.find(':checkbox').each(function(){
									in_array($(this).val(), destArr) && $(this).attr('checked', true);
								})
	$container = $('#grp_content');
	destArr = ($('#edagroup').val()).split(',');
	$container.find(':checkbox').each(function(){
									in_array($(this).val(), destArr) && $(this).attr('checked', true);
								})
	$container = $('#flt_content');
	origArr = ($('#edastudent').val()).split(',');	
	destArr = [];
	for(var i = 0; i < origArr.length; i++){
		tempArr = origArr[i].split('|');	
		in_array(tempArr[0], destArr) || destArr.push(tempArr[0]);
	}
	$container.find(':checkbox').each(function(){
									in_array($(this).val(), destArr) && $(this).attr('checked', true);
								})
	$('#src_content').find(':checkbox').each(function(){
											if(in_array($(this).attr('belongs'), destArr)){
												$(this).parent().css('display', 'block');
												in_array($(this).attr('belongs') + '|' + $(this).val(), origArr) && $(this).attr('checked', true);	
											}
										})							
	moveItems();																				
}

$(document).ready(function() {
	//debugger;
	var DATA_FEED_URL = "../php/datafeed.php";
	var arrT = [];
	var tt = "{0}:{1}";
	for (var i = 0; i < 24; i++) {
		arrT.push({ text: StrFormat(tt, [i >= 10 ? i : "0" + i, "00"]) }, { text: StrFormat(tt, [i >= 10 ? i : "0" + i, "30"]) });
	}
	$("#timezone").val(new Date().getTimezoneOffset()/60 * -1);
	$("#stparttime").dropdown({
		dropheight: 200,
		dropwidth:60,
		selectedchange: function() { },
		items: arrT
	});
	$("#etparttime").dropdown({
		dropheight: 200,
		dropwidth:60,
		selectedchange: function() { },
		items: arrT
	});
	var check = $("#IsAllDayEvent").click(function(e) {
		if ($(this).attr('checked')) {
			$("#stparttime").val("00:00").hide();
			$("#etparttime").val("00:00").hide();
		}
		else {
			var d = new Date();
			var p = 60 - d.getMinutes();
			if (p > 30) p = p - 30;
			d = DateAdd("n", p, d);
			$("#stparttime").val(getHM(d)).show();
			$("#etparttime").val(getHM(DateAdd("h", 1, d))).show();
		}
		checkTime();
	});
	if (check.attr("checked") == true) {
		$("#stparttime").val("00:00").hide();
		$("#etparttime").val("00:00").hide();
	}
	CKEDITOR.replace('Message', {
		toolbar: [{ name: 'basicstyles', items: [ 'Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript', 'RemoveFormat', 'TextColor', 'BGColor' , 'Link' , 'Smiley' ] }],
		width:400,
		height:200,
		resize_enabled:false,
		startupFocus:true,
		extraPlugins:'divarea'	
	});
	$2('#tabs').tabs();
	$("#Savebtn").click(function() { formSubmit(); });
	$("#Closebtn").click(function() { CloseModelWindow(); });
	$("#Deletebtn").click(function() {
		 if (confirm("Are you sure to remove this event")) {  
			var param = [{ "name": "calendarId", value: 8}];                
			$.post(DATA_FEED_URL + "?method=remove",
				param,
				function(data){
					  if (data.IsSuccess) {
							alert(data.Msg); 
							CloseModelWindow(null,true);                            
						}
						else {
							alert("Error occurs.\r\n" + data.Msg);
						}
				}
			,"json");
		}
	});
   	$("#stpartdate,#etpartdate").datepicker();
	
	var cv =$("#colorvalue").val() ;
	if(cv=="")
	{
		cv="-1";
	}
	$("#calendarcolor").colorselect({ title: "Color", index: cv, hiddenid: "colorvalue" });
	var type = document.getElementsByTagName('title')[0].innerHTML.toString().split('|')[1];
	initForm(type);
	if($('#edtype').length > 0){
		initEditForm();	
	}
	var options = {
		type: 'post',
		beforeSubmit: function() {
			return true;
		},
		dataType: "json",
		success: function(data) {
			alert(data.Msg);
			CloseModelWindow(null,true);  
		},
		error: function(xhr, textStatus, errorThrown){
			alert("You seem to meet trouble. Please contact ACE-Learning Support.");
			CloseModelWindow(parent.refreshWindow,false);
		}
	};
	$("#fmEdit").validate({
		submitHandler: function(form) { $("#fmEdit").ajaxSubmit(options); }
	});
	mobilecheck() && $('#swrapper').css('height', '600px');
});