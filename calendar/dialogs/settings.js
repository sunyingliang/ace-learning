function retrieveCourses(){
	var courseObj = {
		url: '../php/retrieveCourses.php',	
		type: 'GET',
		async: false,
		cache: false,
		dataType: 'json',
		success: function(responseObj, status){
			var _str = '';
			for(p in responseObj){
				_str += p + ': ' + responseObj[p] + '|';	
			}		
			if(!!responseObj['error'] === false){
				var chkbox, span, tempArr, $ul, $li;
				$ul = $('<ul></ul>').addClass('ul').appendTo($('#div_cos_content'));
				for(p in responseObj){
					tempArr = responseObj[p].split('|');
					$li = $('<li></li>').css('width', '90%').appendTo($ul);
					chkbox = $('<input type="checkbox" id="chk_' + tempArr[0].toLowerCase() + '" value="' + tempArr[0] + '" />').appendTo($li);
					span = $('<span><label for="chk_' + tempArr[0].toLowerCase() + '">&ensp;' + tempArr[1] + '</label></span>').appendTo($li);																													
				}		
			}
		}
	};	
	$.ajax(courseObj);
}
function retrieveTeachers(){
	var teacherObj = {
		url: '../php/retrieveTeachers.php',		
		type: 'GET',
		async: false,
		cache: false,
		dataType: 'json',
		success: function(responseObj, status){
			if(!!responseObj['error'] === false){
				var $ul = $('<ul></ul>').addClass('ul-items').appendTo($("#assignby_content")), $li, $chkbox, $span, _item;	
				for(var i = 0; !!responseObj[i]; i++){
					_item = responseObj[i].split('|');	
					$li = $('<li></li>').appendTo($ul);
					$chkbox = $('<input type="checkbox" id="assignby_content_' + _item[0] + '" value="' + _item[0] + '" name="Teachers[]" />').appendTo($li);
					$span = $('<span><label for="assignby_content_' + _item[0] + '">&ensp;' + _item[1] + '</label></span>').appendTo($li);	
				}
				attachEventAll($('#div_assignby_content_all'));
			}	
		}
	};	
	$.ajax(teacherObj);
}
function retrieveTypes($droot, vtype, $clsdiv){
	var cid = '', divid = $droot.attr('id');
	if(vtype == 'Student'){
		$clsdiv.find(':checkbox').each(function(){
									 cid += cid == '' ? $(this).val() : ',' + $(this).val();
								 });
	}	
	var	typeObj = {
		url: '../php/retrieveTypeItemsNav.php',	
		type: 'POST',
		async: false,
		cache: false,
		data: {classIds: cid, type: vtype},
		dataType: 'json',
		success: function(responseObj, status){
			$droot.empty();	
			if(responseObj['success']){
				var $ui = $('<ul></ul>').addClass('ui-items').appendTo($droot), $li, $chkbox, $span, _item;
				switch(vtype){
					case 'Class':
						var clsType = $droot.attr('id') == 'cls_content' ? 'Class' : 'StuClass';
						for(var i = 0; !!responseObj[i]; i++){
							_item = responseObj[i].split(',');	
							$li = $('<li></li>').appendTo($ui);
							$chkbox = $droot.attr('id') == 'cls_content' ? $('<input type="checkbox" id="' + divid + '_' + _item[0].toLowerCase() + '" value="' + _item[1] + '" name="Class[]" />').appendTo($li) : $('<input type="checkbox" id="' + divid + '_' + _item[0].toLowerCase() + '" value="' + _item[1] + '" />').appendTo($li);
							$span = $('<span><label for="' + divid + '_' + _item[0].toLowerCase() + '">&ensp;' + _item[1] + '</label></span>').appendTo($li);	
						}
						if($droot.attr('id') == 'cls_content'){
							attachEventAll($('#cls_all'));
						}else{
							retrieveTypes($('#src_content'), 'Student', $droot);
							attachEventFilter();
						}
						break;
					case 'Group':
						for(var i = 0; !!responseObj[i]; i++){
							_item = responseObj[i].split(',');	
							$li = $('<li></li>').appendTo($ui.removeClass('ui-items').addClass('ul-items'));
							$chkbox = $('<input type="checkbox" id="' + divid + '_' + _item[0].toLowerCase() + '" value="' + _item[0] + '" name="Group[]" />').appendTo($li);
							$span = $('<span><label for="' + divid + '_' + _item[0].toLowerCase() + '">&ensp;' + _item[1] + '</label></span>').appendTo($li);	
						}
						attachEventAll($('#grp_all'));
						break;
					case 'Student':
						for(var i = 0; !!responseObj[i]; i++){
							_item = responseObj[i].split(',');	
							$li = $('<li show="0"></li>').appendTo($ui);
							$chkbox = $('<input type="checkbox" id="' + divid + '_' + _item[0].toLowerCase() + '" value="' + _item[0] + '" belongs="' + _item[2] + '" />').appendTo($li);
							$span = $('<span><label for="' + divid + '_' + _item[0].toLowerCase() + '">&ensp;' + _item[1] + '&ensp;(' + _item[2] + ')</label></span>').appendTo($li);	
						}
						$('#src_content').find('ul li').css('display', 'none');
						$('#src_content').find(':checkbox').click(function(){
																moveItems();
 														   });	
						break;	
				}	
			}
		}
	};	
	$.ajax(typeObj);	
}
function getLiName($li){
	return $li.find('label').html().toLowerCase();	
}
function attachEventAll($chk){
	$chk.click(function(){
			var $destdiv = $chk.parent().parent().next(), _chked = $chk.attr('checked');
			$destdiv.find(':checkbox').each(function(){
									  	  $(this).attr('checked', _chked);
									  });
		});	
}
function attachEventFilter(){
	$('#flt_content').find(':checkbox').click(function(){
											$('#search_str').val('').attr({'default':1}).val('Search Students').addClass('default_search');
											$('#src_content').find('ul [show="1"]').css('display', 'block');
											var clsid = $(this).val(), display = $(this).attr('checked') == true ? 'block' : 'none', show = $(this).attr('checked') == true ? 1 : 0;
											$('#src_content ul li').each(function(){
																			if($(this).children(':checkbox').attr('belongs') == clsid){
																				$(this).css('display', display).attr('show', show);
																			}
																		})
										});
}
function attachEventSelect(){
	$('#src_content').find(':checkbox').click(function(){
											moveItems();
										});								
}
function moveItems(){
	var $items = $('#src_content').find(':checked'), $destul = $('#des_content').find('ul'), $li, $chkbox, $span;
	$('#sltd_stu_num').text($items.length);
	$destul.empty();
	$items.each(function(){
			  $li = $(this).parent().clone();
			  $chkbox = $li.children(':checkbox');
			  $chkbox.attr({'id':$chkbox.val(), 'name':'Student[]', 'checked':true}).val($chkbox.attr('belongs') + '|' + $chkbox.val());
			  $li.find('label').attr('for', $chkbox.attr('id'));
			  $li.css('display', 'block').appendTo($destul);					 
		  });
	$destul.find(':checkbox').click(function(){
									var _itemval = $(this).attr('id');
									$('#src_content').find(':checked').each(function(){
																			if($(this).val() == _itemval){
																				$(this).attr('checked', false);	
																				moveItems();
																			}
																		});
								});	  
}
function initCourses(){
	retrieveCourses();
}
function initTeachers(){
	retrieveTeachers();
	if($('#assignby_content ul li').length < 7){
		var offlines = 4 -	$('#assignby_content ul li').length / 2;
		$('#assignby_content').css('height', 120 - 24 * offlines);
	}	
}
function initViewBy(){
	retrieveTypes($('#cls_content'), 'Class');
	retrieveTypes($('#grp_content'), 'Group');
	retrieveTypes($('#flt_content'), 'Class');
	$('#search_str').focus(function(){
						if($(this).attr('default') == 1){
							$(this).val('');	
							$(this).removeClass('default_search');
						}
					})
					.blur(function(){
						if($(this).val() == ''){
							$(this).attr({'default':1})	
								   .val('Search Students')
								   .addClass('default_search');
						}else{
							$(this).attr({'default':0})
								   .removeClass('default_search');		
						}	
					})
					.keyup(function(){
						var $srcdiv = $('#src_content').find('ul [show="1"]'), searchStr = $(this).val().toLowerCase();	
						if(searchStr == ''){
							$srcdiv.each(function(){
										$(this).css('display', 'block');
									});
						}else{
							$srcdiv.each(function(){
										getLiName($(this)).indexOf(searchStr) > -1 ? $(this).css('display', 'block') : $(this).css('display', 'none');
									});	
						}
					});	
}
function initEvents(){
	$('#btn_confirm').click(function(){
						parent.initSettings(getConditions());
						CloseModelWindow(null, true);
					})	
	$('#btn_cancel').click(function(){
						CloseModelWindow(null, false);
					})	
}
function getConditions(){
	var getValues = function($chkboxes){
			var valueStr = '';
			$chkboxes.each(function(){
					 	 if($(this).attr('checked')){
							 valueStr += valueStr == '' ? $(this).val() : (',' + $(this).val());	
					 	 }	
					 });		 
			return valueStr;		 
		};	
	return [
				{name: "courseStr", value: getValues($('#div_cos_content :checkbox'))},
				{name: "eventStr", value: getValues($('#div_et_content :checkbox'))},
				{name: "teachers", value: getValues($('#assignby_content :checkbox'))},
				{name: "classes", value: getValues($('#cls_content :checkbox'))},
				{name: "groups", value: getValues($('#grp_content :checkbox'))},
				{name: "students", value: getValues($('#des_content :checkbox'))}
		   ];	 	
}
function inarray(ele, arr){
	for(var i = 0; i < arr.length; i++){
		if(arr[i] == ele){
			return true;	
		}
	}	
	return false;
}
function initConfig(){
	if($("#first").val() == 0){
		if($("#ecourse").length > 0){
			var _coursearr = $("#ecourse").val().split(",");
			$("#div_cos_content :checkbox").each(function(){
				inarray($(this).val(), _coursearr) && $(this).attr("checked", true);	
			});
		}
		if($("#eevent").length > 0){
			var _eventarr = $("#eevent").val().split(",");
			$("#div_et_content :checkbox").each(function(){
				inarray($(this).val(), _eventarr) && $(this).attr("checked", true);	
			});	
		}
		if($("#eteacher").length > 0){
			var _teacherarr = $("#eteacher").val().split(",");
			$("#assignby_content :checkbox").each(function(){
				inarray($(this).val(), _teacherarr) && $(this).attr("checked", true);	
			});	
		}
		if($("#eclass").length > 0){
			var _classarr = $("#eclass").val().split(",");
			$("#cls_content :checkbox").each(function(){
				inarray($(this).val(), _classarr) && $(this).attr("checked", true);	
			});	
		}
		if($("#egroup").length > 0){
			var _grouparr = $("#egroup").val().split(",");
			$("#grp_content :checkbox").each(function(){
				inarray($(this).val(), _grouparr) && $(this).attr("checked", true);	
			});	
		}
		if($("#estudent").length > 0){
			var _clsstuparr = $("#estudent").val().split(","), _clsarr = new Array(), _studentarr = new Array(), _temparr;
			for(var i = 0; i < _clsstuparr.length; i++){
				_temparr = _clsstuparr[i].split('|');
				_clsarr.push(_temparr[0]);
				_studentarr.push(_temparr[1]);	
			}
			$("#flt_content :checkbox").each(function(){
				inarray($(this).val(), _clsarr) && $(this).attr("checked", true);	
			});
			$("#src_content :checkbox").each(function(){
				inarray($(this).attr('belongs'), _clsarr) && $(this).parent().css('display', 'block').attr('show', 1);
				inarray($(this).val(), _studentarr) && ($(this).attr("checked", true), moveItems());	
			});
		}
	
	}else{
		$("#div_cos_content :checkbox, #div_et_content :checkbox").each(function(){$(this).attr("checked", true);});
		var _userid = $('#userid').val();
		$('#assignby :checkbox').each(function(){
			$(this).val() == _userid && $(this).attr('checked', true);
		});
	}
}


$(document).ready(function(){
	$2('#tabs').tabs();
	initCourses();	
	initTeachers();
	initViewBy();
	initEvents();
	initConfig();	
	mobilecheck() && $('#swrapper').css('height', '600px');
});
