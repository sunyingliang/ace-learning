<?php
include_once('parameters.php');
include_once('tempForm.php');

$_GET = array();
$params = explode('&', $_SERVER['QUERY_STRING']);
foreach($params as $pair){
	list($key, $value) = explode('=', $pair);	
	$_GET[urldecode($key)] = urldecode($value);
}
if($_GET['type']){
	$eventType = $_GET['type'];
	$course = $_GET['course'];
	$title = $_GET['title'];
	$color = $_GET['color'];
	$message = urlencode(str_replace(" ", "20%", $_GET['message']));
	$start = $_GET['start'];
	$end = $_GET['end'];
	$isallday = $_GET['isallday'];
	$classes = $_GET['classes'];
	$groups = $_GET['groups'];
	$students = $_GET['students'];
	$assignids = $_GET['assignids']; 
	$event;
	switch($eventType){
		case 'Announcement':
			$event = new TempForm($eventType, $course, $title, $color, $message, $start, $end, $isallday, $assignids, $classes, $groups, $students);
			$event->init();
			break;
		case 'Links':
			$url = $_GET['url'];
			$event = new TpLinks($eventType, $course, $title, $color, $message, $start, $end, $isallday, $assignids, $classes, $groups, $students, $url);
			$event->init();
			break;
		case 'RL':
			$topicstr = $_GET['topicstr'];
			$topicids = $_GET['topicids'];
			$event = new TpRL($eventType, $course, $title, $color, $message, $start, $end, $isallday, $assignids, $classes, $groups, $students, $topicstr, $topicids);
			$event->init();
			break;
		case 'Resources':
			$schoolid = $_GET['schoolid'];
			$userid = $_GET['userid'];
			$protoid = $_GET['protoid'];
			$filename = $_GET['filename'];
			$orgfilename = $_GET['originalfilename'];
			$filesize = $_GET['filesize'];
			$event = new TpResources($eventType, $course, $title, $color, $message, $start, $end, $isallday, $assignids, $classes, $groups, $students, $schoolid, $userid, $protoid, $filename, $orgfilename, $filesize);
			$event->init();
			break;		
	}
}else{
	$eventType = 'Event';		
}
$c_path = 'event'.','.$_SESSION['SessionSchoolID'].','.$_SESSION['SessionUserID']; 
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<!-- saved from url=(0011)192.168.1.2 -->
<html xmlns="http://www.w3.org/1999/xhtml" >
<head>    
<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">    
<title><?php echo $c_path.'|'.$eventType;?></title>         
<link href="../css/dp.css" rel="stylesheet" />    
<link href="../css/dropdown.css" rel="stylesheet" />    
<link href="../css/colorselect.css" rel="stylesheet" />
<link href="../../stylesheets/stylesheet.css" rel="stylesheet" />
<link href="../../stylesheets/getProgressbar.css" rel="stylesheet" />  
<link type="text/css" rel="stylesheet" href="dialogs.css" />
<link type="text/css" rel="stylesheet" href="../../stylesheets/calendar_1.css" /> 
<link type="text/css" rel="stylesheet" href="../../css/jqueryui/jqueryui.css" /> 
<script type="text/javascript" src="../../jsexternal/jsexternal.js"></script>
<script src="../src/jquery.js" type="text/javascript"></script>    
<script type="text/javascript" src="../../js/jquery-1.9.1.js"></script>
<script type="text/javascript" src="../../js/jqueryui/jquery-ui-1.10.3.js"></script>
<script type="text/javascript" src="transfer.js"></script>
<script src="../src/Plugins/Common.js" type="text/javascript"></script>        
<script src="../src/Plugins/jquery.form.js" type="text/javascript"></script>     
<script src="../src/Plugins/jquery.validate.js" type="text/javascript"></script>     
<script src="../src/Plugins/datepicker_lang_US.js" type="text/javascript"></script>        
<script src="../src/Plugins/jquery.datepicker.js" type="text/javascript"></script>     
<script src="../src/Plugins/jquery.dropdown.js" type="text/javascript"></script>     
<script src="../src/Plugins/jquery.colorselect.js" type="text/javascript"></script>    
<script type="text/javascript" src="../../ckeditor/ckeditor.js"></script>
<script src="dialogs.js" type="text/javascript"></script>
</head>
<body>
<div id="swrapper">	   
	<a name="top"></a> 
    <div>        
        <div class="infocontainer">            
            <form action="../php/datafeed.php?method=adddetails<?php echo isset($event)?'&ids='.$event->assignids:''; ?>" enctype="multipart/form-data" class="fform" id="fmEdit" method="post" type="Announcement" visibility="1">           
                <div id="devent" class="item">
                    <div id="delabel" class="llabel">
	                    <label for="sevent"><span>Event</span></label>
                    </div>
                    <div id="detext" class="rcontent">
                    	<select id="sevent" name="event" class="select">
                        	<option value="select">Select an event</option>
                        	<option value="Announcement">Announcement</option>
                        	<option value="RL">Reading Assignment</option>
                        	<option value="Resources">Documents</option>
                        	<option value="Links">Link</option>
                        </select>
                        <?php
                        	if($event){
						?>
                        <input type="hidden" id="edtype" name="edtype" value="<?php echo $event->type; ?>" />
                        <?php			
							}
						?>
                    </div>
                </div>
                <div id="dcourse" class="item">
                    <div id="dclabel" class="llabel">
	                    <label for="icourse"><span>Course</span></label>
                    </div>
                    <div id="dctext" class="rcontent">
                    	<select id="icourse" name="course" class="select">
                        </select>
                        <?php
                        	if($event){
						?>
                        <input type="hidden" id="edcourse" name="edcourse" value="<?php echo $event->course; ?>" />
                        <?php			
							}
						?>
                    </div>
                </div>
                <div id="dtitle" class="item">
                    <div id="dtlabel" class="llabel">
    	                <label for="ititle"><span>Title</span></label>
                    </div>
                    <div id="dtcontent" class="rcontent">
                        <div id="dttext" class="ttext">
	                        <input type="text" id="ititle" name="title" value="<?php echo isset($event)?$event->title:''; ?>" maxlength="200" onblur="checkTitle();" />
                            <?php
								if($event){
							?>
							<input type="hidden" id="otitle" name="otitle" value="<?php echo isset($event)?$event->title:''; ?>" />
							<?php			
								}
							?>
                        </div>
                        <div id="calendarcolor" class="inline" style="margin-top:4px;">
                        </div>
                        <div id="titleerror" class="errordiv"></div>
                        <input id="colorvalue" name="colorvalue" type="hidden" value="<?php echo isset($event)?$event->color:''; ?>" />
                    </div>
                </div>
                <div class="spliter optional"></div>
                <div id="durl" class="item optional" event="Links">
                    <div id="dulabel" class="llabel">
	                    <label for="Url"><span>URL</span></label>
                    </div>
                    <div id="ducontent" class="rcontent">
                    	<div id="dutext" class="ttext">
	                        <input type="text" id='Url' name='Url' value="<?php echo ($event && $event->type == 'Links')?$event->url:'http://'; ?>" maxlength="200" onblur="checkURL();" />
                        </div>
                        <div id="duimg">
	                        <input type="image" src="images/test.gif" title="View Link" class="urlimg" />
                        </div>
                        <div id="urlerror" class="errordiv"></div>
                    </div>
                </div>
                <div id="dresource" class="item optional" event="Resources">
                	<div style="display:table; width:100%;">
                    	<div id="drslabel" class="llabel">
                            <label for="Resource"><span>File</span></label>
                        </div>
                        <div id="drscontent" class="rcontent">
                            <div id="drstext" class="left" style="width:310px; height:24px;">
                            	<div id="uploadfilefield" style="display:inline-block; width:225px; margin:0px; padding:0px;">
                                	<input type="file" id='filefieldname' name='filefieldname' value="scan" style="width:225px;" onclick="javascript:return checkBeforeUpload();" onchange="showAttach()" />
                                </div>
                                <input type="button" id="Attach" name="Attach" value="Upload" style="width:65px; visibility:hidden;" onclick="uploadFile();" class="JcreateAttachbtn" />
                                <input type="hidden" id="MAX_FILE_SIZE" name="MAX_FILE_SIZE" value="<?php echo $maxEachFileSize;?>" />
                                <input type="hidden" id="TOTAL_FILE_SIZE" name="TOTAL_FILE_SIZE" value="" />
                                <input type="hidden" id="TOTAL_MAX_FILE_SIZE" name="TOTAL_MAX_FILE_SIZE" value="<?php echo $maxTotalFileSize;?>" />
                                <input type="hidden" id="UploadFile" name="UploadFile" value="1" />
                                <input type="hidden" id="DeleteFile" name="DeleteFile" value="" />
                                <input type="hidden" id="AllowFileExtensions" value="<?php echo implode('|', $allowed_file_types);?>" />
                            </div>
		                    <div id="fileerror" class="errordiv"></div>
                        </div>
                    </div>
					<div style="display:table; width:100%;">
                    	<div class="llabel" style="width:200px; display:table;">
                            <p>
                            	Max allowed files: <?php echo $maxNoOfFile;?><br>
                            	Max file size / file: <?php echo $maxSizePerFile;?> MB<br>
                            </p>
                        </div>
                        <div class="rcontent" style="display:table;">
                        	<div id="cnt">
                            	<div id="uploadedfile"></div>
                                <?php
									if($event && $event->type == 'Resources'){
								?>
                                <input type="hidden" id="edschoolid" name="edschoolid" value="<?php echo $event->schoolid; ?>" />
                                <input type="hidden" id="eduserid" name="eduserid" value="<?php echo $event->userid; ?>" />
								<input type="hidden" id="edprotoid" name="edprotoid" value="<?php echo $event->protoid; ?>" />
								<input type="hidden" id="edfilename" name="edfilename" value="<?php echo $event->filename; ?>" />
								<input type="hidden" id="edorgfilename" name="edorgfilename" value="<?php echo $event->origfilename; ?>" />
								<input type="hidden" id="edfilesize" name="edfilesize" value="<?php echo $event->filesizes; ?>" />
								<?php			
									}
								?>
                                <div id="pb_outer" style="display: none;">
                                	Uploading file ...<br>
	                                <img id="loader" border="0" style="float:left;" alt="loading" src="images/loader.gif" name="loader">
                                </div>
                            </div>
                            <div id="hiddenfield"></div>
                            <iframe name="hiddenframe" style="display:none;" >Loading...</iframe>
                        </div>
                    </div>                    
                </div>
                <div id="dreadingassignment" class="item optional" event="RL">
                    <div id="drllabel" class="llabel">
	                    <label><span>Topics</span></label>
                    </div>
                    <div id="drscontent" class="rcontent">
                    	<div id="disList" class="topicshow">
                        	<ul>
                            	<li><a href="javascript:openTopicsWindow();"><u>Select Topics</u></a></li>
                            </ul>
                        </div>
                        <input id="TopicCodeArr" type="hidden" value="<?php echo ($event && $event->type == 'RL')?$event->topicids:''; ?>" name="TopicCodeArr">
                        <?php
							if($event && $event->type == 'RL'){
						?>
						<input type="hidden" id="edtopicnames" name="edtopicnames" value="<?php echo $event->topicnames; ?>" />
						<?php			
							}
						?>
                    </div>
                </div>
                <div class="spliter optional"></div>
                <div id="dmessage" class="item">
                    <div id="dmlabel" class="llabel">
	                    <label for="Message"><span>Message</span></label>
                    </div>
                    <div id="dmcontent" class="rcontent">
                        <textarea id='Message' style='width: 400px; height: 300px;' class='FieldInput' name='Message'><?php echo $event ? $event->message : ''; ?></textarea>
                        <?php
							if($event){
						?>
						<input type="hidden" id="edmessage" name="edmessage" value="<?php echo $event->message; ?>" />
						<?php			
							}
						?>
                    </div>
                </div>
                <div class="spliter"></div>
                <div class="item">
                	<div class="llabel">
	                    <label><span class="section_label label_section">Date</span></label>
                    </div>
                </div>
                <div id="ddstart" class="item">
                    <div id="ddslabel" class="llabel">
	                    <label for="stpartdate"><span>From</span></label>
                    </div>
                    <div id="ddscontent" class="rcontent">
                    	<div id="ddsdfrom" class="left" style="width:120px;">
	                        <input MaxLength="10" class="calpick" id="stpartdate" name="stpartdate" style="width:100px;" type="text" value="<?php echo isset($event)?$event->startday:''; ?>" onblur="checkFromTime();" />
                        </div>
                        <div id="ddstfrom" class="left" style="width:60px;">
                        	<input MaxLength="5" id="stparttime" name="stparttime" style="width:40px;" type="text" value="<?php echo isset($event)?$event->starttime:''; ?>" onblur="checkFromTime();" />
                        </div>
                        <?php
							if(isset($_GET["sdt"]) && !empty($_GET["sdt"])){
						?>
						<input type="hidden" id="sdatetime" name="sdatetime" value="<?php echo $_GET["sdt"]; ?>" />
						<?php			
							}
						?>
	                    <div id="ddserror" class="errordiv"></div>
                    </div>
                </div>
                <div id="ddend" class="item">
                    <div id="ddelabel" class="llabel">
	                    <label for="etpartdate"><span>To</span></label>
                    </div>
                    <div id="decontent" class="rcontent">
                    	<div id="ddedto" class="left" style="width:120px;">
                        	<input MaxLength="10" class="calpick" id="etpartdate" name="etpartdate" style="width:100px;" type="text" value="<?php echo isset($event)?$event->endday:''; ?>" onblur="checkEndTime();" />                       
                        </div>
                        <div id="ddedto" class="left" style="width:60px;">
                        	<input MaxLength="5" id="etparttime" name="etparttime" style="width:40px;" type="text" value="<?php echo isset($event)?$event->endtime:''; ?>" onblur="checkEndTime();" />
                        </div>
                        <?php
							if(isset($_GET["edt"]) && !empty($_GET["edt"])){
						?>
						<input type="hidden" id="edatetime" name="edatetime" value="<?php echo $_GET["edt"]; ?>" />
						<?php			
							}
						?>
	                    <div id="ddeerror" class="errordiv"></div>
                    </div>
                </div>
                <div id="ddall" class="item">
                    <div id="ddalabel" class="llabel">
	                    <label for="IsAllDayEvent"><span>All day event</span></label>
                    </div>
                    <div id="decontent" class="rcontent">
    	                <input type="checkbox" id="IsAllDayEvent" name="IsAllDayEvent" />	 
                        <?php
							if($event){
						?>
						<input type="hidden" id="edisallday" name="edisallday" value="<?php echo $event->isallday; ?>" />
						<?php			
							}
						?>          	
                    </div>
                </div>
                <div class="spliter"></div>
                <div class="item">
                	<div class="llabel">
	                    <label><span id="assigntolbl" class="section_label label_section">Assign to</span></label>
                    </div>
                    <div id="assignerror" class="errordiv"></div>
                </div>
                <div id="tabs" style="margin-top:5px; margin-bottom:20px;" class="phony">
                    <ul>
                        <li>
                            <a href="#tabs-classes">Classes</a>
                        </li>
                        <li>
                            <a href="#tabs-groups">Groups</a>
                        </li>
                        <li>
                            <a href="#tabs-students">Students</a>
                        </li>
                    </ul>
                    <?php
						include("assigntypes_f.php");
						if($event){
					?>
					<input type="hidden" id="edaclass" name="edaclass" value="<?php echo $event->classes; ?>" />
					<input type="hidden" id="edagroup" name="edagroup" value="<?php echo $event->groups; ?>" />
					<input type="hidden" id="edastudent" name="edastudent" value="<?php echo $event->students; ?>" />
					<?php			
						}
					?>
                </div>        
                <input id="timezone" name="timezone" type="hidden" value="" />
                <input id="newFile" type="hidden" value="0" name="newFile">    
                <?php
					if($event){
				?>
				<input type="hidden" id="edmode" name="edmode" value="1" />
				<?php			
					}
				?>       
            </form>         
        </div>                  
        <div style="clear: both">         
        </div>      
        <div class="toolBotton phony"> 
        	<div class="btns1">
                <input type="button" id="Savebtn" value="Save" />
            </div>
            <div class="btns2">
                <input type="button" id="Closebtn" value="Cancel" class="cancel" />
            </div>      
        </div>         
    </div>
</div>    
</body>
</html>