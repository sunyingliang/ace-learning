xinha_editors = null;
xinha_init    = null;
xinha_config  = null;
xinha_plugins = null;

//alert('I am in xinha config now..');
// This contains the names of textareas we will make into Xinha editors
xinha_init = xinha_init ? xinha_init : function()
{
	/** STEP 1 ***************************************************************
	* First, specify the textareas that shall be turned into Xinhas. 
	* For each one add the respective id to the xinha_editors array.
	* I you want add more than on textarea, keep in mind that these 
	* values are comma seperated BUT there is no comma after the last value.
	* If you are going to use this configuration on several pages with different
	* textarea ids, you can add them all. The ones that are not found on the
	* current page will just be skipped.
	************************************************************************/
	
	xinha_editors = xinha_editors ? xinha_editors :
	[
		'Message'
	];
	
	/** STEP 2 ***************************************************************
	* Now, what are the plugins you will be using in the editors on this
	* page.  List all the plugins you will need, even if not all the editors
	* will use all the plugins.
	*
	* The list of plugins below is a good starting point, but if you prefer
	* a simpler editor to start with then you can use the following 
	* 
	* xinha_plugins = xinha_plugins ? xinha_plugins : [ ];
	*
	* which will load no extra plugins at all.
	************************************************************************/
	
	xinha_plugins = xinha_plugins ? xinha_plugins :
	[
		'InsertSmiley', 'formula'/*, 'xinha_wiris'*/
	];
	
	// THIS BIT OF JAVASCRIPT LOADS THE PLUGINS, NO TOUCHING  :)
	if(!Xinha.loadPlugins(xinha_plugins, xinha_init)) return;
	
	
	/** STEP 3 ***************************************************************
	* We create a default configuration to be used by all the editors.
	* If you wish to configure some of the editors differently this will be
	* done in step 5.
	*
	* If you want to modify the default config you might do something like this.
	*
	*   xinha_config = new Xinha.Config();
	*   xinha_config.width  = '640px';
	*   xinha_config.height = '420px';
	*
	*************************************************************************/
	
	xinha_config = xinha_config ? xinha_config() : new Xinha.Config();
	xinha_config.width  = '400px';
	xinha_config.height = '300px';
	
	if(insertImage)
	{
		xinha_config.toolbar =
				[
					["formatblock","fontname","fontsize","bold","italic","underline","strikethrough", "insertunorderedlist","outdent","indent"],
					["linebreak","forecolor","hilitecolor","textindicator"],
					["subscript","superscript"],	
					["separator","inserthorizontalrule","createlink", "insertimage"],
					["linebreak","separator","undo","redo","selectall"],
					["separator","clearfonts","removeformat"],
					["showhelp"]
				];
	}else{
		xinha_config.toolbar =
				[
					["formatblock","fontname","fontsize","bold","italic","underline","strikethrough", "insertunorderedlist","outdent","indent"],
					["linebreak","forecolor","hilitecolor","textindicator"],
					["subscript","superscript"],	
					["separator","inserthorizontalrule","createlink"],
					["linebreak","separator","undo","redo","selectall"],
					["separator","clearfonts","removeformat"],
					["showhelp"]
				];
	}
	
	// To adjust the styling inside the editor, we can load an external stylesheet like this
	// NOTE : YOU MUST GIVE AN ABSOLUTE URL
	xinha_config.pageStyle = "body {font-family: Verdana; font-size: 10pt;} p{margin:1px;}";
	xinha_config.statusBar = false;
	xinha_config.autofocus = true;
	//   alert(_editor_url + "examples/files/full_example.css");
	
	/** STEP 4 ***************************************************************
	* We first create editors for the textareas.
	*
	* You can do this in two ways, either
	*
	*   xinha_editors   = Xinha.makeEditors(xinha_editors, xinha_config, xinha_plugins);
	*
	* if you want all the editor objects to use the same set of plugins, OR;
	*
	*   xinha_editors = Xinha.makeEditors(xinha_editors, xinha_config);
	*   xinha_editors.myTextArea.registerPlugins(['Stylist']);
	*   xinha_editors.anotherOne.registerPlugins(['CSS','SuperClean']);
	*
	* if you want to use a different set of plugins for one or more of the
	* editors.
	************************************************************************/
	/*
	xinha_editors   = Xinha.makeEditors(xinha_editors, xinha_config, xinha_plugins);
	*/
	xinha_editors = Xinha.makeEditors(xinha_editors, xinha_config);			
	xinha_editors.Message.registerPlugins(['formula']);
	
	/** STEP 5 ***************************************************************
	* If you want to change the configuration variables of any of the
	* editors,  this is the place to do that, for example you might want to
	* change the width and height of one of the editors, like this...
	*
	*   xinha_editors.myTextArea.config.width  = '640px';
	*   xinha_editors.myTextArea.config.height = '480px';
	*
	************************************************************************/
	
	xinha_editors.Message.config.formatblock={"Normal":"p","Heading 1":"h1","Heading 2":"h2","Heading 3":"h3","Heading 4":"h4","Heading 5":"h5","Heading 6":"h6","Normal":"p","Address":"address","Formatted":"pre"};
	xinha_editors.Message.config.fontname={"Verdana":"verdana,arial,helvetica,sans-serif","Arial":"arial,helvetica,sans-serif","Courier New":"courier new,courier,monospace","Georgia":"georgia,times new roman,times,serif","Tahoma":"tahoma,arial,helvetica,sans-serif","Times New Roman":"times new roman,times,serif","Verdana":"verdana,arial,helvetica,sans-serif","impact":"impact","WingDings":"wingdings"};
	xinha_editors.Message.config.fontsize={"10 pt":"2","8 pt":"1","10 pt":"2","12 pt":"3","14 pt":"4","18 pt":"5","24 pt":"6","36 pt":"7"};
	xinha_editors.Message.config.width  = '400px';
	xinha_editors.Message.config.height  = '300px';
	
	/** STEP 6 ***************************************************************
	* Finally we "start" the editors, this turns the textareas into
	* Xinha editors.
	************************************************************************/
	
	Xinha.startEditors(xinha_editors);
	xinha_editors.Message.whenDocReady(ActivateEditor);
}

function ActivateEditor()
{
	
	xinha_editors.Message.activateEditor();
}
Xinha.addOnloadHandler(xinha_init);

Xinha._addEvent(window,'load', xinha_init); // this executes the xinha_init function on page load 
                                            // and does not interfere with window.onload properties set by other scripts