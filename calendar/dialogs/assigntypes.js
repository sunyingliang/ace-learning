$(document).ready(function(){
	//initial some fields firstly
	retrieveTypes($('#cls_content'), 'Class');
	retrieveTypes($('#grp_content'), 'Group');
	retrieveTypes($('#flt_content'), 'Class');
	//default search string
	$('#search_str').focus(function(){
						if($(this).attr('default') == 1){
							$(this).val('');	
							$(this).removeClass('default_search');
						}
					})
					.blur(function(){
						if($(this).val() == ''){
							$(this).attr({'default':1})	
								   .val('Search Students')
								   .addClass('default_search');
						}else{
							$(this).attr({'default':0})
								   .removeClass('default_search');		
						}	
					})
					.keyup(function(){
						var $srcdiv = $('#src_content').find('ul [show="1"]'), searchStr = $(this).val().toLowerCase();	
						if(searchStr == ''){
							$srcdiv.each(function(){
										$(this).css('display', 'block');
									});
						}else{
							$srcdiv.each(function(){
										getLiName($(this)).indexOf(searchStr) > -1 ? $(this).css('display', 'block') : $(this).css('display', 'none');
									});	
						}
					});	
});


function getLiName($li){
	return $li.find('label').html().toLowerCase();	
}
function attachEventAll($chk){
	$chk.click(function(){
			var $destdiv = $chk.parent().parent().next(), _chked = $chk.attr('checked');
			$destdiv.find(':checkbox').each(function(){
									  	  $(this).attr('checked', _chked);
									  });
		});	
}
function attachEventFilter(){
	$('#flt_content').find(':checkbox').click(function(){
											$('#search_str').val('').attr({'default':1}).val('Search Students').addClass('default_search');
											$('#src_content').find('ul [show="1"]').css('display', 'block');
											var clsid = $(this).val(), display = $(this).attr('checked') == true ? 'block' : 'none', show = $(this).attr('checked') == true ? 1 : 0;
											$('#src_content ul li').each(function(){
																			if($(this).children(':checkbox').attr('belongs') == clsid){
																				$(this).css('display', display).attr('show', show);
																			}
																		})
										});
}
function attachEventSelect(){
	$('#src_content').find(':checkbox').click(function(){
											moveItems();
										});								
}
function moveItems(){
	var $items = $('#src_content').find(':checked'), $destul = $('#des_content').find('ul'), $li, $chkbox, $span;
	$('#sltd_stu_num').text($items.length);
	$destul.empty();
	$items.each(function(){
			  $li = $(this).parent().clone();
			  $chkbox = $li.children(':checkbox');
			  $chkbox.attr({'id':$chkbox.val(), 'name':'Student[]', 'checked':true}).val($chkbox.attr('belongs') + '|' + $chkbox.val());
			  $li.find('label').attr('for', $chkbox.attr('id'));
			  $li.css('display', 'block').appendTo($destul);					 
		  });
	$destul.find(':checkbox').click(function(){
									var _itemval = $(this).attr('id');
									$('#src_content').find(':checked').each(function(){
																			if($(this).val() == _itemval){
																				$(this).attr('checked', false);	
																				moveItems();
																			}
																		});
								});	  
}

function retrieveTypes($droot, vtype, $clsdiv){
	var cid = '', divid = $droot.attr('id');
	if(vtype == 'Student'){
		//construct the classids' string	
		$clsdiv.find(':checkbox').each(function(){
									 cid += cid == '' ? $(this).val() : ',' + $(this).val();
								 });
	}												
	var	typeObj = {
		url: '../php/retrieveTypeItems.php',	
		type: 'POST',
		data: {classIds: cid, type: vtype},
		dataType: 'json',
		success: function(responseObj, status){
			$droot.empty();	
			//create the tiem div which including all type items
			if(responseObj['success']){
				var $ui = $('<ul></ul>').addClass('ui-items').appendTo($droot), $li, $chkbox, $span, _item;
				switch(vtype){
					case 'Class':
						var clsType = $droot.attr('id') == 'cls_content' ? 'Class' : 'StuClass';
						for(var i = 0; !!responseObj[i]; i++){
							_item = responseObj[i].split(',');	
							$li = $('<li></li>').appendTo($ui);
							$chkbox = $droot.attr('id') == 'cls_content' ? $('<input type="checkbox" id="' + divid + '_' + _item[0].toLowerCase() + '" value="' + _item[1] + '" name="Class[]" />').appendTo($li) : $('<input type="checkbox" id="' + divid + '_' + _item[0].toLowerCase() + '" value="' + _item[1] + '" />').appendTo($li);
							$span = $('<span><label for="' + divid + '_' + _item[0].toLowerCase() + '">&ensp;' + _item[1] + '</label></span>').appendTo($li);	
						}
						if($droot.attr('id') == 'cls_content'){
							attachEventAll($('#cls_all'));
						}else{
							retrieveTypes($('#src_content'), 'Student', $droot);
							attachEventFilter();
						}
						break;
					case 'Group':
						for(var i = 0; !!responseObj[i]; i++){
							_item = responseObj[i].split(',');	
							$li = $('<li></li>').appendTo($ui);
							$chkbox = $('<input type="checkbox" id="' + divid + '_' + _item[0].toLowerCase() + '" value="' + _item[0] + '" name="Group[]" />').appendTo($li);
							$span = $('<span><label for="' + divid + '_' + _item[0].toLowerCase() + '">&ensp;' + _item[1] + '</label></span>').appendTo($li);	
						}
						attachEventAll($('#grp_all'));
						break;
					case 'Student':
						for(var i = 0; !!responseObj[i]; i++){
							_item = responseObj[i].split(',');	
							$li = $('<li show="0"></li>').appendTo($ui);
							$chkbox = $('<input type="checkbox" id="' + divid + '_' + _item[0].toLowerCase() + '" value="' + _item[0] + '" belongs="' + _item[2] + '" />').appendTo($li);
							$span = $('<span><label for="' + divid + '_' + _item[0].toLowerCase() + '">&ensp;' + _item[1] + '&ensp;(' + _item[2] + ')</label></span>').appendTo($li);	
						}
						$('#src_content').find('ul li').css('display', 'none');
						$('#src_content').find(':checkbox').click(function(){
																moveItems();
 														   });	
						break;	
				}	
			}
		}
	};	
	$.ajax(typeObj);	
}