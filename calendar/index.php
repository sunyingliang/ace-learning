<?php	
	include ('../includes/session.php');
	include_once('../config/mysystemParameters.php');
	include_once('../utils/jumpbar.php');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1">
<title><?php echo $Page_Title_Prefix.$Page_Title_Calendar;?></title>
<!--
<meta http-equiv="X-UA-Compatible" content="IE=8" />
-->
<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
<link rel="SHORTCUT ICON" href="http://www.ace-learning.com/img/web/ace-ico.png">
<link href="../css/system.css" rel="stylesheet" type="text/css" />
<link href="../css/theme_1.css" rel="stylesheet" type="text/css" />
<link href="css/dailog.css" rel="stylesheet" type="text/css" />
<link href="css/calendar.css" rel="stylesheet" type="text/css" /> 
<link href="css/dp.css" rel="stylesheet" type="text/css" />   
<link href="css/alert.css" rel="stylesheet" type="text/css" /> 
<link href="css/main.css" rel="stylesheet" type="text/css" /> 
<script type="text/javascript" src="../jsexternal/jsexternal.js"></script>

<script src="src/jquery.js" type="text/javascript"></script>
<script src="src/Plugins/Common.js" type="text/javascript"></script>    
<script src="src/Plugins/datepicker_lang_US.js" type="text/javascript"></script>     
<script src="src/Plugins/jquery.datepicker.js" type="text/javascript"></script>
<script src="src/Plugins/jquery.alert.js" type="text/javascript"></script>    
<script src="src/Plugins/jquery.ifrmdailog.js" defer="defer" type="text/javascript"></script>
<script src="src/Plugins/wdCalendar_lang_US.js" type="text/javascript"></script>    
<script src="src/Plugins/jquery.calendar.js" type="text/javascript"></script>   
<script type="text/javascript" src="src/sample.js"></script>        
</head>
<body>
	<div style="height:30px;">
		<?php
            include('../utils/top_menu.php');
        ?>
    </div>
	<div id="div_jumpber" style="height:30px; overflow:hidden; background-color:#ffffff;">
    	<?php 
			new jumpbar($_SESSION['SessionUserID'], $_SESSION['SessionUserTypeID'], $_SESSION['SessionSchoolID'], true, $SystemFolder); 
		?>
    </div>
    <div>    
        <div id="calhead" style="padding-left:1px;padding-right:1px;"> 
            <div id="caltoolbar" class="ctoolbar">
			    <input type="hidden" id="userType" value="" />
			    <input type="hidden" id="sysFolder" value="<?php echo $SystemFolder;?>" />
                <div id="fsettingbtn" class="fbutton">
                    <div>
                        <span class="setcal">Settings</span>
                    </div>
                </div>
                <div class="btnseparator"></div>
                <div id="faddbtn" class="fbutton">
                    <div>
                        <span class="addcal">New Event</span>
                    </div>
                </div>
                <div class="btnseparator"></div>
                <div id="showtodaybtn" class="fbutton">
                    <div>
                        <span class="showtoday">Today</span>
                    </div>
                </div>
                <div class="btnseparator"></div>
                <div id="showdaybtn" class="fbutton">
                    <div>
                        <span class="showdayview">Day</span>
                    </div>
                </div>
                <div id="showweekbtn" class="fbutton fcurrent">
                    <div>
                        <span class="showweekview">Week</span>
                    </div>
                </div>
                <div id="showmonthbtn" class="fbutton">
                    <div>
                        <span class="showmonthview">Month</span>
                    </div>
                </div>
                <div class="btnseparator"></div>
                <div id="showyearbtn" class="fbutton">
                    <div>
                        <span class="showmonthview">Year</span>
                    </div>
                </div>
                <div class="btnseparator"></div>
                <div id="showreflashbtn" class="fbutton">
                    <div>
                        <span class="showdayflash">Refresh</span>
                    </div>
                </div>
                <div class="btnseparator"></div>
                <div id="sfprevbtn" title="Prev"  class="fbutton">
                    <span class="fprev"></span>
                </div>
                <div id="sfnextbtn" title="Next" class="fbutton">
                    <span class="fnext"></span>
                </div>
                <div class="fshowdatep fbutton">
                    <div>
                        <input type="hidden" name="txtshow" id="hdtxtshow" />
                        <span id="txtdatetimeshow">Loading</span>            
                    </div>
                </div>    
                <div class="clear"></div>
                <div id="loadingpannel" class="ptogtitle loadicon" style="display: none;">Loading data...</div>
                <div id="errorpannel" class="ptogtitle loaderror" style="display: none;">Unable to load data, please contact ACE-Learning Support.</div>
                <div id="colorlegend" class="fbutton">
                	<div>
                        <span class="colorcode">Color Code</span>
                    </div>
                </div>
                <div id="clcontent">
                	<div>
                        <table cellpadding="0" cellspacing="0">
                        	<tr>
                            	<td style="border-color:#fad65b; background-color:#fffae7;">
                                	Quiz
                                </td>
                            </tr>
                            <tr>
                            	<td style="border-color:#61e97b; background-color:#f7fce5;">
                                	Worksheet
                                </td>
                            </tr>
                            <tr>
                            	<td style="border-color:#f3767a; background-color:#fbdddd;">
                                	Journal
                                </td>
                            </tr>
                            <tr>
                            	<td style="border-color:#d2b593; background-color:#f8f5f0;">
                                	PMP
                                </td>
                            </tr>
                            <tr>
                            	<td style="border-color:#8dcceb; background-color:#eef7fc;">
                                	EA
                                </td>
                            </tr>
                            <tr>
                            	<td style="border-color:#ddbc53; background-color:#fbf5e5;">
                                	Annotation
                                </td>
                            </tr>
                            <tr>
                            	<td style="border-color:#d5de23; background-color:#fafade;">
                                	Survey
                                </td>
                            </tr>
                            <tr>
                            	<td style="border-color:#acc5c2; background-color:#f4f5f7;">
                                	Forum
                                </td>
                            </tr>
                            <tr>
                            	<td style="border-color:#20e0c9; background-color:#defdf8;">
                                	MPG
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
       		</div>
        </div>
        <div style="padding:1px;">
            <div class="t1 chromeColor">&nbsp;</div>
            <div class="t2 chromeColor">&nbsp;</div>
            <div id="dvCalMain" class="calmain printborder">
                <div id="gridcontainer" style="overflow-y: visible;"></div>
            </div>
            <div class="t2 chromeColor">&nbsp;</div>
            <div class="t1 chromeColor">&nbsp;</div>   
        </div>
    </div>    
</body>
</html>