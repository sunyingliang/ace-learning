<?php

     // Declare minimum UserTypeID
     $MinUserTypeID = 3;

     // Include SESSION.PHP
     include ('../includes/session.php');

     // Configuration file for error management and such
     require_once ('../includes/config.inc');

     // Calling MySQL connection
     require ('../includes/mysql_connect.php');
     
     // Set the page title
     $PageTitle = 'ACE-Learning :: Lesson Scheduler';

     // Include START.PHP
     include ('../includes/start.php');

     // Include HEADER.PHP
     include ('../includes/header.php');
     
      // Include SUBMENU_MAILBOX.PHP
      include ('../includes/submenu_calendar.php');

      // Include FOLDERTREE.JS
      echo "<link href=\"/~ace/stylesheets/stylesheet2.css\" rel=\"stylesheet\" type=\"text/css\" />\n\n";
      echo "<link href=\"/~ace/stylesheets/print.css\" rel=\"stylesheet\" type=\"text/css\" media=\"print\" />\n\n";
      echo "<script language=\"JavaScript\" src=\"/~ace/jsexternal/jsexternal.js\"></script>\n\n";
	
      echo "<script language=\"JavaScript\" src=\"/~ace/jsexternal/validation_TeachersResources.js\"></script>\n\n";
      echo "<script language=\"JavaScript\" src=\"/~ace/jsexternal/CalendarPopup.js\"></script>\n\n";
      echo "<script language=\"JavaScript\" src=\"CalendarPopup.js\"></script>\n\n";
      echo "<SCRIPT LANGUAGE=\"JavaScript\">\n\n";
      echo "var cal = new CalendarPopup()\n\n";
      echo "cal.setDisabledWeekDays(0,2,3,4,5,6)\n\n";
      echo "</SCRIPT>\n\n";
      
?>

<?php
if (isset($_POST['Submit'])){
	
   if (isset($_POST['DeleteID'])){
	  
	  $ID = $_POST['ID'];
	  $DateID = $_POST['DateID'];
	  $AssignID = $_POST['AssignID'];
	  
	  $Str = "";
          $Str .= " <p class=\"Location\"><a href=\"index.php?DateID=$DateID\">Weekly Scheduler </a> >> Delete Announcement</p>\n";
          $Str .= "        <fieldset>\n";
          $Str .= "          <legend>Delete Announcement</legend>\n";
          $Str .= "          <table border=\"0\" width=\"100%\" cellspacing=\"0\" cellpadding=\"0\">\n";
          $Str .= "            <tr>\n";
          $Str .= "              <td width=\"100%\" valign=\"top\">\n";
          $Str .= "                <p>Please see below for report.&nbsp; <img src=\"../images/misc/yes.gif\" width=\"12\" height=\"12\" /> indicates successful while <img src=\"../images/misc/no.gif\" width=\"12\" height=\"12\" /> indicates unsuccessful.&nbsp; If a process is unsuccessful, please contact your system administrator.</p>\n";
          $Str .= "              </td>\n";
          $Str .= "            </tr>\n";
          $Str .= "          </table>\n";
          $Str .= "        </fieldset>\n";
          $Str .= "        <br>\n";
          $Str .= "        <fieldset>\n";
          $Str .= "          <table border=\"0\" width=\"100%\" cellspacing=\"0\" cellpadding=\"0\" class=\"TableBorder\">\n";
          $Str .= "            <tr class=\"TableHeader\">\n";
          $Str .= "              <td width=\"70%\">Event</td>\n";
          $Str .= "              <td width=\"30%\">Status</td>\n";
          $Str .= "            </tr>\n";
          /*
          $Str .= "            <tr class=\"TableBody0\" onMouseOver=\"mOvrTable0(this)\" onMouseOut=\"mOutTable0(this)\">\n";
          $Str .= "              <td width=\"70%\">Delete Link</td>\n";
          $Str .= "              <td width=\"30%\" align=\"center\">";
          
          $Query = "DELETE FROM QRY_".$_SESSION['SessionSchoolID']."_Announcement WHERE ID=$ID AND UserID=".$_SESSION['SessionUserID']."";
          $Result = mysql_query($Query);
          
          if (mysql_affected_rows() > 0) {
               $Str .= "<img src=\"../images/misc/yes.gif\" width=\"12\" height=\"12\" /></td>\n                </tr>\n";
          } else {
               $Str .= "<img src=\"../images/misc/no.gif\" width=\"12\" height=\"12\" /></td>\n                </tr>\n";
          }
          */
          
          $Str .= "            <tr class=\"TableBody0\" onMouseOver=\"mOvrTable0(this)\" onMouseOut=\"mOutTable0(this)\">\n";
          $Str .= "              <td width=\"70%\">Delete Assigned Class and Date</td>\n";
          $Str .= "              <td width=\"30%\" align=\"center\">";
          
          $Query = "DELETE FROM QRY_".$_SESSION['SessionSchoolID']."_AnnouncementAssignment WHERE AssignID=$AssignID";
          $Result = mysql_query($Query);
          
          if (mysql_affected_rows() > 0) {
               $Str .= "<img src=\"../images/misc/yes.gif\" width=\"12\" height=\"12\" /></td>\n                </tr>\n";
          } else {
               $Str .= "<img src=\"../images/misc/no.gif\" width=\"12\" height=\"12\" /></td>\n                </tr>\n";
          }

          $Str .= "          </table>\n";
          $Str .= "        </fieldset>\n";
          echo $Str;
   }else{
	
	$ClassID = $_POST['ClassID'];
	$SyllabusID = $_POST['SyllabusID'];
	$DateID = $_POST['DateID'];
	$DateIDNormal = $_POST['DateID'];
	$DID = explode("/", $DateID);
        $DateID = "$DID[2]-$DID[1]-$DID[0] 00:00:00 AM";
	
	$ID = $_POST['ID'];
        $Title = trim(strip_tags($_POST['Title']));
        $URL = str_replace("http://", "", trim(strip_tags($_POST['URL'])));
        $Message = trim(strip_tags($_POST['Message']));
        if (isset($_POST['ShareID'])) { $ShareID = 1; } else { $ShareID = 0; }
        $AssignID = $_POST['AssignID'];
        
        $SectionID = 0;
        $TopicID = 0;
        $SubTopicID = 0;
        
	
	$Str = "";
	$Str .= " <p class=\"Location\"><a href=\"index.php?DateID=$DateIDNormal\">Weekly Scheduler </a> >> <a href=\"../calendar/EditAnnouncement.php?ClassID=$ClassID&SyllabusID=$SyllabusID&DateID=$DateIDNormal&Title=$Title&URL=$URL&Message=$Message&ID=$ID\"> Edit Announcement </a> >> Result</p>\n";
	$Str .= " <fieldset>\n";
        $Str .= "   <legend>Edit Announcement</legend>\n";
        $Str .= "     <table border=\"0\" width=\"100%\" cellspacing=\"0\" cellpadding=\"0\">\n";
        $Str .= "       <tr>\n";
        $Str .= "         <td width=\"100%\">\n";
        $Str .= "                <p>Please see below for report.&nbsp; <img src=\"../images/misc/yes.gif\" width=\"12\" height=\"12\" /> indicates successful while <img src=\"../images/misc/no.gif\" width=\"12\" height=\"12\" /> indicates unsuccessful.&nbsp; If a process is unsuccessful, please contact your system administrator.</p>\n";
        $Str .= "         </td>\n";
        $Str .= "       </tr>\n";
        $Str .= "     </table>\n";
        $Str .= "     <br>\n";
        
        $Str .= "     <table border=\"0\" width=\"100%\" cellspacing=\"0\" cellpadding=\"0\">\n";
        $Str .= "       <tr>\n";               
        $Str .= "         <td width=\"20%\" valign=\"top\"><p class=\"FieldLabel\"><font color=\"000000\"><b>Class</b></font></p></td>\n";
        $Str .= "         <td width=\"80%\" align=\"left\"><p class=\"FieldLabel\">$ClassID</p></td>\n";
        $Str .= "       </tr>\n";
        $Str .= "       <tr>\n";               
        $Str .= "         <td width=\"20%\" valign=\"top\"><p class=\"FieldLabel\"><font color=\"000000\"><b>Syllabus</b></font></p></td>\n";
        $Str .= "         <td width=\"80%\" align=\"left\"><p class=\"FieldLabel\">$SyllabusID</p></td>\n";
        $Str .= "       </tr>\n";
        $Str .= "       <tr>\n";               
        $Str .= "         <td width=\"20%\" valign=\"top\"><p class=\"FieldLabel\"><font color=\"000000\"><b>Date</b></font></p></td>\n";
        $Str .= "         <td width=\"80%\" align=\"left\"><p class=\"FieldLabel\">$DateIDNormal</p></td>\n";
        $Str .= "       </tr>\n";
        $Str .= "       <tr>\n";               
     	$Str .= "         <td width=\"20%\" valign=\"top\"><p class=\"FieldLabel\"><font color=\"000000\"><b>Title</b></font></p></td>\n";
     	$Str .= "         <td width=\"80%\" valign=\"top\"><p class=\"FieldLabel\">$Title</p></td>\n";
     	$Str .= "       </tr>\n";
     	$Str .= "       <tr>\n";
        $Str .= "         <td width=\"20%\" valign=\"top\"><p class=\"FieldLabel\"><font color=\"000000\"><b>Announcement</b></font></p></td>\n";
        $Str .= "         <td width=\"80%\" valign=\"top\"><p class=\"FieldLabel\">".nl2br($Message)."</p></td>\n";
        $Str .= "       </tr>\n";
        $Str .= "     </table>\n";
        $Str .= " </fieldset>\n";
        
        $Str .= " <br>\n";
        
        $Str .= " <fieldset>\n";
        $Str .= " <table border=\"0\" width=\"100%\" cellspacing=\"0\" cellpadding=\"0\" class=\"TableBorder\">\n";
        $Str .= "   <tr class=\"TableHeader\">\n";
        $Str .= "     <td width=\"70%\">Event</td>\n";
        $Str .= "     <td width=\"30%\">Status</td>\n";
        $Str .= "   </tr>\n";
        $Str .= "   <tr class=\"TableBody0\" onMouseOver=\"mOvrTable0(this)\" onMouseOut=\"mOutTable0(this)\">\n";
        $Str .= "     <td width=\"70%\">Edit Title and Announcement</td>\n";
        $Str .= "     <td width=\"30%\" align=\"center\">";
        
        $Query = "UPDATE QRY_".$_SESSION['SessionSchoolID']."_Announcement SET Title='$Title', Message='$Message', ShareID='$ShareID' WHERE ID=$ID";
        $Result = mysql_query($Query);
        
        if ($Result) {
               $Str .= "<img src=\"../images/misc/yes.gif\" width=\"12\" height=\"12\" /></td>\n                </tr>\n";
        } else {
               $Str .= "<img src=\"../images/misc/no.gif\" width=\"12\" height=\"12\" /></td>\n                </tr>\n";
        }
        
        $Str .= "          </table>\n";
        $Str .= "        </fieldset>\n";
        
	echo $Str;	
   }
   mysql_close();
   exit();
	
}
?>


<?php
if (isset($_GET['ClassID'])){
	
	$ClassID = $_GET['ClassID'];
	$SyllabusID = $_GET['SyllabusID'];
	$DateID = $_GET['DateID'];
	
	$ID = $_GET['ID'];
	$Title = $_GET['Title'];
	$URL = $_GET['URL'];
	$Message = $_GET['Message'];
	$AssignID = $_GET['AssignID'];
	
	$Str = "";
	$Str .= " <p class=\"Location\"><a href=\"index.php?DateID=$DateID\">Weekly Scheduler </a> >> Edit Announcement</p>\n";
	$Str .= " <form enctype=\"multipart/form-data\" action=".$_SERVER['PHP_SELF']." method=\"post\" name=\"form\" onSubmit=\"return validLinkInput(this)\">\n";
        $Str .= " <fieldset>\n";
        $Str .= "   <legend>Edit Announcement</legend>\n";
        $Str .= "     <table border=\"0\" width=\"100%\" cellspacing=\"0\" cellpadding=\"0\">\n";
        $Str .= "       <tr>\n";               
        $Str .= "         <td width=\"20%\" valign=\"top\"><p class=\"FieldLabel\"><font color=\"000000\"><b>Class</b></font></p></td>\n";
        $Str .= "         <td width=\"80%\" align=\"left\"><p class=\"FieldLabel\">$ClassID</p></td>\n";
        $Str .= "       </tr>\n";
        $Str .= "       <tr>\n";               
        $Str .= "         <td width=\"20%\" valign=\"top\"><p class=\"FieldLabel\"><font color=\"000000\"><b>Syllabus</b></font></p></td>\n";
        $Str .= "         <td width=\"80%\" align=\"left\"><p class=\"FieldLabel\">$SyllabusID</p></td>\n";
        $Str .= "       </tr>\n";
        $Str .= "       <tr>\n";               
        $Str .= "         <td width=\"20%\" valign=\"top\"><p class=\"FieldLabel\"><font color=\"000000\"><b>Date</b></font></p></td>\n";
        $Str .= "         <td width=\"80%\" align=\"left\"><p class=\"FieldLabel\">$DateID</p></td>\n";
        $Str .= "       </tr>\n";
        $Str .= "       <tr>\n";               
     	$Str .= "         <td width=\"20%\" valign=\"top\"><p class=\"FieldLabel\"><font color=\"000000\"><b>Title</b></font><font color=\"#FF0000\">*</font></p></td>\n";
     	$Str .= "         <td width=\"80%\" valign=\"top\"><input type=\"text\" name=\"Title\" value= \"$Title\"class=\"FieldInput\" maxlength=\"100\" style=\"width:385px\" /></td>\n";
     	$Str .= "       </tr>\n";
     	$Query = "SELECT Message FROM QRY_".$_SESSION['SessionSchoolID']."_Announcement WHERE ID=$ID";
     	$Result = mysql_query($Query);
     	if (mysql_affected_rows() > 0) {
     	
     		$Row = mysql_fetch_array($Result, MYSQL_NUM);
     		$Str .= "       <tr>\n";
	        $Str .= "         <td width=\"20%\" valign=\"top\"><p class=\"FieldLabel\"><font color=\"000000\"><b>Announcement</b></font></p></td>\n";
	        $Str .= "         <td width=\"80%\" valign=\"top\"><textarea name=\"Message\" style=\"width:385px\" rows=\"6\" class=\"FieldInput\" >".nl2br($Row[0])."</textarea></td>\n";			
	        $Str .= "       </tr>\n";
     		
     	}
        $Str .= "       <tr>\n";
	$Str .= "         <td width=\"20%\" align=\"absmiddle\"><p class=\"FieldLabel\"><font color=\"000000\"><b>Share Announcement</b></font></p></td>\n";
	$Str .= "         <td width=\"80%\" align=\"absmiddle\"><p class=\"FieldOutput\">";
        $Query2 = "SELECT ShareID FROM QRY_".$_SESSION['SessionSchoolID']."_Announcement WHERE ID=$ID";
        $Result2 = mysql_query($Query2);

        if (mysql_affected_rows() > 0) {
		 $Row2 = mysql_fetch_array($Result2, MYSQL_NUM);
		 if ($Row2[0]==1){
		 	$Str .= "<input type=\"checkbox\" name=\"ShareID\" value=\"1\" class=\"FieldInput\" checked/>";	
		 }else{
		 	$Str .= "<input type=\"checkbox\" name=\"ShareID\" value=\"1\" class=\"FieldInput\" />";
		 }
                         
        } 
        $Str .= "</td>\n";
	
	$Str .= "       </tr>\n";
        $Str .= "       <tr>\n";
        $Str .= "         <td width=\"20%\" align=\"absmiddle\"><p class=\"FieldLabel\"><font color=\"000000\"><b>Delete Announcement</b></font></p></td>\n";
        $Str .= "         <td width=\"80%\" align=\"absmiddle\"><p class=\"FieldOutput\">\n";
        $Str .= "         <input type=\"checkbox\" name=\"DeleteID\" value=\"1\" class=\"FieldInput\" /></td>\n";
        $Str .= "       </tr>\n";
        $Str .= "       <tr>\n";
        $Str .= "         <td width=\"20%\">\n";
        $Str .= "             <p><font color=\"#FF0000\">* Compulsory</font></p>\n";
        $Str .= "         </td>\n";
        $Str .= "       </tr>\n";
        
        $Str .= "     </table>\n";
        $Str .= " </fieldset>\n";
        
        $Str .= "</br>\n";
        
        $Str .= " <fieldset>\n";
        $Str .= "   <table border=\"0\" width=\"100%\" cellspacing=\"0\" cellpadding=\"0\">\n";
        $Str .= "     <tr>\n";
        $Str .= "       <td width=\"100%\" align=\"center\">\n";
        $Str .= "         <input type=\"submit\" name=\"Submit\" value=\"Submit\" class=\"FieldButton\" style=\"width:120px\" />\n";
        $Str .= "         <input type=\"reset\" name=\"Reset\" value=\"Reset\" class=\"FieldButton\" style=\"width:120px\" />\n";
        $Str .= "         <input type=\"hidden\" name=\"ClassID\" value=\"$ClassID\" />\n";
        $Str .= "         <input type=\"hidden\" name=\"SyllabusID\" value=\"$SyllabusID\" />\n";
        $Str .= "         <input type=\"hidden\" name=\"DateID\" value=\"$DateID\" />\n";
        $Str .= "         <input type=\"hidden\" name=\"ID\" value=\"$ID\" />\n";
        $Str .= "         <input type=\"hidden\" name=\"AssignID\" value=\"$AssignID\" />\n";
        $Str .= "       </td>\n";
        $Str .= "     </tr>\n";
        $Str .= "   </table>\n";
        $Str .= " </fieldset>\n";
        $Str .= " </form>\n";
	
	echo $Str;
}
?>



<?php
	mysql_close();
?>