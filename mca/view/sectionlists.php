<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" href="../css/jqueryui/jqueryui.css" type="text/css" />
<link href="../css/system.css" rel="stylesheet" type="text/css" />
<link href="../css/theme_1.css" rel="stylesheet" type="text/css" />
<link type="text/css" rel="stylesheet" href="css/section.css" />
<script type="text/javascript" src="../js/jquery-1.9.1.min.js"></script>
<script type="text/javascript" src="../js/jqueryui/jquery-ui-1.10.3.js"></script>
<script type="text/javascript" src="js/form.plugin.js"></script>
<script type="text/javascript" src="js/section.js"></script>
<title>Assessment >> Section</title>
</head>

<body>
<div id="wrapper">
	<?php
		$useragent = strtolower($_SERVER['HTTP_USER_AGENT']);
		$userbrowser = '';
		if(strpos($useragent, 'android') !== false){
			$userbrowser .= 'android';	
			if(strpos($useragent, 'safari') !== false){
				$userbrowser .= '-safari';		
			}
		}
    	new jumpbar($_SESSION['SessionUserID'], $_SESSION['SessionUserTypeID'], $_SESSION['SessionSchoolID'], true, $this->getSysFolder());
	?>
	<div id="container" class="contentMargin">
   	  <div id="header">
        	<?php print($this->getTitle()); ?>
            <input type="hidden" id="asmtid" value="<?php print($this->getAsmtID());?>" />
            <input type="hidden" id="cwd" value="freshCritic|freshCritic" />
            <input type="hidden" id="useragent" value="<?php print($userbrowser);?>" />
        </div>
    	<div id="main">
        	<div id="tabs" current="Assessment" style="border:0px;">
            	<ul>
                	<li><a href="javascript:void(0);return false;">Assessment</a></li>
                	<li><a href="javascript:void(0);return false;">Reports</a></li>
                </ul>
            </div>
            <div id="content">
            	<div id="nav">
                	<ul class="navgtr">
                    	<li id="nav_section">
                        	<div class="sec-label">
                            	Section
                            </div>
                            <div class="sec-selector">
                            	<select id="sec_selector">
                                	<option value="0">All</option>
									<?php
                                        foreach($this->getSections() as $section){
									?>
                                    <option value="<?php print($section['SectionID']); ?>"><?php print($section['SectionTitle']); ?></option>
                                    <?php                                                
                                        }    
                                    ?>
                                </select>
                            </div>
                        </li>
                        <li id="nav_critic">
                        	<div class="sec-label">
                            	Critic
                            </div>
                            <div class="sec-selector">
                            	<select id="critic_selector">
									<?php
                                        foreach($this->getCritics() as $critic){
									?>
                                    <option value="<?php print($critic['CriticRoleID']); ?>"><?php print($critic['CriticDetails']); ?></option>
                                    <?php                                                
                                        }    
                                    ?>
                                </select>
                            </div>
                        </li>
                    	<li id="nav_view">
                        	<div class="sec-label">
                            	View Students
                            </div>
                            <div class="sec-selector">
                            	<select id="view_selector">
                                	<option value="0">All in one page</option>
                                	<option value="1">One-by-one</option>
                                </select>
                            </div>
                        </li>
                    	<li id="nav_class" class="hidden">
                        	<div class="sec-label">
                            	Class
                            </div>
                            <div class="sec-selector">
                            	<select id="cls_selector">
                                	<option value="0">All Classes</option>
                                    <?php
                                    	foreach($this->getClasses() as $class){
									?>
                                    <option value="<?php print($class); ?>"><?php print($class); ?></option>		
                                    <?php			
										}
									?>
                                </select>
                            </div>
                        </li>
                    	<li id="nav_student" class="hidden">
                        	<div class="sec-label">
                            	Student
                            </div>
                            <div class="sec-selector">
                            	<select id="stu_selector">
                                	<option value="0">All Students</option>
                                </select>
                            </div>
                        </li>
                    </ul>
                </div>
                <div id="comment">
                	<div id="rating" class="panel" style="width:332px;">
                    	<span class="rubric">Rating:</span>
                        <ul>
                        	<?php
                            	print($this->_ratingli);
							?>
                        </ul>
                    </div>
                	<div id="criteria" class="panel" style="width:630px; margin-left:26px;">
                    	<span class="rubric">Criteron:</span>
                        <ul>
                        	<?php
                            	print($this->_criteriali);
							?>
                        </ul>
                    </div>
                </div>
                <div id="show">
                	<?php
						print($this->_outputli);
					?>
                </div>
                <div id="commit">
                	<div id="btn_draft" class="btn-commit">
                    	<input type="button" id="draft" value="Save As Draft" />
                    </div>
                	<div id="btn_submit" class="btn-commit">
                    	<input type="button" id="submit" value="Submit" />
                    </div>
                	<div id="btn_reset" class="btn-commit">
                    	<input type="button" id="reset" value="Reset" />
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>    
</body>
</html>