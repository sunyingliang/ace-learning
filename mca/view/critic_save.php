<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link type="text/css" rel="stylesheet" href="./css/critic.css" />
<title>Critic Save</title>
</head>

<body>
	<div id="container">
	    <div id="header">
        	<?php print($this->_title); ?>
        </div>
        <div id="main">
	    	<?php print($this->_output); ?>
        </div>
    </div>
</body>
</html>