<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="../css/system.css" rel="stylesheet" type="text/css" />
<link href="../css/theme_1.css" rel="stylesheet" type="text/css" />
<link type="text/css" rel="stylesheet" href="./css/section.css" />
<script type="text/javascript" src="../js/jquery-1.9.1.min.js"></script>
<script type="text/javascript" src="./js/form.plugin.js"></script>
<script type="text/javascript" src="./js/section-student.js"></script>
<title>Assessment >> Section</title>
</head>

<body>
	<?php
    	new jumpbar($_SESSION['SessionUserID'], $_SESSION['SessionUserTypeID'], $_SESSION['SessionSchoolID'], true, $this->getSysFolder());
	?>
	<div id="container" class="contentMargin">
    	<div id="header">
        	<?php print($this->getTitle()); ?>
            <input type="hidden" id="asmtid" value="<?php print($this->getAsmtID());?>" />
            <input type="hidden" id="cwd" value="freshStudents|freshStudents" />
        </div>
    	<div id="main">
            <div id="content">
                <div id="nav" class="hidden" style="margin-top:0px;">
                    <ul class="navgtr">
                        <li>
                            <div class="sec-label">
                                Student
                            </div>
                            <div class="sec-selector">
                                <select id="stu_selector">
                                <?php
                                    print($outputarr[0]);
                                ?>
                                </select>
                            </div>
                        </li>
                    </ul>
                </div>
                <div id="show">
                    <?php
                        print($outputarr[1]);
                    ?>
                </div>
                <div id="comment">
                    <div id="rating" class="panel" style="width:332px;">
                    	<span class="rubric">Legend:</span>
                        <ul>
                        	<?php
                            	print($outputarr[2]);
							?>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>
</html>