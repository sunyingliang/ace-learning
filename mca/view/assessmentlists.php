<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link type="text/css" rel="stylesheet" href="./css/assessment.css" />
<script type="text/javascript" src="../js/jquery-1.9.1.min.js"></script>
<script type="text/javascript" src="./js/form.plugin.js"></script>
<script type="text/javascript" src="./js/assessment.js"></script>
<title><?php print($this->_usertype); ?></title>
</head>

<body style="display:none;">
	<div id="container">
    	<div id="welcome">
        	<span>Welcome <?php print($_SESSION['SessionUsername']); ?>, Please select an assessment from belowing to critic or view.</span>
            <input type="hidden" id="sysfolder" value="<?php print($this->_sysfolder); ?>" />
        </div>
        <div id="assessments">
        	<ul>
            	<li style="background-color:#adeab2; font-weight:bold;">
                	<span class="asmt-title">Assessment Title</span>
                    <span class="asmt-dt">StartDate</span>
                    <span class="asmt-dt">EndDate</span>
                </li>
			<?php
                print($this->_outputli);
            ?>
            </ul>
        </div>
    </div>
</body>
</html>