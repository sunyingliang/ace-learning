<?php
	include_once('includes/autoload.php');
	include_once('../includes/session.php');
	include_once('../includes/mysystemParameters.php');
	include_once('../includes/jumpbar.php');
	
	$cType = $_POST['c'];
	$aType = $_POST['a'];
	$params = $_POST['p'];
	$controller = null;
	
	if(!empty($cType)){
		$controllerType = $cType.'Controller';
		$controller = new $controllerType($params);
		$controller->$aType();	
	}else{
		$controller = new AssessmentController();
		$controller->lists($SystemFolder);
	}
?>