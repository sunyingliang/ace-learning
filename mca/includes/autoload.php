<?php
	function __autoload($class_name){
		$class_name = strtolower($class_name);
		if(file_exists('./controller/'.$class_name.'.php')){
			require_once('./controller/'.$class_name.'.php');
		}elseif(file_exists('./model/'.$class_name.'.php')){
			require_once('./model/'.$class_name.'.php');
		}else{
			echo "there are not: ".'./controller/'.$class_name.'.php';	
		}	
	}
?>