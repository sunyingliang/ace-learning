<?php
	class CriticResultModel{
		private	$_resultid;
		private $_criteriaid;
		private $_score;
		private $_criticid;
		private $_draft;
		
		public function __construct(){
			$argsnum = func_num_args();
			if($argsnum == 4){
				$this->setCriteriaID(func_get_arg(0));
				$this->setScore(func_get_arg(1));
				$this->setCriticID(func_get_arg(2));
				$this->setDraft(func_get_arg(3));	
			}elseif($argsnum == 5){
				$this->setResultID(func_get_arg(0));
				$this->setCriteriaID(func_get_arg(1));
				$this->setScore(func_get_arg(2));
				$this->setCriticID(func_get_arg(3));
				$this->setDraft(func_get_arg(4));	
			}
		}
		public function setResultID($resultid){
			$this->_resultid = $resultid;	
		}
		public function setCriteriaID($criteriaid){
			$this->_criteriaid = $criteriaid;	
		}
		public function setScore($score){
			$this->_score = $score;	
		}
		public function setCriticID($criticid){
			$this->_criticid = $criticid;	
		}
		public function setDraft($draft){
			$this->_draft = $draft;	
		}
		public function getResultID(){
			return $this->_resultid;	
		}
		public function getCriteriaID(){
			return $this->_criteriaid;	
		}
		public function getScore(){
			return $this->_score;	
		}
		public function getCriticID(){
			return $this->_criticid;	
		}
		public function getDraft(){
			return $this->_draft;	
		}
	}
?>