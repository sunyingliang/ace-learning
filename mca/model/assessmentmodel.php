<?php
	class AssessmentModel{
		private $_id;
		private $_schoolid;
		private $_title;
		private $_startdate;
		private $_enddate;
		
		public function getId(){
			return $this->_id;	
		}
		public function getSchoolId(){
			return $this->_schoolid;	
		}
		public function setSchoolId($schoolid){
			$this->_schoolid = $schoolid;	
		}
		public function getTitle(){
			return $this->_title;	
		}
		public function setTitle($title){
			$this->_title = $title;	
		}
		public function getStartDate(){
			return $this->_startdate;	
		}
		public function setStartDate($stdate){
			$this->_startdate = $stdate;	
		}
		public function getEndDate(){
			return $this->_enddate;	
		}
		public function setEndDate($etdate){
			$this->_enddate = $etdate;	
		}
	}
?>