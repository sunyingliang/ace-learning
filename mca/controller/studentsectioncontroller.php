<?php
	require_once('./controller/controller.php');
	
	class StudentSectionController extends Controller{
		private $_asmtid;
		private $_sysfolder = '';
		private $_asmttitle = '';
		private $_rolearr = array();
		private $_sectionarr = array();
		private $_studentarr = array();
		private $_classarr = array();
		private $_criticid = 0;
		private $_userid = 0;
		private $_criteriaarr = array();
		private $_ratingarr = array();
		private $_ratingli = '';
		private $_output = '';
		
		public function __construct(){
			$this->_dao = new DataAccess();
			if(func_num_args() == 1){
				$paramsarr = $this->getParamArray(func_get_arg(0));	
				if(isset($paramsarr['assessmentid'])){
					$this->_asmtid = $paramsarr['assessmentid'];					
				}
				if(isset($paramsarr['sysfolder'])){
					$this->_sysfolder = $paramsarr['sysfolder'];					
				}
				if(isset($paramsarr['roleid'])){
					$this->_rolearr[] = $paramsarr['roleid'];					
				}
				if(isset($paramsarr['sectionid'])){
					$this->_sectionarr[] = $paramsarr['sectionid'];					
				}
				if(isset($paramsarr['criticid'])){
					$this->_criticid = $paramsarr['criticid'];					
				}
				if(isset($paramsarr['userid'])){
					$this->_userid = $paramsarr['userid'];					
				}
			}
		}
		public function getAsmtID(){
			return $this->_asmtid;	
		}
		public function getSysFolder(){
			return $this->_sysfolder;
		}
		public function getTitle(){
			return $this->_asmttitle;	
		}
		
		private function _sortby(&$arr, $field){
			if(is_array($arr)){
				for($i = 0; $i < count($arr) - 1; $i++){
					for($j = $i + 1; $j < count($arr); $j++){
						if($arr[$i][$field] > $arr[$j][$field]){
							$temp = $arr[$i];	
							$arr[$i] = $arr[$j];
							$arr[$j] = $temp;
						}	
					}	
				}
			}
		}
		
		private function _initTitle(){
			$sql = 'select Title, StartDate, EndDate from TBL_MultiCriteriaAssessment where AssessmentID = '.$this->_asmtid;	
			$this->_dao->execute($sql);
			if($this->_dao->getQuery()){
				if($row = $this->_dao->getRow()){
					$this->_asmttitle .= '<span>'.$row['Title'].'</span>';
				}	
			}
		}
		
		private function _initRoles(){
			$this->_rolearr = array();
			$sql = 'select distinct R.CriticRoleID, R.OrderID, R.CriticDetails from TBL_MultiCriteriaAssessment_CriticRole R, TBL_MultiCriteriaAssessment_Critic C where R.CriticRoleID = C.CriticRoleID and C.Critic = '.$_SESSION['SessionUserID'];	
			$this->_dao->execute($sql);
			if($this->_dao->getQuery()){
				while($row = $this->_dao->getRow()){
					$this->_rolearr[] = $row;	
				}	
				$this->_sortby($this->_rolearr, 'OrderID');	
			}
		}
		
		private function _initSections($roleid){
			$this->_sectionarr = array();
			$sql = 'select S.SectionID, S.OrderID, S.SectionTitle from TBL_MultiCriteriaAssessment_Section S, TBL_MultiCriteriaAssessment_CriticRole C where C.SectionID = S.SectionID and C.CriticRoleID = '.$roleid;	
			$this->_dao->execute($sql);
			if($this->_dao->getQuery()){
				while($row = $this->_dao->getRow()){
					$this->_sectionarr[] = $row;	
				}	
			}
			$this->_sortby($this->_sectionarr, 'OrderID');	
		}
		
		private function _initCriteria($sectionid){
			$this->_criteriaarr = array();
			$sql = 'SELECT CriteriaID, OrderID, Weighting, Criteria, Details FROM TBL_MultiCriteriaAssessment_Criteria where SectionID = '.$sectionid;	
			$this->_dao->execute($sql);
			if($this->_dao->getQuery()){
				while($row = $this->_dao->getRow()){
					$this->_criteriaarr[] = $row;	
				}	
				$this->_sortby($this->_criteriaarr, 'OrderID');
			}
		}
		
		private function _initRating($criteriaid, $roleid){
			$this->_ratingarr = array();
			$sql = 'select Rate, Details from TBL_MultiCriteriaAssessment_RatingItems where RatingID = (select RatingID from TBL_MultiCriteriaAssessment_Criteria_Role_Rating where CriteriaID = '.$criteriaid.' and CriticRoleID = '.$roleid.') order by OrderID';	
			$this->_dao->execute($sql);
			if($this->_dao->getQuery()){
				while($row = $this->_dao->getRow()){
					$this->_ratingarr[] = $row;	
				}
			}
			$this->_ratingli = '';
			foreach($this->_ratingarr as $item){   
				$this->_ratingli .= '<li>';
				$this->_ratingli .= '<div style="width:20px; vertical-align:top;"><span>'.$item['Rate'].': </span></div>';
				$this->_ratingli .= '<div style="width:310px;"><span>'.$item['Details'].'</span></div>';
				$this->_ratingli .= '</li>';					
			}
		}
		
		private function _initStudents(&$role){
			$sectionarr = array();
			$sql = 'select SectionID from TBL_MultiCriteriaAssessment_Section where AssessmentID = '.$this->_asmtid;
			$this->_dao->executei($sql);
			if($this->_dao->getQuery()){
				while($row = $this->_dao->getRow()){
					$sectionarr[$row['SectionID']] = $row['SectionID'];
				}	
			}

			$sql = 'select SP.ClassID, SP.IndexID, U.Name, U.UserID, CriticID from (select TU.UserID, CONCAT(FirstName, LastName) AS Name, C.CriticID from TBL_Users TU, (select UserID, CriticID from TBL_MultiCriteriaAssessment_Critic where Critic = '.$_SESSION['SessionUserID'].' and CriticRoleID = '.$role['CriticRoleID'].' and SectionID in ('.implode(',', $sectionarr).')) C where TU.UserID = C.UserID) as U, TBL_StudentsProfile as SP where U.UserID = SP.UserID order by SP.ClassID, SP.IndexID';	
			$this->_dao->execute($sql);
			if($this->_dao->getQuery()){
				while($rowS = $this->_dao->getRow()){
					$flag = 0x2;
					foreach($sectionarr as $section){
						$sqlr = 'select CriticRoleID from TBL_MultiCriteriaAssessment_CriticRole where SectionID = '.$section;
						$recordset = mysql_query($sqlr);
						if($recordset){
							while($rowR = mysql_fetch_array($recordset)){
								$sqld = 'select count(*) as Submit from TBL_MultiCriteriaAssessment_Results where SaveAsDraft = 0 and CriticID in (select CriticID from TBL_MultiCriteriaAssessment_Critic '.
								'where SectionID = '.$section.' and CriticRoleID = '.$rowR['CriticRoleID'].' and UserID = '.$rowS['UserID'].')';
								$resultset = mysql_query($sqld);
								if($resultset){
									if($rowd = mysql_fetch_array($resultset)){
										if($rowd['Submit'] == 0){
											$flag &= 0x1;
										}else{
											$flag |= 0x1;
										}	
									}	
								}
							}
						}
					}
					$rowS['Flag'] = $flag;
					$rowS['Name'] = $role['CriticDetails'] == 'Self' ? 'Self' : $rowS['Name'];
					$this->_studentarr[] = $rowS;
				}	
			}
		}
		
		private function _initSectionli(&$section){
			$criteriaTitle = '';
			$sql = 'select CriteriaTitle from TBL_MultiCriteriaAssessment_Section where SectionID = '.$section['SectionID'];
			$this->_dao->execute($sql);
			if($this->_dao->getQuery()){
				if($row = $this->_dao->getRow()){
					$criteriaTitle = $row['CriteriaTitle'];	
				}	
			}
			$roleName = '';
			$sql = 'select CriticDetails from TBL_MultiCriteriaAssessment_CriticRole where CriticRoleID = '.$section['CriticRoleID'];
			$this->_dao->execute($sql);
			if($this->_dao->getQuery()){
				if($row = $this->_dao->getRow()){
					$roleName = $row['CriticDetails'];	
				}	
			}
			$criticid = 0;
			$sql = 'select CriticID from TBL_MultiCriteriaAssessment_Critic where SectionID = '.$section['SectionID'].' and UserID = '.$this->_userid.' and CriticRoleID = '.$section['CriticRoleID'].' and Critic = '
					.$_SESSION['SessionUserID'];
			$this->_dao->execute($sql);
			if($this->_dao->getQuery()){
				if($row = $this->_dao->getRow()){
					$criticid = $row['CriticID'];	
				}	
			}
			$this->_initCriteria($section['SectionID']);
			$optpercent = 48;
			$this->_output .= '<ul>';
			$this->_output .= '<li style="background-color:#adeab2; font-weight:bold; padding-top:4px;" class="TableHeader">';	
			$this->_output .= '<div style="width:110px;"><span></span></div>';	
			$this->_output .= '<div style="width:40%"><span style="text-align:left;">'.$criteriaTitle.'</span></div>';	
			$this->_output .= '<div style="width:'.$optpercent.'%;"><span style="text-align:left;">&ensp;&emsp;&emsp;'.$roleName.'</span></div>';	
			$this->_output .= '</li>';
			$isdraft = 0x2;
			for($i = 0; $i < count($this->_criteriaarr); $i++){
				if($i % 2 == 0){
					$this->_output .= '<li>';	
				}else{
					$this->_output .= '<li class="even rowAlternateColored">';	
				}	
				$this->_output .= '<div style="width:110px;"><span style="text-align:left;">'.$this->_criteriaarr[$i]['Criteria'].'</span></div>';	
				$this->_output .= '<div style="width:40%;"><span style="text-align:left;">'.$this->_criteriaarr[$i]['Details'].'</span></div>';
				$initscore = '';
				$sql = 'select RatingItemID from TBL_MultiCriteriaAssessment_Results where CriteriaID = '.$this->_criteriaarr[$i]['CriteriaID'].' and SaveAsDraft = 0 and CriticID in (select CriticID from '.
						'TBL_MultiCriteriaAssessment_Critic where SectionID = '.$section['SectionID'].' and UserID = '.$this->_userid.' and CriticRoleID = '.$section['CriticRoleID'].')';		
				$this->_dao->execute($sql);
				if($this->_dao->getQuery()){
					if($row = $this->_dao->getRow()){
						$isdraft |= 0x1;
						$initscore .= '<input type="hidden" value="'.$row['RatingItemID'].'" />';	
					}else{
						$isdraft &= 0x1;
						$sql = 'select RatingItemID from TBL_MultiCriteriaAssessment_Results where CriteriaID = '.$this->_criteriaarr[$i]['CriteriaID'].' and SaveAsDraft = 1 and CriticID in (select CriticID from '
								.'TBL_MultiCriteriaAssessment_Critic where SectionID = '.$section['SectionID'].' and UserID = '.$this->_userid.' and CriticRoleID = '.$section['CriticRoleID'].' and Critic = '
								.$_SESSION['SessionUserID'].') order by ResultID desc';
						$this->_dao->execute($sql);
						if($this->_dao->getQuery()){
							if($row = $this->_dao->getRow()){
								$initscore .= '<input type="hidden" value="'.$row['RatingItemID'].'" />';	
							}else{
								$initscore .= '<input type="hidden" value="999" />';
							}
						}		
					}
				}
				$this->_output .= '<div style="width:'.$optpercent.'%;"><span style="text-align:left;" class="radio">&emsp;&emsp;';
				$this->_initRating($this->_criteriaarr[$i]['CriteriaID'], $section['CriticRoleID']);					
				foreach($this->_ratingarr as $ratingitem){
					$this->_output .= '<input type="radio" id="'.$this->_criteriaarr[$i]['Criteria'].'_'.$this->_criteriaarr[$i]['CriteriaID'].'_'.$criticid.'_'.$ratingitem['Rate'].
										'" name="'.$this->_criteriaarr[$i]['Criteria'].'_'.$this->_criteriaarr[$i]['CriteriaID'].'_'.$criticid.'" value="'.$ratingitem['Rate'].'" />';
					$this->_output .= '<label for="'.$this->_criteriaarr[$i]['Criteria'].'_'.$this->_criteriaarr[$i]['CriteriaID'].'_'.$criticid.'_'.$ratingitem['Rate'].'">'.$ratingitem['Rate'].'</label>&emsp;';
				}					
				$this->_output .= $initscore.'</span></div>';											
				$this->_output .= '</li>';
			}
			$this->_output .= '<input type="hidden" class="isdraft" value="'.$isdraft.'" />';
			$this->_output .= '</ul>';	
		}
		
		private function _initStudentsli(){
			$this->_initRoles();
			
			foreach($this->_rolearr as $role){
				$this->_initStudents($role);		
			}
			foreach($this->_studentarr as $student){
				$this->_output .= '<option value="'.$student['UserID'].'">'.$student['Name'].'</option>';	
			}
			$this->_output .= '|';
			$this->_output .= '<ul>';
			$this->_output .= '<li style="background-color:#adeab2; font-weight:bold; padding-top:4px;" class="TableHeader">';	
			$this->_output .= '<div style="width:50px;"><span></span></div>';	
			$this->_output .= '<div style="width:25%;"><span style="text-align:left;">Student</span></div>';	
			$this->_output .= '<div style="width:120px;"><span>Status</span></div>';
			$this->_output .= '</li>';
			for($i = 0; $i < count($this->_studentarr); $i++){
				if($i % 2 == 0){
					$this->_output .= '<li>';	
				}else{
					$this->_output .= '<li class="even rowAlternateColored">';	
				}	
				$this->_output .= '<div style="width:50px;"><span>'.($i + 1).'.</span></div>';
				$this->_output .= '<div style="width:25%;"><span style="text-align:left;"><a href="javascript:void(0);" userid="'.$this->_studentarr[$i]['UserID'].'">'.$this->_studentarr[$i]['Name'].'</a></span></div>';
				$status = '<img src="../images/misc/ddf.gif" border="0" width="12" height="12" style="padding-top:3px;" />';
				if($this->_studentarr[$i]['Flag'] == 1){
					$status = '<img src="../images/misc/ddk.gif" border="0" width="12" height="12" style="padding-top:3px;" />';
				}elseif($this->_studentarr[$i]['Flag'] == 3){
					$status = '<img src="../images/misc/ddt.gif" border="0" width="12" height="12" style="padding-top:3px;" />';
				}
				$this->_output .= '<div style="width:120px;"><span>'.$status.'</span></div>';
				$this->_output .= '</li>';
			}
			$this->_output .= '</ul>';
			$this->_output .= '|';
			$this->_output .= '<li>';
			$this->_output .= '<div style="width:16px;"><span><img src="../images/misc/ddt.gif" border="0" width="12" height="12" style="padding-top:3px;" /></span></div>';
			$this->_output .= '<div style="width:310px; vertical-align:top;"><span>: The assessment has been graded.</span></div>';
			$this->_output .= '</li>';
			$this->_output .= '<li>';
			$this->_output .= '<div style="width:16px;"><span><img src="../images/misc/ddk.gif" border="0" width="12" height="12" style="padding-top:3px;" /></span></div>';
			$this->_output .= '<div style="width:310px; vertical-align:top;"><span>: The assessment is currently being graded.</span></div>';
			$this->_output .= '</li>';	
		}
		
		public function lists(){
			$this->_initTitle();
			$this->_initStudentsli();
			
			$this->display();
		}
		
		public function freshSection(){
			$this->_initSections($this->_rolearr[0]);
			$this->_initStudents($this->_rolearr[0], $this->_sectionarr[0]['SectionID']);	
			
			foreach($this->_sectionarr as $section){
				$this->_output .= '<option value="'.$section['SectionID'].'">'.$section['SectionTitle'].'</option>';	
			}
			$this->_output .= '|';
			$this->_output .= '<ul>';
			$this->_output .= '<li style="background-color:#adeab2; font-weight:bold; padding-top:4px;" class="TableHeader">';	
			$this->_output .= '<div style="width:50px;"><span></span></div>';	
			$this->_output .= '<div style="width:25%;"><span style="text-align:left;">Student</span></div>';	
			$this->_output .= '<div style="width:60px;"><span>Status</span></div>';
			$this->_output .= '</li>';
			for($i = 0; $i < count($this->_studentarr); $i++){
				$flag = 0x2;
				if($i % 2 == 0){
					$this->_output .= '<li>';	
				}else{
					$this->_output .= '<li class="even rowAlternateColored">';		
				}	
				$this->_output .= '<div style="width:50px;"><span>'.($i + 1).'</span></div>';
				$this->_output .= '<div style="width:25%;"><span style="text-align:left;"><a href="javascript:void(0);" criticid="'.$this->_studentarr[$i]['CriticID'].'">'.$this->_studentarr[$i]['Name'].'</a></span></div>';
				$status = '<img src="../images/misc/ddf.gif" border="0" width="12" height="12" style="padding-top:3px;" />';
				if($this->_studentarr[$i]['Flag'] == 1){
					$status = '<img src="../images/misc/ddk.gif" border="0" width="12" height="12" style="padding-top:3px;" />';
				}elseif($this->_studentarr[$i]['Flag'] == 3){
					$status = '<img src="../images/misc/ddt.gif" border="0" width="12" height="12" style="padding-top:3px;" />';
				}
				$this->_output .= '<div style="width:60px;"><span>'.$status.'</span></div>';
				$this->_output .= '</li>';
			}
			$this->_output .= '</ul>';
			$this->_output .= '<br /><br />';
			echo $this->_output;
		}
		
		public function freshStudent(){
			$this->_initStudentsli();
			
			echo $this->_output;	
		}
		
		public function listSection1By1(){
			$sectionarr = array();
			$sql = 'select distinct SectionID, CriticRoleID from TBL_MultiCriteriaAssessment_Critic where UserID = '.$this->_userid.' and Critic = '.$_SESSION['SessionUserID'];
			$this->_dao->executei($sql);
			if($this->_dao->getQuery()){
				while($row = $this->_dao->getRow()){
					$sectionarr[] = $row;
				}	
			}
			foreach($sectionarr as $section){
				$this->_initSectionli($section);		
				$this->_output .= '<br /><br />';
			}
			
			echo $this->_output.'|'.$this->_ratingli;	
		}
		
		public function display(){
			$outputarr = explode('|', $this->_output);
			require_once('./view/studentsectionlists.php');	
		}	
	}
?>