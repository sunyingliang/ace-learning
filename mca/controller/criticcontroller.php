<?php
	require_once('./controller/controller.php');
	
	class CriticController extends Controller{
		private $_paramarr;
		private $_output;
		private $_title;
		
		public function __construct(){	
			$this->_dao = new DataAccess();
			$this->_output = '';
			$this->_title = '';
			if(func_num_args() != 0){
				$param = func_get_arg(0);	
				$this->_paramarr = $this->getParamArray($param);
				$this->_title = $this->_paramarr['title'];
			};
		}
		private function _insert(& $criticresultmodel){
			$sql = 'select SaveAsDraft from TBL_MultiCriteriaAssessment_Results where CriteriaID = '.$criticresultmodel->getCriteriaID().' and CriticID in (select C1.CriticID from TBL_MultiCriteriaAssessment_Critic C1, TBL_MultiCriteriaAssessment_Critic C2 where	C1.UserID = C2.UserID and C1.CriticRoleID = C2.CriticRoleID and C2.CriticID = '.$criticresultmodel->getCriticID().')';
			$isdraft = 1;
			$this->_dao->execute($sql);
			if($this->_dao->getQuery()){
				while($row = $this->_dao->getRow()){
					$isdraft &= $row['SaveAsDraft'];	
				}
				if(1 == $isdraft){
					$sql = 'insert into TBL_MultiCriteriaAssessment_Results(CriteriaID, RatingItemID, CriticID, SaveAsDraft) value('.$criticresultmodel->getCriteriaID().', '.$criticresultmodel->getScore().', '.$criticresultmodel->getCriticID().', '.$criticresultmodel->getDraft().')';
					$this->_dao->execute($sql);
				}	
			}
			return $isdraft;
		}	
		
		public function save(){
			$flag = false;
			$version = 0;
			$sql = 'alter table TBL_MultiCriteriaAssessment_Results type = InnoDB';
			$this->_dao->execute($sql);
			if($this->_dao->getQuery()){
				$flag = true;	
			}else{
				$sql = 'alter table TBL_MultiCriteriaAssessment_Results ENGINE = InnoDB';
				$this->_dao->execute($sql);	
				if($this->_dao->getQuery()){
					$flag = true;	
					$version = 1;
				}
			}
			
			if($flag == true){
				$sql = 'start transaction';
				$this->_dao->execute($sql);
				$draft = $this->_paramarr['type'];
				$isdraft = 1;
				$failed = false;
				foreach($this->_paramarr as $key => $value){
					if($key != 'type' && $key != 'title'){
						$record = explode('_', $key);	
						$criticresult = new CriticResultModel($record[1], $value, $record[2], $draft);
						$isdraft = $this->_insert($criticresult);
						if(0 == $isdraft){
							continue;	
						}
						if(!$this->_dao->getQuery()){
							$failed = true;
							break;	
						}		
					}
				}

				if($failed == true){
					$sql = 'rollback';
					$this->_dao->execute($sql);	
					//$this->_output .= 'OOooop, critic operation failed..';
					$this->_output .= ($this->_paramarr['type'] == 1 ? 0 : 1).'|0';
				}else{
					$sql = 'commit';
					$this->_dao->execute($sql);
					//$this->_output .= 'Congratulation, critic operation successed..';
					$this->_output .= ($this->_paramarr['type'] == 1 ? 0 : 1).'|1';		
				}
				$sql = 'alter table TBL_MultiCriteriaAssessment_Results '.($version == 0 ? 'type = MyISAM' : 'ENGINE = MyISAM');
				$this->_dao->execute($sql);	
			}else{
				//$this->_output .= 'OOooop, critic operation failed..';
				$this->_output .= ($this->_paramarr['type'] == 1 ? 0 : 1).'|0';	
			}
			
			//$this->display();	
			echo $this->_output;
		}
		public function display(){
			require_once('./view/critic_save.php');	
		}
	}
?>