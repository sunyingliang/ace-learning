<?php
	require_once('./controller/controller.php');

	class SectionController extends Controller{
		private $_asmtid;
		private $_sysfolder = '';
		private $_asmttitle = '';	
		private $_sectionarr = array();
		private $_rolearr = array();
		private $_studentarr = array();
		private $_classarr = array();
		private $_stunumarr = array();
		private $_criteriaarr = array();
		private $_ratingarr = array();
		private $_outputli = '';
		private $_criteriali = '';
		private $_ratingli = '';
		//for ajax requestion.
		private $_sectionid;
		private $_roleid;
		private $_classid;
		private $_criticid;
		private $_userid;
		private $_score = array();
		private $_percentage = array();

		public function __construct(){
			$this->_dao = new DataAccess();
			if(func_num_args() == 1){
				$paramsarr = $this->getParamArray(func_get_arg(0));
				if(isset($paramsarr['assessmentid'])){
					$this->_asmtid = $paramsarr['assessmentid'];					
				}
				if(isset($paramsarr['sysfolder'])){
					$this->_sysfolder = $paramsarr['sysfolder'];					
				}
				if(isset($paramsarr['roleid'])){
					$this->_roleid = $paramsarr['roleid'];
				}
				if(isset($paramsarr['sectionid'])){
					$this->_sectionid = $paramsarr['sectionid'];
				}
				if(isset($paramsarr['classid'])){
					$this->_classid = $paramsarr['classid'];
				}
				if(isset($paramsarr['criticid'])){
					$this->_criticid = $paramsarr['criticid'];
				}
				if(isset($paramsarr['userid'])){
					$this->_userid = $paramsarr['userid'];
				}
			}
		}
		public function getAsmtID(){
			return $this->_asmtid;
		}
		public function getSysFolder(){
			return $this->_sysfolder;
		}
		public function getTitle(){
			return $this->_asmttitle;	
		}
		public function getSections(){
			return $this->_sectionarr;	
		}
		public function getCritics(){
			return $this->_rolearr;	
		}
		public function getStudents(){
			return $this->_studentarr;	
		}
		public function getClasses(){
			return $this->_classarr;
		}
		public function getCriteria(){
			return $this->_criteriaarr;
		}
		
		private function _sortby(&$arr, $field){
			if(is_array($arr)){
				for($i = 0; $i < count($arr) - 1; $i++){
					for($j = $i + 1; $j < count($arr); $j++){
						if($arr[$i][$field] > $arr[$j][$field]){
							$temp = $arr[$i];	
							$arr[$i] = $arr[$j];
							$arr[$j] = $temp;
						}	
					}	
				}
			}
		}
		private function _sortbyr(&$arr, $field){
			if(is_array($arr)){
				for($i = 0; $i < count($arr) - 1; $i++){
					for($j = $i + 1; $j < count($arr); $j++){
						if($arr[$i][$field] < $arr[$j][$field]){
							$temp = $arr[$i];	
							$arr[$i] = $arr[$j];
							$arr[$j] = $temp;
						}	
					}	
				}
			}
		}
		private function _initTitle(){
			$sql = 'select Title, StartDate, EndDate from TBL_MultiCriteriaAssessment where AssessmentID = '.$this->_asmtid;	
			$this->_dao->execute($sql);
			if($this->_dao->getQuery()){
				if($row = $this->_dao->getRow()){
					$this->_asmttitle .= '<span>'.$row['Title'].'</span>';
					//$this->_asmttitle .= '<span>'.$row['StartDate'].'--'.$row['EndDate'].'</span>';	
				}	
			}
		}
		private function _initSections(){
			$sql = 'select distinct S.SectionID, SectionTitle from TBL_MultiCriteriaAssessment_Section S, TBL_MultiCriteriaAssessment_Critic C where S.AssessmentID = '.$this->_asmtid.' and C.Critic = '
					.$_SESSION['SessionUserID'].' and S.SectionID = C.SectionID order by OrderID';
			$this->_dao->execute($sql);
			if($this->_dao->getQuery()){
				while($row = $this->_dao->getRow()){
					$this->_sectionarr[] = $row;
				}
			}
		}
		private function _initRoles($sectionid){
			$sql = 'select distinct R.CriticRoleID, R.CriticDetails, R.Weighting from TBL_MultiCriteriaAssessment_CriticRole R, TBL_MultiCriteriaAssessment_Critic C where C.SectionID = '.$sectionid.' and C.Critic = '
					.$_SESSION['SessionUserID'].' and C.CriticRoleID = R.CriticRoleID order by OrderID';
			$this->_dao->execute($sql);
			if($this->_dao->getQuery()){
				while($row = $this->_dao->getRow()){
					$this->_rolearr[] = $row;
				}
			}
		}
		private function _initStudentsClasses($roleid){
			$sql = 'select SP.ClassID, SP.IndexID, U.Name, U.UserID, U.CriticID from (select TU.UserID, CONCAT(FirstName, LastName) AS Name, C.CriticID from TBL_Users TU, (select distinct UserID, CriticID from '
					.'TBL_MultiCriteriaAssessment_Critic where Critic = '.$_SESSION['SessionUserID'].' and CriticRoleID = '.$roleid.') C where TU.UserID = C.UserID) as U, TBL_StudentsProfile as SP where '
					.'U.UserID = SP.UserID order by SP.ClassID, SP.IndexID';			
			$this->_dao->execute($sql);
			if($this->_dao->getQuery()){
				while($row = $this->_dao->getRow()){
					$this->_studentarr[] = $row;
					if(!in_array($row['ClassID'], $this->_classarr)){
						$this->_classarr[] = $row['ClassID'];	
						$this->_stunumarr[$row['ClassID']] = 1;
					}else{
						$this->_stunumarr[$row['ClassID']]++;
					}
				}	
			}
		}
		private function _initStudentsClassesAll(){
			$sql = 'select SP.ClassID, SP.IndexID, U.Name, SP.ICNumber, U.UserID from (select TU.UserID, CONCAT(FirstName, LastName) AS Name from TBL_Users TU, (select distinct UserID from TBL_MultiCriteriaAssessment_Critic where Critic = '.$_SESSION['SessionUserID'].' and SectionID in (select SectionID from TBL_MultiCriteriaAssessment_Section where AssessmentID = '.$this->_asmtid.')) C where TU.UserID = C.UserID) as U, TBL_StudentsProfile as SP where U.UserID = SP.UserID order by SP.ClassID, SP.IndexID';	
			$this->_dao->execute($sql);
			if($this->_dao->getQuery()){
				while($row = $this->_dao->getRow()){
					$this->_studentarr[] = $row;
					if(!in_array($row['ClassID'], $this->_classarr)){
						$this->_classarr[] = $row['ClassID'];	
						$this->_stunumarr[$row['ClassID']] = 1;
					}else{
						$this->_stunumarr[$row['ClassID']]++;
					}
				}	
			}
		}
		private function _initStudentsClassesR(){
			$sql = 'select SP.ClassID, SP.IndexID, U.Name, SP.ICNumber, CriticID from (select TU.UserID, CONCAT(FirstName, LastName) AS Name, C.CriticID from TBL_Users TU, (select UserID, CriticID from TBL_MultiCriteriaAssessment_Critic where SectionID in (select SectionID from TBL_MultiCriteriaAssessment_Section where AssessmentID = '.$this->_asmtid.') group by UserID) C where TU.UserID = C.UserID) as U, TBL_StudentsProfile as SP where U.UserID = SP.UserID order by SP.ClassID, SP.IndexID';
			$this->_dao->execute($sql);
			if($this->_dao->getQuery()){
				while($row = $this->_dao->getRow()){
					$this->_studentarr[] = $row;
					if(!in_array($row['ClassID'], $this->_classarr)){
						$this->_classarr[] = $row['ClassID'];	
						$this->_stunumarr[$row['ClassID']] = 1;
					}else{
						$this->_stunumarr[$row['ClassID']]++;
					}
				}	
			}
		}
		private function _initCriteria($sectionid){
			$this->_criteriaarr = array();
			$sql = 'SELECT CriteriaID, OrderID, Weighting, Criteria, Details FROM TBL_MultiCriteriaAssessment_Criteria where SectionID = '.$sectionid;	
			$this->_dao->execute($sql);
			if($this->_dao->getQuery()){
				while($row = $this->_dao->getRow()){
					$this->_criteriaarr[] = $row;	
				}	
				$this->_sortby($this->_criteriaarr, 'OrderID');
			}
			$this->_criteriali = '';
			foreach($this->_criteriaarr as $criteria){   
				$this->_criteriali .= '<li>';
				$this->_criteriali .= '<div style="width:100px; vertical-align:top;"><span>'.$criteria['Criteria'].': </span></div>';
				$this->_criteriali .= '<div style="width:528px;"><span>'.$criteria['Details'].'</span></div>';
				$this->_criteriali .= '</li>';					
			}
		}
		private function _initRating($criteriaid, $roleid){
			$this->_ratingarr = array();
			$sql = 'select Rate, Details from TBL_MultiCriteriaAssessment_RatingItems where RatingID = (select RatingID from TBL_MultiCriteriaAssessment_Criteria_Role_Rating where CriteriaID = '.$criteriaid.' and CriticRoleID = '.$roleid.') order by OrderID';	
			$this->_dao->execute($sql);
			if($this->_dao->getQuery()){
				while($row = $this->_dao->getRow()){
					$this->_ratingarr[] = $row;	
				}
			}
			$this->_ratingli = '';
			foreach($this->_ratingarr as $item){   
				$this->_ratingli .= '<li>';
				$this->_ratingli .= '<div style="width:20px; vertical-align:top;"><span>'.$item['Rate'].': </span></div>';
				$this->_ratingli .= '<div style="width:310px;"><span>'.$item['Details'].'</span></div>';
				$this->_ratingli .= '</li>';					
			}
		}
		public function _initStudentliA($roleid){
			$sectionid = 0;
			$sql = 'select SectionID from TBL_MultiCriteriaAssessment_CriticRole where CriticRoleID = '.$roleid;
			$this->_dao->execute($sql);
			if($this->_dao->getQuery()){
				if($row = $this->_dao->getRow()){
					$sectionid = $row['SectionID'];	
				}	
			}
			$this->_initCriteria($sectionid);
			//$this->_sortbyr($this->_criteriaarr, 'OrderID');
			
			$isdraft = 0x2;
			$optpercent = 60 / count($this->_criteriaarr);
			$optpercent = $optpercent > 10 ? 10 : $optpercent;
			$this->_outputli .= '<ul>';
			$this->_outputli .= '<li style="background-color:#adeab2; font-weight:bold; padding-top:4px;" class="TableHeader">';	
			$this->_outputli .= '<div style="width:60px;"><span>Class</span></div>';	
			$this->_outputli .= '<div style="width:40px;"><span>S/N</span></div>';	
			$this->_outputli .= '<div style="width:30%;"><span style="text-align:left;">&emsp;Name</span></div>';	
			foreach($this->_criteriaarr as $criteria){
				$this->_outputli .= '<div style="width:'.$optpercent.'%;"><span>'.$criteria['Criteria'].'</span></div>';	
			}
			$this->_outputli .= '</li>';
			
			for($i = 0; $i < count($this->_studentarr); $i++){
				if($i % 2 == 0){
					$this->_outputli .= '<li>';	
				}else{
					$this->_outputli .= '<li class="even rowAlternateColored">';
				}	
				$this->_outputli .= '<div style="width:60px;"><span>'.$this->_studentarr[$i]['ClassID'].'</span></div>';	
				$this->_outputli .= '<div style="width:40px;"><span>'.$this->_studentarr[$i]['IndexID'].'</span></div>';	
				$this->_outputli .= '<div style="width:30%;"><span style="text-align:left;">&emsp;'.$this->_studentarr[$i]['Name'].'</span></div>';	
				foreach($this->_criteriaarr as $criteria){
					$initscore = '';
					//retrieve db to detect weather there is a score already.
					$sql = 'select RatingItemID from TBL_MultiCriteriaAssessment_Results where CriteriaID = '.$criteria['CriteriaID'].' and SaveAsDraft = 0 and CriticID in (select CriticID from '.
							'TBL_MultiCriteriaAssessment_Critic where SectionID = '.$sectionid.' and UserID = '.$this->_studentarr[$i]['UserID'].' and CriticRoleID = '.$roleid.')';
					/*		
					$fhder = fopen('issubmit.txt', 'a+');		
					if($fhder){
						if(chmod('issubmit.txt', 0777)){
							fwrite($fhder, $sql.'===');	
						}	
					}
					fclose($fhder);*/
							
					$this->_dao->execute($sql);
					if($this->_dao->getQuery()){
						//this criteria has been saved as submit.
						if($row = $this->_dao->getRow()){
							$isdraft |= 0x1;
							$initscore .= '<input type="hidden" value="'.$row['RatingItemID'].'" />';
							$initscore .= '<input type="hidden" value="1" />';	
						}else{
							$isdraft &= 0x1;
							$sql = 'select RatingItemID from TBL_MultiCriteriaAssessment_Results where CriteriaID = '.$criteria['CriteriaID'].' and SaveAsDraft = 1 and CriticID in (select CriticID from '
									.'TBL_MultiCriteriaAssessment_Critic where SectionID = '.$sectionid.' and UserID = '.$this->_studentarr[$i]['UserID'].' and CriticRoleID = '.$roleid.' and Critic = '
									.$_SESSION['SessionUserID'].') order by ResultID desc';	
							$this->_dao->execute($sql);
							if($this->_dao->getQuery()){
								//this criteria has been saved as draft.
								if($row = $this->_dao->getRow()){
									$initscore .= '<input type="hidden" value="'.$row['RatingItemID'].'" />';	
								}else{
									//there are no critic before.	
									$initscore .= '<input type="hidden" value="999" />';
								}
							}
							$initscore .= '<input type="hidden" value="0" />';		
						}
					}
					
					$this->_outputli .= '<div style="width:'.$optpercent.'%;"><span><select name="'.$criteria['Criteria'].'_'.$criteria['CriteriaID'].'_'.$this->_studentarr[$i]['CriticID'].'">'
										.'<option value="999" selected="selected">--</option>';	
					$this->_initRating($criteria['CriteriaID'], $roleid);					
					foreach($this->_ratingarr as $ratingitem){
						$this->_outputli .= '<option value="'.$ratingitem['Rate'].'">'.$ratingitem['Rate'].'</option>';		
					}
					$this->_outputli .= '</select>'.$initscore.'</span></div>';											
				}
				$this->_outputli .= '</li>';					
			}
			$this->_outputli .= '<input type="hidden" class="isdraft" value="'.$isdraft.'" />';
			$this->_outputli .= '</ul>';
		}
		private function _initStudentliR(){
			$oppercent = 60 / count($this->_criteriaarr);
			$this->_outputli .= '<li style="background-color:#adeab2; font-weight:bold; padding-top:4px;" class="TableHeader">';	
			$this->_outputli .= '<div style="width:10%"><span>Class</span></div>';	
			$this->_outputli .= '<div style="width:5%"><span>S/N</span></div>';	
			$this->_outputli .= '<div style="width:25%"><span style="text-align:left;">Name</span></div>';	
			foreach($this->_criteriaarr as $criteria){
				$this->_outputli .= '<div style="width:'.$oppercent.'%"><span>'.$criteria['Criteria'].'</span></div>';	
			}
			$this->_outputli .= '<div style="width:'.$idxpercent.'%"><span>Score</span></div>';
			$this->_outputli .= '</li>';
			
			for($i = 0; $i < count($this->_studentarr); $i++){
				$scorearr = array();
				if($i % 2 == 0){
					$this->_outputli .= '<li>';	
				}else{
					$this->_outputli .= '<li class="even rowAlternateColored">';
				}	
				$this->_outputli .= '<div style="width:10%"><span>'.$this->_studentarr[$i]['ClassID'].'</span></div>';	
				$this->_outputli .= '<div style="width:5%"><span>'.$this->_studentarr[$i]['IndexID'].'</span></div>';	
				$this->_outputli .= '<div style="width:25%"><span style="text-align:left;">'.$this->_studentarr[$i]['Name'].'</span></div>';	
				foreach($this->_criteriaarr as $criteria){
					$initscore = '';
					//retrieve db to detect weather there is a score already.
					$sql = 'select RatingItemID from TBL_MultiCriteriaAssessment_Results where CriteriaID = '.$criteria['CriteriaID'].' and SaveAsDraft = 0 and CriticID in (select C1.CriticID from '.
							'TBL_MultiCriteriaAssessment_Critic C1, TBL_MultiCriteriaAssessment_Critic C2 where C1.SectionID = C2.SectionID and C1.UserID = C2.UserID and C1.CriticRoleID = C2.CriticRoleID and '.
							'C2.CriticID = '.$this->_studentarr[$i]['CriticID'].')';	
					$this->_dao->execute($sql);
					if($this->_dao->getQuery()){
						//this criteria has been saved as submit.
						if($row = $this->_dao->getRow()){
							$this->_outputli .= '<div style="width:'.$oppercent.'%"><span>'.$row['RatingItemID'].'</span></div>';	
							$sqlW = 'select Weighting from TBL_MultiCriteriaAssessment_Criteria where CriteriaID = '.$criteria['CriteriaID'];
							$rsW = mysql_query($sqlW);
							if($rowW = mysql_fetch_array($rsW)){
								$scorearr[$criteria['Criteria']] = $row['RatingItemID'] * $rowW['Weighting'];
							}else{
								$scorearr[$criteria['Criteria']] = $row['RatingItemID'];	
							}
						}else{
							$this->_outputli .= '<div style="width:'.$oppercent.'%">--</span></div>';		
							$scorearr[$criteria['Criteria']] = 999;
						}
					}
				}
				$this->_outputli .= '<div style="width:'.$idxpercent.'%"><span>'.(array_sum($scorearr) > 999 ? '--' : array_sum($scorearr)).'</span></div>';
				$this->_outputli .= '</li>';					
			}
		}
		
		public function lists(){
			$this->_initTitle();
			$this->_initSections();
			$this->_initRoles($this->_sectionarr[0]['SectionID']);
			$this->_initStudentsClasses($this->_rolearr[0]['CriticRoleID']);
			$this->_initStudentliA($this->_rolearr[0]['CriticRoleID']);
			$this->_outputli .= '<br /><br />';
			$this->display();
		}
		
		public function freshSectionA(){
			//construct the content of panel.
			$this->_initRoles($this->_sectionid);
			//construct the options of critic's selector.
			foreach($this->_rolearr as $role){
				$this->_outputli .= '<option value="'.$role['CriticRoleID'].'">'.$role['CriticDetails'].'</option>';	
			}
			$this->_initStudentsClasses($this->_rolearr[0]['CriticRoleID']);
			$this->_outputli .= '|';
			$this->_initStudentliA($this->_rolearr[0]['CriticRoleID']);
			$this->_outputli .= '<br /><br />';
			//construct the options of class's selector.
			$this->_outputli .= '|';
			$this->_outputli .= '<option value="0">All Classes</option>';
			foreach($this->_classarr as $class){
				$this->_outputli .= '<option value="'.$class.'">'.$class.'</option>';	
			}
			//construct the criteria panel.
			$this->_outputli .= '|';
			$this->_outputli .= $this->_criteriali;
			//construct the rating panel.
			$this->_outputli .= '|';
			$this->_outputli .= $this->_ratingli;
			
			echo $this->_outputli;
		}
		
		public function freshSectionR(){
			$this->_initStudentsClassesR();
			//construct the options of class's selector.
			$this->_outputli = '';
			$this->_outputli .= '<option value="0">All Classes</option>';
			foreach($this->_classarr as $class){
				$this->_outputli .= '<option value="'.$class.'">'.$class.'</option>';	
			}
			//construct the classes' panel.
			$this->_outputli .= '|';
			$this->_outputli .= $this->_initClassliR();
			
			echo $this->_outputli;
		}
		
		public function freshCritic(){
			//construct the content of panel.
			$this->_initStudentsClasses($this->_roleid);
			$this->_initStudentliA($this->_roleid);
			$this->_outputli .= '<br /><br />';
			//construct the options of class's selector.
			$this->_outputli .= '|';
			$this->_outputli .= '<option value="0">All Classes</option>';
			foreach($this->_classarr as $class){
				$this->_outputli .= '<option value="'.$class.'">'.$class.'</option>';	
			}
			//construct the criteria panel.
			$this->_outputli .= '|';
			$this->_outputli .= $this->_criteriali;
			//construct the rating panel.
			$this->_outputli .= '|';
			$this->_outputli .= $this->_ratingli;
			
			echo $this->_outputli;
		}
		
		private function _initClassliA(){
			$output = '<ul>';
			$output .= '<li style="background-color:#adeab2; font-weight:bold; padding-top:4px;" class="TableHeader">';	
			$output .= '<div style="width:50px;"><span></span></div>';	
			$output .= '<div style="width:60px;"><span>Class</span></div>';	
			$output .= '<div style="width:20%"><span>Number of Assessment</span></div>';
			$output .= '</li>';
			
			for($i = 0; $i < count($this->_classarr); $i++){
				if($i % 2 == 0){
					$output .= '<li>';	
				}else{
					$output .= '<li class="even rowAlternateColored">';	
				}	
				$output .= '<div style="width:50px;"><span>'.($i + 1).'.</span></div>';
				$output .= '<div style="width:60px;"><span><a href="javascript:void(0);" classid="'.$this->_classarr[$i].'">'.$this->_classarr[$i].'</a></span></div>';
				$output .= '<div style="width:20%"><span>'.$this->_stunumarr[$this->_classarr[$i]].'</span></div>';
				$output .= '</li>';
			}
			$output .= '</ul>';
			return $output;	
		}
		
		private function _initClassliR(){
			$output = '<ul>';
			$output .= '<li style="background-color:#adeab2; font-weight:bold; padding-top:4px;" class="TableHeader">';	
			$output .= '<div style="width:50px;"><span></span></div>';	
			$output .= '<div style="width:60px;"><span>Class</span></div>';
			$output .= '</li>';
			
			for($i = 0; $i < count($this->_classarr); $i++){
				if($i % 2 == 0){
					$output .= '<li>';	
				}else{
					$output .= '<li class="even rowAlternateColored">';	
				}	
				$output .= '<div style="width:50px;"><span>'.($i + 1).'.</span></div>';
				$output .= '<div style="width:60px;"><span><a href="javascript:void(0);" classid="'.$this->_classarr[$i].'">'.$this->_classarr[$i].'</a></span></div>';
				$output .= '</li>';
			}
			$output .= '</ul>';
			return $output;	
		}
		
		public function listClassesA(){
			$output = '';
			if($this->_sectionid == 0){
				$this->_initStudentsClassesAll();			
			}else{
				$this->_initStudentsClasses($this->_roleid);		
			}
			
			$output .= '<option value="0">All Classes</option>';
			foreach($this->_classarr as $class){
				$output .= '<option value="'.$class.'">'.$class.'</option>';	
			}
			echo $output.'|'.$this->_initClassliA();	
		}
		
		public function listClassesR(){
			$sql = 'select SP.ClassID, SP.IndexID, U.Name, U.UserID from (select TU.UserID, CONCAT(FirstName, LastName) AS Name from TBL_Users TU, (select distinct UserID from TBL_MultiCriteriaAssessment_Critic C, '
					.'TBL_MultiCriteriaAssessment_Section S where C.SectionID = S.SectionID and S.AssessmentID = '.$this->_asmtid.') CS where TU.UserID = CS.UserID) as U, TBL_StudentsProfile as SP '
					.'where U.UserID = SP.UserID order by SP.ClassID, SP.IndexID';			
			$this->_dao->execute($sql);
			if($this->_dao->getQuery()){
				while($row = $this->_dao->getRow()){
					$this->_studentarr[] = $row;
					if(!in_array($row['ClassID'], $this->_classarr)){
						$this->_classarr[] = $row['ClassID'];	
						$this->_stunumarr[$row['ClassID']] = 1;
					}else{
						$this->_stunumarr[$row['ClassID']]++;
					}
				}	
			}
			$output = '';
			$output .= '<option value="0">All Classes</option>';
			foreach($this->_classarr as $class){
				$output .= '<option value="'.$class.'">'.$class.'</option>';	
			}
			
			echo $output.'|'.$this->_initClassliR();	
		}
		
		private function _initStudentsA($_roleid){
			$sectionarr = array();
			$sql = 'select SectionID from TBL_MultiCriteriaAssessment_Section where AssessmentID = '.$this->_asmtid;
			$this->_dao->executei($sql);
			if($this->_dao->getQuery()){
				while($row = $this->_dao->getRow()){
					$sectionarr[] = $row['SectionID'];	
				}	
			}
			$sql = 'select SP.IndexID, U.Name, U.UserID from (select TU.UserID, CONCAT(FirstName, LastName) AS Name from TBL_Users TU, (select distinct UserID from TBL_MultiCriteriaAssessment_Critic where Critic = '
					.$_SESSION['SessionUserID'].' and CriticRoleID = '.$_roleid.') C where TU.UserID = C.UserID) as U, TBL_StudentsProfile as SP where U.UserID = SP.UserID and SP.SchoolID = "'.$_SESSION['SessionSchoolID']
					.'" and SP.ClassID = "'.$this->_classid.'" order by SP.IndexID';	
			$this->_dao->executei($sql);
			if($this->_dao->getQuery()){
				while($rowS = $this->_dao->getRow()){
					$flag = 0x2;	//(0) means no section has been criticed, (1) means part of sections are criticed, (3) means all sections are criticed.
					foreach($sectionarr as $sectionid){
						$sqlr = 'select distinct R.CriticRoleID from TBL_MultiCriteriaAssessment_CriticRole R, TBL_MultiCriteriaAssessment_Critic C where R.CriticRoleID = C.CriticRoleID and C.SectionID = '.$sectionid
								.' and C.UserID = '.$rowS['UserID'].' order by R.OrderID';
						$recordset = mysql_query($sqlr);
						if($recordset){
							while($rowR = mysql_fetch_array($recordset)){
								$sqld = 'select count(*) as Submit from TBL_MultiCriteriaAssessment_Results where SaveAsDraft = 0 and CriticID in (select CriticID from TBL_MultiCriteriaAssessment_Critic'
										.' where SectionID = '.$sectionid.' and CriticRoleID = '.$rowR['CriticRoleID'].' and UserID = '.$rowS['UserID'].')';	
								$resultset = mysql_query($sqld);
								if($resultset){
									if($rowd = mysql_fetch_array($resultset)){
										if($rowd['Submit'] == 0){
											$flag &= 0x1;
										}else{
											$flag |= 0x1;
										}	
									}	
								}
							}
						}
					}
					$rowS['Flag'] = $flag;
					$this->_studentarr[] = $rowS;
				}
			}
		}
		
		private function _initStudentsAAll(){
			$sectionarr = array();
			$sql = 'select SectionID from TBL_MultiCriteriaAssessment_Section where AssessmentID = '.$this->_asmtid;
			$this->_dao->executei($sql);
			if($this->_dao->getQuery()){
				while($row = $this->_dao->getRow()){
					$sectionarr[] = $row['SectionID'];	
				}	
			}
			$sql = 'select SP.IndexID, U.Name, U.UserID from (select TU.UserID, CONCAT(FirstName, LastName) AS Name from TBL_Users TU, (select distinct UserID from TBL_MultiCriteriaAssessment_Critic where Critic = '
					.$_SESSION['SessionUserID'].' and SectionID in ('.implode(',', $sectionarr).')) C where TU.UserID = C.UserID) as U, TBL_StudentsProfile as SP where U.UserID = SP.UserID and SP.SchoolID = "'
					.$_SESSION['SessionSchoolID'].'" and SP.ClassID = "'.$this->_classid.'" order by SP.IndexID';			
			$this->_dao->executei($sql);
			if($this->_dao->getQuery()){
				while($rowS = $this->_dao->getRow()){
					$flag = 0x2;	//(0) means no section has been criticed, (1) means part of sections are criticed, (3) means all sections are criticed.
					foreach($sectionarr as $sectionid){
						$sqlr = 'select distinct R.CriticRoleID from TBL_MultiCriteriaAssessment_CriticRole R, TBL_MultiCriteriaAssessment_Critic C where R.CriticRoleID = C.CriticRoleID and C.SectionID = '.$sectionid
								.' and C.UserID = '.$rowS['UserID'].' order by R.OrderID';
						$recordset = mysql_query($sqlr);
						if($recordset){
							while($rowR = mysql_fetch_array($recordset)){
								$sqld = 'select count(*) as Submit from TBL_MultiCriteriaAssessment_Results where SaveAsDraft = 0 and CriticID in (select CriticID from TBL_MultiCriteriaAssessment_Critic '.
								'where SectionID = '.$sectionid.' and CriticRoleID = '.$rowR['CriticRoleID'].' and UserID = '.$rowS['UserID'].')';	
								$resultset = mysql_query($sqld);
								if($resultset){
									if($rowd = mysql_fetch_array($resultset)){
										if($rowd['Submit'] == 0){
											$flag &= 0x1;
										}else{
											$flag |= 0x1;
										}	
									}	
								}
							}
						}
					}
					$rowS['Flag'] = $flag;
					$this->_studentarr[] = $rowS;
				}
			}
		}
		
		private function _initStudentsR(){
			$sectionarr = array();
			$rolearr = array();
			$criteriaarr = array();
			$scorearr = array();
			//init the section array.
			$sql = 'select SectionID from TBL_MultiCriteriaAssessment_Section where AssessmentID = '.$this->_asmtid;
			$this->_dao->executei($sql);
			if($this->_dao->getQuery()){
				while($row = $this->_dao->getRow()){
					$sectionarr[$row['SectionID']] = $row['SectionID'];
				}	
			}
			//init the role array.
			$sectionstr = '';
			foreach($sectionarr as $section){
				$sectionstr .= empty($sectionstr) ? $section : ','.$section;	
			}
			$sql = 'select CriticRoleID, Weighting from TBL_MultiCriteriaAssessment_CriticRole where SectionID in ('.$sectionstr.')';
			$this->_dao->executei($sql);
			if($this->_dao->getQuery()){
				while($row = $this->_dao->getRow()){
					$rolearr[$row['CriticRoleID']] = $row['Weighting'];
				}	
			}
			//init the criteria array.
			$rolestr = '';
			foreach(array_keys($rolearr) as $role){
				$rolestr .= empty($rolestr) ? $role : ','.$role;	
			}
			$sql = 'select TC.CriteriaID, TC.Weighting, max(Rate) as MScore from TBL_MultiCriteriaAssessment_RatingItems RI, (select C.CriteriaID, C.Weighting, CRR.RatingID from TBL_MultiCriteriaAssessment_Criteria C, TBL_MultiCriteriaAssessment_Criteria_Role_Rating CRR where C.CriteriaID = CRR.CriteriaID and CRR.CriticRoleID in ('.$rolestr.') and C.SectionID in ('.$sectionstr.')) TC where RI.RatingID = TC.RatingID group by CriteriaID';
			$this->_dao->executei($sql);
			if($this->_dao->getQuery()){
				while($row = $this->_dao->getRow()){
					$criteriaarr[$row['CriteriaID']] = $row;
				}	
			}
			//loop by student.
			$sql = 'select SP.IndexID, U.Name, SP.ICNumber, U.UserID from (select TU.UserID, CONCAT(FirstName, LastName) AS Name from TBL_Users TU, (select distinct UserID from TBL_MultiCriteriaAssessment_Critic where SectionID in ('.$sectionstr.')) C where TU.UserID = C.UserID) as U, TBL_StudentsProfile as SP where U.UserID = SP.UserID and SP.SchoolID = "'.$_SESSION['SessionSchoolID'].'" and SP.ClassID = "'.$this->_classid.'" order by SP.IndexID';
			$this->_dao->executei($sql);
			if($this->_dao->getQuery()){
				while($rowS = $this->_dao->getRow()){
					$markarr = array();
					$scoreR = array();
					$scoreRM = array();
					$scoreW = 0;
					//re-init section array which the student has attended.
					$sectionarr = array();
					$sqlsec = 'select distinct S.SectionID from TBL_MultiCriteriaAssessment_Section S, TBL_MultiCriteriaAssessment_Critic C where S.AssessmentID = '.$this->_asmtid.' and C.UserID = '.$rowS['UserID']
							.' and S.SectionID = C.SectionID order by OrderID';
					$rssec = mysql_query($sqlsec);
					if($rssec){
						while($rowSec = mysql_fetch_array($rssec)){		
							$sectionarr[] = $rowSec['SectionID'];
						}
					}
					$flag = 0x2;	//(0) means no section has been criticed, (1) means part of sections are criticed, (3) means all sections are criticed.
					foreach($sectionarr as $sectionid){
						if(!array_key_exists($sectionid, $markarr)){
							$markarr[$sectionid] = array();	
						}
						$sqlr = 'select distinct R.CriticRoleID from TBL_MultiCriteriaAssessment_CriticRole R, TBL_MultiCriteriaAssessment_Critic C where R.CriticRoleID = C.CriticRoleID and C.SectionID = '.$sectionid
								.' and C.UserID = '.$rowS['UserID'].' order by R.OrderID';
						$recordset = mysql_query($sqlr);
						if($recordset){
							while($rowR = mysql_fetch_array($recordset)){		
								$sqld = 'select count(*) as Submit from TBL_MultiCriteriaAssessment_Results where SaveAsDraft = 0 and CriticID in (select CriticID from TBL_MultiCriteriaAssessment_Critic where SectionID = '
										.$sectionid.' and UserID = '.$rowS['UserID'].' and CriticRoleID = '.$rowR['CriticRoleID'].')';		
								$resultset = mysql_query($sqld);
								if($resultset){
									if($rowd = mysql_fetch_array($resultset)){
										if($rowd['Submit'] == 0){
											$flag &= 0x1;
										}else{
											$flag |= 0x1;
										}	
									}	
								}
								
								if(!array_key_exists($rowR['CriticRoleID'], $markarr[$sectionid])){
									$markarr[$sectionid][$rowR['CriticRoleID']] = array();	
								}
								//retrieve the score of criteria criticed by the role.
								$sqlC = 'select CriteriaID from TBL_MultiCriteriaAssessment_Criteria where SectionID = '.$sectionid;
								$rsC = mysql_query($sqlC);
								if($rsC){
									while($rowC = mysql_fetch_array($rsC)){
										$sqlM = 'select RatingItemID, count(*) as Submit from TBL_MultiCriteriaAssessment_Results where CriteriaID = '.$rowC['CriteriaID'].' and SaveAsDraft = 0 and CriticID in ('.
												'select CriticID from TBL_MultiCriteriaAssessment_Critic where SectionID = '.$sectionid.' and UserID = '.$rowS['UserID'].' and CriticRoleID = '.$rowR['CriticRoleID'].')';
										$rsM = mysql_query($sqlM);
										if($rsM){
											if($rowM = mysql_fetch_array($rsM)){
												$markarr[$sectionid][$rowR['CriticRoleID']][$rowC['CriteriaID']] = $rowM['Submit'] > 0 ? $rowM['RatingItemID'] : 999;	
											}	
										}
																					
									}	
								}
							}
						}
					}
					
					$rowS['Roles'] = array();
					foreach($markarr as $keysectionid => $valuerolearr){
						foreach($valuerolearr as $keyroleid => $valuecriteriaarr){
							foreach($valuecriteriaarr as $keycriteriaid => $valuescore){
								$scoreR[$keyroleid]	+= $valuescore * $criteriaarr[$keycriteriaid]['Weighting'];
								$scoreRM[$keyroleid] += $criteriaarr[$keycriteriaid]['MScore'] * $criteriaarr[$keycriteriaid]['Weighting'];
							}
							$rowS['Roles'][$keyroleid] = $scoreR[$keyroleid];
							$scoreR[$keyroleid] /= $scoreRM[$keyroleid];
							$scoreW += $scoreR[$keyroleid] * $rolearr[$keyroleid] * 100;
						}	
					}
					list($kcriteria, $vcriteria) = each($criteriaarr);
					$rowS['Mean'] = $scoreW > 100 ? '--' : sprintf('%.2f', ($scoreW / 100 * $vcriteria['MScore']));
					$sqlG = 'select Grade, count(*) as Number from TBL_MultiCriteriaAssessment_Grade G, TBL_MultiCriteriaAssessment_Section S where G.AssessmentID = S.AssessmentID and S.SectionID = '.$this->_sectionid.
							' and G.PercentageFrom <= '.$scoreW.' and G.PercentageTo > '.$scoreW;
					$rsG = mysql_query($sqlG);		
					if($rsG){
						if($rowG = mysql_fetch_array($rsG)){
							$rowS['Grade'] = $rowG['Number'] > 0 ? $rowG['Grade'] : '--';
						}	
					}
					$rowS['Flag'] = $flag;
					$this->_studentarr[] = $rowS;
				}
			}
		}
		
		public function liststudentsA(){
			if($this->_sectionid == 0){
				$this->_initStudentsAAll();
				
			}else{
				$this->_initStudentsA($this->_roleid);
			}
			$outputopt = '<option value="0">All Students</option>';
			$outputli = '<ul>';
			$outputli .= '<li style="background-color:#adeab2; font-weight:bold; padding-top:4px;" class="TableHeader">';	
			$outputli .= '<div style="width:50px;"><span></span></div>';	
			$outputli .= '<div style="width:25%;"><span style="text-align:left;">Student</span></div>';	
			$outputli .= '<div style="width:120px;"><span>Status</span></div>';
			$outputli .= '</li>';
			for( $i = 0; $i < count($this->_studentarr); $i++){
				$outputopt .= '<option value="'.$this->_studentarr[$i]['UserID'].'">'.$this->_studentarr[$i]['Name'].'</option>';
				if($i % 2 == 0){
					$outputli .= '<li>';	
				}else{
					$outputli .= '<li class="even rowAlternateColored">';	
				}	
				$outputli .= '<div style="width:50px;"><span>'.($i + 1).'.</span></div>';
				$outputli .= '<div style="width:25%;"><span style="text-align:left;"><a href="javascript:void(0);" userid="'.$this->_studentarr[$i]['UserID'].'">'.$this->_studentarr[$i]['Name'].'</a></span></div>';
				$status = '<img src="../images/misc/ddf.gif" border="0" width="12" height="12" style="padding-top:3px;" />';
				if($this->_studentarr[$i]['Flag'] == 1){
					$status = '<img src="../images/misc/ddk.gif" border="0" width="12" height="12" style="padding-top:3px;" />';
				}elseif($this->_studentarr[$i]['Flag'] == 3){
					$status = '<img src="../images/misc/ddt.gif" border="0" width="12" height="12" style="padding-top:3px;" />';
				}
				$outputli .= '<div style="width:120px;"><span>'.$status.'</span></div>';
				$outputli .= '</li>';
			}	
			$outputli .= '</ul>';
			$outputtip = '<li>';
			$outputtip .= '<div style="width:16px;"><span><img src="../images/misc/ddt.gif" border="0" width="12" height="12" style="padding-top:3px;" /></span></div>';
			$outputtip .= '<div style="width:310px; vertical-align:top;"><span>: The assessment has been graded.</span></div>';
			$outputtip .= '</li>';
			$outputtip .= '<li>';
			$outputtip .= '<div style="width:16px;"><span><img src="../images/misc/ddk.gif" border="0" width="12" height="12" style="padding-top:3px;" /></span></div>';
			$outputtip .= '<div style="width:310px; vertical-align:top;"><span>: The assessment is currently being graded.</span></div>';
			$outputtip .= '</li>';	
			
			echo $outputopt.'|'.$outputli.'|'.$outputtip;
		}
		
		public function liststudentsR(){
			$this->_initStudentsR();
			$outputopt = '';
			$outputopt .= '<option value="0">All Students</option>';
			$outputli = '<ul>';
			$outputli .= '<li style="background-color:#adeab2; font-weight:bold; padding-top:4px;" class="TableHeader">';	
			$outputli .= '<div style="width:50px;"><span></span></div>';	
			$outputli .= '<div style="width:25%;"><span style="text-align:left;">Student</span></div>';	
			$outputli .= '<div style="width:78px;"><span>Status</span></div>';	
			$outputli .= '<div style="width:78px;"><span>Grade</span></div>';	
			$outputli .= '<div style="width:78px;"><span>Mean</span></div>';	
			$outputli .= '<div style="width:78px;"><span>Total</span></div>';	
			$outputli .= '<div style="width:78px;"><span>FT1</span></div>';	
			$outputli .= '<div style="width:78px;"><span>FT2</span></div>';	
			$outputli .= '<div style="width:78px;"><span>Self</span></div>';	
			$outputli .= '<div style="width:78px;"><span>Peer</span></div>';	
			$outputli .= '<div style="width:78px;"><span>CCA</span></div>';
			$outputli .= '</li>';
			for( $i = 0; $i < count($this->_studentarr); $i++){
				$outputopt .= '<option value="'.$this->_studentarr[$i]['UserID'].'">'.$this->_studentarr[$i]['Name'].'</option>';
				if($i % 2 == 0){
					$outputli .= '<li>';	
				}else{
					$outputli .= '<li class="even rowAlternateColored">';	
				}	
				$outputli .= '<div style="width:50px;"><span>'.($i + 1).'.</span></div>';
				$outputli .= '<div style="width:25%;"><span style="text-align:left;"><a href="javascript:void(0);" userid="'.$this->_studentarr[$i]['UserID'].'">'.$this->_studentarr[$i]['Name'].'</a></span></div>';
				$status = '<img src="../images/misc/ddf.gif" border="0" width="12" height="12" style="padding-top:3px;" />';
				if($this->_studentarr[$i]['Flag'] == 1){
					$status = '<img src="../images/misc/ddk.gif" border="0" width="12" height="12" style="padding-top:3px;" />';
				}elseif($this->_studentarr[$i]['Flag'] == 3){
					$status = '<img src="../images/misc/ddt.gif" border="0" width="12" height="12" style="padding-top:3px;" />';
				}
				$outputli .= '<div style="width:78px;"><span>'.$status.'</span></div>';
				$outputli .= '<div style="width:78px;"><span>'.$this->_studentarr[$i]['Grade'].'</span></div>';
				$outputli .= '<div style="width:78px;"><span>'.$this->_studentarr[$i]['Mean'].'</span></div>';
				$total = array_sum($this->_studentarr[$i]['Roles']);
				$outputli .= '<div style="width:78px;"><span>'.($total >= 999 ? '--' : $total).'</span></div>';
				$sholder = '<div style="width:78px;"><span></span></div>';
				foreach($this->_studentarr[$i]['Roles'] as $kroleid => $rolescores){
					switch($kroleid){
						case 1:
							$outputli .= '<div style="width:78px;"><span>'.($rolescores >= 999 ? '--' : $rolescores).'</span></div>';							
							break;
						case 2:
							if(!array_key_exists(1, $this->_studentarr[$i]['Roles'])){
								$outputli .= $sholder;
							}
							$outputli .= '<div style="width:78px;"><span>'.($rolescores >= 999 ? '--' : $rolescores).'</span></div>';							
							break;
						case 3:
							if(!array_key_exists(2, $this->_studentarr[$i]['Roles'])){
								if(!array_key_exists(1, $this->_studentarr[$i]['Roles'])){
									$outputli .= $sholder;	
								}
								$outputli .= $sholder;
							}
							$outputli .= '<div style="width:78px;"><span>'.($rolescores >= 999 ? '--' : $rolescores).'</span></div>';							
							break;
						case 4:
							if(!array_key_exists(3, $this->_studentarr[$i]['Roles'])){
								if(!array_key_exists(2, $this->_studentarr[$i]['Roles'])){
									if(!array_key_exists(1, $this->_studentarr[$i]['Roles'])){
										$outputli .= $sholder;	
									}
									$outputli .= $sholder;	
								}
								$outputli .= $sholder;
							}
							$outputli .= '<div style="width:78px;"><span>'.($rolescores >= 999 ? '--' : $rolescores).'</span></div>';							
							break;	
						default:
							if(!array_key_exists(4, $this->_studentarr[$i]['Roles'])){
								if(!array_key_exists(3, $this->_studentarr[$i]['Roles'])){
									if(!array_key_exists(2, $this->_studentarr[$i]['Roles'])){
										if(!array_key_exists(1, $this->_studentarr[$i]['Roles'])){
											$outputli .= $sholder;	
										}
										$outputli .= $sholder;	
									}
									$outputli .= $sholder;	
								}
								$outputli .= $sholder;
							}
							$outputli .= '<div style="width:78px;"><span>'.($rolescores >= 999 ? '--' : $rolescores).'</span></div>';	
					}
				}
				$outputli .= '</li>';
			}
			$outputli .= '</ul>';
			$outputtip = '<li>';
			$outputtip .= '<div style="width:16px;"><span><img src="../images/misc/ddt.gif" border="0" width="12" height="12" style="padding-top:3px;" /></span></div>';
			$outputtip .= '<div style="width:310px; vertical-align:top;"><span>: The assessment has been graded.</span></div>';
			$outputtip .= '</li>';
			$outputtip .= '<li>';
			$outputtip .= '<div style="width:16px;"><span><img src="../images/misc/ddk.gif" border="0" width="12" height="12" style="padding-top:3px;" /></span></div>';
			$outputtip .= '<div style="width:310px; vertical-align:top;"><span>: The assessment is currently being graded.</span></div>';
			$outputtip .= '</li>';	
			echo $outputopt.'|'.$outputli.'|'.$outputtip;
		}
		
		private function _initSectionliA(){
			$criteriaTitle = '';
			$sql = 'select CriteriaTitle from TBL_MultiCriteriaAssessment_Section where SectionID = '.$this->_sectionid;
			$this->_dao->execute($sql);
			if($this->_dao->getQuery()){
				if($row = $this->_dao->getRow()){
					$criteriaTitle = $row['CriteriaTitle'];	
				}	
			}
			$roleName = '';
			$sql = 'select CriticDetails from TBL_MultiCriteriaAssessment_CriticRole where CriticRoleID = '.$this->_roleid;
			$this->_dao->execute($sql);
			if($this->_dao->getQuery()){
				if($row = $this->_dao->getRow()){
					$roleName = $row[CriticDetails];	
				}	
			}
			$criticid = 0;
			$sql = 'select CriticID from TBL_MultiCriteriaAssessment_Critic where SectionID = '.$this->_sectionid.' and UserID = '.$this->_userid.' and CriticRoleID = '.$this->_roleid.' and Critic = '
					.$_SESSION['SessionUserID'];
			$this->_dao->execute($sql);
			if($this->_dao->getQuery()){
				if($row = $this->_dao->getRow()){
					$criticid = $row['CriticID'];	
				}	
			}
			
			$this->_initCriteria($this->_sectionid);
			$optpercent = 48;
			$this->_outputli .= '<ul>';
			$this->_outputli .= '<li style="background-color:#adeab2; font-weight:bold; padding-top:4px;" class="TableHeader">';	
			$this->_outputli .= '<div style="width:110px;"><span></span></div>';	
			$this->_outputli .= '<div style="width:40%;"><span style="text-align:left;">'.$criteriaTitle.'</span></div>';	
			$this->_outputli .= '<div style="width:'.$optpercent.'%;"><span style="text-align:left;">&ensp;&emsp;&emsp;'.$roleName.'</span></div>';	
			$this->_outputli .= '</li>';
			$isdraft = 0x2;
			for($i = 0; $i < count($this->_criteriaarr); $i++){
				if($i % 2 == 0){
					$this->_outputli .= '<li>';	
				}else{
					$this->_outputli .= '<li class="even rowAlternateColored">';	
				}	
				$this->_outputli .= '<div style="width:110px;"><span style="text-align:left;">'.$this->_criteriaarr[$i]['Criteria'].'</span></div>';	
				$this->_outputli .= '<div style="width:40%;"><span style="text-align:left;">'.$this->_criteriaarr[$i]['Details'].'</span></div>';
				$initscore = '';
				//retrieve db to detect weather there is a score already.
				$sql = 'select RatingItemID from TBL_MultiCriteriaAssessment_Results where CriteriaID = '.$this->_criteriaarr[$i]['CriteriaID'].' and SaveAsDraft = 0 and CriticID in (select CriticID from '.
						'TBL_MultiCriteriaAssessment_Critic where SectionID = '.$this->_sectionid.' and UserID = '.$this->_userid.' and CriticRoleID = '.$this->_roleid.')';
				$this->_dao->execute($sql);
				if($this->_dao->getQuery()){
					if($row = $this->_dao->getRow()){
						$isdraft |= 0x1;
						$initscore .= '<input type="hidden" value="'.$row['RatingItemID'].'" />';	
					}else{
						$isdraft &= 0x1;
						$sql = 'select RatingItemID, ResultID from TBL_MultiCriteriaAssessment_Results where CriteriaID = '.$this->_criteriaarr[$i]['CriteriaID'].' and SaveAsDraft = 1 and CriticID in (select CriticID from '
								.'TBL_MultiCriteriaAssessment_Critic where SectionID = '.$this->_sectionid.' and UserID = '.$this->_userid.' and CriticRoleID = '.$this->_roleid.' and Critic = '.$_SESSION['SessionUserID']
								.') order by ResultID desc';
						$this->_dao->execute($sql);
						if($this->_dao->getQuery()){
							if($row = $this->_dao->getRow()){
								$initscore .= '<input type="hidden" value="'.$row['RatingItemID'].'" />';	
							}else{
								$initscore .= '<input type="hidden" value="999" />';
							}
						}		
					}
				}
				
				$this->_outputli .= '<div style="width:'.$optpercent.'%;"><span style="text-align:left;" class="radio">&emsp;&emsp;';
				/*
				$this->_outputli .= '<div style="width:10%;"><span><select name="'.$this->_criteriaarr[$i]['Criteria'].'_'.$this->_criteriaarr[$i]['CriteriaID'].'_'.$criticid.'">'
									.'<option value="999" selected="selected">--</option>';	
				$this->_initRating($this->_criteriaarr[$i]['CriteriaID'], $this->_roleid);					
				foreach($this->_ratingarr as $ratingitem){
					$this->_outputli .= '<option value="'.$ratingitem['Rate'].'">'.$ratingitem['Rate'].'</option>';	
				}
				$this->_outputli .= '</select>'.$initscore.'</span></div>';											
				$this->_outputli .= '</li>';	*/	
				
				$this->_initRating($this->_criteriaarr[$i]['CriteriaID'], $this->_roleid);					
				foreach($this->_ratingarr as $ratingitem){
					$this->_outputli .= '<input type="radio" id="'.$this->_criteriaarr[$i]['Criteria'].'_'.$this->_criteriaarr[$i]['CriteriaID'].'_'.$criticid.'_'.$ratingitem['Rate'].
										'" name="'.$this->_criteriaarr[$i]['Criteria'].'_'.$this->_criteriaarr[$i]['CriteriaID'].'_'.$criticid.'" value="'.$ratingitem['Rate'].'" />';
					$this->_outputli .= '<label for="'.$this->_criteriaarr[$i]['Criteria'].'_'.$this->_criteriaarr[$i]['CriteriaID'].'_'.$criticid.'_'.$ratingitem['Rate'].'">'.$ratingitem['Rate'].'</label>&emsp;';
				}					
				$this->_outputli .= $initscore.'</span></div>';											
				$this->_outputli .= '</li>';	
			}
			$this->_outputli .= '<input type="hidden" class="isdraft" value="'.$isdraft.'" />';	
			$this->_outputli .= '</ul>';	
		}
		
		private function _initSectionliAAll($sectionid){
			$criteriaTitle = '';
			$sql = 'select CriteriaTitle from TBL_MultiCriteriaAssessment_Section where SectionID = '.$sectionid;
			$this->_dao->execute($sql);
			if($this->_dao->getQuery()){
				if($row = $this->_dao->getRow()){
					$criteriaTitle = $row['CriteriaTitle'];	
				}	
			}
			//init the role array belong to the certain section.
			$rolearr = array();
			$sql = 'select distinct R.CriticRoleID, R.CriticDetails from TBL_MultiCriteriaAssessment_CriticRole R, TBL_MultiCriteriaAssessment_Critic C where C.SectionID = '.$sectionid.' and C.UserID = '.$this->_userid
					.' and C.Critic = '.$_SESSION['SessionUserID'].' and C.CriticRoleID = R.CriticRoleID order by R.OrderID';
			$this->_dao->executei($sql);
			if($this->_dao->getQuery()){
				while($row = $this->_dao->getRow()){
					$rolearr[$row['CriticRoleID']] = $row['CriticDetails'];
				}	
			}
			//init the criteria array belong to the certain section.
			$criteriaarr = array();
			$sql = 'select CriteriaID, Criteria, Details from TBL_MultiCriteriaAssessment_Criteria where SectionID = '.$sectionid.' order by OrderID';
			$this->_dao->executei($sql);
			if($this->_dao->getQuery()){
				while($row = $this->_dao->getRow()){
					$criteriaarr[$row['CriteriaID']] = array('Criteria'=>$row['Criteria'], 'Details'=>$row['Details']);	
				}	
			}
			//init the criteria array belong to the certain section.
			/*
			$criteriaarr = array();
			$sql = 'select CriteriaID, OrderID, Weighting, Criteria, Details from TBL_MultiCriteriaAssessment_Criteria where SectionID = '.$sectionid;
			$this->_dao->executei($sql);
			if($this->_dao->getQuery()){
				while($row = $this->_dao->getRow()){
					$criteriaarr[] = $row;
				}	
			}
			$this->_sortby($criteriaarr, 'OrderID');	*/
			$this->_initCriteria($sectionid);
			//$optpercent = (50 / count($rolearr)) > 10 ? 10 : (50 / count($rolearr));
			$optpercent = 48 / count($rolearr);
			$this->_outputli .= '<ul>';
			$this->_outputli .= '<li style="background-color:#adeab2; font-weight:bold; padding-top:4px;" class="TableHeader">';	
			$this->_outputli .= '<div style="width:110px;"><span></span></div>';	
			$this->_outputli .= '<div style="width:40%;"><span style="text-align:left;">'.$criteriaTitle.'</span></div>';	
			foreach($rolearr as $roleid => $roledetail){
				$this->_outputli .= '<div style="width:'.$optpercent.'%;"><span style="text-align:left;">&ensp;&emsp;&emsp;'.$roledetail.'</span></div>';			
			}		
			$this->_outputli .= '</li>';
			$isdraft = 0x2;
			for($i = 0; $i < count($this->_criteriaarr); $i++){
				if($i % 2 == 0){
					$this->_outputli .= '<li>';	
				}else{
					$this->_outputli .= '<li class="even rowAlternateColored">';	
				}	
				$this->_outputli .= '<div style="width:110px;"><span style="text-align:left;">'.$this->_criteriaarr[$i]['Criteria'].'</span></div>';	
				$this->_outputli .= '<div style="width:40%;"><span style="text-align:left;">'.$this->_criteriaarr[$i]['Details'].'</span></div>';
				foreach($rolearr as $roleid => $roledetail){
					$initscore = '';
					//retrieve db to detect weather there is a score already.
					$sql = 'select RatingItemID from TBL_MultiCriteriaAssessment_Results where CriteriaID = '.$this->_criteriaarr[$i]['CriteriaID'].' and SaveAsDraft = 0 and CriticID in (select CriticID from '.
							'TBL_MultiCriteriaAssessment_Critic where SectionID = '.$sectionid.' and UserID = '.$this->_userid.' and CriticRoleID = '.$roleid.')';
					$this->_dao->execute($sql);
					if($this->_dao->getQuery()){
						if($row = $this->_dao->getRow()){
							$isdraft |= 0x1;
							$initscore .= '<input type="hidden" value="'.$row['RatingItemID'].'" />';	
							$initscore .= '<input type="hidden" value="1" />';
						}else{
							$isdraft &= 0x1;
							$sql = 'select RatingItemID, ResultID from TBL_MultiCriteriaAssessment_Results where CriteriaID = '.$this->_criteriaarr[$i]['CriteriaID'].' and SaveAsDraft = 1 and CriticID in ('
									.'select CriticID from TBL_MultiCriteriaAssessment_Critic where SectionID = '.$sectionid.' and UserID = '.$this->_userid.' and CriticRoleID = '.$roleid.' and Critic = '
									.$_SESSION['SessionUserID'].') order by ResultID desc';
							$this->_dao->execute($sql);
							if($this->_dao->getQuery()){
								if($row = $this->_dao->getRow()){
									$initscore .= '<input type="hidden" value="'.$row['RatingItemID'].'" />';	
								}else{
									$initscore .= '<input type="hidden" value="999" />';
								}
							}
							$initscore .= '<input type="hidden" value="0" />';		
						}
					}
					
					$criticid = 0;
					$sql = 'select CriticID from TBL_MultiCriteriaAssessment_Critic where SectionID = '.$sectionid.' and UserID = '.$this->_userid.' and CriticRoleID = '.$roleid.' and Critic = '.$_SESSION['SessionUserID'];
					$this->_dao->execute($sql);
					if($this->_dao->getQuery()){
						if($row = $this->_dao->getRow()){
							$criticid = $row['CriticID'];	
						}	
					}
					/*
					$this->_outputli .= '<div style="width:'.$optpercent.'%;"><span><select name="'.$this->_criteriaarr[$i]['Criteria'].'_'.$this->_criteriaarr[$i]['CriteriaID'].'_'.$criticid.'">'
										.'<option value="999" selected="selected">--</option>';	
					$this->_initRating($this->_criteriaarr[$i]['CriteriaID'], $roleid);					
					foreach($this->_ratingarr as $ratingitem){
						$this->_outputli .= '<option value="'.$ratingitem['Rate'].'">'.$ratingitem['Rate'].'</option>';		
					}
					$this->_outputli .= '</select>'.$initscore.'</span></div>';		*/
					$this->_outputli .= '<div style="width:'.$optpercent.'%;"><span style="text-align:left;" class="radio">&emsp;&emsp;';
					$this->_initRating($this->_criteriaarr[$i]['CriteriaID'], $roleid);					
					foreach($this->_ratingarr as $ratingitem){
						$this->_outputli .= '<input type="radio" id="'.$this->_criteriaarr[$i]['Criteria'].'_'.$this->_criteriaarr[$i]['CriteriaID'].'_'.$criticid.'_'.$ratingitem['Rate'].
											'" name="'.$this->_criteriaarr[$i]['Criteria'].'_'.$this->_criteriaarr[$i]['CriteriaID'].'_'.$criticid.'" value="'.$ratingitem['Rate'].'" />';
						$this->_outputli .= '<label for="'.$this->_criteriaarr[$i]['Criteria'].'_'.$this->_criteriaarr[$i]['CriteriaID'].'_'.$criticid.'_'.$ratingitem['Rate'].'">'.$ratingitem['Rate'].'</label>&emsp;';
					}					
					$this->_outputli .= $initscore.'</span></div>';												
				}
														
				$this->_outputli .= '</li>';						
			}
			$this->_outputli .= '<input type="hidden" class="isdraft" value="'.$isdraft.'" />';	
			$this->_outputli .= '</ul>';
		}
		
		private function _initSectionliR($sectionid){
			$criteriaTitle = '';
			$sql = 'select CriteriaTitle from TBL_MultiCriteriaAssessment_Section where SectionID = '.$sectionid;
			$this->_dao->execute($sql);
			if($this->_dao->getQuery()){
				if($row = $this->_dao->getRow()){
					$criteriaTitle = $row['CriteriaTitle'];	
				}	
			}
			$scoreR = array();
			$scoreRT = array();
			$marksarr = array();
			//init the role array belong to the certain section.
			$rolearr = array();
			$sql = 'select distinct R.CriticRoleID, R.CriticDetails, R.Weighting from TBL_MultiCriteriaAssessment_CriticRole R, TBL_MultiCriteriaAssessment_Critic C where R.CriticRoleID = C.CriticRoleID and C.SectionID = '
					.$sectionid.' and C.UserID = '.$this->_userid.' order by R.OrderID';
			$this->_dao->executei($sql);
			if($this->_dao->getQuery()){
				while($row = $this->_dao->getRow()){
					$rolearr[$row['CriticRoleID']] = array('CriticDetails'=>$row['CriticDetails'], 'Weighting'=>$row['Weighting']);
				}	
			}
			//init the criteria array belong to the certain section.
			$criteriaarr = array();
			$sql = 'select CriteriaID, OrderID, Weighting, Criteria, Details from TBL_MultiCriteriaAssessment_Criteria where SectionID = '.$sectionid.' order by OrderID';
			$this->_dao->executei($sql);
			if($this->_dao->getQuery()){
				while($row = $this->_dao->getRow()){
					$criteriaarr[$row['CriteriaID']] = array('Details'=>$row['Details'], 'Weighting'=>$row['Weighting'], 'Criteria'=>$row['Criteria']);	
				}	
			}
			//loop roles and criteron to retrieve scores of student.
			foreach($rolearr as $keyroleid => $valuerole){
				if(!array_key_exists($keyroleid, $marksarr)){
					$marksarr[$keyroleid] = array();	
				}
				if(!array_key_exists($keyroleid, $scoreR)){
					$scoreR[$keyroleid] = 0;	
				}
				if(!array_key_exists($keyroleid, $scoreRT)){
					$scoreRT[$keyroleid] = 0;	
				}	
				foreach($criteriaarr as $keycriteriaid => $valuecriteria){
					$sql = 'select RatingItemID, count(*) as Submit from TBL_MultiCriteriaAssessment_Results where CriteriaID = '.$keycriteriaid.' and SaveAsDraft = 0 and CriticID in (select CriticID from '.
							'TBL_MultiCriteriaAssessment_Critic where SectionID = '.$sectionid.' and UserID = '.$this->_userid.' and CriticRoleID = '.$keyroleid.')';	
					$this->_dao->executei($sql);
					if($this->_dao->getQuery()){
						if($row = $this->_dao->getRow()){
							$marksarr[$keyroleid][$keycriteriaid] = $row['Submit'] > 0 ? $row['RatingItemID'] : 999;
						}	
					}
					
					$sql = 'select MAX(RI.Rate) as Mark from TBL_MultiCriteriaAssessment_RatingItems RI, TBL_MultiCriteriaAssessment_Criteria_Role_Rating CRR where RI.RatingID = CRR.RatingID and CRR.CriteriaID = '
							.$keycriteriaid.' and CriticRoleID = '.$keyroleid;	
					$this->_dao->executei($sql);
					if($this->_dao->getQuery()){
						if($row = $this->_dao->getRow()){
							$scoreRT[$keyroleid] += $row['Mark'] * $criteriaarr[$keycriteriaid]['Weighting'];
						}	
					}	
				}
			}
			foreach($marksarr as $keyroleid => $criteron){
				foreach($criteron as $keycriteriaid => $valuescore){
					$scoreR[$keyroleid]	+= $valuescore * $criteriaarr[$keycriteriaid]['Weighting'];
				}
				$this->_score[$keyroleid] = $scoreR[$keyroleid];
				$this->_percentage[$keyroleid] = (($this->_score[$keyroleid] * 100) / $scoreRT[$keyroleid]) * $rolearr[$keyroleid]['Weighting'];
				$scoreR[$keyroleid] = $scoreR[$keyroleid] >= 999 ? '--' : $scoreR[$keyroleid];	
			}
			
			//construct the html part.
			$optpercent = (50 / count($rolearr)) > 10 ? 10 : (50 / count($rolearr));
			$this->_outputli .= '<ul>';
			$this->_outputli .= '<li style="background-color:#adeab2; font-weight:bold; padding-top:4px;" class="TableHeader">';	
			$this->_outputli .= '<div style="width:110px;"><span></span></div>';	
			$this->_outputli .= '<div style="width:40%;"><span style="text-align:left;">'.$criteriaTitle.'</span></div>';
			foreach($rolearr as $role){
				$this->_outputli .= '<div style="width:'.$optpercent.'%;"><span>'.$role['CriticDetails'].'</span></div>';			
			}		
			$this->_outputli .= '</li>';
			
			$i = 0;
			foreach($criteriaarr as $criteriaid => $criteriarow){
				if($i % 2 == 0){
					$this->_outputli .= '<li>';	
				}else{
					$this->_outputli .= '<li class="even rowAlternateColored">';	
				}	
				$i++;
				$this->_outputli .= '<div style="width:110px;"><span style="text-align:left;">'.$criteriarow['Criteria'].'</span></div>';	
				$this->_outputli .= '<div style="width:40%;"><span style="text-align:left;">'.$criteriarow['Details'].'</span></div>';
				foreach($rolearr as $roleid => $role){
					$this->_outputli .= '<div style="width:'.$optpercent.'%;"><span>'.($marksarr[$roleid][$criteriaid] == 999 ? '--' : $marksarr[$roleid][$criteriaid])
									 .'</span></div>';			
				}											
				$this->_outputli .= '</li>';							
			}
			if($i % 2 == 0){
				$this->_outputli .= '<li>';	
			}else{
				$this->_outputli .= '<li class="even rowAlternateColored">';	
			}	
			$this->_outputli .= '<div style="width:110px;"><span style="text-align:left;"></span></div>';	
			$this->_outputli .= '<div style="width:40%;"><span style="text-align:right;">SUB-TOTAL</span></div>';
			foreach($rolearr as $roleid => $role){
				$this->_outputli .= '<div style="width:'.$optpercent.'%;"><span>'.$scoreR[$roleid].'</span></div>';			
			}											
			$this->_outputli .= '</li>';
			$this->_outputli .= '</ul>';
		}
		
		public function listSection1By1A(){
			if($this->_sectionid == 0){
				//init the section array and construct html of each section.
				$sectionarr = array();
				$sql = 'select distinct S.SectionID from TBL_MultiCriteriaAssessment_Section S, TBL_MultiCriteriaAssessment_Critic C where S.AssessmentID = '.$this->_asmtid.' and C.UserID = '.$this->_userid
						.' and C.Critic = '.$_SESSION['SessionUserID'].' and S.SectionID = C.SectionID order by OrderID';
				$this->_dao->executei($sql);
				if($this->_dao->getQuery()){
					while($row = $this->_dao->getRow()){
						$sectionarr[$row['SectionID']] = $row['SectionID'];
					}	
				}				
				
				foreach($sectionarr as $sectionid){
					$this->_initSectionliAAll($sectionid);	
					$this->_outputli .= '<br /><br />';
				}	
			}else{
				$this->_initSectionliA();	
				$this->_outputli .= '<br /><br />';		
			}
			
			echo $this->_outputli.'|'.$this->_ratingli;
		}
		
		private function _initRating4Report(){
			$output = '';
			$ratingarr = array();
			$rolearr = array();
			$sql = 'select RI.RatingID, RI.Rate, RI.Details from TBL_MultiCriteriaAssessment_RatingItems RI where RatingID in (select RatingID from TBL_MultiCriteriaAssessment_Rating R where AssessmentID = '
					.$this->_asmtid.') order by RI.RatingID, RI.OrderID';
			$this->_dao->executei($sql);
			if($this->_dao->getQuery()){
				while($row = $this->_dao->getRow()){
					if(!array_key_exists($row['RatingID'], $ratingarr)){
						$ratingarr[$row['RatingID']] = array();	
					}
					$ratingarr[$row['RatingID']][] = $row;
				}	
			}	
			$sql = 'select distinct CRR.RatingID, CR.CriticDetails from TBL_MultiCriteriaAssessment_CriticRole CR, TBL_MultiCriteriaAssessment_Criteria_Role_Rating CRR where CR.CriticRoleID = CRR.CriticRoleID and '
					.'CRR.RatingID in (select RatingID from TBL_MultiCriteriaAssessment_Rating R where AssessmentID = '.$this->_asmtid.') and CRR.CriticRoleID in (select CriticRoleID from '
					.'TBL_MultiCriteriaAssessment_Critic C, TBL_MultiCriteriaAssessment_Section S where C.UserID = '.$this->_userid.' and C.SectionID = S.SectionID and S.AssessmentID = '.$this->_asmtid.') '
					.'order by CRR.RatingID, CR.SectionID, CR.OrderID';
			$this->_dao->executei($sql);
			if($this->_dao->getQuery()){
				while($row = $this->_dao->getRow()){
					if(!array_key_exists($row['RatingID'], $rolearr)){
						$rolearr[$row['RatingID']] = array();	
					}
					$rolearr[$row['RatingID']][] = $row['CriticDetails'];
				}	
			}

			//construct the every rating in the current assessment.
			$indexarr = array_keys($rolearr);
			for($i = 0; $i < count($indexarr); $i++){
				$output .= $i == 0 ? '<div class="panel" style="width:332px;">' : '<div class="panel" style="width:332px; margin-left:13px;">';
				$output .= '<span class="rubric">';
				$output .= 'Rating for '.implode(', ', $rolearr[$indexarr[$i]]).':';			
				$output .= '</span>';			
				$output .= '<ul>';
				for($j = 0; $j < count($ratingarr[$indexarr[$i]]); $j++){
					$output .= '<li>';						
					$output .= '<div style="width:20px; vertical-align:top;"><span>'.$ratingarr[$indexarr[$i]][$j]['Rate'].': </span></div>';
					$output .= '<div style="width:310px;"><span>'.$ratingarr[$indexarr[$i]][$j]['Details'].'</span></div>';
					$output .= '</li>';	
				}
				$output .= '</ul>';
				$output .= '</div>';		
			}
			
			return $output;	
		}
		
		public function listSection1By1R(){
			//init the section array and construct html of each section.
			$sectionarr = array();
			$sql = 'select distinct S.SectionID from TBL_MultiCriteriaAssessment_Section S, TBL_MultiCriteriaAssessment_Critic C where S.AssessmentID = '.$this->_asmtid.' and C.UserID = '.$this->_userid
					.' and S.SectionID = C.SectionID order by OrderID';
			$this->_dao->executei($sql);
			if($this->_dao->getQuery()){
				while($row = $this->_dao->getRow()){
					$sectionarr[$row['SectionID']] = $row['SectionID'];
				}	
			}
			foreach($sectionarr as $sectionid){
				$this->_initSectionliR($sectionid);	
				$this->_outputli .= '<br /><br />';
			}
			//construct the grade result according to the scores each critic given.
			$total = 0;
			foreach($this->_score as $score){
				$total += $score;	
			}
			$total = $total < 999 ? $total : '--';
			$percentage = 0;
			foreach($this->_percentage as $percent){
				$percentage += $percent;	
			}
			
			$percentage = $percentage > 100 ? '--' : sprintf('%.2f', $percentage).'%';
			$grade = '--';
			$sqlG = 'select Grade, count(*) as Number from TBL_MultiCriteriaAssessment_Grade G where AssessmentID = '.$this->_asmtid.' and G.PercentageFrom <= '.$percentage.' and G.PercentageTo > '.$percentage;
			$rsG = mysql_query($sqlG);		
			if($rsG){
				if($rowG = mysql_fetch_array($rsG)){
					$grade = $rowG['Number'] > 0 ? $rowG['Grade'] : '--';
				}	
			}
			$this->_outputli .= '<ul style="width:100%;">';
			$this->_outputli .= '<li style="display:none;;"></li>';
			$this->_outputli .= '<li>';
			$this->_outputli .= '<div style="width:100px;"><span style="text-align:left;">Total</span></div>';
			$this->_outputli .= '<div style="width:80px;"><span style="text-align:left; font-weight:bold;">'.$total.'</span></div>';	
			$this->_outputli .= '</li>';
			$this->_outputli .= '<li>';
			$this->_outputli .= '<div style="width:100px;"><span style="text-align:left;">Percentage</span></div>';
			$this->_outputli .= '<div style="width:80px;"><span style="text-align:left; font-weight:bold;">'.$percentage.'</span></div>';	
			$this->_outputli .= '</li>';
			$this->_outputli .= '<li>';
			$this->_outputli .= '<div style="width:100px;"><span style="text-align:left;">Grade</span></div>';
			$this->_outputli .= '<div style="width:80px;"><span style="text-align:left; font-weight:bold;">'.$grade.'</span></div>';	
			$this->_outputli .= '</li>';	
			$this->_outputli .= '</ul>';		
			//construct the comment panel with rating of each role.
			$this->_outputli .= '|';			
			$this->_outputli .= $this->_initRating4Report();
			echo $this->_outputli;
		}
		
		public function display(){
			require_once('./view/sectionlists.php');		
		}
	}
?>