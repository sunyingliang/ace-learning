<?php
	require_once('./controller/controller.php');
	
	class AssessmentController extends Controller{
		private $_usertype = 2;
		private $_sysfolder = '';
		private $_outputli = '';
		
		public function __construct(){
			$this->_dao = new DataAccess();
			$this->_usertype = $_SESSION['SessionUserTypeID'];
		}
		
		public function getModelArr(){
			return $this->_modelArr;	
		}
		
		public function lists($sysfolder){
			$this->_sysfolder = $sysfolder;
			$sql = 'select AssessmentID, Title, StartDate, EndDate from TBL_MultiCriteriaAssessment where SchoolID = "'.$_SESSION['SessionSchoolID'].'"';
			$this->_dao->execute($sql);
			if($this->_dao->getQuery()){
				$i = 1;	
				while($row = $this->_dao->getRow()){
					if($i % 2 == 0){
						$this->_outputli .= '<li class="even">';					
					}else{
						$this->_outputli .= '<li>';						
					}
					$this->_outputli .= '<span class="asmt-title"><a href="javascript:void(0);" asid="'.$row['AssessmentID'].'">'.$row['Title'].'</a></span>';
					$this->_outputli .= '<span class="asmt-dt">'.$row['StartDate'].'</span>';
					$this->_outputli .= '<span class="asmt-dt">'.$row['EndDate'].'</span>';
					$this->_outputli .= '</li>';
					$i++;						
				}
			}
			
			$this->display();
		}	
		
		public function display(){
			require_once('./view/assessmentlists.php');		
		}
	}
?>