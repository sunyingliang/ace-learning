<?php
	abstract class Controller{
		protected $_dao;
		abstract public function display();
		
		private function getorignalstring($pstr){
			$match = array();
			$res = preg_match('/(\w+)/', $pstr, $match);
			return $res != 0 ? $match[1] : '';	
		}
		
		private function string2array($pstr){
			$spos = strpos($pstr, '{');	
			$epos = strpos($pstr, '}');	
			$str = substr($pstr, $spos + 1, $epos - $spos - 1);
			
			$rearr = array();
			$paramarr = explode(',', $str);
			foreach($paramarr as $param){
				$itemarr = explode(':', $param);	
				$rearr[$this->getorignalstring($itemarr[0])] = $this->getorignalstring($itemarr[1]);
			}
			
			return $rearr;
		}
		
		private function object2array(&$obj){
			if(is_object($obj)){
				$obj = get_object_vars($obj);	
			}	
			if(is_array($obj)){
				return $obj;
			}else{
				return false;	
			}
		}
		public function getParamArray($jsonstr){
			$pobj = json_decode($jsonstr);
			if($pobj == NULL){
				return $this->string2array($jsonstr);			
			}else{
				return $this->object2array($pobj);	
			}
		}
	}
?>