//retrieve all classes the teacher can critic.
function initClasses(){
	var _tab = $('#tabs').attr('current');
	var _url = './index.php';
	var _a = _tab == 'Assessment' ? 'listclassesa' : 'listclassesr';
	var _data = {
		'c':'section',
		'a':_a,
		'p':JSON.stringify({'assessmentid':$("#asmtid").val(), 'sectionid':$("#sec_selector").val(), 'roleid':$("#critic_selector").val()})	
	};	
	if(_tab != 'Assessment'){
		_data.p = JSON.stringify({'assessmentid':$("#asmtid").val(), 'sectionid':$("#sec_selector").val()});
	}
	
	var _datatype = 'html';
	var _callback = _tab == 'Assessment' ? cb_initclasses_a : cb_initclasses_r;
	xhr(_url, _data, _datatype, _callback);
}
function cb_initclasses_a(data, xhr, status){
	var htmlel = data.split('|');
	$('#cwd').val('freshClass|freshClass');
	$("#cls_selector").html(htmlel[0]).unbind().change(function(){
		freshClass();
	});
	$('#commit').length > 0 && $('#commit').remove();
	$('#comment').length > 0 && $('#comment').remove();
	$('#show').html(htmlel[1]);
	initShow();
	$('#show ul li span a').click(function(){
		var me = $(this);
		$('#cls_selector option').each(function(){
			me.attr('classid') == $(this).val() && $(this).prop('selected', true);	
		});
		$("#nav_student").removeClass("hidden");
		initStudents();	
	});	
	chkempty();
}
function cb_initclasses_r(data, xhr, status){
	var htmlel = data.split('|');
	$('#cwd').val('freshClass|freshClass');
	$("#cls_selector").html(htmlel[0]).unbind().change(function(){
		freshClass();
	});
	$('#commit').length > 0 && $('#commit').remove();
	$('#comment').length > 0 && $('#comment').remove();
	$('#show').html(htmlel[1]);
	initShow();	
	$('#show ul li span a').click(function(){
		var me = $(this);
		$('#cls_selector option').each(function(){
			me.attr('classid') == $(this).val() && $(this).prop('selected', true);	
		});
		$("#nav_student").removeClass("hidden");
		initStudents();	
	});	
	chkempty();
}


//retrieve all students belong to the selected class.
function initStudents(){
	var _tab = $('#tabs').attr('current');
	var _url = './index.php';
	var _a = _tab == 'Assessment' ? 'liststudentsa' : 'liststudentsr';
	var _data = {
		'c':'section',
		'a':_a,
		'p':JSON.stringify({'assessmentid':$("#asmtid").val(), 'sectionid':$("#sec_selector").val(), 'roleid':$("#critic_selector").val(), 'classid':$('#cls_selector').val()})
	};	
	if(_tab != 'Assessment'){
		_data.p = JSON.stringify({'assessmentid':$("#asmtid").val(), 'sectionid':$("#sec_selector").val(), 'classid':$('#cls_selector').val()});
	}
	
	var _datatype = 'html';
	var _callback = _tab == 'Assessment' ? cb_initstudents_a : cb_initstudents_r;
	xhr(_url, _data, _datatype, _callback);
}
function cb_initstudents_a(data, xhr, status){
	var htmlel = data.split('|');
	$('#cwd').val('freshStudent|freshStudent');
	$('#stu_selector').html(htmlel[0]).unbind().change(function(){
		freshStudent()
	});
	initComment('Legend', 'width:332px;', htmlel[2]);
	$('#commit').length > 0 && $('#commit').remove();
	$('#show').html(htmlel[1]);
	initShow();
	$('#show ul li span a').click(function(){
		var me = $(this);
		$('#stu_selector option').each(function(){
			me.attr('userid') == $(this).val() && $(this).prop('selected', true);				
		});
		listSection1by1();	
	});
	chkempty();
}
function cb_initstudents_r(data, xhr, status){
	var htmlel = data.split('|');
	$('#cwd').val('freshStudent|freshStudent');
	$('#stu_selector').html(htmlel[0]);
	initComment('Legend', 'width:332px;', htmlel[2]);
	$('#commit').length > 0 && $('#commit').remove();
	$('#show').html(htmlel[1]);
	initShow();
	$("#stu_selector").unbind().change(function(){
		freshStudent();
	});	
	$('#show ul li span a').click(function(){
		var me = $(this);
		$('#stu_selector option').each(function(){
			me.attr('userid') == $(this).val() && $(this).prop('selected', true);	
		});
		listSection1by1();	
	});
	chkempty();
}

//list the sesction to be assessed 1 by 1, and fresh the show panel.
function listSection1by1(){
	var _tab = $('#tabs').attr('current');
	var _url = './index.php';
	var _a = _tab == 'Assessment' ? 'listsection1by1a' : 'listsection1by1r';
	var _data = {
		'c':'section',
		'a':_a,
		'p':JSON.stringify({'assessmentid':$("#asmtid").val(), 'sectionid':$("#sec_selector").val(), 'roleid':$("#critic_selector").val(), 'userid':$('#stu_selector').val()})
	};	
	if(_tab != 'Assessment'){
		_data.p = JSON.stringify({'assessmentid':$("#asmtid").val(), 'userid':$('#stu_selector').val()});
	}
	
	var _datatype = 'html';
	var _callback = _tab == 'Assessment' ? cb_listsection1by1_a : cb_listsection1by1_r;
	xhr(_url, _data, _datatype, _callback);
}
function cb_listsection1by1_a(data, xhr, status){
	var htmlel = data.split('|');
	$('#cwd').val('listSection1by1|initStudents');
	initComment('Rating', 'width:332px;', htmlel[1]);
	$('#show').html(htmlel[0]);
	initCommit();
	initShow();	
	enableSubmit();
	initCriteron();	
	chkempty();
}
function cb_listsection1by1_r(data, xhr, status){
	var htmlel = data.split('|');
	$('#cwd').val('listSection1by1|initStudents');
	initComment('RFR', htmlel[1]);
	$('#show').html(htmlel[0]);
	initShow();
	chkempty();	
}

//fresh current page by the section selected.
function freshSection(){
	if($('#sec_selector').val() == 0){	
		$("#nav_critic, #nav_student").addClass("hidden");
		$("#view_selector option:eq(0)").attr('value') == 0 && $("#view_selector option:eq(0)").remove();
		$("#view_selector option:eq(0)").prop('selected', true);
		$("#nav_class").removeClass("hidden");	
		$("#cls_selector option:eq(0)").prop('selected', true);
		initClasses();	
	}else{
		$("#nav_section, #nav_critic, #nav_view").removeClass("hidden");
		$("#view_selector option:eq(0)").attr('value') == 1 && $("#view_selector").prepend($('<option value="0">All in one page</option>'));
		$("#view_selector option:eq(0)").prop('selected', true);
		$("#nav_class, #nav_student").addClass("hidden");	
		
		var _tab = $('#tabs').attr('current');
		var _url = './index.php';
		var _a = _tab == 'Assessment' ? 'freshsectiona' : 'freshsectionr';
		var _data = {
			'c':'section',
			'a':_a,
			'p':JSON.stringify({'assessmentid':$("#asmtid").val(), 'sectionid':$("#sec_selector").val()})	
		};	
		var _datatype = 'html';
		var _callback = _tab == 'Assessment' ? cb_freshsection_a : cb_freshsection_r;
		xhr(_url, _data, _datatype, _callback);
	}
}
function cb_freshsection_a(data, xhr, status){
	var htmlel = data.split('|');
	$('#cwd').val('freshCritic|freshCritic');
	$('#critic_selector').html(htmlel[0]).unbind().change(function(){
		freshCritic();
	}).find('option:eq(0)').prop('selected', true);
	$('#cls_selector').html(htmlel[2]).unbind().change(function(){
		freshClass();
	}).find('option:eq(0)').prop('selected', true);
	initComment(htmlel[3], htmlel[4]);
	$('#show').html(htmlel[1]);
	initCommit();
	initShow();
	enableSubmit();
	initCriteron();	
	chkempty();	
}
function cb_freshsection_r(data, xhr, status){
	var htmlel = data.split('|');
	$('#cwd').val('freshCritic|freshCritic');
	$("#nav_section, #nav_view").addClass("hidden");
	$("#nav_class").removeClass("hidden");
	$('#cls_selector').html(htmlel[0]);
	$('#commit').length > 0 && $('#commit').remove();
	$('#comment').length > 0 && $('#comment').remove();
	$('#show').html(htmlel[1]);
	initShow();	
	$("#cls_selector").unbind().change(function(){
		if($(this).val() == 0){
			$("#nav_student").addClass("hidden");
			initClasses();	
		}else{
			$("#nav_student").removeClass("hidden");
			initStudents();	
		}	
	});
	$('#show ul li span a').click(function(){
		var me = $(this);
		$('#cls_selector option').each(function(){
			me.attr('classid') == $(this).val() && $(this).prop('selected', true);	
		});
		$("#nav_student").removeClass("hidden");
		initStudents();	
	});	
	chkempty();
}

function freshCritic(){
	var _url = './index.php';
	var _data = {
		'c':'section',
		'a':'freshcritic',
		'p':JSON.stringify({'assessmentid':$("#asmtid").val(), 'sectionid':$("#sec_selector").val(), 'roleid':$("#critic_selector").val()})	
	};	
	var _datatype = 'html';
	var _callback = cb_freshcritic;
	xhr(_url, _data, _datatype, _callback);
}
function cb_freshcritic(data, xhr, status){
	var htmlel = data.split('|');
	$('#cwd').val('freshCritic|freshCritic');
	$("#nav_section, #nav_view").removeClass("hidden");
	$("#view_selector option:eq(0)").attr('value') == 1 && $("#view_selector").prepend($('<option value="0">All in one page</option>'));
	$("#view_selector option:eq(0)").prop('selected', true);
	$("#nav_class, #nav_student").addClass("hidden");
	$('#cls_selector').html(htmlel[1]).unbind().change(function(){
		freshClass();
	}).find('option:eq(0)').prop('selected', true);
	initComment(htmlel[2], htmlel[3]);
	$('#show').html(htmlel[0]);
	initCommit();
	initShow();
	enableSubmit();
	initCriteron();	
	chkempty();	
}

function freshClass(){
	if($('#cls_selector').val() == 0){
		$("#nav_student").addClass("hidden");
		initClasses();	
	}else{
		$("#nav_student").removeClass("hidden");
		initStudents();	
	}	
}

function freshStudent(){
	if($('#stu_selector').val() == 0){
		initStudents();
	}else{
		listSection1by1();	
	}	
}

function freshTab(){
	var _tab = $('#tabs').attr('current');
	if(_tab == 'Assessment'){
		$("#sec_selector option:eq(0)").attr('value') == 0 ? $("#sec_selector option:eq(1)").prop('selected', true) : $("#sec_selector option:eq(0)").prop('selected', true);
		freshSection();
	}else{
		$("#nav_section, #nav_critic, #nav_view, #nav_student").addClass('hidden');
		$("#nav_class").removeClass('hidden');
		$("#view_selector option:eq(0)").prop('selected', true);	
		initClasses();
	}
}

function initNav(){
	//init sec_selector and its event.
	var sectionopts = $("#sec_selector option");
	sectionopts.length < 3 && $("#sec_selector option:eq(0)").remove();
	sectionopts.length == 1 ? $("#sec_selector option:eq(0)").prop('selected', true) : $("#sec_selector option:eq(1)").prop('selected', true);
	$("#sec_selector").change(function(){
		freshSection();
	});
	$("#critic_selector").change(function(){
		freshCritic();
	});
	//init view_selector and its event.
	$("#view_selector option:eq(0)").attr('selected', true);	
	$("#view_selector").change(function(){
		if($(this).val() == 1){
			$("#nav_class").removeClass("hidden");
			$("#cls_selector option:eq(0)").prop('selected', true);
			initClasses();	
		}else{
			$("#nav_student").addClass("hidden");	
			$("#nav_class").addClass("hidden");	
			$("#sec_selector").val() == 0 ? freshSection() : freshCritic();	
		}
	});	
}

function initShow(){
	$("#show ul").each(function(){
		$(this).find("li:gt(0)").each(function(){
			$(this).mouseover(function(){
				$(this).css('background-color', '#cedfff');	
			}).mouseout(function(){
				$(this).hasClass('even') ? $(this).css('background-color', '#f1f1f1') : $(this).css('background-color', '#ffffff');		
			});
			
			if($(this).find('.radio').length > 0){
				$(this).find('.radio').each(function(){
					var $gradio = $(this), svalue = $(this).find(':hidden :first').val();
					$(this).find(':radio').each(function(){
						$(this).val() == svalue && $(this).prop('checked', true);		
						($gradio.find(':hidden').length == 2) && ($gradio.find(':hidden :last').val() == 1) && $(this).prop('disabled', true);		
					});	
				});	
			}else{
				$(this).find('select').each(function(){
					var me = $(this);	
					me.find('option').each(function(){
						//$(this).val() == me.next().val() && $(this).prop('selected', true);	//for style1.
						/*
						if($(this).val() == me.next().val()){		
							$(this).prop('selected', true);
						}else if(me.next().next().val() == 1){
							$(this).prop('disabled', true);
						}	*/	//for style2.
						
						if($(this).val() == me.next().val()){	//for style2.
							$(this).prop('selected', true);
						}else if($('#useragent').val() == 'android-safari'){
							if(me.next().next().val() == 1 && me.val() != 999){
								$(this).val() == 999 ? $(this).remove() : $(this).prop('disabled', true);
							}
						} 
					});
					$('#useragent').val() != 'android-safari' && me.next().next().val() == 1 && me.val() != 999 && me.prop('disabled', true);	//for style0 and style2.
				});	
			}	
		});	
	});	
}

function initTab(){
	$("#tabs").tabs();
	$("#tabs ul li a").click(function(){
		$('#tabs').attr('current', $(this).html());
		freshTab();
	});	
}

function initscroll(){
	$('#wrapper').scroll(function(e){
		var distance = 0;
		if($('#cwd').val() == 'freshCritic|freshCritic'){
			distance = 580;
		}else if($('#cwd').val() == 'freshStudent|freshStudent'){
			distance = 350;
		}
		if(distance != 0){			
			if($('#wrapper').scrollTop() > distance){
				var $dnmheader = $('#dtableheader');
				if($dnmheader.length == 0){
					$dnmheader = $('<div id="dtableheader" class="TableHeader" style="background-color:#adeab2; font-weight:bold; padding-top:4px;"></div>').html($('.TableHeader').html())
								.css({'position':'absolute'}).prependTo('#show');
				}
				$dnmheader.stop().animate({'left':'0px', 'top':parseInt($('#wrapper').scrollTop() - 72, 10) + 'px'}, 'slow');
			}else{
				$('#dtableheader') && $('#dtableheader').remove();	
			}		
		}
	});
}

function chkempty(){
	var $hasrd = $("#show ul .isdraft");
	if($hasrd.length > 0 && $hasrd.val() == 2){
		$('#nav_section, #nav_critic, #nav_view').addClass('hidden');
		var $addedli = $('.TableHeader').clone().html('').css({'background-color':'#ffffff', 'text-align':'center'});
		$addedli.clone().appendTo('#show ul');
		$addedli.html('No Assessment Found.').appendTo('#show ul');
		$('#commit, #comment') && $('#commit, #comment').remove();
	}
}

$(document).ready(function(){
	initTab();
	initNav();	
	initShow();
	initCommit();
	enableSubmit();
	initCriteron();
	initscroll();
	chkempty();
});