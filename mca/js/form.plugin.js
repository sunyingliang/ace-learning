function Form($container){
	var $_form = $('<form></form>').appendTo($container || $('body'));	
	this.setAction = function(url){
		$_form.attr('action', url);	
	};
	this.setMethod = function(method){
		$_form.attr('method', method);		
	};
	this.setEnctype = function(enc){
		if(!enc){
			$_form.attr('enctype', 'application/x-www-form-encoded');
		}else{
			$_form.attr('enctype', 'multipart/form-data');
		}
	};
	this.addElement = function(name, value){
		$_form.append($('<input type="hidden" name="' + name + '" value="" />').val(value));	
	};
	this.submitForm = function(){
		$_form.submit();	
		$_form.remove();
	};
}

/**
*@Utilities for #show events' attachment, #commit panel and #comment panel.
 **/
//init #commit panel with events attachment, and #comment panel. 
function _initCommit(){
	$('#commit input').click(function(){
		if($(this).attr('id') == 'reset'){
			$('#show ul').each(function(){
				var me = $(this);	
				if(me.find('li .radio').length > 0){
					me.find('li .radio').each(function(){
						var $hidden = $(this).find(':hidden');	
						if($hidden.length == 1 || $(this).find(':hidden :last').val() == 0){
							$(this).find(':radio').each(function(){
								$(this).prop('checked', false);	
							});	
						}
					});
				}else{
					me.find('li select').each(function(){
						!$(this).prop('disabled') && $(this).find('option:eq(0)').prop('selected', true);			
					});		
				}
			});
			checkCriteron();
		}else{
			var _url = './index.php';
			var param = {};
			if($('#show ul li .radio').length > 0){
				$('#show ul li .radio').each(function(){
					$(this).children(':checked').each(function(){
						param[$(this).attr('name')] = $(this).val();			
					});
						
				});					
			}else{
				$('#show ul li select').each(function(){
					//($(this).find('option:disabled').length == 0) && (param[$(this).attr('name')] = $(this).val());		//for style2.
					//param[$(this).attr('name')] = $(this).val();	//for style1.
					($('#useragent').val() != 'android-safari') ? (!$(this).prop('disabled') && $(this).val() != 999 && (param[$(this).attr('name')] = $(this).val())) : (($(this).find('option:disabled').length == 0) && $(this).val() != 999 && (param[$(this).attr('name')] = $(this).val()));
				});
			}
			
			param.type = $(this).attr('id') == 'draft' ? 1 : 0;
			param.title = $('#header').html();
			var _data = {
				'c': 'critic',
				'a': 'save',
				'p': JSON.stringify(param)	
			};
			var _datatype = '';
			var _callback = showResult;
			xhr(_url, _data, _datatype, _callback);
		}	
	});	
}
function initCommit(){
	var $commit = $('#commit');
	if($commit.length == 0){
		$('<div id="commit"></div>').insertAfter($('#show'));
	}
	$commit = $('#commit').empty();	
	$('<div id="btn_draft"></div>').addClass('btn-commit').appendTo($commit);
	$('<input type="button" id="draft" value="Save As Draft" />').appendTo($('#btn_draft'));	
	$('<div id="btn_submit"></div>').addClass('btn-commit').appendTo($commit);
	$('<input type="button" id="submit" value="Submit" />').appendTo($('#btn_submit'));
	$('<div id="btn_reset"></div>').addClass('btn-commit').appendTo($commit);
	$('<input type="button" id="reset" value="Reset" />').appendTo($('#btn_reset'));
	//init events of buttons.
	_initCommit();
}
function initComment(){
	var $comment = $('#comment');
	if($comment.length == 0){
		$('<div id="comment"></div>').insertBefore($('#show'));
	}
	$comment = $('#comment').empty();
	if(arguments.length == 1){
		$('<div id="rating" class="panel" style="width:332px;"><span class="rubric">Rating:</span><ul></ul></div>').appendTo($('#comment'));
		$('#rating ul').html(arguments[0]);					
	}else if(arguments.length == 2){
		if(arguments[0] == 'RFR'){
			$(arguments[1]).appendTo($('#comment'));
		}else{
			$('<div id="rating" class="panel" style="width:332px;"><span class="rubric">Rating:</span><ul></ul></div>').appendTo($('#comment'));
			$('#rating ul').html(arguments[1]);					
			$('<div id="criteria" class="panel" style="width:630px; margin-left:26px;"><span class="rubric">Criteron:</span><ul></ul></div>').appendTo($('#comment'));
			$('#criteria ul').html(arguments[0]);							
		}
	}else{
		$('<div id="rating" class="panel" style="' + arguments[1] + '"><span class="rubric">' + arguments[0] + ':</span><ul></ul></div>').appendTo($('#comment'));
		$('#rating ul').html(arguments[2]);						
	}
} 

//attach events of every select in #show panel.
function checkCriteron(){
	var complete = true;		
	$('#show ul').each(function(){
		if($(this).find('li .radio').length > 0){
			$(this).find('li .radio').each(function(){
				var flag = false;
				$(this).find(':radio').each(function(){
					($(this).prop('checked') == true) && (flag = true);	
				});
				(flag == false) && (complete = false);				
			});
			complete == true ? $('#draft, #submit').prop('disabled', false).removeClass('grey') : $('#draft, #submit').prop('disabled', true).addClass('grey');
		}else{
			complete = false;
			$(this).find('li select').each(function(){
				($(this).val() != 999) && (complete = true); 	
			});		
			complete == true ? $('#submit').prop('disabled', false).removeClass('grey') : $('#submit').prop('disabled', true).addClass('grey');
		}		
	});
			
}
function enableSubmit(){
	$('#draft, #submit, #reset').prop('disabled', false).removeClass('grey');
	if($('#show ul li .radio').length > 0){
		var draft = 0;
		$('#show ul').each(function(){
			var me = $(this);
			draft += parseInt(me.find('.isdraft').val(), 10);
			if(parseInt(me.find('.isdraft').val(), 10) == 3){
				me.find('li .radio :radio').each(function(){
					$(this).prop('disabled', true);
				});		
			}	
		});
		
		if(draft > 0 && draft % 3 == 0 && draft / 3 == $('#show ul').length){
			$('#draft, #submit, #reset').prop('disabled', true).addClass('grey');
		}else{
			$('#draft, #submit').prop('disabled', true).addClass('grey');	
			checkCriteron();	
		}
	}else{
		if($('#show ul li select').length == 0){
			$('#draft, #submit, #reset').prop('disabled', true);
		}else{
			var draft = 0;
			$('#show ul').each(function(){
				var me = $(this);
				draft += parseInt(me.find('.isdraft').val(), 10);
				if(parseInt(me.find('.isdraft').val(), 10) == 3){
					me.find('li select').each(function(){		
						if($('#useragent').val() != 'android-safari'){
							$(this).val() != 999 && $(this).prop('disabled', true);
						}else{
							var value = $(this).val();
							$(this).val() != 999 && $(this).find('option').each(function(){
								$(this).val() != value && $(this).prop('disabled', true);	
							});
						}
					});		
				}	
			});
			//checkCriteron();
			if(draft > 0 && draft % 3 == 0 && draft / 3 == $('#show ul').length){
				$('#draft, #submit, #reset').prop('disabled', true).addClass('grey');
			}else{
				$('#submit').prop('disabled', true).addClass('grey');	
				checkCriteron();	
			}
		}				
	}
}
function initCriteron(){
	$('#show ul').each(function(){
		if($(this).find('li .radio').length > 0){
			$(this).find(':radio').each(function(){
				$(this).unbind().click(checkCriteron);		
			});	
		}else{
			$(this).find('li select').each(function(){
				$(this).unbind().change(checkCriteron);	
			});			
		}		
	});	
}

//unify ajax method for all the pages.
function xhr(purl, pdata, pdatatype, pcallback){
	$.ajax({
		url:purl,
		type:'POST',	
		data:pdata,
		dataType:pdatatype,
		success:pcallback
	});	
}
	
//define unified fascad method calling different function according to result of ajax for response client actions.
function showResult(pdata, pxhr, pstatus){
	var revalue = pdata.split('|'), type = revalue[0], status = revalue[1], msg = '', cwd = '';
	if(type == 0){
		msg = status == 1 ? 'A draft of this assessment has been saved.' : 'The draft of this assessment cannot be saved.  Please try again.';
	}else{
		msg = status == 1 ? 'The assessment has been saved.' : 'The assessment cannot be saved.  Please try again.';
	}		
	alert(msg);
	cwd = $('#cwd').val().split('|');
	eval(cwd[type] + '()');
}
/*
//reserved function to make an unified alert dialog.
function showResult(pdata, pxhr, pstatus){
	var revalue = pdata.split('|'), type = revalue[0], status = revalue[1], msg = '', cwd = '';
	if(type == 0){
		msg = status == 1 ? 'A draft of this assessment has been saved.' : 'The draft of this assessment cannot be saved.  Please try again.';
	}else{
		msg = status == 1 ? 'The assessment has been saved.' : 'The assessment cannot be saved.  Please try again.';
	}		
	$('#alert_dialog').html(msg);
	$('#alert_dialog').dialog({
		buttons: {
			Ok: function() {
				$( this ).dialog( "close" );
			}
		}	
	});
	cwd = $('#cwd').val().split('|');
	eval(cwd[type] + '()');
}*/