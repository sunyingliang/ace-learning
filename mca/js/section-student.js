function freshSection(){
	var _url = './index.php';	
	var _data = {
		'c':'studentsection',	
		'a':'freshsection',
		'p':JSON.stringify({'roleid':$('#role_selector').val()})
	};
	var _datatype = 'html';
	var _callback = cb_initsection;
	xhr(_url, _data, _datatype, _callback);
}
function cb_initsection(data, xhr, status){
	var htmlel = data.split('|');
	$('#sec_selector').html(htmlel[0]).unbind().change(freshStudents);
	$("#sec_selector option:eq(0)").prop('selected', true);
	$('#show ul').html(htmlel[1]);
	initShow();	
	$('#commit') && $('#commit').remove();
	$('#comment') && $('#comment').remove();
}

function freshStudents(){
	var _url = './index.php';	
	var _data = {
		'c':'studentsection',	
		'a':'freshstudent',
		'p':JSON.stringify({'assessmentid':$("#asmtid").val()})
	};
	var _datatype = 'html';
	var _callback = cb_initstudents;
	xhr(_url, _data, _datatype, _callback);	
}
function cb_initstudents(data, xhr, status){
	$('#cwd').val('freshStudents|freshStudents');
	$('#nav').addClass('hidden');
	var htmlel = data.split('|');
	$('#stu_selector').html(htmlel[0]);
	$('#show').html(htmlel[1]);
	initShow();	
	initComment('Legend', 'width:332px;', htmlel[2]);
	$('#commit') && $('#commit').remove();
	$('#comment') && $('#comment').remove();		
}

function listSection1by1(userid){
	var _url = './index.php';
	var _data = {
		'c':'studentsection',
		'a':'listsection1by1',
		'p':JSON.stringify({'assessmentid':$("#asmtid").val(), 'userid':userid || $('#stu_selector').val()})	
	};	
	var _datatype = 'html';
	var _callback = cb_listsection1by1;
	xhr(_url, _data, _datatype, _callback);
}
function cb_listsection1by1(data, xhr, status){
	$('#cwd').val('listSection1by1|freshStudents');
	$('#nav').removeClass('hidden');
	var htmlel = data.split('|');
	$('#show').html(htmlel[0]);
	initCommit();
	initComment('Rating', 'width:332px;', htmlel[1]);
	initShow();	
	enableSubmit();
	initCriteron();	
}

function initNav(){
	$("#stu_selector option:eq(0)").prop('selected', true);
	$("#stu_selector").change(function(){
		if($(this).val() == 0){
			freshStudents();		
		}else{
			listSection1by1();	
		}
	});
}
function initShow(){
	$("#show ul li:gt(0)").each(function(){	
		$(this).mouseover(function(){
			$(this).css('background-color', '#cedfff');	
		}).mouseout(function(){
			$(this).hasClass('even') ? $(this).css('background-color', '#f1f1f1') : $(this).css('background-color', '#ffffff');		
		});
		if($(this).find('.radio').length > 0){
				$(this).find('.radio').each(function(){
					var $gradio = $(this), svalue = $(this).find(':hidden :first').val();
					$(this).find(':radio').each(function(){
						$(this).val() == svalue && $(this).prop('checked', true);		
						($gradio.find(':hidden').length == 2) && ($gradio.find(':hidden :last').val() == 1) && $(this).prop('disabled', true);		
					});	
				});	
			}else{
				$(this).find('select').each(function(){
					var me = $(this);	
					me.find('option').each(function(){
						$(this).val() == me.next().val() && $(this).prop('selected', true);	
					});
					me.next().next().val() == 1 && me.prop('disabled', true);
				});	
			}
	});
	$('#show ul li span a').click(function(){
		var me = $(this);
		$('#stu_selector option').each(function(){
			me.attr('userid') == $(this).val() && $(this).prop('selected', true);				
		});
		listSection1by1($(this).attr('userid'));	
	});
}

$(document).ready(function(){
	initNav();	
	initShow();
});